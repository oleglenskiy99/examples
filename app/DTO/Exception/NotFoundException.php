<?php

declare(strict_types=1);

namespace App\DTO\Exception;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="NotFoundException"),
 * )
 */
final class NotFoundException
{

    /**
     * @OA\Property(
     *     property="code",
     *     type="int",
     *     readOnly="true",
     *     example="404"
     * ),
     */
    private int $code;

    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     readOnly="true",
     *     example="Not found"
     * ),
     */
    private string $message;

    public function getCode(): int
    {
        return $this->code;
    }

    public function setCode(int $code): NotFoundException
    {
        $this->code = $code;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): NotFoundException
    {
        $this->message = $message;
        return $this;
    }

}
