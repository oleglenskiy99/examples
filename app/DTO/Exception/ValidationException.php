<?php

declare(strict_types=1);

namespace App\DTO\Exception;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="ValidationException"),
 * )
 */
final class ValidationException
{

    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     readOnly="true",
     *     example="Validation exception"
     * ),
     */
    public string $message;

    /**
     * @OA\Property(
     *     property="data",
     *     type="object",
     *     readOnly="true"
     * ),
     */
    public object $data;

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): ValidationException
    {
        $this->message = $message;
        return $this;
    }

    public function getData(): object
    {
        return $this->data;
    }

    public function setData(object $data): ValidationException
    {
        $this->data = $data;
        return $this;
    }

}
