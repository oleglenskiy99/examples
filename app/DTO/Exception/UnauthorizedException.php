<?php

declare(strict_types=1);

namespace App\DTO\Exception;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="UnauthorizedException"),
 * )
 */
final class UnauthorizedException
{

    /**
     * @OA\Property(
     *     property="code",
     *     type="int",
     *     readOnly="true",
     *     example="401"
     * ),
     */
    private int $code;

    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     readOnly="true",
     *     example="Unauthorized"
     * ),
     */
    private string $message;

    public function getCode(): int
    {
        return $this->code;
    }

    public function setCode(int $code): UnauthorizedException
    {
        $this->code = $code;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): UnauthorizedException
    {
        $this->message = $message;
        return $this;
    }

}
