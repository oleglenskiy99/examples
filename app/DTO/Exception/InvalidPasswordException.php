<?php

declare(strict_types=1);

namespace App\DTO\Exception;

/**
 * @OA\Schema(
 * @OA\Xml(name="InvalidPasswordException"),
 * )
 */
final class InvalidPasswordException
{

    /**
     * @OA\Property(
     *     property="code",
     *     type="int",
     *     readOnly="true",
     *     example="400"
     * ),
     */
    private int $code;

    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     readOnly="true",
     *     example="Invalid password"
     * ),
     */
    private string $message;

    public function getCode(): int
    {
        return $this->code;
    }

    public function setCode(int $code): InvalidPasswordException
    {
        $this->code = $code;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): InvalidPasswordException
    {
        $this->message = $message;
        return $this;
    }

}
