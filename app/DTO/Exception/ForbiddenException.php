<?php

declare(strict_types=1);

namespace App\DTO\Exception;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="ForbiddenException"),
 * )
 */
final class ForbiddenException
{

    /**
     * @OA\Property(
     *     property="code",
     *     type="int",
     *     readOnly="true",
     *     example="403"
     * ),
     */
    private int $code;

    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     readOnly="true",
     *     example="Operation is forbidden for you or your token is invalid"
     * ),
     */
    private string $message;

    public function getCode(): int
    {
        return $this->code;
    }

    public function setCode(int $code): ForbiddenException
    {
        $this->code = $code;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): ForbiddenException
    {
        $this->message = $message;
        return $this;
    }

}
