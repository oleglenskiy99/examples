<?php

declare(strict_types=1);

namespace App\DTO\Response\Chats;

/**
 * @OA\Schema(
 * @OA\Xml(name="ChatHistoryResponse"),
 * )
 */
class ChatHistory
{

    /**
     * @OA\Property(
     *     property="items",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/ChatMessageItem"),
     *     readOnly="true"
     * ),
     */
    private array $items;

    private int $countOfItems;

    /**
     * @OA\Property(
     *     property="last_page",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private int $lastPage;
    private int $nextPage;
    private int $prevPage;

    /**
     * @OA\Property(
     *     property="current_page",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private int $currentPage;

    public function getItems(): array
    {
        return $this->items;
    }

    public function setItems(array $items): ChatHistory
    {
        $this->items = $items;
        return $this;
    }

    public function getCountOfItems(): int
    {
        return $this->countOfItems;
    }

    public function setCountOfItems(int $countOfItems): ChatHistory
    {
        $this->countOfItems = $countOfItems;
        return $this;
    }

    public function getLastPage(): int
    {
        return $this->lastPage;
    }

    public function setLastPage(int $lastPage): ChatHistory
    {
        $this->lastPage = $lastPage;
        return $this;
    }

    public function getNextPage(): int
    {
        return $this->nextPage;
    }

    public function setNextPage(int $nextPage): ChatHistory
    {
        $this->nextPage = $nextPage;
        return $this;
    }

    public function getPrevPage(): int
    {
        return $this->prevPage;
    }

    public function setPrevPage(int $prevPage): ChatHistory
    {
        $this->prevPage = $prevPage;
        return $this;
    }

    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    public function setCurrentPage(int $currentPage): ChatHistory
    {
        $this->currentPage = $currentPage;
        return $this;
    }

}
