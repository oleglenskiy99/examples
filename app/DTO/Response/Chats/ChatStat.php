<?php

declare(strict_types=1);

namespace App\DTO\Response\Chats;

/**
 * @OA\Schema(
 * @OA\Xml(name="ChatStat"),
 * )
 */
class ChatStat
{

    /**
     * @OA\Property(
     *     property="unreadCount",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private ?int $unreadCount;

    /**
     * @OA\Property(
     *     property="balance",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private ?int $balance;

    /**
     * @OA\Property(
     *     property="totalReceived",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private ?int $totalReceived;

    /**
     * @OA\Property(
     *     property="totalSpent",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private ?int $totalSpent;

    public function getBalance(): ?int
    {
        return $this->balance;
    }

    public function setBalance(?int $balance): ChatStat
    {
        $this->balance = $balance;
        return $this;
    }

    public function getUnreadCount(): ?int
    {
        return $this->unreadCount;
    }

    public function setUnreadCount(?int $unreadCount): ChatStat
    {
        $this->unreadCount = $unreadCount;
        return $this;
    }

    public function getTotalReceived(): ?int
    {
        return $this->totalReceived;
    }

    public function setTotalReceived(?int $totalReceived): ChatStat
    {
        $this->totalReceived = $totalReceived;
        return $this;
    }

    public function getTotalSpent(): ?int
    {
        return $this->totalSpent;
    }

    public function setTotalSpent(?int $totalSpent): ChatStat
    {
        $this->totalSpent = $totalSpent;
        return $this;
    }

}
