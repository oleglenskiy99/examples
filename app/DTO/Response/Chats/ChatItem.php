<?php

declare(strict_types=1);

namespace App\DTO\Response\Chats;

use App\DTO\Response\User\UserItem;

/**
 * @OA\Schema(
 * @OA\Xml(name="ChatItem"),
 * )
 */
class ChatItem
{

    /**
     * @OA\Property(
     *     property="id",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private int $id;

    /**
     * @OA\Property(
     *     property="title",
     *     type="string",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private ?string $title;

    /**
     * @OA\Property(
     *     property="expert_id",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private int $expert_id;

    /**
     * @OA\Property(
     *     property="user",
     *     type="object",
     *     @OA\Schema(ref="#/components/schemas/UserItem"),
     *     readOnly="true"
     * ),
     */
    private UserItem $user;

    /**
     * @OA\Property(
     *     property="stats",
     *     type="object",
     *     @OA\Schema(ref="#/components/schemas/ChatStat"),
     *     readOnly="true"
     * ),
     */
    private ChatStat $stats;

    /**
     * @OA\Property(
     *     property="message",
     *     type="object",
     *     @OA\Schema(ref="#/components/schemas/ChatMessageItem"),
     *     readOnly="true"
     * ),
     */
    private ChatMessageItem $message;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): ChatItem
    {
        $this->id = $id;
        return $this;
    }
    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): ChatItem
    {
        $this->title = $title;
        return $this;
    }

    public function getExpertId(): int
    {
        return $this->expert_id;
    }

    public function setExpertId(int $expert_id): ChatItem
    {
        $this->expert_id = $expert_id;
        return $this;
    }

    public function getUser(): UserItem
    {
        return $this->user;
    }

    public function setUser(UserItem $user): ChatItem
    {
        $this->user = $user;
        return $this;
    }

    public function getStats(): ChatStat
    {
        return $this->stats;
    }

    public function setStats(ChatStat $stat): ChatItem
    {
        $this->stats = $stat;
        return $this;
    }

    public function getMessage(): ChatMessageItem
    {
        return $this->message;
    }

    public function setMessage(ChatMessageItem $message): ChatItem
    {
        $this->message = $message;
        return $this;
    }

}
