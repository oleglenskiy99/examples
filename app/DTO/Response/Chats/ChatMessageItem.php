<?php

declare(strict_types=1);

namespace App\DTO\Response\Chats;

use App\DTO\Response\Objects\Media;

/**
 * @OA\Schema(
 * @OA\Xml(name="ChatMessageItem"),
 * )
 */
class ChatMessageItem
{

    /**
     * @OA\Property(
     *     property="id",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private int $id;

    /**
     * @OA\Property(
     *     property="userId",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private int $userId;

    /**
     * @OA\Property(
     *     property="cost",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private ?int $cost;

    /**
     * @OA\Property(
     *     property="text",
     *     type="string",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private ?string $text;

    /**
     * @var Media[]|null
     */

    private ?array $attachments;

    private ?ChatMessageItem $reply;

    /**
     * @OA\Property(
     *     property="timestamp",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private int $timestamp;

    /**
     * @OA\Property(
     *     property="out",
     *     type="bool",
     *     readOnly="true",
     *     example="true"
     * ),
     */
    private ?bool $out;

    /**
     * @OA\Property(
     *     property="paid",
     *     type="bool",
     *     readOnly="true",
     *     example="true"
     * ),
     */
    private ?bool $paid;

    /**
     * @OA\Property(
     *     property="read",
     *     type="bool",
     *     readOnly="true",
     *     example="true"
     * ),
     */
    private ?bool $read;

    /**
     * @OA\Property(
     *     property="edit",
     *     type="bool",
     *     readOnly="true",
     *     example="true"
     * ),
     */
    private ?bool $edit;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): ChatMessageItem
    {
        $this->id = $id;
        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): ChatMessageItem
    {
        $this->userId = $userId;
        return $this;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function setCost(?int $cost): ChatMessageItem
    {
        $this->cost = $cost;
        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): ChatMessageItem
    {
        $this->text = $text;
        return $this;
    }

    public function getAttachments(): ?array
    {
        return $this->attachments;
    }

    public function setAttachments(?array $attachments): ChatMessageItem
    {
        $this->attachments = $attachments;
        return $this;
    }


    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    public function setTimestamp(int $timestamp): ChatMessageItem
    {
        $this->timestamp = $timestamp;
        return $this;
    }

    public function isOut(): ?bool
    {
        return $this->out;
    }

    public function setOut(bool $out): ChatMessageItem
    {
        $this->out = $out;
        return $this;
    }

    public function isPaid(): ?bool
    {
        return $this->paid;
    }

    public function setPaid(?bool $paid): ChatMessageItem
    {
        $this->paid = $paid;
        return $this;
    }

    public function isRead(): ?bool
    {
        return $this->read;
    }

    public function setRead(?bool $read): ChatMessageItem
    {
        $this->read = $read;
        return $this;
    }

    public function isEdit(): ?bool
    {
        return $this->edit;
    }

    public function setEdit(?bool $edit): ChatMessageItem
    {
        $this->edit = $edit;
        return $this;
    }

    public function getReply(): ?ChatMessageItem
    {
        return $this->reply;
    }

    public function setReply(?ChatMessageItem $reply): ChatMessageItem
    {
        $this->reply = $reply;
        return $this;
    }

}
