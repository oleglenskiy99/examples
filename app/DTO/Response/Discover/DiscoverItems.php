<?php

declare(strict_types=1);

namespace App\DTO\Response\Discover;

use App\DTO\Response\User\Category;
use App\DTO\Response\User\UserItem;

/**
 * @OA\Schema(
 * @OA\Xml(name="EventItem"),
 * )
 */
final class DiscoverItems
{

    /**
     * @OA\Property(
     *     property="country",
     *     type="string",
     *     readOnly="true",
     *     example="Russian"
     * ),
     */
    private string $country;

    /**
     * @var Category[]
     * @OA\Property(
     *     property="categories",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/Category"),
     *     readOnly="true"
     * ),
     */
    private array $categories;

    /**
     * @var UserItem[]
     * @OA\Property(
     *     property="recentlyViewed",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/UserItem"),
     *     readOnly="true"
     * ),
     */
    private array $recentlyViewed;

    /**
     * @var UserItem[]
     * @OA\Property(
     *     property="popularInWorld",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/UserItem"),
     *     readOnly="true"
     * ),
     */
    private array $popularInWorld;

    /**
     * @var UserItem[]
     * @OA\Property(
     *     property="popularInYourRegion",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/UserItem"),
     *     readOnly="true"
     * ),
     */
    private array $popularInYourRegion;

    /**
     * @var UserItem[]
     * @OA\Property(
     *     property="speciallyForYou",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/UserItem"),
     *     readOnly="true"
     * ),
     */
    private array $speciallyForYou;

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): DiscoverItems
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return Category[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param Category[] $categories
     */
    public function setCategories(array $categories): DiscoverItems
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return UserItem[]
     */
    public function getRecentlyViewed(): array
    {
        return $this->recentlyViewed;
    }

    /**
     * @param UserItem[] $recentlyViewed
     */
    public function setRecentlyViewed(array $recentlyViewed): DiscoverItems
    {
        $this->recentlyViewed = $recentlyViewed;
        return $this;
    }

    /**
     * @return UserItem[]
     */
    public function getPopularInWorld(): array
    {
        return $this->popularInWorld;
    }

    /**
     * @param UserItem[] $popularInWorld
     */
    public function setPopularInWorld(array $popularInWorld): DiscoverItems
    {
        $this->popularInWorld = $popularInWorld;
        return $this;
    }

    /**
     * @return UserItem[]
     */
    public function getPopularInYourRegion(): array
    {
        return $this->popularInYourRegion;
    }

    /**
     * @param UserItem[] $popularInYourRegion
     */
    public function setPopularInYourRegion(array $popularInYourRegion): DiscoverItems
    {
        $this->popularInYourRegion = $popularInYourRegion;
        return $this;
    }

    /**
     * @return UserItem[]
     */
    public function getSpeciallyForYou(): array
    {
        return $this->speciallyForYou;
    }

    /**
     * @param UserItem[] $speciallyForYou
     */
    public function setSpeciallyForYou(array $speciallyForYou): DiscoverItems
    {
        $this->speciallyForYou = $speciallyForYou;
        return $this;
    }

}
