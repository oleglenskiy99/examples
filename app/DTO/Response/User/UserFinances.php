<?php

declare(strict_types=1);

namespace App\DTO\Response\User;

use App\DTO\Common\Price;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="UserFinances"),
 * )
 */
class UserFinances
{

    /**
     * @OA\Property(
     *     property="forSpending",
     *     type="object",
     *     ref="#/components/schemas/Price",
     *     readOnly="true"
     * ),
     */
    private Price $forSpending;

    /**
     * @OA\Property(
     *     property="forWithdrawal",
     *     type="object",
     *     ref="#/components/schemas/Price",
     *     readOnly="true"
     * ),
     */
    private Price $forWithdrawal;

    public function getForWithdrawal(): Price
    {
        return $this->forWithdrawal;
    }

    public function setForWithdrawal(Price $forWithdrawal): UserFinances
    {
        $this->forWithdrawal = $forWithdrawal;
        return $this;
    }

    public function getForSpending(): Price
    {
        return $this->forSpending;
    }

    public function setForSpending(Price $forSpending): UserFinances
    {
        $this->forSpending = $forSpending;
        return $this;
    }

}
