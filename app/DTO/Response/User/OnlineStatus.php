<?php

declare(strict_types=1);

namespace App\DTO\Response\User;

use DateTimeImmutable;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     @OA\Xml(name="OnlineStatus"),
 * )
 */
final class OnlineStatus
{

    /**
     * @OA\Property(
     *     property="isOnline",
     *     type="bool",
     *     readOnly="true",
     *     example="false"
     * ),
     */
    public bool $isOnline;

    /**
     * @OA\Property(
     *     property="lastOnline",
     *     type="date",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    public ?DateTimeImmutable $lastOnline;

    public function isOnline(): bool
    {
        return $this->isOnline;
    }

    public function setIsOnline(bool $isOnline): OnlineStatus
    {
        $this->isOnline = $isOnline;
        return $this;
    }

    public function getLastOnline(): ?DateTimeImmutable
    {
        return $this->lastOnline;
    }

    public function setLastOnline(?DateTimeImmutable $lastOnline): OnlineStatus
    {
        $this->lastOnline = $lastOnline;
        return $this;
    }

}
