<?php

declare(strict_types=1);

namespace App\DTO\Response\User;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     @OA\Xml(name="Category"),
 * )
 */
final class Category
{

    /**
     * @OA\Property(
     *     property="id",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $id;

    /**
     * @OA\Property(
     *     property="name",
     *     type="string",
     *     readOnly="true",
     *     example="Travel"
     * ),
     */
    private string $name;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Category
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Category
    {
        $this->name = $name;
        return $this;
    }

}
