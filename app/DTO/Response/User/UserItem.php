<?php

declare(strict_types=1);

namespace App\DTO\Response\User;

use App\DTO\Common\Communication;
use App\DTO\Response\Event\EventItem;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="UserItem"),
 * )
 */
final class UserItem
{

    /**
     * @OA\Property(
     *     property="userInfo",
     *     type="object",
     *     ref="#/components/schemas/UserInfo",
     *     readOnly="true"
     * ),
     */
    private UserInfo $userInfo;

    /**
     * @OA\Property(
     *     property="categories",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/Category"),
     *     readOnly="true"
     * ),
     */
    private array $categories;

    /**
     * @var array<SocialNetwork>
     * @OA\Property(
     *     property="socialNetworks",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/SocialNetwork"),
     *     readOnly="true"
     * ),
     */
    private array $socialNetworks;

    /**
     * @OA\Property(
     *     property="stats",
     *     type="object",
     *     ref="#/components/schemas/Stat",
     *     readOnly="true"
     * ),
     */
    private Stat $stats;

    /**
     * @var array<Communication>
     * @OA\Property(
     *     property="communications",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/Communication"),
     *     readOnly="true"
     * ),
     */
    private array $communications;

    /**
     * @var array<string>
     * @OA\Property(
     *     property="charity",
     *     type="array",
     *     @OA\Items(
     *         type="string",
     *     ),
     *     readOnly="true"
     * ),
     */
    private array $charity = [];

    /**
     * @OA\Property(
     *     property="isFollow",
     *     type="boolean",
     *     readOnly="true",
     *     example="false"
     * ),
     */
    private bool $isFollow;

    /**
     * @OA\Property(
     *     property="isFollowing",
     *     type="boolean",
     *     readOnly="true",
     *     example="true"
     * ),
     */
    private bool $isFollowing;

    /**
     * @var array<EventItem>
     * @OA\Property(
     *     property="events",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/EventItem"),
     *     readOnly="true"
     * ),
     */
    private array $events;

    /**
     * @OA\Property(
     *     property="balance",
     *     type="float",
     *     readOnly="true",
     *     example="10"
     * ),
     */
    private ?float $balance;

    /**
     * @OA\Property(
     *     property="withdraw",
     *     type="float",
     *     readOnly="true",
     *     example="10"
     * ),
     */
    private ?float $withdraw;

    /**
     * @OA\Property(
     *     property="email",
     *     type="string",
     *     readOnly="true",
     *     example="example@mail.com"
     * ),
     */
    private ?string $email;

    /**
     * @OA\Property(
     *     property="phone",
     *     type="string",
     *     readOnly="true",
     *     example="77070770700"
     * ),
     */
    private ?string $phone;

    public function getUserInfo(): UserInfo
    {
        return $this->userInfo;
    }

    public function setUserInfo(UserInfo $userInfo): UserItem
    {
        $this->userInfo = $userInfo;
        return $this;
    }

    /**
     * @return array<Category>
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param array<Category> $categories
     */
    public function setCategories(array $categories): UserItem
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return array<SocialNetwork>
     */
    public function getSocialNetworks(): array
    {
        return $this->socialNetworks;
    }

    /**
     * @param array<SocialNetwork> $socialNetworks
     */
    public function setSocialNetworks(array $socialNetworks): UserItem
    {
        $this->socialNetworks = $socialNetworks;
        return $this;
    }

    public function getStats(): Stat
    {
        return $this->stats;
    }

    public function setStats(Stat $stats): UserItem
    {
        $this->stats = $stats;
        return $this;
    }

    /**
     * @return array<Communication>
     */
    public function getCommunications(): array
    {
        return $this->communications;
    }

    /**
     * @param array<Communication> $communications
     */
    public function setCommunications(array $communications): UserItem
    {
        $this->communications = $communications;
        return $this;
    }

    /**
     * @return array<string>
     */
    public function getCharity(): array
    {
        return $this->charity;
    }

    /**
     * @param array<string> $charity
     */
    public function setCharity(array $charity): UserItem
    {
        $this->charity = $charity;
        return $this;
    }

    public function getIsFollow(): bool
    {
        return $this->isFollow;
    }

    public function setIsFollow(bool $isFollow): UserItem
    {
        $this->isFollow = $isFollow;
        return $this;
    }

    public function getIsFollowing(): bool
    {
        return $this->isFollowing;
    }

    public function setIsFollowing(bool $isFollowing): UserItem
    {
        $this->isFollowing = $isFollowing;
        return $this;
    }

    /**
     * @return array<EventItem>
     */
    public function getEvents(): array
    {
        return $this->events;
    }

    /**
     * @param array<EventItem> $events
     */
    public function setEvents(array $events): UserItem
    {
        $this->events = $events;
        return $this;
    }

    public function getBalance(): ?float
    {
        return $this->balance;
    }

    public function setBalance(?float $balance): UserItem
    {
        $this->balance = $balance;
        return $this;
    }

    public function getWithdraw(): ?float
    {
        return $this->withdraw;
    }

    public function setWithdraw(?float $withdraw): UserItem
    {
        $this->withdraw = $withdraw;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): UserItem
    {
        $this->email = $email;
        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): UserItem
    {
        $this->phone = $phone;
        return $this;
    }

}
