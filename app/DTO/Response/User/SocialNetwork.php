<?php

declare(strict_types=1);

namespace App\DTO\Response\User;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="SocialNetwork"),
 * )
 */
final class SocialNetwork
{

    /**
     * @OA\Property(
     *     property="type",
     *     type="string",
     *     readOnly="true",
     *     example="FB"
     * ),
     */
    private string $type;

    /**
     * @OA\Property(
     *     property="nickname",
     *     type="string",
     *     readOnly="true",
     *     example="brookecagle"
     * ),
     */
    private string $nickname;

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): SocialNetwork
    {
        $this->type = $type;
        return $this;
    }

    public function getNickname(): string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): SocialNetwork
    {
        $this->nickname = $nickname;
        return $this;
    }

}
