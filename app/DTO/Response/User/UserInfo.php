<?php

declare(strict_types=1);

namespace App\DTO\Response\User;

use App\DTO\Response\Country\CountryItem;

/**
 * @OA\Schema(
 * @OA\Xml(name="UserInfo"),
 * )
 */
final class UserInfo
{

    /**
     * @OA\Property(
     *     property="userId",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $userId;

    /**
     * @OA\Property(
     *     property="firstName",
     *     type="string",
     *     readOnly="true",
     *     example="Florian"
     * ),
     */
    private ?string $firstName;

    /**
     * @OA\Property(
     *     property="lastName",
     *     type="string",
     *     readOnly="true",
     *     example="Weither"
     * ),
     */
    private ?string $lastName;

    /**
     * @OA\Property(
     *     property="nickname",
     *     type="string",
     *     readOnly="true",
     *     example="Dovahkiin"
     * ),
     */
    private ?string $nickname;

    /**
     * @OA\Property(
     *     property="isVerified",
     *     type="bool",
     *     readOnly="true",
     *     example="true"
     * ),
     */
    private bool $isVerified;

    /**
     * @OA\Property(
     *     property="avatar",
     *     type="string",
     *     readOnly="true",
     *     example="{path_to_photo}"
     * ),
     */
    private string $avatar;

    /**
     * @OA\Property(
     *     property="description",
     *     type="string",
     *     readOnly="true",
     *     example="User description"
     * ),
     */
    private string $description;

    /**
     * @OA\Property(
     *     property="status",
     *     type="object",
     *     ref="#/components/schemas/OnlineStatus",
     *     readOnly="true"
     * ),
     */
    private OnlineStatus $status;

    /**
     * @OA\Property(
     *     property="country",
     *     type="CountryItem",
     *     ref="#/components/schemas/CountryItem",
     *     readOnly="true"
     * ),
     */
    private ?CountryItem $country;

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): UserInfo
    {
        $this->userId = $userId;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): UserInfo
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): UserInfo
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(?string $nickname): UserInfo
    {
        $this->nickname = $nickname;
        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setVerified(bool $verified): UserInfo
    {
        $this->isVerified = $verified;
        return $this;
    }

    public function getAvatar(): string
    {
        return $this->avatar;
    }

    public function setAvatar(string $avatar): UserInfo
    {
        $this->avatar = $avatar;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): UserInfo
    {
        $this->description = $description;
        return $this;
    }

    public function getStatus(): OnlineStatus
    {
        return $this->status;
    }

    public function setStatus(OnlineStatus $status): UserInfo
    {
        $this->status = $status;
        return $this;
    }

    public function getCountry(): ?CountryItem
    {
        return $this->country;
    }

    public function setCountry(?CountryItem $country): UserInfo
    {
        $this->country = $country;
        return $this;
    }

}
