<?php

declare(strict_types=1);

namespace App\DTO\Response\User;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="Stat"),
 * )
 */
final class Stat
{

    /**
     * @OA\Property(
     *     property="rating",
     *     type="number",
     *     readOnly="true",
     *     example="4.8"
     * ),
     */
    private float $rating;

    /**
     * @OA\Property(
     *     property="responses",
     *     type="int",
     *     readOnly="true",
     *     example="2315"
     * ),
     */
    private int $responses;

    /**
     * @OA\Property(
     *     property="followers",
     *     type="int",
     *     readOnly="true",
     *     example="9256"
     * ),
     */
    private int $followers;

    /**
     * @OA\Property(
     *     property="following",
     *     type="int",
     *     readOnly="true",
     *     example="1811"
     * ),
     */
    private int $following;

    public function getRating(): float
    {
        return $this->rating;
    }

    public function setRating(float $rating): Stat
    {
        $this->rating = $rating;
        return $this;
    }

    public function getResponses(): int
    {
        return $this->responses;
    }

    public function setResponses(int $responses): Stat
    {
        $this->responses = $responses;
        return $this;
    }

    public function getFollowers(): int
    {
        return $this->followers;
    }

    public function setFollowers(int $followers): Stat
    {
        $this->followers = $followers;
        return $this;
    }

    public function getFollowing(): int
    {
        return $this->following;
    }

    public function setFollowing(int $following): Stat
    {
        $this->following = $following;
        return $this;
    }

}
