<?php

declare(strict_types=1);

namespace App\DTO\Response\Connections;

use App\DTO\Response\Chats\ChatItem;
use App\DTO\Response\User\UserItem;

/**
 * @OA\Schema(
 * @OA\Xml(name="ConnectionItem"),
 * )
 */
class ConnectionItem
{

    /**
     * @OA\Property(
     *     property="type",
     *     type="string",
     *     readOnly="true",
     *     example="chat"
     * ),
     */
    private string $type;

    /**
     * @OA\Property(
     *     property="user",
     *     type="object",
     *     @OA\Schema(ref="#/components/schemas/UserItem"),
     *     readOnly="true"
     * ),
     */
    private UserItem $user;

    /**
     * @OA\Property(
     *     property="chat",
     *     type="object",
     *     @OA\Schema(ref="#/components/schemas/ChatItem"),
     *     readOnly="true"
     * ),
     */
    private ChatItem $chat;

    /**
     * @OA\Property(
     *     property="stats",
     *     type="object",
     *     @OA\Schema(ref="#/components/schemas/ConnectionStat"),
     *     readOnly="true"
     * ),
     */
    private ConnectionStat $stats;

    /**
     * @OA\Property(
     *     property="lastActivity",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private int $lastActivity;

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): ConnectionItem
    {
        $this->type = $type;
        return $this;
    }

    public function getChat(): ChatItem
    {
        return $this->chat;
    }

    public function setChat(ChatItem $chat): ConnectionItem
    {
        $this->chat = $chat;
        return $this;
    }

    public function getStats(): ConnectionStat
    {
        return $this->stats;
    }

    public function setStats(ConnectionStat $stats): ConnectionItem
    {
        $this->stats = $stats;
        return $this;
    }

    public function getUser(): UserItem
    {
        return $this->user;
    }

    public function setUser(UserItem $userItem): ConnectionItem
    {
        $this->user = $userItem;
        return $this;
    }

    public function getLastActivity(): int
    {
        return $this->lastActivity;
    }

    public function setLastActivity(int $lastActivity): ConnectionItem
    {
        $this->lastActivity = $lastActivity;
        return $this;
    }

}
