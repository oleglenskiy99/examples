<?php

declare(strict_types=1);

namespace App\DTO\Response\Connections;

/**
 * @OA\Schema(
 * @OA\Xml(name="ConnectionStat"),
 * )
 */
class ConnectionStat
{

    /**
     * @OA\Property(
     *     property="responseCount",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private int $response_count;

    /**
     * @OA\Property(
     *     property="balance",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private int $balance;

    public function getResponseCount(): int
    {
        return $this->response_count;
    }

    public function setResponseCount(int $response_count): ConnectionStat
    {
        $this->response_count = $response_count;
        return $this;
    }

    public function getBalance(): int
    {
        return $this->balance;
    }

    public function setBalance(int $balance): ConnectionStat
    {
        $this->balance = $balance;
        return $this;
    }

}
