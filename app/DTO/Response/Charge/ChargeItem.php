<?php

declare(strict_types=1);

namespace App\DTO\Response\Charge;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="ChargeItem"),
 * )
 */
final class ChargeItem
{

    private string $id;
    private ?string $object;
    private ?float $amount;
    private ?float $amount_captured;
    private ?float $amount_refunded;
    private ?int $created;
    private ?string $currency;
    private ?string $customer;
    private ?bool $paid;
    private ?string $payment_method;
    private ?string $receipt_url;
    private ?bool $refunded;
    private ?string $status;
    private ?bool $captured;
    private ?string $payment_intent;
    private ?float $amount_receivable;
    private ?string $category;
    private ?bool $out;
    private ?string $parent;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): ChargeItem
    {
        $this->id = $id;
        return $this;
    }

    public function getObject(): ?string
    {
        return $this->object;
    }

    public function setObject(?string $object): ChargeItem
    {
        $this->object = $object;
        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): ChargeItem
    {
        $this->amount = $amount;
        return $this;
    }

    public function getAmountCaptured(): ?float
    {
        return $this->amount_captured;
    }

    public function setAmountCaptured(?float $amount_captured): ChargeItem
    {
        $this->amount_captured = $amount_captured;
        return $this;
    }

    public function getAmountRefunded(): ?float
    {
        return $this->amount_refunded;
    }

    public function setAmountRefunded(?float $amount_refunded): ChargeItem
    {
        $this->amount_refunded = $amount_refunded;
        return $this;
    }

    public function getCreated(): ?int
    {
        return $this->created;
    }

    public function setCreated(?int $created): ChargeItem
    {
        $this->created = $created;
        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): ChargeItem
    {
        $this->currency = $currency;
        return $this;
    }

    public function getCustomer(): ?string
    {
        return $this->customer;
    }

    public function setCustomer(?string $customer): ChargeItem
    {
        $this->customer = $customer;
        return $this;
    }

    public function getPaid(): ?bool
    {
        return $this->paid;
    }

    public function setPaid(?bool $paid): ChargeItem
    {
        $this->paid = $paid;
        return $this;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->payment_method;
    }

    public function setPaymentMethod(?string $payment_method): ChargeItem
    {
        $this->payment_method = $payment_method;
        return $this;
    }

    public function getReceiptUrl(): ?string
    {
        return $this->receipt_url;
    }

    public function setReceiptUrl(?string $receipt_url): ChargeItem
    {
        $this->receipt_url = $receipt_url;
        return $this;
    }

    public function getRefunded(): ?bool
    {
        return $this->refunded;
    }

    public function setRefunded(?bool $refunded): ChargeItem
    {
        $this->refunded = $refunded;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): ChargeItem
    {
        $this->status = $status;
        return $this;
    }

    public function getCaptured(): ?bool
    {
        return $this->captured;
    }

    public function setCaptured(?bool $captured): ChargeItem
    {
        $this->captured = $captured;
        return $this;
    }

    public function getPaymentIntent(): ?string
    {
        return $this->payment_intent;
    }

    public function setPaymentIntent(?string $payment_intent): ChargeItem
    {
        $this->payment_intent = $payment_intent;
        return $this;
    }

    public function getAmountReceivable(): ?float
    {
        return $this->amount_receivable;
    }

    public function setAmountReceivable(?float $amount_receivable): ChargeItem
    {
        $this->amount_receivable = $amount_receivable;
        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): ChargeItem
    {
        $this->category = $category;
        return $this;
    }

    public function getOut(): ?bool
    {
        return $this->out;
    }

    public function setOut(?bool $out): ChargeItem
    {
        $this->out = $out;
        return $this;
    }

    public function getParent(): ?string
    {
        return $this->parent;
    }

    public function setParent(?string $parent): ChargeItem
    {
        $this->parent = $parent;
        return $this;
    }

}
