<?php

declare(strict_types=1);

namespace App\DTO\Response;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="Logout"),
 * )
 */
final class Logout
{

    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     readOnly="true",
     *     example=""
     * ),
     */
    public string $message = 'Successfully logged out';

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): Logout
    {
        $this->message = $message;
        return $this;
    }

}
