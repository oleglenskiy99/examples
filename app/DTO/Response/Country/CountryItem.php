<?php

declare(strict_types=1);

namespace App\DTO\Response\Country;

/**
 * @OA\Schema(
 * @OA\Xml(name="CountryItem"),
 * )
 */
final class CountryItem
{

    /**
     * @OA\Property(
     *     property="id",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private int $id;

    /**
     * @OA\Property(
     *     property="sortname",
     *     type="string",
     *     readOnly="true",
     *     example="RU"
     * ),
     */
    private ?string $sortname;

    /**
     * @OA\Property(
     *     property="name",
     *     type="string",
     *     readOnly="true",
     *     example="Россия"
     * ),
     */
    private ?string $name;

    /**
     * @OA\Property(
     *     property="phonecode",
     *     type="int",
     *     readOnly="true",
     *     example="7"
     * ),
     */
    private ?int $phonecode;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): CountryItem
    {
        $this->id = $id;
        return $this;
    }

    public function getSortname(): ?string
    {
        return $this->sortname;
    }

    public function setSortname(string $sortname): CountryItem
    {
        $this->sortname = $sortname;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): CountryItem
    {
        $this->name = $name;
        return $this;
    }

    public function getPhonecode(): ?int
    {
        return $this->phonecode;
    }

    public function setPhonecode(int $phonecode): CountryItem
    {
        $this->phonecode = $phonecode;
        return $this;
    }

}
