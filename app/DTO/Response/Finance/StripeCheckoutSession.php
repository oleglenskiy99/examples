<?php

declare(strict_types=1);

namespace App\DTO\Response\Finance;

/**
 * @OA\Schema(
 * @OA\Xml(name="StripeCheckoutSession"),
 * )
 */
class StripeCheckoutSession
{

    /**
     * @OA\Property(
     *     property="id",
     *     type="string",
     *     readOnly="true",
     *     example="cs_test_c1thIMvxAGlBTx94Pk3ROOjCHsdqURisGlgU83U3nWHnkVe4DNAACCDIE5"
     * ),
     */
    private string $id;

    /**
     * @OA\Property(
     *     property="object",
     *     type="string",
     *     readOnly="true",
     *     example="checkout.session"
     * ),
     */
    private string $object;

    /**
     * @OA\Property(
     *     property="currency",
     *     type="string",
     *     readOnly="true",
     *     example="usd"
     * ),
     */
    private ?string $currency;

    /**
     * @OA\Property(
     *     property="customer",
     *     type="string",
     *     readOnly="true",
     *     example="cus_KmXTDoxHd8Uz37"
     * ),
     */
    private ?string $customer;

    /**
     * @OA\Property(
     *     property="expiresAt",
     *     type="int",
     *     readOnly="true",
     *     example="1646868263"
     * ),
     */
    private int $expires_at;

    /**
     * @OA\Property(
     *     property="mode",
     *     type="string",
     *     readOnly="true",
     *     example="setup"
     * ),
     */
    private string $mode;

    /**
     * @OA\Property(
     *     property="status",
     *     type="string",
     *     readOnly="true",
     *     example="open"
     * ),
     */
    private string $status;

    /**
     * @OA\Property(
     *     property="formUrl",
     *     type="string",
     *     readOnly="true",
     * ),
     */
    private string $form_url;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): StripeCheckoutSession
    {
        $this->id = $id;
        return $this;
    }

    public function getObject(): string
    {
        return $this->object;
    }

    public function setObject(string $object): StripeCheckoutSession
    {
        $this->object = $object;
        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): StripeCheckoutSession
    {
        $this->currency = $currency;
        return $this;
    }

    public function getCustomer(): ?string
    {
        return $this->customer;
    }

    public function setCustomer(?string $customer): StripeCheckoutSession
    {
        $this->customer = $customer;
        return $this;
    }

    public function getExpiresAt(): int
    {
        return $this->expires_at;
    }

    public function setExpiresAt(int $expires_at): StripeCheckoutSession
    {
        $this->expires_at = $expires_at;
        return $this;
    }

    public function getMode(): string
    {
        return $this->mode;
    }

    public function setMode(string $mode): StripeCheckoutSession
    {
        $this->mode = $mode;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): StripeCheckoutSession
    {
        $this->status = $status;
        return $this;
    }

    public function getFormUrl(): string
    {
        return $this->form_url;
    }

    public function setFormUrl(string $form_url): StripeCheckoutSession
    {
        $this->form_url = $form_url;
        return $this;
    }

}
