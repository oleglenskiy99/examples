<?php

declare(strict_types=1);

namespace App\DTO\Response;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="StatusMessage"),
 * )
 */
final class StatusMessage
{

    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     readOnly="true",
     *     example=""
     * ),
     */
    private string $message;

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): StatusMessage
    {
        $this->message = $message;
        return $this;
    }

}
