<?php

declare(strict_types=1);

namespace App\DTO\Response\Objects;

class Auth
{

    private string $access_token;
    private string $jwt_token;
    private string $jwt_token_type;
    private int $jwt_expires;

    public function getAccessToken(): string
    {
        return $this->access_token;
    }

    public function setAccessToken(string $access_token): Auth
    {
        $this->access_token = $access_token;
        return $this;
    }

    public function getJwtToken(): string
    {
        return $this->jwt_token;
    }

    public function setJwtToken(string $jwt_token): Auth
    {
        $this->jwt_token = $jwt_token;
        return $this;
    }

    public function getJwtTokenType(): string
    {
        return $this->jwt_token_type;
    }

    public function setJwtTokenType(string $jwt_token_type): Auth
    {
        $this->jwt_token_type = $jwt_token_type;
        return $this;
    }

    public function getJwtExpires(): int
    {
        return $this->jwt_expires;
    }

    public function setJwtExpires(int $jwt_expires): Auth
    {
        $this->jwt_expires = $jwt_expires;
        return $this;
    }

}
