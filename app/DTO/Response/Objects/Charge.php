<?php

declare(strict_types=1);

namespace App\DTO\Response\Objects;

/**
 * @OA\Schema(
 * @OA\Xml(name="Charge"),
 * )
 */
class Charge
{

    /**
     * @OA\Property(
     *     property="id",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $id;

    /**
     * @OA\Property(
     *     property="amount",
     *     type="int",
     *     readOnly="true",
     *     example="300"
     * ),
     */
    private int $amount;

    /**
     * @OA\Property(
     *     property="category",
     *     type="string",
     *     readOnly="true",
     *     example="message"
     * ),
     */
    private string $category;

    /**
     * @OA\Property(
     *     property="currency",
     *     type="string",
     *     readOnly="true",
     *     example="usd"
     * ),
     */
    private string $currency;

    /**
     * @OA\Property(
     *     property="status",
     *     type="string",
     *     readOnly="true",
     *     example="captured"
     * ),
     */
    private string $status;

    /**
     * @OA\Property(
     *     property="object",
     *     type="string",
     *     readOnly="true",
     *     example=""
     * ),
     */
    private string $object;

    /**
     * @OA\Property(
     *     property="captured",
     *     type="bool",
     *     readOnly="true",
     *     example=""
     * ),
     */
    private bool $captured;

    /**
     * @OA\Property(
     *     property="refunded",
     *     type="bool",
     *     readOnly="true",
     *     example=""
     * ),
     */
    private bool $refunded;

    /**
     * @OA\Property(
     *     property="paid",
     *     type="bool",
     *     readOnly="true",
     *     example=""
     * ),
     */
    private bool $paid;

    /**
     * @OA\Property(
     *     property="out",
     *     type="bool",
     *     readOnly="true",
     *     example=""
     * ),
     */
    private bool $out;

    /**
     * @OA\Property(
     *     property="created",
     *     type="int",
     *     readOnly="true",
     *     example=""
     * ),
     */
    private int $created;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Charge
    {
        $this->id = $id;
        return $this;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): Charge
    {
        $this->amount = $amount;
        return $this;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function setCategory(string $category): Charge
    {
        $this->category = $category;
        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): Charge
    {
        $this->currency = $currency;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): Charge
    {
        $this->status = $status;
        return $this;
    }

    public function getObject(): string
    {
        return $this->object;
    }

    public function setObject(string $object): Charge
    {
        $this->object = $object;
        return $this;
    }

    public function isCaptured(): bool
    {
        return $this->captured;
    }

    public function setCaptured(bool $captured): Charge
    {
        $this->captured = $captured;
        return $this;
    }

    public function isRefunded(): bool
    {
        return $this->refunded;
    }

    public function setRefunded(bool $refunded): Charge
    {
        $this->refunded = $refunded;
        return $this;
    }

    public function isPaid(): bool
    {
        return $this->paid;
    }

    public function setPaid(bool $paid): Charge
    {
        $this->paid = $paid;
        return $this;
    }

    public function isOut(): bool
    {
        return $this->out;
    }

    public function setOut(bool $out): Charge
    {
        $this->out = $out;
        return $this;
    }

    public function getCreated(): int
    {
        return $this->created;
    }

    public function setCreated(int $created): Charge
    {
        $this->created = $created;
        return $this;
    }

}
