<?php

declare(strict_types=1);

namespace App\DTO\Response\Objects;

/**
 * @OA\Schema(
 * @OA\Xml(name="Country"),
 * )
 */
class Country
{

    /**
     * @OA\Property(
     *     property="id",
     *     type="int",
     *     readOnly="true",
     *     example="13"
     * ),
     */
    private int $id;

    /**
     * @OA\Property(
     *     property="name",
     *     type="string",
     *     readOnly="true",
     *     example="Россия"
     * ),
     */
    private string $name;

    /**
     * @OA\Property(
     *     property="sort_name",
     *     type="string",
     *     readOnly="true",
     *     example="RU"
     * ),
     */
    private string $sort_name;

    /**
     * @OA\Property(
     *     property="phone_code",
     *     type="int",
     *     readOnly="true",
     *     example="7"
     * ),
     */
    private int $phone_code;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Country
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Country
    {
        $this->name = $name;
        return $this;
    }

    public function getSortName(): string
    {
        return $this->sort_name;
    }

    public function setSortName(string $sort_name): Country
    {
        $this->sort_name = $sort_name;
        return $this;
    }

    public function getPhoneCode(): int
    {
        return $this->phone_code;
    }

    public function setPhoneCode(int $phone_code): Country
    {
        $this->phone_code = $phone_code;
        return $this;
    }

}
