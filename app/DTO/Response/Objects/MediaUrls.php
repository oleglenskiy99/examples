<?php

declare(strict_types=1);

namespace App\DTO\Response\Objects;

/**
 * @OA\Schema(
 * @OA\Xml(name="MediaUrls"),
 * )
 */
class MediaUrls
{

    /**
     * @OA\Property(
     *     property="placeholder",
     *     type="string",
     *     readOnly="true",
     *     example=""
     * ),
     */
    private ?string $placeholder;

    /**
     * @OA\Property(
     *     property="thumb",
     *     type="string",
     *     readOnly="true",
     *     example=""
     * ),
     */
    private ?string $thumb;

    /**
     * @OA\Property(
     *     property="original",
     *     type="string",
     *     readOnly="true",
     *     example=""
     * ),
     */
    private ?string $original;

    public function getPlaceholder(): ?string
    {
        return $this->placeholder;
    }

    public function setPlaceholder(?string $placeholder): MediaUrls
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    public function getThumb(): ?string
    {
        return $this->thumb;
    }

    public function setThumb(?string $thumb): MediaUrls
    {
        $this->thumb = $thumb;
        return $this;
    }

    public function getOriginal(): ?string
    {
        return $this->original;
    }

    public function setOriginal(?string $original): MediaUrls
    {
        $this->original = $original;
        return $this;
    }

}
