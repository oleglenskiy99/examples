<?php

declare(strict_types=1);

namespace App\DTO\Response\Objects;

/**
 * @OA\Schema(
 * @OA\Xml(name="Card"),
 * )
 */
class Card
{

    /**
     * @OA\Property(
     *     property="id",
     *     type="int",
     *     readOnly="true",
     *     example="13"
     * ),
     */
    private int $id;

    /**
     * @OA\Property(
     *     property="last4",
     *     type="string",
     *     readOnly="true",
     *     example="4422"
     * ),
     */
    private string $last4;

    /**
     * @OA\Property(
     *     property="brand",
     *     type="string",
     *     readOnly="true",
     *     example="visa"
     * ),
     */
    private string $brand;

    /**
     * @OA\Property(
     *     property="funding",
     *     type="string",
     *     readOnly="true",
     *     example="vias"
     * ),
     */
    private string $funding;

    /**
     * @OA\Property(
     *     property="exp_month",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $exp_month;

    /**
     * @OA\Property(
     *     property="exp_year",
     *     type="string",
     *     readOnly="true",
     *     example="22"
     * ),
     */
    private int $exp_year;

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): Card
    {
        $this->id = $id;
        return $this;
    }

    public function getLast4(): string
    {
        return $this->last4;
    }

    /**
     * @param string $last4
     * @return $this
     */
    public function setLast4(string $last4): Card
    {
        $this->last4 = $last4;
        return $this;
    }

    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     * @return $this
     */
    public function setBrand(string $brand): Card
    {
        $this->brand = $brand;
        return $this;
    }

    public function getFunding(): string
    {
        return $this->funding;
    }

    /**
     * @param string $funding
     * @return $this
     */
    public function setFunding(string $funding): Card
    {
        $this->funding = $funding;
        return $this;
    }

    public function getExpMonth(): int
    {
        return $this->exp_month;
    }

    /**
     * @param int $exp_month
     * @return $this
     */
    public function setExpMonth(int $exp_month): Card
    {
        $this->exp_month = $exp_month;
        return $this;
    }

    public function getExpYear(): int
    {
        return $this->exp_year;
    }

    /**
     * @param int $exp_year
     * @return $this
     */
    public function setExpYear(int $exp_year): Card
    {
        $this->exp_year = $exp_year;
        return $this;
    }

}
