<?php

declare(strict_types=1);

namespace App\DTO\Response\Objects;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     schema="MediaDto",
 *     @OA\Xml(name="MediaDto")
 * )
 */
class Media
{

    /**
     * @OA\Property(
     *     property="id",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $id;

    /**
     * @OA\Property(
     *     property="width",
     *     type="int",
     *     readOnly="true",
     *     example="100"
     * ),
     */
    private ?int $width;

    /**
     * @OA\Property(
     *     property="height",
     *     type="int",
     *     readOnly="true",
     *     example="100"
     * ),
     */
    private ?int $height;

    /**
     * @OA\Property(
     *     property="size",
     *     type="int",
     *     readOnly="true",
     *     example="100"
     * ),
     */
    private int $size;

    /**
     * @OA\Property(
     *     property="duration",
     *     type="int",
     *     readOnly="true",
     *     example="100"
     * ),
     */
    private ?int $duration;

    /**
     * @OA\Property(
     *     property="type",
     *     type="string",
     *     readOnly="true",
     *     example="video"
     * ),
     */
    private string $type;

    /**
     * @OA\Property(
     *     property="urls",
     *     type="object",
     *     @OA\Schema(ref="#/components/schemas/MediaUrls"),
     *         readOnly="true",
     *         example=""
     *     ),
     */
    private MediaUrls $urls;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Media
    {
        $this->id = $id;
        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(?int $width): Media
    {
        $this->width = $width;
        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(?int $height): Media
    {
        $this->height = $height;
        return $this;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function setSize(int $size): Media
    {
        $this->size = $size;
        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): Media
    {
        $this->duration = $duration;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): Media
    {
        $this->type = $type;
        return $this;
    }

    public function getUrls(): MediaUrls
    {
        return $this->urls;
    }

    public function setUrls(MediaUrls $urls): Media
    {
        $this->urls = $urls;
        return $this;
    }

}
