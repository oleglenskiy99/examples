<?php

declare(strict_types=1);

namespace App\DTO\Response\Objects;

/**
 * @OA\Schema(
 * @OA\Xml(name="UserTruncated"),
 * )
 */
class UserTruncated
{

    /**
     * @OA\Property(
     *     property="id",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $id;

    /**
     * @OA\Property(
     *     property="domain",
     *     type="string",
     *     readOnly="false",
     *     example=""
     * ),
     */
    private string $domain;

    /**
     * @OA\Property(
     *     property="first_name",
     *     type="string",
     *     readOnly="false",
     *     example=""
     * ),
     */
    private string $first_name;

    /**
     * @OA\Property(
     *     property="last_name",
     *     type="string",
     *     readOnly="false",
     *     example=""
     * ),
     */
    private string $last_name;

    /**
     * @OA\Property(
     *     property="photo",
     *     type="string",
     *     readOnly="true",
     *     example=""
     * ),
     */
    private string $photo;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): UserTruncated
    {
        $this->id = $id;
        return $this;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): UserTruncated
    {
        $this->domain = $domain;
        return $this;
    }

    public function getFirstName(): string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): UserTruncated
    {
        $this->first_name = $first_name;
        return $this;
    }

    public function getLastName(): string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): UserTruncated
    {
        $this->last_name = $last_name;
        return $this;
    }

    public function getPhoto(): string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): UserTruncated
    {
        $this->photo = $photo;
        return $this;
    }

}
