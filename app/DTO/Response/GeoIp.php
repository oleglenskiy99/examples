<?php

declare(strict_types=1);

namespace App\DTO\Response;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="GeoIp"),
 * )
 */
final class GeoIp
{

    /**
     * @OA\Property(
     *     property="ipAddress",
     *     type="integer",
     *     readOnly="true",
     *     example="62.341.233.211"
     * ),
     */
    private int $ipAddress;

    /**
     * @OA\Property(
     *     property="countryCode",
     *     type="string",
     *     readOnly="true",
     *     example="MSK"
     * ),
     */
    private string $countryCode;

    /**
     * @OA\Property(
     *     property="countryName",
     *     type="string",
     *     readOnly="true",
     *     example="Russia"
     * ),
     */
    private string $countryName;

    /**
     * @OA\Property(
     *     property="regionName",
     *     type="string",
     *     readOnly="true",
     *     example="Moscow"
     * ),
     */
    private string $regionName;

    /**
     * @OA\Property(
     *     property="cityName",
     *     type="string",
     *     readOnly="true",
     *     example="Moscow"
     * ),
     */
    private string $cityName;

    public function getIpAddress(): int
    {
        return $this->ipAddress;
    }

    public function setIpAddress(int $ipAddress): GeoIp
    {
        $this->ipAddress = $ipAddress;
        return $this;
    }

    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    public function setCountryCode(string $countryCode): GeoIp
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    public function getCountryName(): string
    {
        return $this->countryName;
    }

    public function setCountryName(string $countryName): GeoIp
    {
        $this->countryName = $countryName;
        return $this;
    }

    public function getRegionName(): string
    {
        return $this->regionName;
    }

    public function setRegionName(string $regionName): GeoIp
    {
        $this->regionName = $regionName;
        return $this;
    }

    public function getCityName(): string
    {
        return $this->cityName;
    }

    public function setCityName(string $cityName): GeoIp
    {
        $this->cityName = $cityName;
        return $this;
    }

}
