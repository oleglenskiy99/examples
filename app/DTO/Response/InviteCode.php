<?php

declare(strict_types=1);

namespace App\DTO\Response;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="InviteCode"),
 * )
 */
final class InviteCode
{

    /**
     * @OA\Property(
     *     property="is_active",
     *     type="boolean",
     *     example="true"
     * ),
     */
    private bool $is_active;

    /**
     * @OA\Property(
     *     property="event_id",
     *     type="int",
     *     example="1"
     * ),
     */
    private int $event_id;

    public function getIsActive(): bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): InviteCode
    {
        $this->is_active = $is_active;
        return $this;
    }

    public function getEventId(): int
    {
        return $this->event_id;
    }

    public function setEventId(int $event_id): InviteCode
    {
        $this->event_id = $event_id;
        return $this;
    }

}
