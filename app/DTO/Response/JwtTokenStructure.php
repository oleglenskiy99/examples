<?php

declare(strict_types=1);

namespace App\DTO\Response;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="JwtTokenStructure"),
 * )
 */
final class JwtTokenStructure
{

    /**
     * @OA\Property(
     *     property="accessToken",
     *     type="string",
     *     readOnly="true",
     *     example=""
     * ),
     */
    private string $accessToken;

    /**
     * @OA\Property(
     *     property="tokenType",
     *     type="string",
     *     readOnly="true",
     *     example="bearer"
     * ),
     */
    private string $tokenType;

    /**
     * @OA\Property(
     *     property="expiresIn",
     *     type="integer",
     *     readOnly="true",
     *     example="3600"
     * ),
     */
    private int $expiresIn;

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    public function setAccessToken(string $accessToken): JwtTokenStructure
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    public function getTokenType(): string
    {
        return $this->tokenType;
    }

    public function setTokenType(string $tokenType): JwtTokenStructure
    {
        $this->tokenType = $tokenType;
        return $this;
    }

    public function getExpiresIn(): int
    {
        return $this->expiresIn;
    }

    public function setExpiresIn(int $expiresIn): JwtTokenStructure
    {
        $this->expiresIn = $expiresIn;
        return $this;
    }

}
