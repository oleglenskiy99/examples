<?php

declare(strict_types=1);

namespace App\DTO\Response\Event;

/**
 * @OA\Schema(
 * @OA\Xml(name="EventItem"),
 * )
 */
final class EventItem
{

    /**
     * @OA\Property(
     *     property="id",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private int $id;

    /**
     * @OA\Property(
     *     property="image",
     *     type="string",
     *     readOnly="true",
     *     example="url"
     * ),
     */
    private ?string $image;

    /**
     * @OA\Property(
     *     property="name",
     *     type="string",
     *     readOnly="true",
     *     example="Событие 1"
     * ),
     */
    private string $name;

    /**
     * @OA\Property(
     *     property="description",
     *     type="string",
     *     readOnly="true",
     *     example="Событие 1"
     * ),
     */
    private ?string $description;

    /**
     * @OA\Property(
     *     property="fullImage",
     *     type="string",
     *     readOnly="true",
     *     example="url"
     * ),
     */
    private ?string $fullImage;

    /**
     * @OA\Property(
     *     property="fullDescription",
     *     type="string",
     *     readOnly="true",
     *     example="Событие 1"
     * ),
     */
    private ?string $fullDescription;

    /**
     * @OA\Property(
     *     property="userTypes",
     *     @OA\Items(ref="#/components/schemas/EventUserTypeItem"),
     *     type="array",
     * ),
     */
    private array $user_types;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): EventItem
    {
        $this->id = $id;
        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): EventItem
    {
        $this->image = $image;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): EventItem
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): EventItem
    {
        $this->description = $description;
        return $this;
    }

    public function getFullImage(): ?string
    {
        return $this->fullImage;
    }

    public function setFullImage(?string $fullImage): EventItem
    {
        $this->fullImage = $fullImage;
        return $this;
    }

    public function getFullDescription(): ?string
    {
        return $this->fullDescription;
    }

    public function setFullDescription(?string $fullDescription): EventItem
    {
        $this->fullDescription = $fullDescription;
        return $this;
    }

    public function getUserTypes(): array
    {
        return $this->user_types;
    }

    public function setUserTypes(array $user_types): EventItem
    {
        $this->user_types = $user_types;
        return $this;
    }

}
