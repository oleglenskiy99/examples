<?php

declare(strict_types=1);

namespace App\DTO\Response\Event;

/**
 * @OA\Schema(
 * @OA\Xml(name="EventUserTypeItem"),
 * )
 */
final class EventUserTypeItem
{

    /**
     * @OA\Property(
     *     property="id",
     *     type="int",
     *     readOnly="true",
     *     example="1"
     * ),
     */
    private int $id;

    /**
     * @OA\Property(
     *     property="name",
     *     type="string",
     *     readOnly="true",
     *     example="Участник"
     * ),
     */
    private string $name;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): EventUserTypeItem
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): EventUserTypeItem
    {
        $this->name = $name;
        return $this;
    }

}
