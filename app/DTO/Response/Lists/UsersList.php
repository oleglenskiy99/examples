<?php

declare(strict_types=1);

namespace App\DTO\Response\Lists;

use App\DTO\Response\Objects\UserTruncated;

class UsersList extends BaseList
{

    /**
     * @var array<UserTruncated>
     */
    private array $items;

    /**
     * @return UserTruncated[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param UserTruncated[] $items
     * @return UsersList
     */
    public function setItems(array $items): UsersList
    {
        $this->items = $items;
        return $this;
    }

}
