<?php

declare(strict_types=1);

namespace App\DTO\Response\Lists;

use App\DTO\Response\Objects\Charge;

class ChargesList extends BaseList
{

    /**
     * @var array<Charge>
     */
    private array $items;

    /**
     * @return Charge[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Charge[] $items
     * @return ChargesList
     */
    public function setItems(array $items): ChargesList
    {
        $this->items = $items;
        return $this;
    }

}
