<?php

declare(strict_types=1);

namespace App\DTO\Response\Lists;

use App\DTO\Response\Connections\ConnectionItem;

/**
 * @OA\Schema(
 * @OA\Xml(name="ConnectionsList"),
 * )
 */
class ConnectionsList
{

    /**
     * @var ConnectionItem[] $items
     * @OA\Property(
     *     property="items",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/ConnectionItem"),
     *     readOnly="true"
     * ),
     */
    private array $items;

    /**
     * @OA\Property(
     *     property="count",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $count;

    /**
     * @OA\Property(
     *     property="pageNumber",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $pageNumber;

    /**
     * @OA\Property(
     *     property="totalCount",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $totalCount;

    /**
     * @OA\Property(
     *     property="totalPages",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $totalPages;

    public function getItems(): array
    {
        return $this->items;
    }

    public function setItems(array $items): ConnectionsList
    {
        $this->items = $items;
        return $this;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count): ConnectionsList
    {
        $this->count = $count;
        return $this;
    }

    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    public function setPageNumber(int $pageNumber): ConnectionsList
    {
        $this->pageNumber = $pageNumber;
        return $this;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function setTotalCount(int $totalCount): ConnectionsList
    {
        $this->totalCount = $totalCount;
        return $this;
    }

    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    public function setTotalPages(int $totalPages): ConnectionsList
    {
        $this->totalPages = $totalPages;
        return $this;
    }

}
