<?php

declare(strict_types=1);

namespace App\DTO\Response\Lists;

class BaseList
{

    private int $total_count;
    private int $display_count;
    private int $current_page;
    private int $next_page;
    private int $prev_page;

    public function getTotalCount(): int
    {
        return $this->total_count;
    }

    public function setTotalCount(int $total_count): BaseList
    {
        $this->total_count = $total_count;
        return $this;
    }

    public function getDisplayCount(): int
    {
        return $this->display_count;
    }

    public function setDisplayCount(int $display_count): BaseList
    {
        $this->display_count = $display_count;
        return $this;
    }

    public function getCurrentPage(): int
    {
        return $this->current_page;
    }

    public function setCurrentPage(int $current_page): BaseList
    {
        $this->current_page = $current_page;
        return $this;
    }

    public function getNextPage(): int
    {
        return $this->next_page;
    }

    public function setNextPage(int $next_page): BaseList
    {
        $this->next_page = $next_page;
        return $this;
    }

    public function getPrevPage(): int
    {
        return $this->prev_page;
    }

    public function setPrevPage(int $prev_page): BaseList
    {
        $this->prev_page = $prev_page;
        return $this;
    }

}
