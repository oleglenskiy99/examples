<?php

declare(strict_types=1);

namespace App\DTO\Response;

use App\DTO\Response\User\UserItem;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="UserList"),
 * )
 */
final class UserList
{

    /**
     * @var UserItem[]
     * @OA\Property(
     *     property="items",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/UserItem"),
     *     readOnly="true"
     * ),
     */
    private array $items;

    /**
     * @OA\Property(
     *     property="count",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $count;

    /**
     * @OA\Property(
     *     property="pageNumber",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $pageNumber;

    /**
     * @OA\Property(
     *     property="totalCount",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $totalCount;

    /**
     * @return UserItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param UserItem[] $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $pageCount): UserList
    {
        $this->count = $pageCount;
        return $this;
    }

    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    public function setPageNumber(int $pageNumber): UserList
    {
        $this->pageNumber = $pageNumber;
        return $this;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function setTotalCount(int $totalCount): UserList
    {
        $this->totalCount = $totalCount;
        return $this;
    }

}
