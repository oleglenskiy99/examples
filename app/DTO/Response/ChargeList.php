<?php

declare(strict_types=1);

namespace App\DTO\Response;

use App\DTO\Response\Charge\ChargeItem;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="ChargeList"),
 * )
 */
final class ChargeList
{

    /**
     * @var ChargeItem[]
     * @OA\Property(
     *     property="items",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/ChargeItem"),
     *     readOnly="true"
     * ),
     */
    private array $items;

    /**
     * @OA\Property(
     *     property="count",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $count;

    /**
     * @OA\Property(
     *     property="pageNumber",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $pageNumber;

    /**
     * @OA\Property(
     *     property="totalCount",
     *     type="int",
     *     readOnly="true",
     *     example="3"
     * ),
     */
    private int $totalCount;

    /**
     * @return ChargeItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param ChargeItem[] $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $pageCount): ChargeList
    {
        $this->count = $pageCount;
        return $this;
    }

    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    public function setPageNumber(int $pageNumber): ChargeList
    {
        $this->pageNumber = $pageNumber;
        return $this;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function setTotalCount(int $totalCount): ChargeList
    {
        $this->totalCount = $totalCount;
        return $this;
    }

}
