<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="UpdateUser"),
 * )
 */
final class UpdateUser
{

    /**
     * @OA\Property(
     *     property="photo",
     *     type="string",
     *     example="{path_to_file}"
     * ),
     */
    private string $image;

    /**
     * @OA\Property(
     *     property="firstName",
     *     type="string",
     *     example="Florian"
     * ),
     */
    private string $firstName;

    /**
     * @OA\Property(
     *     property="lastName",
     *     type="string",
     *     example="Weither"
     * ),
     */
    private string $lastName;

    /**
     * @OA\Property(
     *     property="nickname",
     *     type="string",
     *     example="florianApex"
     * ),
     */
    private string $nickname;

    /**
     * @OA\Property(
     *     property="description",
     *     type="string",
     *     example="I'm a super heroes, I usually go to street at night and try to find shop with super power elixir..."
     * ),
     */
    private string $description;

    public function getPhoto(): string
    {
        return $this->image;
    }

    public function setPhoto(string $image): UpdateUser
    {
        $this->image = $image;
        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): UpdateUser
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): UpdateUser
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getNickname(): string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): UpdateUser
    {
        $this->nickname = $nickname;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): UpdateUser
    {
        $this->description = $description;
        return $this;
    }

}
