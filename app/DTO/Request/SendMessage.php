<?php

declare(strict_types=1);

namespace App\DTO\Request;

class SendMessage
{

    private int $chatId;
    private string $text = '';
    private array $mediaIds = [];

    public function getChatId(): int
    {
        return $this->chatId;
    }

    public function setChatId(int $chatId): SendMessage
    {
        $this->chatId = $chatId;
        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): SendMessage
    {
        $this->text = $text;
        return $this;
    }

    public function getMediaIds(): array
    {
        return $this->mediaIds;
    }

    public function setMediaIds(array $mediaIds): SendMessage
    {
        $this->mediaIds = $mediaIds;
        return $this;
    }

}
