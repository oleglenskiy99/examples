<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="ReviewUser"),
 * )
 */
final class ReviewUser
{

    /**
     * @OA\Property(
     *     property="targetId",
     *     type="int",
     *     example=""
     * ),
     */
    private int $targetId = 0;

    /**
     * @OA\Property(
     *     property="serviceId",
     *     type="int",
     *     example=""
     * ),
     */
    private int $serviceId = 0;

    /**
     * @OA\Property(
     *     property="serviceType",
     *     type="string",
     *     example="video"
     * ),
     */
    private string $serviceType = 'video';

    /**
     * @OA\Property(
     *     property="value",
     *     type="int",
     *     example=""
     * ),
     */
    private int $value = 0;

    /**
     * @OA\Property(
     *     property="caption",
     *     type="string",
     *     example="awesome"
     * ),
     */
    private string $caption = '';

    public function getTargetId(): int
    {
        return $this->targetId;
    }

    public function setTargetId(int $targetId): ReviewUser
    {
        $this->targetId = $targetId;
        return $this;
    }

    public function getServiceId(): int
    {
        return $this->serviceId;
    }

    public function setServiceId(int $serviceId): ReviewUser
    {
        $this->serviceId = $serviceId;
        return $this;
    }

    public function getServiceType(): string
    {
        return $this->serviceType;
    }

    public function setServiceType(string $serviceType): ReviewUser
    {
        $this->serviceType = $serviceType;
        return $this;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): ReviewUser
    {
        $this->value = $value;
        return $this;
    }

    public function getCaption(): string
    {
        return $this->caption;
    }

    public function setCaption(string $caption): ReviewUser
    {
        $this->caption = $caption;
        return $this;
    }

}
