<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="Subscribe"),
 * )
 */
final class Subscribe
{

    /**
     * @OA\Property(
     *     property="userId",
     *     type="int",
     *     example="30"
     * ),
     */
    private int $userId;

    /**
     * @OA\Property(
     *     property="isSubscribe",
     *     type="boolean",
     *     example="false"
     * ),
     */
    private bool $isSubscribe;

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): Subscribe
    {
        $this->userId = $userId;
        return $this;
    }

    public function isSubscribe(): bool
    {
        return $this->isSubscribe;
    }

    public function setIsSubscribe(bool $isSubscribe): Subscribe
    {
        $this->isSubscribe = $isSubscribe;
        return $this;
    }

}
