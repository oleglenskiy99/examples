<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="AttachCategory"),
 * )
 */
final class AttachCategory
{

    /**
     * @var int[]
     * @OA\Property(
     *     property="ids",
     *     type="array",
     *     @OA\Items(
     *         example="115",
     *         type="int",
     *     ),
     * ),
     */
    private array $ids;

    public function getIds(): array
    {
        return $this->ids;
    }

    public function setIds(array $ids): AttachCategory
    {
        $this->ids = $ids;
        return $this;
    }

}
