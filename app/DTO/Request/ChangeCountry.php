<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="ChangeCountry"),
 * )
 */
final class ChangeCountry
{

    /**
     * @OA\Property(
     *     property="country_id",
     *     type="int",
     *     example="1"
     * ),
     */
    private int $country_id;

    public function getCountryId(): int
    {
        return $this->country_id;
    }

    public function setCountryId(int $country_id): ChangeCountry
    {
        $this->country_id = $country_id;
        return $this;
    }

}
