<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="RegistrationUser"),
 * )
 */
final class RegistrationUser
{

    /**
     * @OA\Property(
     *     property="name",
     *     type="string",
     *     example="Florian"
     * ),
     */
    private string $name;

    /**
     * @OA\Property(
     *     property="nickname",
     *     type="string",
     *     example="florianApex"
     * ),
     */
    private string $nickname;

    /**
     * @OA\Property(
     *     property="email",
     *     type="string",
     *     example="t.myEmail@myDomain.com"
     * ),
     */
    private string $email;

    /**
     * @OA\Property(
     *     property="password",
     *     type="string",
     *     example="************"
     * ),
     */
    private string $password;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): RegistrationUser
    {
        $this->name = $name;
        return $this;
    }

    public function getNickname(): string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): RegistrationUser
    {
        $this->nickname = $nickname;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): RegistrationUser
    {
        $this->email = $email;
        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): RegistrationUser
    {
        $this->password = $password;
        return $this;
    }

}
