<?php

declare(strict_types=1);

namespace App\DTO\Request;

final class GetFollowers
{

    private int $userId;
    private int $page;
    private int $count;

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): GetFollowers
    {
        $this->userId = $userId;
        return $this;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): GetFollowers
    {
        $this->page = $page;
        return $this;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count): GetFollowers
    {
        $this->count = $count;
        return $this;
    }

}
