<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="CheckEmailVerification"),
 * )
 */
final class CheckEmailVerification
{

    /**
     * @OA\Property(
     *     property="email",
     *     type="string",
     *     example="t.myEmail@myDomain.com"
     * ),
     */
    public string $email;

    /**
     * @OA\Property(
     *     property="code",
     *     type="int",
     *     example="123412"
     * ),
     */
    public int $code;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): CheckEmailVerification
    {
        $this->email = $email;
        return $this;
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function setCode(int $code): CheckEmailVerification
    {
        $this->code = $code;
        return $this;
    }

}
