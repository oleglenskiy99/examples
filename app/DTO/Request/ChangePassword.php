<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="ChangePassword"),
 * )
 */
final class ChangePassword
{

    /**
     * @OA\Property(
     *     property="oldPassword",
     *     type="string",
     *     example="**************"
     * ),
     */
    private string $oldPassword;

    /**
     * @OA\Property(
     *     property="newPassword",
     *     type="string",
     *     example="**************"
     * ),
     */
    private string $newPassword;

    public function getOldPassword(): string
    {
        return $this->oldPassword;
    }

    public function setOldPassword(string $oldPassword): ChangePassword
    {
        $this->oldPassword = $oldPassword;
        return $this;
    }

    public function getNewPassword(): string
    {
        return $this->newPassword;
    }

    public function setNewPassword(string $newPassword): ChangePassword
    {
        $this->newPassword = $newPassword;
        return $this;
    }

}
