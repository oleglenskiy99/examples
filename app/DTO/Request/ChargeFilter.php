<?php

declare(strict_types=1);

namespace App\DTO\Request;

final class ChargeFilter
{

    private int $countOfItems = 10;
    private int $page = 1;

    public function getCountOfItems(): int
    {
        return $this->countOfItems;
    }

    public function setCountOfItems(int $countOfItems): ChargeFilter
    {
        $this->countOfItems = $countOfItems;
        return $this;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): ChargeFilter
    {
        $this->page = $page;
        return $this;
    }

}
