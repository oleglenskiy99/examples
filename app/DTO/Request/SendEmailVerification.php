<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="SendEmailVerification"),
 * )
 */
final class SendEmailVerification
{

    /**
     * @OA\Property(
     *     property="email",
     *     type="string",
     *     example="t.myEmail@myDomain.com"
     * ),
     */
    public string $email;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): SendEmailVerification
    {
        $this->email = $email;
        return $this;
    }

}
