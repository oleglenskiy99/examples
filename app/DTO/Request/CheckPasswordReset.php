<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="CheckPasswordReset"),
 * )
 */
final class CheckPasswordReset
{

    /**
     * @OA\Property(
     *     property="email",
     *     type="string",
     *     example="t.myEmail@myDomain.com"
     * ),
     */
    public string $email;

    /**
     * @OA\Property(
     *     property="code",
     *     type="int",
     *     example="123412"
     * ),
     */
    public int $code;

    /**
     * @OA\Property(
     *     property="new_password",
     *     type="string",
     *     example="*********"
     * ),
     */
    public string $new_password;

    /**
     * @OA\Property(
     *     property="new_password_confirm",
     *     type="string",
     *     example="*********"
     * ),
     */
    public string $new_password_confirm;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): CheckPasswordReset
    {
        $this->email = $email;
        return $this;
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function setCode(int $code): CheckPasswordReset
    {
        $this->code = $code;
        return $this;
    }

    public function getNewPassword(): string
    {
        return $this->new_password;
    }

    public function setNewPassword(string $new_password): CheckPasswordReset
    {
        $this->new_password = $new_password;
        return $this;
    }

    public function getNewPasswordConfirm(): string
    {
        return $this->new_password_confirm;
    }

    public function setNewPasswordConfirm(string $new_password_confirm): CheckPasswordReset
    {
        $this->new_password_confirm = $new_password_confirm;
        return $this;
    }

}
