<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="AddPhone"),
 * )
 */
final class AddPayout
{

    /**
     * @OA\Property(
     *     property="card",
     *     type="string",
     *     example="cardstring"
     * ),
     */
    private int $card;

    /**
     * @OA\Property(
     *     property="amount",
     *     type="string",
     *     example="cardstring"
     * ),
     */
    private ?float $amount;

    public function getCard(): int
    {
        return $this->card;
    }

    public function setCard(int $card): AddPayout
    {
        $this->card = $card;
        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(?float $amount): AddPayout
    {
        $this->amount = $amount;
        return $this;
    }
}
