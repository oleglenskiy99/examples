<?php

declare(strict_types=1);

namespace App\DTO\Request;

use App\DTO\Common\Communication;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="AttachCommunication"),
 * )
 */
final class AttachCommunication
{

    /**
     * @var Communication[]
     * @OA\Property(
     *     property="items",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/Communication"),
     * ),
     */
    private array $items;

    /**
     * @return Communication[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Communication[] $items
     */
    public function setItems(array $items): AttachCommunication
    {
        $this->items = $items;
        return $this;
    }

}
