<?php

declare(strict_types=1);

namespace App\DTO\Request;

class ChatCreate
{

    private string $title;
    private int $userId;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): ChatCreate
    {
        $this->title = $title;
        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): ChatCreate
    {
        $this->userId = $userId;
        return $this;
    }

}
