<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="ViewedUser"),
 * )
 */
final class ViewedUser
{

    /**
     * @OA\Property(
     *     property="userId",
     *     type="int",
     *     example="1"
     * ),
     */
    private int $userId;

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): ViewedUser
    {
        $this->userId = $userId;
        return $this;
    }

}
