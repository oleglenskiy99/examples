<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="AddPhone"),
 * )
 */
final class AddPhone
{

    /**
     * @OA\Property(
     *     property="phone",
     *     type="string",
     *     example="7111111111"
     * ),
     */
    private ?string $phone;

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): AddPhone
    {
        $this->phone = $phone;
        return $this;
    }

}
