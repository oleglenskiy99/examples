<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="ConfirmPhone"),
 * )
 */
final class ConfirmPhone
{

    /**
     * @OA\Property(
     *     property="phone",
     *     type="string",
     *     example="7111111111"
     * ),
     */
    private ?string $phone;

    /**
     * @OA\Property(
     *     property="c",
     *     type="string",
     *     example="123123"
     * ),
     */
    private string $code;

    /**
     * @OA\Property(
     *     property="i",
     *     type="string",
     *     example="12sdc3dfvsd123"
     * ),
     */
    private string $ident;

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): ConfirmPhone
    {
        $this->phone = $phone;
        return $this;
    }

    public function getC(): ?string
    {
        return $this->code;
    }

    public function setC(?string $code): ConfirmPhone
    {
        $this->code = $code;
        return $this;
    }

    public function getI(): ?string
    {
        return $this->ident;
    }

    public function setI(?string $ident): ConfirmPhone
    {
        $this->ident = $ident;
        return $this;
    }

}
