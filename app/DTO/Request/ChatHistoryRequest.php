<?php

declare(strict_types=1);

namespace App\DTO\Request;

/**
 * @OA\Schema(
 * @OA\Xml(name="MessagesHistoryRequest"),
 * )
 */
class ChatHistoryRequest
{

    /**
     * @OA\Property(
     *     property="chatId",
     *     type="int",
     *     example="122"
     * ),
     */
    private int $chatId;

    /**
     * @OA\Property(
     *     property="countOfItems",
     *     type="int",
     *     example="20"
     * ),
     */
    private int $countOfItems = 20;

    /**
     * @OA\Property(
     *     property="page",
     *     type="int",
     *     example="1"
     * ),
     */
    private int $page = 1;

    public function getChatId(): int
    {
        return $this->chatId;
    }


    public function setChatId(int $chatId): ChatHistoryRequest
    {
        $this->chatId = $chatId;
        return $this;
    }

    public function getCountOfItems(): int
    {
        return $this->countOfItems;
    }

    public function setCountOfItems(int $countOfItems): ChatHistoryRequest
    {
        $this->countOfItems = $countOfItems;
        return $this;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): ChatHistoryRequest
    {
        $this->page = $page;
        return $this;
    }

}
