<?php

declare(strict_types=1);

namespace App\DTO\Request;

final class UserFilter
{

    /**
     * @var int[]
     */
    private array $categoryIds = [];

    /**
     * @var int[]
     */
    private array $countryIds = [];
    private int $ratingsFrom = 0;
    private int $ratingsTo = 0;
    private bool $isChatRequestAvailable = false;
    private bool $isAudioRequestAvailable = false;
    private bool $isVideoRequestAvailable = false;
    private bool $isImageRequestAvailable = false;
    private float $priceFrom = 0;
    private float $priceTo = 0;
    private bool $isVerification = false;
    private string $searchText = '';
    private int $currentUserId;
    private int $countOfItems = 10;
    private int $page = 1;
    private int $eventId = 0;
    private int $eventUserTypeId = 0;
    private bool $onlyFilled = false;

    /**
     * @return int[]
     */
    public function getCategoryIds(): array
    {
        return $this->categoryIds;
    }

    /**
     * @param int[] $categoryIds
     */
    public function setCategoryIds(array $categoryIds): UserFilter
    {
        $this->categoryIds = $categoryIds;
        return $this;
    }

    /**
     * @return int[]
     */
    public function getCountryIds(): array
    {
        return $this->countryIds;
    }

    /**
     * @param int[] $countryId
     */
    public function setCountryIds(array $countryId): UserFilter
    {
        $this->countryIds = $countryId;
        return $this;
    }

    public function getRatingsFrom(): int
    {
        return $this->ratingsFrom;
    }

    public function setRatingsFrom(int $ratingsFrom): UserFilter
    {
        $this->ratingsFrom = $ratingsFrom;
        return $this;
    }

    public function getRatingsTo(): int
    {
        return $this->ratingsTo;
    }

    public function setRatingsTo(int $ratingsTo): UserFilter
    {
        $this->ratingsTo = $ratingsTo;
        return $this;
    }

    public function isChatRequestAvailable(): bool
    {
        return $this->isChatRequestAvailable;
    }

    public function setIsChatRequestAvailable(bool $isChatRequestAvailable): UserFilter
    {
        $this->isChatRequestAvailable = $isChatRequestAvailable;
        return $this;
    }

    public function isAudioRequestAvailable(): bool
    {
        return $this->isAudioRequestAvailable;
    }

    public function setIsAudioRequestAvailable(bool $isAudioRequestAvailable): UserFilter
    {
        $this->isAudioRequestAvailable = $isAudioRequestAvailable;
        return $this;
    }

    public function isVideoRequestAvailable(): bool
    {
        return $this->isVideoRequestAvailable;
    }

    public function setIsVideoRequestAvailable(bool $isVideoRequestAvailable): UserFilter
    {
        $this->isVideoRequestAvailable = $isVideoRequestAvailable;
        return $this;
    }

    public function isImageRequestAvailable(): bool
    {
        return $this->isImageRequestAvailable;
    }

    public function setIsImageRequestAvailable(bool $isImageRequestAvailable): UserFilter
    {
        $this->isImageRequestAvailable = $isImageRequestAvailable;
        return $this;
    }

    public function getPriceFrom(): float
    {
        return $this->priceFrom;
    }

    public function setPriceFrom(float $priceFrom): UserFilter
    {
        $this->priceFrom = $priceFrom;
        return $this;
    }

    public function getPriceTo(): float
    {
        return $this->priceTo;
    }

    public function setPriceTo(float $priceTo): UserFilter
    {
        $this->priceTo = $priceTo;
        return $this;
    }

    public function isVerification(): bool
    {
        return $this->isVerification;
    }

    public function setIsVerification(bool $isVerification): UserFilter
    {
        $this->isVerification = $isVerification;
        return $this;
    }

    public function getOnlyFilled(): bool
    {
        return $this->onlyFilled;
    }

    public function setOnlyFilled(bool $onlyFilled): UserFilter
    {
        $this->onlyFilled = $onlyFilled;
        return $this;
    }

    public function getSearchText(): string
    {
        return $this->searchText;
    }

    public function setSearchText(string $searchText): UserFilter
    {
        $this->searchText = $searchText;
        return $this;
    }

    public function getCurrentUserId(): int
    {
        return $this->currentUserId;
    }

    public function setCurrentUserId(int $currentUserId): UserFilter
    {
        $this->currentUserId = $currentUserId;
        return $this;
    }

    public function getCountOfItems(): int
    {
        return $this->countOfItems;
    }

    public function setCountOfItems(int $countOfItems): UserFilter
    {
        $this->countOfItems = $countOfItems;
        return $this;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): UserFilter
    {
        $this->page = $page;
        return $this;
    }

    public function getEventId(): int
    {
        return $this->eventId;
    }

    public function setEventId(int $eventId): UserFilter
    {
        $this->eventId = $eventId;
        return $this;
    }

    public function getEventUserTypeId(): int
    {
        return $this->eventUserTypeId;
    }

    public function setEventUserTypeId(int $eventUserTypeId): UserFilter
    {
        $this->eventUserTypeId = $eventUserTypeId;
        return $this;
    }

}
