<?php

declare(strict_types=1);

namespace App\DTO\Request;

class ConnectionsRequest
{

    private ?int $userId = null;
    private ?int $countOfItems = null;
    private ?int $offset = null;
    private ?int $isOnlyNew = null;
    private ?int $section = null;

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(?int $userId): ConnectionsRequest
    {
        $this->userId = $userId;
        return $this;
    }

    public function getCountOfItems(): ?int
    {
        return $this->countOfItems;
    }

    public function setCountOfItems(?int $countOfItems): ConnectionsRequest
    {
        $this->countOfItems = $countOfItems;
        return $this;
    }

    public function getOffset(): ?int
    {
        return $this->offset;
    }

    public function setOffset(?int $offset): ConnectionsRequest
    {
        $this->offset = $offset;
        return $this;
    }

    public function getIsOnlyNew(): ?int
    {
        return $this->isOnlyNew;
    }

    public function setIsOnlyNew(?int $isOnlyNew): ConnectionsRequest
    {
        $this->isOnlyNew = $isOnlyNew;
        return $this;
    }


    public function hasIsOnlyNew(): bool
    {
        return !is_null($this->isOnlyNew);
    }

    public function hasUserId(): bool
    {
        return !is_null($this->userId);
    }

    public function hasCountOfItems(): bool
    {
        return !is_null($this->countOfItems);
    }

    public function hasOffset(): bool
    {
        return !is_null($this->offset);
    }


    public function getSection(): ?int
    {
        return $this->section;
    }

    public function setSection(?int $section): ConnectionsRequest
    {
        $this->section = $section;
        return $this;
    }

}
