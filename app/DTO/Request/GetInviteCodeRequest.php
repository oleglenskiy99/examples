<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="GetInviteCodeRequest"),
 * )
 */
final class GetInviteCodeRequest
{

    /**
     * @OA\Property(
     *     property="invite_code",
     *     type="string",
     *     example="43215321"
     * ),
     */
    public string $inviteCode;

    public function getInviteCode(): string
    {
        return $this->inviteCode;
    }

    public function setInviteCode(string $code): GetInviteCodeRequest
    {
        $this->inviteCode = $code;
        return $this;
    }

}
