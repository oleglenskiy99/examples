<?php

declare(strict_types=1);

namespace App\DTO\Request;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="GeoIp"),
 * )
 */
final class Login
{

    /**
     * @OA\Property(
     *     property="email",
     *     type="string",
     *     example="t.myEmail@myDomain.com"
     * ),
     */
    public string $email;

    /**
     * @OA\Property(
     *     property="password",
     *     type="string",
     *     example="*************"
     * ),
     */
    public string $password;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): Login
    {
        $this->email = $email;
        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): Login
    {
        $this->password = $password;
        return $this;
    }

}
