<?php

declare(strict_types=1);

namespace App\DTO\Common;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="Price"),
 * )
 */
final class Price
{

    /**
     * @OA\Property(
     *     property="amount",
     *     type="int",
     *     example="1.0"
     * ),
     */
    private float $amount;

    /**
     * @OA\Property(
     *     property="currency",
     *     type="string",
     *     example="usd"
     * ),
     */
    private string $currency;

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): Price
    {
        $this->amount = $amount;
        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): Price
    {
        $this->currency = $currency;
        return $this;
    }

}
