<?php

declare(strict_types=1);

namespace App\DTO\Common;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 * @OA\Xml(name="Communication"),
 * )
 */
final class Communication
{

    /**
     * @OA\Property(
     *     property="name",
     *     type="string",
     *     example="chats"
     * ),
     */
    private string $name;

    /**
     * @OA\Property(
     *     property="price",
     *     type="object",
     *     ref="#/components/schemas/Price",
     * ),
     */
    private Price $price;

    /**
     * @OA\Property(
     *     property="isEnable",
     *     type="boolean",
     *     example="true",
     * ),
     */
    private bool $isEnable = true;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Communication
    {
        $this->name = $name;
        return $this;
    }

    public function getPrice(): Price
    {
        return $this->price;
    }

    public function setPrice(Price $price): Communication
    {
        $this->price = $price;
        return $this;
    }

    public function getIsEnable(): bool
    {
        return $this->isEnable;
    }

    public function setIsEnable(bool $isEnable): Communication
    {
        $this->isEnable = $isEnable;
        return $this;
    }

}
