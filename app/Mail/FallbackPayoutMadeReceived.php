<?php

declare(strict_types=1);

namespace App\Mail;

use App\Helpers\EmailTexts;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FallbackPayoutMadeReceived extends Mailable
{

    use Queueable;
    use SerializesModels;

    private const EOL = "\r\n";

    public array $account;
    public float $amount;
    public float $commission;
    public float $donation;
    public int $userId;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $account, float $amount, float $commission, float $donation, int $userId)
    {
        $this->account = $account;
        $this->amount = $amount;
        $this->commission = $commission;
        $this->donation = $donation;
        $this->userId = $userId;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): FallbackPayoutMadeReceived
    {
        $user = User::getUserById($this->userId);
        $text = 'Payout made from:' . self::EOL;
        $text .= 'Email: ' . $user->email . self::EOL;
        $text .= 'Profile: ' . config('app.front_url') . '/@' . $user->domain . self::EOL;
        $text .= '---------------------------------------' . self::EOL;
        // phpcs:disable Squiz.Strings.DoubleQuoteUsage.NotRequired
        $text .= 'Account:' . self::EOL . implode(self::EOL, $this->account) . self::EOL;
        $text .= '---------------------------------------' . PHP_EOL;
        $text .= sprintf('Payout sum: $%s' . self::EOL, $this->amount);
        $text .= '---------------------------------------' . self::EOL;
        $text .= sprintf(
            'Amount: %s' . self::EOL . 'Commission: %s' . self::EOL . 'Donation: %s',
            $this->amount + $this->commission + $this->donation,
            $this->commission,
            $this->donation
        );

        return $this->subject(__(EmailTexts::FALLBACK_PAYOUT_MADE))
            ->text('emails.raw')->with([
                'text' => $text,
            ]);
    }

}
