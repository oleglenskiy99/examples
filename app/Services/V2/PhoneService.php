<?php

declare(strict_types=1);

namespace App\Services\V2;

use App\DTO\Request\AddPhone;
use App\DTO\Request\ConfirmPhone;
use App\Events\User\UpdateUserEvent;
use App\Exceptions\API\AccessException;
use App\Exceptions\API\MessageIdIsNotExist;
use App\Exceptions\API\PhoneAlreadyExist;
use App\Helpers\SMSTexts;
use App\Repository\AuthorizationCodesRepository;
use App\Repository\V2\UserRepository;
use App\Services\SendSMSServiceInterface;

class PhoneService
{

    private UserService $userService;
    private UserRepository $userRepository;
    private AuthorizationCodesRepository $authorizationCodesRepository;
    private SendSMSServiceInterface $serviceSMS;

    public function __construct(
        UserService $userService,
        UserRepository $userRepository,
        AuthorizationCodesRepository $authorizationCodesRepository,
        SendSMSServiceInterface $serviceSMS
    )
    {
        $this->userService = $userService;
        $this->userRepository = $userRepository;
        $this->serviceSMS = $serviceSMS;
        $this->authorizationCodesRepository = $authorizationCodesRepository;
    }

    public function addPhone(AddPhone $addPhone): array
    {
        $user = $this->userRepository->findUserByPhone($addPhone->getPhone());
        if ($user) {
            throw new PhoneAlreadyExist();
        }
        $codes = $this->authorizationCodesRepository->createCode($addPhone->getPhone());
        $message = __(SMSTexts::VERIFIED_TEXT) . ' ' . $codes->code;

        $messageID = $this->serviceSMS->send($addPhone->getPhone(), $message);
        if ($messageID === null) {
            throw new MessageIdIsNotExist();
        }
        return [
            'i' => $codes->identification,
            's' => $messageID,
            'n' => false,
        ];
    }

    public function confirmPhone(ConfirmPhone $confirmPhone): array
    {
        $user = $this->userRepository->findUserByPhone($confirmPhone->getPhone());
        if ($user) {
            throw new PhoneAlreadyExist();
        }
        $access = AuthorizationCodesRepository::getCode([
            'identification' => $confirmPhone->getI() ?? '',
            'additional' => '',
            'code' => $confirmPhone->getC(),
        ]);

        if ($access === null) {
            throw new AccessException();
        }

        $user = $this->userService->getCurrentAuthUser();
        $user->phone = $confirmPhone->getPhone();
        $user->save();

        UpdateUserEvent::dispatch($user->id);
        return [];
    }

}
