<?php

declare(strict_types=1);

namespace App\Services\V2;

use App\Models\V2\AuthorizationCode;
use App\Repository\V2\AuthCodeRepository;

class AuthCodeService
{

    private AuthCodeRepository $authCodeRepository;

    public function __construct(AuthCodeRepository $authCodeRepository)
    {
        $this->authCodeRepository = $authCodeRepository;
    }

    public function createVerificationByEmail(string $email): AuthorizationCode
    {
        $this->cleanAllByEmail($email);

		$code = (string) rand(100000, 999999);

        $authCode = new AuthorizationCode();
        $authCode->additional = $email;
        $authCode->code = $code;
        $authCode->identification = sha1(time() . md5($code));
        $authCode->save();

        return $authCode;
    }

    public function findByEmailAndCode(string $email, int $code): ?AuthorizationCode
    {
        return $this->authCodeRepository->findByEmailAndCode($email, $code);
    }

    public function cleanAllByEmail(string $email): void
    {
        $this->authCodeRepository->deleteAllByEmail($email);
    }

}
