<?php

declare(strict_types=1);

namespace App\Services\Payment;

use App\Events\Payment\BalancesUpdated;
use App\Events\Payment\PaymentMethodAdded;
use App\Models\V2\User;
use App\Models\V2\UsersCards;
use App\Repository\V2\FinancesRepository;
use Stripe\Checkout\Session;
use Stripe\Event;
use Stripe\Issuing\Card as StripeCard;
use Stripe\PaymentMethod;
use Stripe\SetupIntent;
use Stripe\StripeClient;

class StripeWebhooksService
{

    private StripeClient $client;
    private FinancesRepository $financesRepository;

    public function __construct(StripeClient $client, FinancesRepository $financesRepository)
	{
        $this->client = $client;
        $this->financesRepository = $financesRepository;
    }

    public function handleStripeEvent(string $payload): void
	{
        $event = Event::constructFrom(
            json_decode($payload, true)
        );

        switch ( $event->type) {
            case Event::SETUP_INTENT_SUCCEEDED:
                $this->setupIntentSucceeded($event->data->object);
                break;
            case Event::CHECKOUT_SESSION_COMPLETED:
                $this->paymentSessionsSucceeded($event->data->object);
                break;
        }
    }


    public function paymentSessionsSucceeded(Session $session): void
    {
        if ( $session->mode !== 'payment' && $session->status !== 'complete') {
            return;
        }

        $userId = (int) $session->client_reference_id;
        $amount = (int) round($session->amount_total / 100);

        $user = User::where('id', $userId)->firstOrFail();
        /**
         * @psalm-suppress UndefinedMagicPropertyAssignment
         * @psalm-suppress UndefinedMagicPropertyFetch;
         */
        $user->balance += $amount;
        $user->save();

        $financeObject = $this->financesRepository->mapDbToUserFinances($user);

        BalancesUpdated::dispatch($user, $financeObject);
    }

    private function setupIntentSucceeded(SetupIntent $intent): void
    {
        /**
         * @var User $user
         */
        $user = User::where([
            'stripe_customer_id' => $intent->customer,
        ])->firstOrFail();

        /**
         * @var PaymentMethod $paymentMethod
         * @psalm-suppress ImplicitToStringCast
         */
        $paymentMethod = $this->client->paymentMethods->retrieve(
            $intent->payment_method
        );

        /**
         * @var StripeCard $stripeCard;
         */
        $stripeCard = $paymentMethod->card;

        /**
         * @psalm-suppress ImplicitToStringCast
         */
        $this->client->customers->update($intent->customer, [
            'invoice_settings' => [
                'default_payment_method' => $intent->payment_method,
            ],
        ]);

        /**
         * @var UsersCards $card;
         * @psalm-suppress ImplicitToStringCast
         */
        $card = UsersCards::create([
            'user_id' => $user->id,
            'customer' => $intent->customer,
            'payment_method' => $intent->payment_method,
            'usage' => $intent->usage,
            'last4' => $stripeCard->last4,
            'brand' => $stripeCard->brand,
            'funding' => $stripeCard->funding,
            'exp_month' => $stripeCard->exp_month,
            'exp_year' => $stripeCard->exp_year,
            'default' => 1,
        ]);

        $cardObject = $this->financesRepository->mapDbToCard($card);

        PaymentMethodAdded::dispatch($cardObject, $user->id);
    }

}
