<?php

declare(strict_types=1);

namespace App\Services\Payment;

use App\User;
use Emitter;
use Illuminate\Support\Facades\Log;
use Throwable;

trait EmitterTrait
{

    protected Emitter $emitter;

    public function emit(User $user, string $event, $payload): void
    {
        try {
            $this->emitter = $this->emitter ?? app(Emitter::class);
            $this->emitter->to(sha1((string) $user->id))->emit($event, $payload);
        } catch (Throwable $exception) {
            Log::warning($exception->getMessage());
        }
    }

}
