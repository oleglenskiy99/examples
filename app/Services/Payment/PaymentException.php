<?php

declare(strict_types=1);

namespace App\Services\Payment;

use DomainException;

class PaymentException extends DomainException
{

    public const INSUFFICIENT_FUNDS = 1;
    public const CHARGE_NOT_FOUND = 2;
    public const UNEXPECTED_STATUS = 3;

}
