<?php

declare(strict_types=1);

namespace App\Services\Payment;

use App\DTO\Response\Finance\StripeCheckoutSession;
use App\Events\Payment\CardAdded;
use App\Events\Payment\CardDeleted;
use App\Events\Transaction\BalanceRefilled;
use App\Events\Transaction\ChargeCreated;
use App\Events\Transaction\ChargeUpdated;
use App\Events\Transaction\PaymentMade;
use App\Events\Transaction\PayoutMade;
use App\Jobs\DonateCharity;
use App\Models\User\Cards;
use App\Models\User\Charges;
use App\Repository\V2\FinancesRepository;
use App\Services\PaymentServiceInterface;
use App\Services\PayoutServiceInterface;
use App\User;
use Illuminate\Support\Facades\DB;
use Stripe\Account;
use Stripe\AccountLink;
use Stripe\AlipayAccount;
use Stripe\BankAccount;
use Stripe\BitcoinReceiver;
use Stripe\Card;
use Stripe\Charge;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\Payout;
use Stripe\SetupIntent;
use Stripe\Source;
use Stripe\StripeClient;

class StripePaymentService implements PaymentServiceInterface, PayoutServiceInterface
{

    use CategoryPriceTrait;

    protected StripeClient $client;
    protected float $payoutFee;
    protected FinancesRepository $financesRepository;

    public function __construct(StripeClient $client, float $payoutFee, FinancesRepository $financesRepository)
    {
        $this->client = $client;
        $this->payoutFee = $payoutFee;
        $this->financesRepository = $financesRepository;
    }


    public function createPaymentSession(int $amount): StripeCheckoutSession
    {
        return $this->financesRepository->createPaymentSession($amount);
    }

    public function createCreditCardAttachSession(): StripeCheckoutSession
    {
        return $this->financesRepository->createCreditCardAttachSession();
    }

    public function addCreditCard(User $user, string $customerToken, ?string $accountToken, string $returnUrl): SetupIntent
    {
        return DB::transaction(function () use ($user, $customerToken, $accountToken, $returnUrl) {
            $source = $this->createCustomerSource($user, $customerToken);
            $externalAccount = $accountToken ? $this->createExternalAccount($user, $accountToken) : null;
            /**
             * @psalm-suppress UndefinedDocblockClass
             */
            $intent = $this->client->setupIntents->create([
                'payment_method' => $source->id,
                'usage' => 'off_session',
                'customer' => $user->stripe_customer_id,
                'metadata' => ['payout' => $externalAccount ? $externalAccount->id : null],
            ]);
            if ($intent->status === SetupIntent::STATUS_REQUIRES_CONFIRMATION) {
                $intent = $this->client->setupIntents->confirm($intent->id, [
                    'return_url' => $returnUrl,
                ]);
            }
            if ($intent->status === SetupIntent::STATUS_SUCCEEDED) {
                $this->attachCreditCard($user, $intent);
            }
            if ($user->isDirty('stripe_customer_id', 'stripe_account_id')) {
                $user->save();
            }
            return $intent;
        });
    }

    public function checkCreditCardStatus(User $user, string $id): bool
    {
        $intent = $this->client->setupIntents->retrieve($id);
        $status = $intent->status === SetupIntent::STATUS_SUCCEEDED;
        if ($status) {
            $this->attachCreditCard($user, $intent);
        }
        return $status;
    }

    public function removeCreditCard(User $user, Cards $card): void
    {
        $card->delete();
        if ($user->stripe_customer_id && $card->payment_method) {
            $this->client->customers->deleteSource($user->stripe_customer_id, $card->payment_method);
        }
        if ($user->stripe_account_id && $card->payout_method) {
            $this->client->accounts->deleteExternalAccount($user->stripe_account_id, $card->payout_method);
        }
        CardDeleted::dispatch($card, $user->id);
    }

    public function addBankAccount(User $user, string $token): void
    {
        DB::transaction(function () use ($user, $token): void {
            $externalAccount = $this->createExternalAccount($user, $token);
            if ($externalAccount->status === BankAccount::STATUS_ERRORED) {
                throw new PaymentException('Unexpected status', PaymentException::UNEXPECTED_STATUS);
            }
            if (!($externalAccount instanceof BankAccount)) {
                return;
            }

            $card = new Cards();
            $card->user_id = $user->id;
            $card->fill([
                'customer' => $user->stripe_customer_id,
                'payment_method' => $externalAccount->id,
                'payout_method' => $externalAccount->id,
                'usage' => 'off_session',
                'last4' => $externalAccount->last4,
                'brand' => $externalAccount->bank_name,
                'funding' => 'bank_account',
                'exp_month' => 0,
                'exp_year' => 0,
                'default' => 1,
            ]);
            $card->save();
            if ($user->isDirty('stripe_account_id')) {
                $user->save();
            }
            CardAdded::dispatch($card, $user->id);
        });
    }

    public function createPayment(
        User $user,
        bool $hold,
        string $category,
        string $currency,
        float $amount,
        ?float $netAmount = null
    ): ?string
    {
        foreach ($user->cards()->where('funding', '!=', 'bank_account')->get() as $card) {
            try {
                return $this->createCardPayment($user, $card, $hold, $category, $currency, $amount, $netAmount)->id;
            } catch (PaymentException $exception) {
                continue;
            }
        }

        return null;
    }

    /**
     * @throws PaymentException
     */
    public function updateBalance(User $user, Cards $card, string $currency, int $amount, ?int $netAmount = null): void
    {
        DB::transaction(function () use ($user, $card, $currency, $amount, $netAmount): void {
            $model = $this->createCardPayment($user, $card, false, self::CATEGORY_BALANCE, $currency, $amount, $netAmount);
            if ($model->status !== self::STATUS_SUCCEEDED) {
                throw new PaymentException('Unexpected status', PaymentException::UNEXPECTED_STATUS);
            }
            $user->increment('balance', $model->amount_receivable / 100);
            BalanceRefilled::dispatch($card, $model, $model->amount_receivable / 100, $user->id);
        });
    }

    public function capturePayment(User $user, string $id): void
    {
        $parent = Charges::find($id);
        if ($parent === null) {
            throw new PaymentException('Charge not found', PaymentException::CHARGE_NOT_FOUND);
        }
        $intent = $this->client->paymentIntents->capture($parent->payment_intent);
        if ($intent->status !== PaymentIntent::STATUS_SUCCEEDED) {
            throw new PaymentException('Unexpected status', PaymentException::UNEXPECTED_STATUS);
        }
        $this->confirmPayment($user, $intent, $parent);
    }

    public function checkPaymentStatus(User $user, string $id): PaymentIntent
    {
        $parent = Charges::find($id);
        if ($parent === null) {
            throw new PaymentException('Charge not found', PaymentException::CHARGE_NOT_FOUND);
        }
        $intent = $this->client->paymentIntents->retrieve($parent->payment_intent);
        if ($intent->status === PaymentIntent::STATUS_SUCCEEDED) {
            $this->confirmPayment($user, $intent, $parent);
        }
        return $intent;
    }

    public function cancelPayment(User $user, string $id): void
    {
        DB::transaction(function () use ($user, $id): void {
            $model = Charges::find($id);
            if ($model === null || $model->payment_intent === null) {
                throw new PaymentException('Charge not found', PaymentException::CHARGE_NOT_FOUND);
            }
            $intent = $this->client->paymentIntents->cancel($model->payment_intent, [
                'cancellation_reason' => 'requested_by_customer',
            ]);
            if ($intent->status !== PaymentIntent::STATUS_CANCELED) {
                throw new PaymentException('Unexpected status', PaymentException::UNEXPECTED_STATUS);
            }
            /** @var Charge $charge */
            [$charge] = $intent->charges->data;
            $model->update([
                'amount' => $charge->amount,
                'amount_refunded' => $charge->amount_refunded,
                'refunded' => $charge->refunded,
                'status' => $charge->status,
                'receipt_url' => $charge->receipt_url,
            ]);
            ChargeUpdated::dispatch($model, $user->id);
        });
    }

    public function checkAccount(User $user, string $refreshUrl, string $returnUrl): ?AccountLink
    {
        $this->ensureAccount($user);
        $account = $this->client->accounts->retrieve($user->stripe_account_id);
        if ($account->payouts_enabled && $account->capabilities[Account::CAPABILITY_TRANSFERS] !== Account::CAPABILITY_STATUS_INACTIVE) {
            return null;
        }
        if ($user->isDirty('stripe_account_id')) {
            $user->save();
        }
        return $this->client->accountLinks->create([
            'account' => $user->stripe_account_id,
            'refresh_url' => $refreshUrl,
            'return_url' => $returnUrl,
            'type' => 'account_onboarding',
        ]);
    }


    public function createPayout(User $user, Cards $card, float $amount): void
    {
        DB::transaction(function () use ($user, $card, $amount): void {
            // Получение свежих данных о балансе
            $user->refresh();
            if ($user->withdraw < (int) $amount) {
                throw new PaymentException('Insufficient funds', PaymentException::INSUFFICIENT_FUNDS);
            }
            // Сумма на пожертвования вычисляется сразу
            // Но каждое отдельное пожертвование обрабатывается позже
            $fee = $amount * $this->payoutFee;
            $sum = $amount - $fee;
            $charity = $sum * min($user->founds()->sum('percent') / 100, 1.0);
            $sum -= $charity;
            $this->client->transfers->create([
                'amount' => $sum * 100,
                'currency' => 'usd',
                'destination' => $user->stripe_account_id,
            ]);
            $payout = $this->client->payouts->create([
                'amount' => $sum * 100,
                'currency' => 'usd',
                'method' => 'instant',
                'source_type' => 'card',
                'destination' => $card->payout_method,
            ], ['stripe_account' => $user->stripe_account_id]);
            // Устроит любой статус кроме ошибки, значит средства на пути к получателю
            $success = in_array($payout->status, [
                Payout::STATUS_PENDING,
                Payout::STATUS_IN_TRANSIT,
                Payout::STATUS_PAID,
            ]);
            // Запись о выплате создается в любом случае
            $model = Charges::create([
                'id' => $payout->id,
                'amount' => $payout->amount + ($fee * 100),
                'amount_captured' => $success ? $payout->amount + ($fee * 100) : 0,
                'amount_refunded' => $success ? 0 : $payout->amount + ($fee * 100),
                'amount_receivable' => $payout->amount,
                'captured' => $success,
                'refunded' => !$success,
                'paid' => true,
                'out' => true,
                'category' => self::CATEGORY_PAYOUT,
                'currency' => $payout->currency,
                'customer' => $user->stripe_customer_id,
                'created' => time(),
                'object' => $payout->object,
                'status' => $payout->status,
            ]);
            ChargeCreated::dispatch($model, $user->id);
            if (!$success) {
                return;
            }

            $user->decrement('withdraw', (int) $amount);
            PayoutMade::dispatch($card, $model, $sum, $fee, $charity, $user->id);
            foreach ($user->founds as $found) {
                DonateCharity::dispatch($user, $found, $amount - $fee)->afterCommit();
            }
        });
    }

    public function registerCharityPayout(User $user, $amount): void
    {
        Charges::create([
            'id' => 'ctx_' . sha1(time() . '' . $user->id),
            'amount' => $amount * 100,
            'amount_captured' => $amount * 100,
            'amount_refunded' => 0,
            'amount_receivable' => 0,
            'captured' => true,
            'category' => self::CATEGORY_CHARITY,
            'created' => time(),
            'currency' => 'usd',
            'customer' => $user->stripe_customer_id,
            'object' => 'transfer',
            'paid' => true,
            'out' => true,
            'refunded' => false,
            'status' => self::STATUS_SUCCEEDED,
        ]);
    }

    /**
     * @psalm-suppress UndefinedDocblockClass
     * @param User $user
     * @param string $token
     * @return AlipayAccount|BankAccount|BitcoinReceiver|Card|Source
     * @throws ApiErrorException
     * @psalm-suppress InvalidReturnType
     */
    protected function createCustomerSource(User $user, string $token)
    {
        $this->ensureCustomer($user);
        return $this->client->customers->createSource($user->stripe_customer_id, [
            'source' => $token,
        ]);
    }

    protected function ensureCustomer(User $user): void
    {
        if ($user->stripe_customer_id === null) {
            $user->stripe_customer_id = $this->client->customers->create()->id;
        }
    }

    /**
     * @return BankAccount|Card|null
     */
    protected function createExternalAccount(User $user, string $token)
    {
        $this->ensureAccount($user);
        return $this->client->accounts->createExternalAccount($user->stripe_account_id, [
            'external_account' => $token,
        ]);
    }

    protected function ensureAccount(User $user): void
    {
        if ($user->stripe_account_id === null) {
            $account = $this->client->accounts->create([
                'type' => Account::TYPE_CUSTOM,
                'capabilities' => [
                    Account::CAPABILITY_TRANSFERS => [
                        'requested' => true,
                    ],
                ],
            ]);
            $user->stripe_account_id = $account->id;
        }
    }

    protected function attachCreditCard(User $user, SetupIntent $intent): void
    {
        $method = $this->client->paymentMethods->retrieve($intent->payment_method);
        $this->client->paymentMethods->attach($intent->payment_method, [
            'customer' => $intent->customer,
        ]);
        $this->client->customers->update($intent->customer, [
            'invoice_settings' => [
                'default_payment_method' => $intent->payment_method,
            ],
        ]);
        $card = new Cards();
        $card->user_id = $user->id;
        $card->fill([
            'customer' => $intent->customer,
            'payment_method' => $intent->payment_method,
            'payout_method' => $intent->metadata->payout ?? null,
            'usage' => $intent->usage,
            'last4' => $method->card->last4,
            'brand' => $method->card->brand,
            'funding' => $method->card->funding,
            'exp_month' => $method->card->exp_month,
            'exp_year' => $method->card->exp_year,
            'default' => 1,
        ]);
        $card->save();
        CardAdded::dispatch($card, $user->id);
    }

    /**
     * @param User $user
     * @param Cards $card
     * @param bool $hold
     * @param string $category
     * @param string $currency
     * @param float $amount
     * @param float|null $netAmount
     * @return ?Charges
     * @throws ApiErrorException
     */
    protected function createCardPayment(
        User $user,
        Cards $card,
        bool $hold,
        string $category,
        string $currency,
        float $amount,
        ?float $netAmount = null
    ): ?Charges
    {
        $intent = $this->client->paymentIntents->create([
            'customer' => $user->stripe_customer_id,
            'amount' => $amount * 100,
            'currency' => $currency,
            'confirm' => true,
            'off_session' => true,
            'capture_method' => $hold ? 'manual' : 'automatic',
            'payment_method' => $card->payment_method,
        ]);
        /** @var Charge $charge */
        [$charge] = $intent->charges->data;

        switch ($charge->status) {
            case Charge::STATUS_SUCCEEDED:
            case Charge::STATUS_PENDING:
                $model = Charges::create([
                    'id' => $charge->id,
                    'customer' => $user->stripe_customer_id,
                    'amount' => $charge->amount,
                    'amount_captured' => $charge->amount_captured,
                    'amount_refunded' => $charge->amount_refunded,
                    'amount_receivable' => ($netAmount ?: $amount) * 100,
                    'category' => $category,
                    'currency' => $charge->currency,
                    'status' => $charge->status,
                    'object' => $charge->object,
                    'captured' => $charge->captured,
                    'refunded' => $charge->refunded,
                    'paid' => $charge->paid,
                    'out' => $hold,
                    'created' => $charge->created,
                    'payment_intent' => $charge->payment_intent,
                    'payment_method' => $charge->payment_method,
                    'receipt_url' => $charge->receipt_url,
                ]);
                ChargeCreated::dispatch($model, $user->id);
                return $model;
            case Charge::STATUS_FAILED:
                throw new PaymentException('Unexpected status', PaymentException::UNEXPECTED_STATUS);
        }

        return null;
    }

    protected function confirmPayment(User $user, PaymentIntent $intent, Charges $parent): void
    {
        DB::transaction(function () use ($user, $intent, $parent): void {
            /** @var Charge $charge */
            [$charge] = $intent->charges->data;
            // Средства переводятся в статус выплачены
            $parent->update([
                'amount' => $charge->amount,
                'amount_captured' => $charge->amount_captured,
                'captured' => $charge->captured,
                'status' => $charge->status,
                'receipt_url' => $charge->receipt_url,
            ]);
            // Создание транзации с перечислением средств получателю
            $model = Charges::create([
                'id' => 'tr_' . sha1(time() . '' . $user->id . '' . $parent->id),
                'amount' => $parent->amount_receivable,
                'amount_captured' => $parent->amount_receivable,
                'amount_refunded' => 0,
                'amount_receivable' => 0,
                'customer' => $user->stripe_customer_id,
                'category' => $parent->category,
                'currency' => $parent->currency,
                'object' => 'transfer',
                'created' => time(),
                'captured' => true,
                'refunded' => false,
                'paid' => true,
                'out' => false,
                'status' => $parent->status,
                'parent' => $parent->id,
            ]);
            // Увеличение баланса получателя
            $user->increment('withdraw', $model->amount / 100);
            ChargeCreated::dispatch($model, $user->id);
            PaymentMade::dispatch($model, $model->amount / 100, $user->id);
            // отправим событие для parent
            $userparent = User::where('stripe_customer_id', $parent->customer)->first();
            if ($userparent) {
                ChargeUpdated::dispatch($parent, $userparent->id);
            }
        });
    }

}
