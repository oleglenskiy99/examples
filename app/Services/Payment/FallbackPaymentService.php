<?php

declare(strict_types=1);

namespace App\Services\Payment;

use App\Services\PaymentServiceInterface;
use App\User;

class FallbackPaymentService implements PaymentServiceInterface
{

    use CategoryPriceTrait;

    /**
     * @var PaymentServiceInterface
     */
    protected $default;

    /**
     * @var PaymentServiceInterface
     */
    protected $fallback;

    public function __construct(PaymentServiceInterface $default, PaymentServiceInterface $fallback)
    {
        $this->default = $default;
        $this->fallback = $fallback;
    }

    /**
     * {@inheritDoc}
     *
     * @see \App\Services\PaymentServiceInterface::createPayment()
     */
    public function createPayment(
        User $user,
        bool $hold,
        string $category,
        string $currency,
        float $amount,
        ?float $netAmount = null
    ): ?string
    {
        try {
            return $this->default->createPayment($user, $hold, $category, $currency, $amount, $netAmount);
        } catch (PaymentException $exception) {
            return $this->fallback->createPayment($user, $hold, $category, $currency, $amount, $netAmount);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see \App\Services\PaymentServiceInterface::capturePayment()
     */
    public function capturePayment(User $user, string $id): void
    {
        try {
            $this->default->capturePayment($user, $id);
        } catch (PaymentException $exception) {
            $this->fallback->capturePayment($user, $id);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see \App\Services\PaymentServiceInterface::cancelPayment()
     */
    public function cancelPayment(User $user, string $id): void
    {
        try {
            $this->default->cancelPayment($user, $id);
        } catch (PaymentException $exception) {
            $this->fallback->cancelPayment($user, $id);
        }
    }

}
