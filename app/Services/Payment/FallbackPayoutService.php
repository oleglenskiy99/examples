<?php

declare(strict_types=1);

namespace App\Services\Payment;

use App\Events\Transaction\ChargeCreated;
use App\Events\Transaction\FallbackPayoutMade;
use App\Jobs\DonateCharity;
use App\Models\User\Charges;
use App\Services\PaymentServiceInterface;
use App\User;
use Illuminate\Support\Facades\DB;

class FallbackPayoutService
{

    protected float $payoutFee;

    public function __construct(float $payoutFee)
    {
        $this->payoutFee = $payoutFee;
    }

    public function createPayout(User $user, array $account, float $amount): void
    {
        DB::transaction(function () use ($user, $account, $amount): void {
            $user->refresh();
            if ($user->withdraw < $amount) {
                throw new PaymentException('Insufficient funds', PaymentException::INSUFFICIENT_FUNDS);
            }
            $fee = $amount * $this->payoutFee;
            $sum = $amount - $fee;
            $charity = $sum * min($user->founds()->sum('percent') / 100, 1.0);
            $sum -= $charity;
            $model = Charges::create([
                'id' => 'pm_' . sha1(time() . $user->id),
                'amount' => $amount * 100,
                'amount_captured' => $amount * 100,
                'amount_refunded' => 0,
                'amount_receivable' => $sum * 100,
                'captured' => true,
                'refunded' => false,
                'paid' => true,
                'out' => true,
                'category' => PaymentServiceInterface::CATEGORY_PAYOUT,
                'currency' => $account['currency'] ?? PaymentServiceInterface::CURRENCY_USD,
                'customer' => $user->stripe_customer_id,
                'created' => time(),
                'object' => 'payout',
                'status' => PaymentServiceInterface::STATUS_SUCCEEDED,
            ]);
            ChargeCreated::dispatch($model, $user->id);
            $user->decrement('withdraw', (int) $amount);
            FallbackPayoutMade::dispatch($account, $model, $sum, $fee, $charity, $user->id);
            foreach ($user->founds as $found) {
                DonateCharity::dispatch($user, $found, $amount - $fee)->afterCommit();
            }
        });
    }

}
