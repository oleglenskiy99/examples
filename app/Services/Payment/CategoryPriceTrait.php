<?php

declare(strict_types=1);

namespace App\Services\Payment;

use App\User;
use Illuminate\Support\Facades\Cache;

trait CategoryPriceTrait
{

    public function getCategoryPrice(User $user, string $category): float
    {
        return (float) Cache::remember(
            sprintf('user:%s:%s:price', $user->id, $category),
            5 * 60,
            function () use ($user, $category) {
                return $user->prices()->where(['key' => $category])->firstOr(function () {
					return (object) ['value' => 0];
                })->value;
            }
        );
    }

}
