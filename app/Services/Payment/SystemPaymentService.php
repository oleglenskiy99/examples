<?php

declare(strict_types=1);

namespace App\Services\Payment;

use App\Events\Transaction\BalanceChanged;
use App\Events\Transaction\ChargeCreated;
use App\Events\Transaction\ChargeUpdated;
use App\Events\Transaction\PaymentMade;
use App\Models\User\Charges;
use App\Services\PaymentServiceInterface;
use App\User;
use Illuminate\Support\Facades\DB;
use Log;

/**
 * Сервис обработки выплат внутри системы
 */
class SystemPaymentService implements PaymentServiceInterface
{

    use CategoryPriceTrait;

    /**
     * {@inheritDoc}
     *
     * @see \App\Services\PaymentServiceInterface::createPayment()
     */
    public function createPayment(
        User $user,
        bool $hold,
        string $category,
        string $currency,
        float $amount,
        ?float $netAmount = null
    ): string
    {
        return DB::transaction(function () use ($user, $hold, $category, $currency, $amount, $netAmount) {
            // Считывание свежих данных о балансе пользователя
            $user->refresh();

            if ($user->balance + $user->withdraw < $amount) {
                throw new PaymentException('Insufficient funds', PaymentException::INSUFFICIENT_FUNDS);
            }
            Log::error('Systempayment create' . $amount);
            // Максимум суммы выплаты берется из баланса, остаток из заработанных средств
            $balancePart = (float) min($amount, $user->balance);
            $withdrawPart = $amount - $balancePart;

            // Создание транзации
            $model = Charges::create([
                'id' => 'ch_' . sha1(time() . '' . $user->id),
                'amount' => $amount * 100,
                'amount_captured' => $hold ? 0 : $amount * 100,
                'amount_refunded' => 0,
                'amount_receivable' => ($netAmount ?: $amount) * 100,
                'customer' => $user->stripe_customer_id,
                'category' => $category,
                'currency' => $currency,
                'created' => time(),
                'object' => 'balance',
                'captured' => false,
                'refunded' => false,
                'paid' => true,
                'out' => true,
                'status' => self::STATUS_SUCCEEDED,
            ]);

            // Средства списываются со счета плательщика, при отмене транзакции они будут возвращены
            $user->balance -= $balancePart;
            $user->withdraw -= (int) $withdrawPart;
            $user->save();

            // События
            ChargeCreated::dispatch($model, $user->id);
            BalanceChanged::dispatch(-$balancePart, -$withdrawPart, $user->id);

            // Возврат id транзации, для последующего подтверждения/отмены
            return $model->id;
        });
    }

    /**
     * {@inheritDoc}
     *
     * @see \App\Services\PaymentServiceInterface::capturePayment()
     */
    public function capturePayment(User $user, string $id): void
    {
        DB::transaction(function () use ($user, $id): void {
            // Поиск транзации с заморозкой средств плательщика
            $parent = Charges::where([
                ['id', '=', $id ],
                ['object', '=', 'balance'],
                ['captured', '=', false],
                ['refunded', '=', false],
            ])->first();
            if ($parent === null) {
                Log::error('Systempayment not model capture');

                throw new PaymentException('Charge not found', PaymentException::CHARGE_NOT_FOUND);
            }
            // Средства переводятся в статус выплачены
            $parent->update([
                'amount_captured' => $parent->amount,
                'captured' => true,
                'status' => self::STATUS_SUCCEEDED,
            ]);
            // Создание транзации с перечислением средств получателю
            $model = Charges::create([
                'id' => 'tr_' . sha1(time() . '' . $user->id . '' . $parent->id),
                'amount' => $parent->amount_receivable,
                'amount_captured' => $parent->amount_receivable,
                'amount_refunded' => 0,
                'amount_receivable' => 0,
                'customer' => $user->stripe_customer_id,
                'category' => $parent->category,
                'currency' => $parent->currency,
                'object' => 'transfer',
                'created' => time(),
                'captured' => true,
                'refunded' => false,
                'paid' => true,
                'out' => false,
                'status' => self::STATUS_SUCCEEDED,
                'parent' => $parent->id,
            ]);
            // Увеличение баланса получателя
            $user->increment('withdraw', (int) $model->amount / 100);
            // События
            ChargeCreated::dispatch($model, $user->id);
            PaymentMade::dispatch($model, $model->amount / 100, $user->id);
        });
    }

    /**
     * {@inheritDoc}
     *
     * @see \App\Services\PaymentServiceInterface::cancelPayment()
     */
    public function cancelPayment(User $user, string $id): void
    {
        DB::transaction(function () use ($user, $id): void {
            // Поиск транзации с заморозкой средств плательщика
            $model = Charges::where([
                ['id', '=', $id],
                ['object', '=', 'balance'],
                ['captured', '=', false],
                ['refunded', '=', false],
            ])
                ->first();

            if ($model === null) {
                Log::error('Systempayment not model cancel');

                throw new PaymentException('Charge not found', PaymentException::CHARGE_NOT_FOUND);
            }
            // Перевод средств в статус возврата
            $model->update([
                'amount_refunded' => $model->amount,
                'refunded' => true,
                'status' => self::STATUS_REFUNDED,
            ]);

            // Увеличение баланса плательщика обратно на возвращенную сумму
            $user->increment('balance', $model->amount / 100);

            // События
            ChargeUpdated::dispatch($model, $user->id);
            BalanceChanged::dispatch($model->amount / 100, 0, $user->id);
        });
    }

}
