<?php

declare(strict_types=1);

namespace App\Services\Mobilipay;

use App\Services\SendSMSServiceInterface;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Throwable;

class MobilipayAPI implements SendSMSServiceInterface
{

    public function send(string $phone, string $message): ?string
    {
		$partnerId = config('services.mobilipay.partnerid');
		$signKey = config('services.mobilipay.signkey');
		$apipath = config('services.mobilipay.apipath');
		$messageID = null;

		try {

			$client = new Client();
			$res = $client->request('GET', $apipath, [
                'query' =>
                [
                    'msisdn' => phoneNumberValid($phone, '+'),
                    'message_text' => base64_encode($message),
                    'partner_id' => $partnerId,
                    'sign' => hash_hmac('sha256', $message, $signKey),
                    'no_need_response' => true,
				],
			]);

			$resarray = json_decode((string) $res->getBody(), true);
			if ( isset($resarray['error-code'])) {

				Log::error('SMS error ' . $phone . ' ' . $resarray['error-description”']);

			}
			if ( isset($resarray['message'])) {
				$messageID = $resarray['message']['messageID'];
			}

		} catch ( Throwable $e) {

			Log::error($e->getMessage());

		}

		return $messageID;
    }

}
