<?php

declare(strict_types=1);

namespace App\Services;

interface SendSMSServiceInterface
{

    public function send(string $phone, string $message): ?string;

}
