<?php

declare(strict_types=1);

namespace App\Services\Sendgrid;

use Illuminate\Support\Facades\Log;
use SendGrid;
use SendGrid\Mail\Mail;
use Throwable;

class SendgridAPI
{

    public static function toMail($to, $subject, $mail): void
    {
		$apikey = config('mail.sendgrid_apikey');

		$email = new Mail();
		$email->setFrom('noreply@paysenger.com', 'Paysenger');
		$email->setSubject($subject);
		$email->addTo($to);
		$email->addContent(
            'text/html',
            $mail
		);
		$sendgrid = new SendGrid($apikey);

		try {
			$sendgrid->send($email);
		} catch (Throwable $e) {
			Log::error('Caught exception: ' . $e->getMessage() . "\n");
		}
    }

}
