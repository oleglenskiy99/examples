<?php

declare(strict_types=1);

namespace App\Services\Twilio;

use App\Services\SendSMSServiceInterface;
use Illuminate\Support\Facades\Log;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class TwilioSimpleAPI implements SendSMSServiceInterface
{

    public function send(string $phone, string $message): ?string
    {
		$twilioAccountSid = config('services.twilio.accountsid');
		$twilioApiKey = config('services.twilio.apikey');
		$twilioapinumber = config('services.twilio.apinumber');
		$messageID = null;

		try {
			$twilio = new Client($twilioAccountSid, $twilioApiKey);

			$messageID = $twilio->messages->create(phoneNumberValid($phone), [
                'body' => $message,
                'from' => $twilioapinumber,
			]);

		} catch ( TwilioException $e) {

			Log::error($e->getMessage());

		}

        return $messageID->id ?? null;
    }

}
