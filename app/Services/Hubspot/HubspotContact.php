<?php

declare(strict_types=1);

namespace App\Services\Hubspot;

use App\Models\User\Info;
use App\Tokenz;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HubspotContact
{

    protected string $hapikey;

    public function __construct()
    {
        $this->hapikey = config('services.hubspot.api_key');
    }

    public function contactAPI(User $user): ?string
    {
		$info = Info::where('id', $user->id)->first();

		if (!$info) {
			Log::error('not info');
			return null;
		}

		$country = DB::table('module_countries')
            ->select('id', 'name', 'phonecode', 'sortname')
            ->where('id', $info->country)->first();
		$country = $country ? $country->name : '';

		$city = '';
		$state = '';
		if ( isset($this->access_token) && $this->access_token) {
			$current = Tokenz::select('id as session_id', 'device', 'ip', 'city_name', 'region_name', 'country_name')
                ->where('access_token', strval($this->access_token))->first();
			if ( $current) {
				$city = $current->city_name;
				$state = $current->region_name;
				if ( empty($city)) {
					$city = $current->country_name;
				}
			}
		}

		$industry_arr = $user->categories->toArray();

		$industry = '';
		foreach ($industry_arr as $item) {
			$industry .= $item['name'] . ', ';
		}

		$properties = [
            'email' => $user->email,
            'firstname' => $info->firstName,
            'lastname' => $info->lastName,
            'phone' => $user->phone,
            'country' => $country,
            'email_is_confirmed' => $user->is_email_verify,
            'industry' => $industry,
            'city' => $city,
            'state' => $state,
		];

        $apiResponse = $this->searchEmail($user->email);
        if ( $apiResponse || $user->_hsid) {
            //update user
            $id = $user->_hsid ?: $apiResponse['id'];
            $apiResponse = $this->curlapi($properties, 'PATCH', $id);
        } else {
            //create user
            $apiResponse = $this->curlapi($properties, 'POST');
        }
        if ( isset($apiResponse['id'])) {
            $user->_hsid = $apiResponse['id'];
            $user->save();

            return $apiResponse['id'];
        }

		return null;
    }

    /**
     * @param array $properties
     * @param string $method
     * @param string $id
     * @return array
     */
    private function curlapi(array $properties, string $method = 'GET', string $id = ''): array
	{
		$post_json = json_encode(['properties' => $properties]);
		$endpoint = 'https://api.hubapi.com/crm/v3/objects/contacts/?hapikey=' . $this->hapikey;

		$ch = curl_init();
		if ( $method === 'PATCH') {
			$endpoint = 'https://api.hubapi.com/crm/v3/objects/contacts/' . $id . '?hapikey=' . $this->hapikey;
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
		}
		if ( $method === 'POST') {
			curl_setopt($ch, CURLOPT_POST, true);
		}
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
		curl_setopt($ch, CURLOPT_URL, $endpoint);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);

		return json_decode($response, true);
    }

    private function searchEmail(?string $email): ?array
	{
        if ($email === null) {
            return null;
        }
		$post_json = json_encode([
            'filterGroups' => [
                [
                    'filters' => [
                        [
                            'propertyName' => 'email',
                            'operator' => 'EQ',
                            'value' => $email,
						],
					],
				],
			],
		]);
		$endpoint = 'https://api.hubapi.com/crm/v3/objects/contacts/search?hapikey=' . $this->hapikey;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
		curl_setopt($ch, CURLOPT_URL, $endpoint);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		$ret = null;
		$response = json_decode($response, true);
		if ( isset($response['results']) && is_array($response['results']) && isset($response['results'][0])) {
			$ret = $response['results'][0];
		}

		return $ret;
    }

}
