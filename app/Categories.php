<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Facades\App;

/**
 * @property int $id
 * @property string $name_en
 * @property string $name_ru
 */
class Categories extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_categories';

    /**
     * @var array<string>
     */
    protected $hidden = ['laravel_through_key', 'cover', 'name_en', 'name_ru', 'placement', 'thumb'];

    /**
     * @var array<string>
     */
    protected $fillable = [
        'name_ru',
        'name_en',
        'default',
    ];

    public function getNameAttribute(): string
    {
        /*TODO переписать, когда поменяется модель под задачу с вложенными категориями и полем локализации*/
        switch (App::getLocale()) {
            case 'en':
                return $this->name_en;
            case 'ru':
                return $this->name_ru;
            default:
                return $this->name_en;
        }
    }

    public function users(): HasManyThrough
    {
        return $this->hasManyThrough(
            '\App\User',
            '\App\Models\User\Categories',
            'category_id',
            'id',
            'id',
            'user_id'
        );
    }

    /**
     * @return array<string>
     */
    public function toArray(): array
    {
        $array = parent::toArray();

        foreach ($this->getMutatedAttributes() as $key) {
            if (!array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};
            }
        }

        return $array;
    }

    public static function builder(): Builder
    {
        $locale = App::getLocale();

        return self::select(
            'id',
            sprintf('name_%s', $locale),
            'default'
        );
    }

}
