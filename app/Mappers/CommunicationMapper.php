<?php

declare(strict_types=1);

namespace App\Mappers;

use App\DTO\Common\Communication;
use App\DTO\Common\Price;
use App\Models\V2\UsersPrices;

class CommunicationMapper
{

    /**
     * @param UsersPrices[] $prices
     * @return Communication[]
     */
    public function mapDbToCommunications(array $prices): array
    {
        $result = [];

        foreach ($prices as $price) {
            $item = new Communication();

            $priceItem = new Price();
            $priceItem
                ->setAmount((int) $price->value)
                ->setCurrency($price->ticker ?? '')
            ;

            $item
                ->setName($price->key ?? '')
                ->setPrice($priceItem)
            ;

            $result[] = $item;
        }

        return $result;
    }

}
