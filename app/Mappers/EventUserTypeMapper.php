<?php

declare(strict_types=1);

namespace App\Mappers;

use App\DTO\Response\Event\EventUserTypeItem;
use App\Models\V2\EventsUserType;
use Illuminate\Database\Eloquent\Collection;

class EventUserTypeMapper
{

    public function mapEventUserTypeListFromDb(Collection $collection): array
    {
        $events = [];

        foreach ($collection as $event) {
            $events[] = $this->mapEventUserTypeFromDb($event);
        }

        return $events;
    }

    public function mapEventUserTypeFromDb(EventsUserType $userType): EventUserTypeItem
    {
        $result = new EventUserTypeItem();
        $result->setId($userType->id);
        $result->setName($userType->name);

        return $result;
    }

}
