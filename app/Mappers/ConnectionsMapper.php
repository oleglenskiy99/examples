<?php

declare(strict_types=1);

namespace App\Mappers;

use App\DTO\Response\Connections\ConnectionItem;
use App\DTO\Response\Lists\ConnectionsList;
use App\Models\V2\Chat;
use Illuminate\Database\Eloquent\Collection;

class ConnectionsMapper
{

    private ChatMapper $chatMapper;
    private UserMapper $userMapper;

    public function __construct(ChatMapper $chatMapper, UserMapper $userMapper)
    {
        $this->chatMapper = $chatMapper;
        $this->userMapper = $userMapper;
    }

    public function mapDbToConnections(Collection $chats, bool $mapWithUser = true): ConnectionsList
    {
        $list = new ConnectionsList();
        $items = [];
        /**
         * @var Chat $chat
         */
        foreach ($chats as $chat) {
            $chatItem = $this->chatMapper->mapDbToChat($chat);
            $item = new ConnectionItem();
            $item = $item
                ->setType('chat')
                ->setLastActivity($chat->message->timestamp)
                ->setChat($chatItem);

            if ( $mapWithUser) {
                $item = $item->setUser($this->userMapper->mapUserFromBd($chat->user));
            }

            $items[] = $item;

        }

        return $list->setItems($items)->setCount($chats->count());
    }

}
