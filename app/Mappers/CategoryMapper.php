<?php

declare(strict_types=1);

namespace App\Mappers;

use App\DTO\Response\User\Category;
use App\Models\V2\Categories;

class CategoryMapper
{

    /**
     * @param  Categories[] $categories
     * @return Category[]
     */
    public function mapDbToCategories(array $categories): array
    {
        $result = [];

        foreach ($categories as $category) {
            $item = new Category();
            $item->setId($category->id)
                ->setName($category->name_en ?? '')
            ;

            $result[] = $item;
        }

        return $result;
    }

}
