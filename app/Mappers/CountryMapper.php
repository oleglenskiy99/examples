<?php

declare(strict_types=1);

namespace App\Mappers;

use App\DTO\Response\Country\CountryItem;
use App\Models\V2\Country;

class CountryMapper
{

    public function mapCountriesListFromDb(array $collection): array
    {
        $countries = [];

        foreach ($collection as $country) {
            $countries[] = $this->mapCountryFromDb($country);
        }

        return $countries;
    }

    public function mapCountryFromDb(Country $country): CountryItem
    {
        $result = new CountryItem();
        $result->setId($country->id);
        $result->setSortname($country->sortname);
        $result->setName($country->name);
        $result->setPhonecode($country->phonecode);

        return $result;
    }

}
