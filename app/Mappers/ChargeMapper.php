<?php

declare(strict_types=1);

namespace App\Mappers;

use App\DTO\Response\Charge\ChargeItem;
use App\DTO\Response\ChargeList;
use App\Models\V2\UsersCharge;
use Illuminate\Pagination\LengthAwarePaginator;

class ChargeMapper
{

    public function mapChargeListFromDb(LengthAwarePaginator $awarePaginator): ChargeList
    {
        $result = new ChargeList();
        $listItems = [];

        /** @var UsersCharge[] $items */
        $items = $awarePaginator->items();
        foreach ($items as $item) {
            $listItems[] = $this->mapChargeFromDb($item);
        }

        $result->setItems($listItems);
        $result->setPageNumber($awarePaginator->currentPage());
        $result->setCount($awarePaginator->count());
        $result->setTotalCount($awarePaginator->total());

        return $result;
    }

    public function mapChargeFromDb(UsersCharge $charge): ChargeItem
    {
        $result = new ChargeItem();
        $result->setId($charge->id);
        $result->setObject($charge->object);
        $result->setAmount($charge->amount);
        $result->setAmountCaptured($charge->amount_captured);
        $result->setAmountRefunded($charge->amount_refunded);
        $result->setCreated($charge->created);
        $result->setCurrency($charge->currency);
        $result->setCustomer($charge->customer);
        $result->setPaid($charge->paid);
        $result->setPaymentMethod($charge->payment_method);
        $result->setReceiptUrl($charge->receipt_url);
        $result->setRefunded($charge->refunded);
        $result->setStatus($charge->status);
        $result->setCaptured($charge->captured);
        $result->setPaymentIntent($charge->payment_intent);
        $result->setAmountReceivable($charge->amount_receivable);
        $result->setCategory($charge->category);
        $result->setOut($charge->out);
        $result->setParent($charge->parent);

        return $result;
    }

}
