<?php

declare(strict_types=1);

namespace App\Mappers;

use App\DTO\Response\Event\EventItem;
use App\Models\V2\Event;

class EventMapper
{

    private EventUserTypeMapper $eventUserTypeMapper;

    public function __construct(EventUserTypeMapper $eventUserTypeMapper)
    {
        $this->eventUserTypeMapper = $eventUserTypeMapper;
    }

    public function mapEventListFromDb(array $collection): array
    {
        $events = [];

        foreach ($collection as $event) {
            $events[] = $this->mapEventFromBd($event);
        }

        return $events;
    }

    public function mapEventFromBd(Event $event): EventItem
    {
        $result = new EventItem();
        $result->setId($event->id);
        $result->setImage($event->image);
        $result->setName($event->name);
        $result->setDescription($event->description);
        $result->setFullImage($event->full_image);
        $result->setFullDescription($event->full_description);
        $result->setUserTypes($this->eventUserTypeMapper->mapEventUserTypeListFromDb($event->userTypes));

        return $result;
    }

}
