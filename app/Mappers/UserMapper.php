<?php

declare(strict_types=1);

namespace App\Mappers;

use App\DTO\Response\User\OnlineStatus;
use App\DTO\Response\User\SocialNetwork;
use App\DTO\Response\User\Stat;
use App\DTO\Response\User\UserInfo;
use App\DTO\Response\User\UserItem;
use App\DTO\Response\UserList;
use App\Models\V2\User;
use App\Models\V2\UsersAbout;
use App\Models\V2\UsersContactsRequests;
use DateTimeImmutable;
use Illuminate\Pagination\LengthAwarePaginator;

class UserMapper
{

    private CommunicationMapper $communicationMapper;
    private CategoryMapper $categoryMapper;
    private EventMapper $eventMapper;
    private CountryMapper $countryMapper;

    public function __construct(
        CommunicationMapper $communicationMapper,
        CategoryMapper $categoryMapper,
        EventMapper $eventMapper,
        CountryMapper $countryMapper
    )
    {
        $this->communicationMapper = $communicationMapper;
        $this->categoryMapper = $categoryMapper;
        $this->eventMapper = $eventMapper;
        $this->countryMapper = $countryMapper;
    }

    public function mapFollowersFromDb(LengthAwarePaginator $awarePaginator): UserList
    {
        $result = new UserList();
        $subscribersResult = [];

        /** @var array<UsersContactsRequests> $subscribers */
        $subscribers = $awarePaginator->items();
        foreach ($subscribers as $subscriber) {
            $user = $subscriber->owner;
            $user->followId = true;
            $user->followingId = $subscriber->respond === 1 ? true : null;
            $subscribersResult[] = $this->mapUserFromBd($user);
        }

        $result->setItems($subscribersResult);
        $result->setPageNumber($awarePaginator->currentPage());
        $result->setCount($awarePaginator->count());
        $result->setTotalCount($awarePaginator->total());

        return $result;
    }

    public function mapFollowingsFromDb(LengthAwarePaginator $awarePaginator): UserList
    {
        $result = new UserList();
        $subscribersResult = [];

        /** @var array<UsersContactsRequests> $subscribers */
        $subscribers = $awarePaginator->items();
        foreach ($subscribers as $subscriber) {
            $user = $subscriber->subscriber;
            $user->followingId = true;
            $user->followId = $subscriber->respond === 1 ? true : null;
            $subscribersResult[] = $this->mapUserFromBd($user);
        }

        $result->setItems($subscribersResult);
        $result->setPageNumber($awarePaginator->currentPage());
        $result->setCount($awarePaginator->count());
        $result->setTotalCount($awarePaginator->total());

        return $result;
    }

    public function mapUserListFromDb(LengthAwarePaginator $awarePaginator): UserList
    {
        $result = new UserList();
        $subscribersResult = [];

        /** @var User[] $users */
        $users = $awarePaginator->items();
        foreach ($users as $user) {
            $subscribersResult[] = $this->mapUserFromBd($user);
        }

        $result->setItems($subscribersResult);
        $result->setPageNumber($awarePaginator->currentPage());
        $result->setCount($awarePaginator->count());
        $result->setTotalCount($awarePaginator->total());

        return $result;
    }

    public function mapUserFromBd(User $user): UserItem
    {
        $result = new UserItem();
        $result
            ->setUserInfo($this->mapDbToUserInfo($user))
            ->setCategories($this->categoryMapper->mapDbToCategories($user->categories->all()))
            ->setSocialNetworks($this->mapDbToSocialNetwork($user))
            ->setStats($this->mapDbToStat($user))
            ->setCommunications($this->communicationMapper->mapDbToCommunications($user->prices->all()))
            ->setIsFollow($user->followId !== null)
            ->setIsFollowing($user->followingId !== null)
            ->setCharity([])
            ->setEvents($this->eventMapper->mapEventListFromDb($user->events->all()));

        if ($user->isMe()) {
            $result
                ->setBalance((float) $user->balance)
                ->setWithdraw((float) $user->withdraw)
                ->setEmail($user->email)
                ->setPhone($user->phone);
        }

        return $result;
    }

    public function mapDbToUserInfo(User $user): UserInfo
    {
        $result = new UserInfo();
        $result->setUserId($user->id)
            ->setFirstName($user->info->firstName ?? '')
            ->setLastName($user->info->lastName ?? '')
            ->setNickname($user->domain ?? '')
            ->setAvatar($user->info->photo ?? '')
            ->setDescription($user->info->biography ?? '')
            ->setStatus($user->info ? $this->mapDbOnlineStatus($user->info) : new OnlineStatus())
            ->setCountry($user->isMe() && $user->info->gcountry ? $this->countryMapper->mapCountryFromDb($user->info->gcountry) : null);

        return $result;
    }

    private function mapDbToStat(User $user): Stat
    {
        $result = new Stat();
        $result
            ->setRating((float) $user->getRating() ? round((float) $user->getRating(), 1) : 0)
            ->setFollowers($user->getFollowersCount())
            ->setFollowing($user->getFollowingsCount())
            ->setResponses($user->getResponsesCount())
        ;

        return $result;
    }

    /**
     * @return array<SocialNetwork>
     */
    private function mapDbToSocialNetwork(User $user): array
    {
        $result = [];

        foreach ($user->linkeds as $linked) {
            $item = new SocialNetwork();
            $item
                ->setNickname($linked->username ?? '')
                ->setType($linked->network->name ?? '')
            ;

            $result[] = $item;
        }

        return $result;
    }

    private function mapDbOnlineStatus(UsersAbout $userAbout): OnlineStatus
    {
        $status = new OnlineStatus();
        $status
            ->setIsOnline($userAbout->is_online ?? false)
            ->setLastOnline(
                $userAbout->last_activity ?
                    new DateTimeImmutable((string) $userAbout->last_activity) :
                    null
            )
        ;

        return $status;
    }

}
