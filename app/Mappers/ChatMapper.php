<?php

declare(strict_types=1);

namespace App\Mappers;

use App\DTO\Response\Chats\ChatHistory;
use App\DTO\Response\Chats\ChatItem;
use App\DTO\Response\Chats\ChatMessageItem;
use App\DTO\Response\Chats\ChatStat;
use App\Models\V2\Chat;
use App\Models\V2\ChatMessages;
use App\Services\V2\UserService;
use Illuminate\Pagination\LengthAwarePaginator;

class ChatMapper
{

    private UserService $userService;
    private MediaMapper $mediaMapper;

    public function __construct(UserService $userService, MediaMapper $mediaMapper)
	{
        $this->userService = $userService;
        $this->mediaMapper = $mediaMapper;
    }

    public function mapDbToChat(Chat $chat): ChatItem
    {
        $item = new ChatItem();

        /**
         * @psalm-suppress UndefinedMagicPropertyFetch
         */
        $item = $item->setId($chat->id)
            ->setTitle($chat->title)
            ->setExpertId($chat->expert_id);

        if ($chat->message) {
            $item = $item->setMessage($this->mapDbToChatMessage($chat->message));
        }

        if ($chat->memberInfo) {
            $item = $item->setStats($this->mapDbToChatStat($chat));
        }

        return $item;
    }


    public function mapDbToChatHistory(LengthAwarePaginator $messages): ChatHistory
    {
        $chatHistory = new ChatHistory();

        $items = [];

        foreach ($messages->items() as $message) {
            $items[] = $this->mapDbToChatMessage($message);
        }

        return $chatHistory
            ->setItems($items)
            ->setCurrentPage($messages->currentPage())
            ->setLastPage($messages->lastPage());
    }

    public function mapDbToChatMessage(ChatMessages $message): ChatMessageItem
    {
        $item = new ChatMessageItem();
        $user = $this->userService->getCurrentAuthUser();

        $item = $item
            ->setId($message->id)
            ->setOut($user->id === $message->user_id)
            ->setUserId($message->user_id)
            ->setCost((int) $message->cost)
            ->setText($message->message)
            ->setTimestamp($message->timestamp)
            ->setPaid($message->is_paid)
            ->setEdit($message->is_edit ?? false)
            ->setRead($message->is_read);

        if ( isset($message->replyMessage)) {
            $item->setReply(
                $this->mapDbToChatMessage($message->replyMessage)
            );
        }

        if ( isset($message->attachments) && $message->attachments->count() > 0) {
            $item->setAttachments(
                $this->mediaMapper->mapMediaFromDb($message->attachments)
            );
        }

        return $item;
    }

    public function mapDbToChatStat(Chat $chat): ChatStat
    {
        $stat = new ChatStat();
        $user = $this->userService->getCurrentAuthUser();

        return $stat
            ->setTotalSpent(
                $chat->totalSpent($user->id)
            )
            ->setTotalReceived(
                $chat->totalReceived($user->id)
            )
            ->setUnreadCount($chat->memberInfo->unread_messages_count)
            ->setBalance($chat->memberInfo->balance_actual);
    }

}
