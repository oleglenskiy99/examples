<?php

declare(strict_types=1);

namespace App\Mappers;

use App\DTO\Response\Objects\Media as Medias;
use App\DTO\Response\Objects\MediaUrls;
use App\Models\V2\Media;
use Illuminate\Database\Eloquent\Collection;

class MediaMapper
{

    public function mapUploadedMediaFromDb(Media $media): array
    {
        return [
            'id' => $media->id,
            'thumb' => $media->formattedThumbUrl,
            'width' => $media->width,
            'height' => $media->height,
            'duration' => $media->duration,
            'size' => $media->size,
            'type' => $media->type,
            'url' => $media->formattedUrl,
            'fileName' => $media->fileName,
            'extension' => $media->extension,
        ];
    }

    public function mapMediaFromDb(Collection $medias): array
    {
        $items = [];
        /**
         * @var Media $media
         */
        foreach ($medias as $media) {
            $items[] = $this->mapDbToMediaObject($media);
        }

        return $items;
    }

    public function mapDbToMediaObject(Media $media): Medias
    {
        $item = new Medias();
        return  $item
            ->setId($media->id)
            ->setHeight($media->height)
            ->setWidth($media->width)
            ->setType($media->type)
            ->setUrls($this->mapDbToMediaUrls($media));
    }

    private function mapDbToMediaUrls(Media $media): MediaUrls
    {
        $mediaUrls = new MediaUrls();

        return $mediaUrls
            ->setOriginal($media->getFormattedUrlAttribute())
            ->setThumb($media->getFormattedThumbUrlAttribute())
            ->setPlaceholder($media->getFormattedPlaceholderUrlAttribute());
    }

}
