<?php

declare(strict_types=1);

namespace App\Enums;

class MessagesTypesEnum
{

    public const MESSAGE = 'message';
    public const AUDIO = 'audio';
    public const STICKER = 'sticker';
    public const PROFILE = 'profile';

}
