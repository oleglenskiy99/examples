<?php

declare(strict_types=1);

namespace App\Enums;

class RequestsSectionsEnum
{

    public const INCOMING = 1;
    public const OUTGOING = 2;

}
