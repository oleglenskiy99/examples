<?php

declare(strict_types=1);

namespace App\Enums;

class SocketEventsEnum
{

    public const CREDIT_CARD_ADD = 'credit:card:add';
    public const USER_BALANCES_UPDATED = 'user:balances:updated';
    public const CREDIT_CARD_REMOVE = 'credit:card:remove';
    public const CHARGE_CREATED = 'charge:created';
    public const CHARGE_UPDATED = 'charge:updated';
    public const USER_BALANCE_INCREMENT = 'user:balance:increment';
    public const USER_WITHDRAW_INCREMENT = 'user:withdraw:increment';
    public const USER_WITHDRAW_DECREMENT = 'user:withdraw:decrement';

}
