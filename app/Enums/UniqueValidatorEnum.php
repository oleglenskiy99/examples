<?php

declare(strict_types=1);

namespace App\Enums;

class UniqueValidatorEnum
{

    public const EMAIL = 'email';
    public const NICKNAME = 'nickname';

}
