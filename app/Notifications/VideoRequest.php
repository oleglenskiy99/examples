<?php

declare(strict_types=1);

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class VideoRequest extends Notification
{

    use Queueable;

    /**
     * @return array<string>
     */
    public function via(): array
	{
        return ['database'];
    }

    /**
     * @return array<string>
     */
    public function toEntity(): array
	{
        return [];
    }

    public function toDatabase(string $notifiable): string
	{
        return $notifiable;
    }

    /**
     * @param array<string> $notifiable
     * @return array<string>
     */
    public function toArray(array $notifiable): array
	{
        return $notifiable;
    }

}
