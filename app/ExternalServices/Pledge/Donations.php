<?php

declare(strict_types=1);

namespace App\ExternalServices\Pledge;

use App\ExternalServices\Pledge\Resources\Donation;
use App\User;

class Donations extends PledgeClient
{

    public function create(User $user, int $organizationId, string $amount): Donation
	{
        return new Donation($this->request('POST', '/donations', [
            'email' => 'george@paysenger.me',
            'first_name' => $user->info->firstName,
            'last_name' => $user->info->lastName,
            'organization_id' => $organizationId,
            'amount' => $amount,
        ]));
    }

}
