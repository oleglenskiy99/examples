<?php

declare(strict_types=1);

namespace App\ExternalServices\Pledge;

use App\ExternalServices\Pledge\Resources\Organization;
use App\ExternalServices\Pledge\Responses\OrganizationsResponse;

class Organizations extends PledgeClient
{

    /**
     * @param array $params
     * @return OrganizationsResponse
     */
    public function list(array $params = []): OrganizationsResponse
    {
        return new OrganizationsResponse($this->request('GET', '/organizations', $params));
    }

    public function retrieve(int $id): Organization
    {
        return new Organization($this->request(
            'GET',
            sprintf('/organizations/%s', $id)
        ));
    }

}
