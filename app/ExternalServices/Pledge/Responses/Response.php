<?php

declare(strict_types=1);

namespace App\ExternalServices\Pledge\Responses;

class Response
{

    public string $page;
    public string $per;
    public string $next;
    public string $previous;
    public string $totalCount;

    /**
     * @var array
     */
    public array $results = [];

    /**
     * @var array
     */
    public array $raw;

    /**
     * @var array
     */
    public array $hidden = ['next', 'per', 'raw'];

    // phpcs:disable SlevomatCodingStandard.Variables.UnusedVariable.UnusedVariable
    public function __construct(array $object)
    {
        $this->raw = $object;
        $vars = get_class_vars(static::class);
        foreach ($vars as $name => $value) {
            if (array_key_exists($name, $object)) {
                $this->$name = $object[$name];
            }
        }
    }

    /**
     * @return array
     */
    public function get(string $name): ?array
    {
        $vars = $this->toArray();
        if (array_key_exists($name, $vars)) {
            return $vars[$name];
        }
        return null;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $arr = (array) $this;

        unset($arr['hidden']);

        foreach ($arr as $key => $value) {
            if (in_array($key, $this->hidden)) {
                unset($arr[$key]);
            }
        }

        foreach ($arr['results'] as $key => $value) {
            $arr['results'][$key] = $value->toArray();
        }

        return $arr;
    }

    public function set(string $name, string $value): void
    {
        $vars = $this->toArray();
        if (array_key_exists($name, $vars)) {
            $this->$name = $value;
        }
    }

    public function toJSON(): string
    {
        return json_encode($this->toArray());
    }

}
