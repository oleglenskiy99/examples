<?php

declare(strict_types=1);

namespace App\ExternalServices\Pledge\Responses;

use App\ExternalServices\Pledge\Resources\Organization;

class OrganizationsResponse extends Response
{

    public function __construct($object)
	{
        parent::__construct($object);

        foreach ($this->results as $key => $value) {
            $this->results[$key] = new Organization($value);
        }
    }

}
