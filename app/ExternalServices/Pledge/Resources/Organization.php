<?php

declare(strict_types=1);

namespace App\ExternalServices\Pledge\Resources;

class Organization extends Resource
{

    public int $id;
    public string $name;
    public string $alias;
    public int $ngoId;
    public int $mission;
    public string $stree1;
    public string $stree2;
    public string $city;
    public string $region;
    public string $postalCode;
    public string $country;
    public string $lat;
    public string $lon;
    public string $cause;
    public string $websiteUrl;
    public string $profileUrl;
    public string $logoUrl;
    public string $disbursementType;

}
