<?php

declare(strict_types=1);

namespace App\ExternalServices\Pledge\Resources;

class Donation extends Resource
{

    protected int $id;
    protected int $userId;
    protected string $email;
    protected string $firstName;
    protected string $lastName;
    protected string $organizationId;
    protected string $organizationName;
    protected string $amount;
    protected string $coverFees;
    protected string $phoneNumber;
    protected string $status;
    protected string $externalId;
    protected string $metadata;
    protected string $createdAt;
    protected string $updatedAt;

}
