<?php

declare(strict_types=1);

namespace App\ExternalServices\Pledge\Resources;

class Resource
{

    /**
     * @var array
     */
    public array $hidden = [];

    // phpcs:disable SlevomatCodingStandard.Variables.UnusedVariable.UnusedVariable
    public function __construct($object)
    {
        $vars = get_class_vars(static::class);
        foreach ($vars as $name => $value) {
            if (array_key_exists($name, $object)) {
                $this->$name = $object[$name];
            }
        }
    }

    /**
     * @return array|null
     */
    public function get(string $name): ?array
    {
        $vars = $this->toArray();
        if (array_key_exists($name, $vars)) {
            return $vars[$name];
        }
        return null;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $arr = (array) $this;

        foreach ($arr as $key => $value) {
            if (in_array($key, $this->hidden)) {
                unset($arr[$key]);
            }
        }

        unset($arr['hidden']);

        return $arr;
    }

    public function set($name, $value): void
    {
        $vars = $this->toArray();
        if (array_key_exists($name, $vars)) {
            $this->$name = $value;
        }
    }

    public function toJSON(): string
    {
        return json_encode($this->toArray());
    }

}
