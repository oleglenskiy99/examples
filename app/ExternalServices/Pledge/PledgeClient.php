<?php

declare(strict_types=1);

namespace App\ExternalServices\Pledge;

use GuzzleHttp\Client;

class PledgeClient
{

    private string $url = 'https://api.pledge.to/v1';
    private string $pk = 'cf672bef4b3a6bae660a11070b834111';
    private Client $http;

    public function __construct()
    {
        $this->http = new Client();
    }

    public static function init(): PledgeClient
    {
        return new PledgeClient();
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param array $params
     * @return string
     */
    public function request(string $method, string $endpoint, array $params = []): string
    {
        $request = [
            'form_params' => [],
            'query' => [],
            'headers' => $this->headers(),
        ];

        switch ($method) {
            case 'POST':
                $request['form_params'] = $params;
                break;
            case 'GET':
                $request['query'] = $params;
                break;
        }

        $response = $this->http->request($method, $this->url . $endpoint, $request);

        return json_decode($response->getBody()->getContents(), true);
    }

    public static function donations(): Donations
    {
        return new Donations();
    }

    public static function organizations(): Organizations
    {
        return new Organizations();
    }

    /** @psalm-suppress UndefinedClass * */
    public static function causes(): Causes
    {
        return new Causes();
    }

    /**
     * @return array
     */
    private function headers(): array
    {
        return [
            'Authorization' => sprintf('Bearer %s', $this->pk),
        ];
    }

}
