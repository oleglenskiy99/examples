<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_countries';

    /**
     * @var array<string>
     */
    protected $fillable = [
        'sortname',
        'name',
        'phonecode',
    ];

}
