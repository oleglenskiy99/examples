<?php

declare(strict_types=1);

namespace App\ReceiptValidator\iTunes;

use App\ReceiptValidator\RunTimeException;
use ArrayAccess;
use Carbon\Carbon;

class PendingRenewalInfo implements ArrayAccess
{

    /*!
     * Developer friendly field codes
     * @link https://developer.apple.com/library/content/releasenotes/General/ValidateAppStoreReceipt/Chapters/ReceiptFields.html
     */

    public const EXPIRATION_INTENT_CANCELLED = 1;
    public const EXPIRATION_INTENT_BILLING_ERROR = 2;
    public const EXPIRATION_INTENT_INCREASE_DECLINED = 3;
    public const EXPIRATION_INTENT_PRODUCT_UNAVAILABLE = 4;
    public const EXPIRATION_INTENT_UNKNOWN = 5;
    public const RETRY_PERIOD_ACTIVE = 1;
    public const RETRY_PERIOD_INACTIVE = 0;
    public const AUTO_RENEW_ACTIVE = 1;
    public const AUTO_RENEW_INACTIVE = 0;

    public const STATUS_ACTIVE = 'active';
    public const STATUS_PENDING = 'pending';
    public const STATUS_EXPIRED = 'expired';

    protected string $productId = '';
    protected string $autoRenewProductId = '';
    protected string $originalTransactionId = '';
    protected bool $autoRenewStatus = false;
    protected ?int $expirationIntent;
    protected Carbon $gracePeriodExpiresDate;
    protected ?int $isInBillingRetryPeriod;

    /**
     * @var array<string>|null
     */
    protected ?array $rawData = null;

    /**
     * @param array<string>|null $data
     * @throws RunTimeException
     */
    public function __construct(?array $data = null)
    {
        $this->rawData = $data;
        $this->parseData();
    }

    /**
     * @throws RunTimeException
     */
    // phpcs:disable Squiz.Commenting.FunctionComment.MissingParamName
    public function parseData(): PendingRenewalInfo
    {
        if (!is_array($this->rawData)) {
            throw new RunTimeException('Response must be a scalar value');
        }

        $this->productId = $this->rawData['product_id'];
        $this->originalTransactionId = $this->rawData['original_transaction_id'];
        $this->autoRenewProductId = $this->rawData['auto_renew_product_id'];
        $this->autoRenewStatus = (bool) $this->rawData['auto_renew_status'];

        if (array_key_exists('expiration_intent', $this->rawData)) {
            $this->expirationIntent = (int) $this->rawData['expiration_intent'];
        }

        if (array_key_exists('grace_period_expires_date_ms', $this->rawData)) {
            $this->gracePeriodExpiresDate = Carbon::createFromTimestampUTC(
                (int) round((int) $this->rawData['grace_period_expires_date_ms'] / 1000)
            );
        }

        if (array_key_exists('is_in_billing_retry_period', $this->rawData)) {
            $this->isInBillingRetryPeriod = (int) $this->rawData['is_in_billing_retry_period'];
        }

        return $this;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getAutoRenewProductId(): string
    {
        return $this->autoRenewProductId;
    }

    public function getAutoRenewStatus(): bool
    {
        return $this->autoRenewStatus;
    }

    public function getOriginalTransactionId(): string
    {
        return $this->originalTransactionId;
    }

    public function getExpirationIntent(): ?int
    {
        return $this->expirationIntent;
    }

    public function isInBillingRetryPeriod(): ?int
    {
        return $this->isInBillingRetryPeriod;
    }

    public function getStatus(): ?string
    {
        // Active when no expiration intent
        if ($this->expirationIntent === null) {
            return $this::STATUS_ACTIVE;
        }

        // Pending when retrying
        if ($this::RETRY_PERIOD_ACTIVE === $this->isInBillingRetryPeriod) {
            return $this::STATUS_PENDING;
        }

        // Expired when not retrying
        if ($this::RETRY_PERIOD_INACTIVE === $this->isInBillingRetryPeriod) {
            return $this::STATUS_EXPIRED;
        }

        return null;
    }

    public function getGracePeriodExpiresDate(): ?Carbon
    {
        return $this->gracePeriodExpiresDate;
    }

    public function isInGracePeriod(): bool
    {
        return $this->isInBillingRetryPeriod === self::RETRY_PERIOD_ACTIVE &&
            $this->gracePeriodExpiresDate->getTimestamp() > time();
    }

    public function offsetSet($key, $value): void
    {
        $this->rawData[$key] = $value;
        $this->parseData();
    }

    // phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint
    public function offsetGet($key)
    {
        return $this->rawData[$key];
    }

    public function offsetUnset($key): void
    {
        unset($this->rawData[$key]);
    }

    public function offsetExists($key): bool
    {
        return isset($this->rawData[$key]);
    }

}
