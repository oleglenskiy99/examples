<?php

declare(strict_types=1);

namespace App\ReceiptValidator\iTunes;

interface EnvironmentResponseInterface
{

    public function isSandbox(): bool;

    public function isProduction(): bool;

}
