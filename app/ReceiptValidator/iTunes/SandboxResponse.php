<?php

declare(strict_types=1);

namespace App\ReceiptValidator\iTunes;

class SandboxResponse extends AbstractResponse implements ResponseInterface
{

    public function isProduction(): bool
    {
        return false;
    }

    public function isSandbox(): bool
    {
        return true;
    }

}
