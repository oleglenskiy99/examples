<?php

declare(strict_types=1);

namespace App\ReceiptValidator\iTunes;

use App\ReceiptValidator\RunTimeException;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use InvalidArgumentException;

class Validator
{

    public const ENDPOINT_SANDBOX = 'https://sandbox.itunes.apple.com';
    public const ENDPOINT_PRODUCTION = 'https://buy.itunes.apple.com';

    protected string $endpoint;
    protected bool $excludeOldTransactions = false;
    protected string $receiptData;
    protected ?string $sharedSecret;
    protected ?HttpClient $client;

    /**
     * @var array<string>
     */
    protected array $requestOptions = [];

    public function __construct(string $endpoint = self::ENDPOINT_PRODUCTION)
    {
        $this->setEndpoint($endpoint);
    }

    public function getSharedSecret(): ?string
    {
        return $this->sharedSecret;
    }

    public function setSharedSecret(?string $sharedSecret = null): Validator
    {
        $this->sharedSecret = $sharedSecret;

        return $this;
    }

    public function getEndpoint(): string
    {
        return $this->endpoint;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function setEndpoint(string $endpoint): Validator
    {
        if ($endpoint !== self::ENDPOINT_PRODUCTION && $endpoint !== self::ENDPOINT_SANDBOX) {
            throw new InvalidArgumentException(sprintf('Invalid endpoint %s', $endpoint));
        }
        $this->endpoint = $endpoint;

        return $this;
    }

    /**
     * Get Client Request Options.
     *
     * @return array<string>
     */
    public function getRequestOptions(): array
    {
        return $this->requestOptions;
    }

    /**
     * @param array $requestOptions
     * @return $this
     */
    public function setRequestOptions(array $requestOptions): Validator
    {
        $this->requestOptions = $requestOptions;

        return $this;
    }

    /**
     * @param string|null $receipt_data
     * @param string|null $shared_secret
     * @return ResponseInterface
     * @throws GuzzleException
     * @throws RunTimeException
     */
    public function validate(?string $receipt_data = null, ?string $shared_secret = null): ResponseInterface
    {
        if ($receipt_data !== null) {
            $this->setReceiptData($receipt_data);
        }

        if ($shared_secret !== null) {
            $this->setSharedSecret($shared_secret);
        }

        $client = $this->getClient();

        return $this->sendRequestUsingClient($client);
    }

    public function getReceiptData(): ?string
    {
        return $this->receiptData;
    }

    public function setReceiptData(?string $receiptData = null): Validator
    {
        $this->receiptData = strpos($receiptData, '{') !== false ? base64_encode($receiptData) : $receiptData;

        return $this;
    }

    public function getExcludeOldTransactions(): bool
    {
        return $this->excludeOldTransactions;
    }

    public function setExcludeOldTransactions(bool $exclude): Validator
    {
        $this->excludeOldTransactions = $exclude;

        return $this;
    }

    /**
     * Returns the Guzzle client.
     *
     * @return HttpClient
     */
    protected function getClient(): HttpClient
    {
        if ($this->client === null) {
            $this->client = new HttpClient($this->getClientConfig());
        }

        return $this->client;
    }

    /**
     * @return array<string>
     */
    protected function getClientConfig(): array
    {
        $base_uri = ['base_uri' => $this->endpoint];

        return array_merge($this->requestOptions, $base_uri);
    }

    /**
     * Prepare request data (json).
     *
     * @return string
     */
    protected function prepareRequestData(): string
    {
        $request = [
            'receipt-data' => $this->getReceiptData(),
            'exclude-old-transactions' => $this->getExcludeOldTransactions(),
        ];

        if ($this->sharedSecret !== null) {
            $request['password'] = $this->sharedSecret;
        }

        return json_encode($request);
    }

    /**
     * @param HttpClient $client
     * @return ProductionResponse|SandboxResponse
     * @throws GuzzleException
     * @throws RunTimeException
     */
    private function sendRequestUsingClient(HttpClient $client)
    {
        $baseUri = (string) $client->getConfig('base_uri');

        $httpResponse = $client->request('POST', '/verifyReceipt', ['body' => $this->prepareRequestData()]);

        if ($httpResponse->getStatusCode() !== 200) {
            throw new RunTimeException('Unable to get response from itunes server');
        }

        $decodedBody = json_decode((string) $httpResponse->getBody(), true);

        if ($baseUri === self::ENDPOINT_PRODUCTION) {
            $response = new ProductionResponse($decodedBody);

            // on a 21007 error, retry the request in the sandbox environment
            // these are receipts from the Apple review team
            if ($response->getResultCode() === ResponseInterface::RESULT_SANDBOX_RECEIPT_SENT_TO_PRODUCTION) {
                $config = array_merge($this->getClientConfig(), ['base_uri' => self::ENDPOINT_SANDBOX]);
                $client = new HttpClient($config);

                return $this->sendRequestUsingClient($client);
            }
        } else {
            $response = new SandboxResponse($decodedBody);
        }

        return $response;
    }

}
