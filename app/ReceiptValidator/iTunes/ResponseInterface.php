<?php

declare(strict_types=1);

namespace App\ReceiptValidator\iTunes;

use Carbon\Carbon;

interface ResponseInterface extends EnvironmentResponseInterface
{

    public const RESULT_OK = 0;
    public const RESULT_APPSTORE_CANNOT_READ = 21000;
    public const RESULT_DATA_MALFORMED = 21002;
    public const RESULT_RECEIPT_NOT_AUTHENTICATED = 21003;
    public const RESULT_SHARED_SECRET_NOT_MATCH = 21004;
    public const RESULT_RECEIPT_SERVER_UNAVAILABLE = 21005;
    public const RESULT_RECEIPT_VALID_BUT_SUB_EXPIRED = 21006;
    public const RESULT_SANDBOX_RECEIPT_SENT_TO_PRODUCTION = 21007;
    public const RESULT_PRODUCTION_RECEIPT_SENT_TO_SANDBOX = 21008;
    public const RESULT_RECEIPT_WITHOUT_PURCHASE = 21010;

    public function getResultCode(): int;

    public function setResultCode(int $code): void;

    /**
     * @return array
     */
    public function getPurchases(): array;

    /**
     * @return array
     */
    public function getReceipt(): array;

    /**
     * @return array
     */
    public function getLatestReceiptInfo(): array;

    public function getLatestReceipt(): ?string;

    public function getBundleId(): int;

    public function getAppItemId(): string;

    public function getOriginalPurchaseDate(): ?Carbon;

    public function getRequestDate(): ?Carbon;

    public function getReceiptCreationDate(): ?Carbon;

    /**
     * @return array
     */
    public function getPendingRenewalInfo(): array;

    // phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingNativeTypeHint
    public function getRawData();

    public function isValid(): bool;

    public function isRetryable(): bool;

    // phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingNativeTypeHint
    public function parseData();

}
