<?php

declare(strict_types=1);

namespace App\ReceiptValidator\iTunes;

use ArrayAccess;
use Carbon\Carbon;
use ReceiptValidator\RunTimeException;

class PurchaseItem implements ArrayAccess
{

    protected int $quantity = 0;
    protected string $productId;
    protected string $webOrderLineItemId;
    protected string $transactionId;
    protected string $originalTransactionId;
    protected Carbon $purchaseDate;
    protected Carbon $originalPurchaseDate;
    protected Carbon $expiresDate;
    protected ?Carbon $cancellationDate;
    protected ?bool $isTrialPeriod;
    protected ?bool $isInIntroOfferPeriod;
    protected ?string $promotionalOfferId;

    /**
     * @var array<string>|null
     */
    protected ?array $rawData;

    /**
     * @param array<string>|null $data
     * @throws RunTimeException
     */
    public function __construct(?array $data = null)
    {
        $this->rawData = $data;
        $this->parseData();
    }

    /**
     * @throws RunTimeException
     */
    // phpcs:disable SlevomatCodingStandard.Namespaces.UnusedUses.MismatchingCaseSensitivity
    public function parseData(): PurchaseItem
    {
        if (!is_array($this->rawData)) {
            throw new RuntimeException('Response must be an array');
        }

        if (array_key_exists('quantity', $this->rawData)) {
            $this->quantity = (int) $this->rawData['quantity'];
        }

        if (array_key_exists('transaction_id', $this->rawData)) {
            $this->transactionId = $this->rawData['transaction_id'];
        }

        if (array_key_exists('original_transaction_id', $this->rawData)) {
            $this->originalTransactionId = $this->rawData['original_transaction_id'];
        }

        if (array_key_exists('product_id', $this->rawData)) {
            $this->productId = $this->rawData['product_id'];
        }

        if (array_key_exists('web_order_line_item_id', $this->rawData)) {
            $this->webOrderLineItemId = $this->rawData['web_order_line_item_id'];
        }

        if (array_key_exists('promotional_offer_id', $this->rawData)) {
            $this->promotionalOfferId = $this->rawData['promotional_offer_id'];
        }

        if (array_key_exists('is_trial_period', $this->rawData)) {
            $this->isTrialPeriod = filter_var($this->rawData['is_trial_period'], FILTER_VALIDATE_BOOLEAN);
        }

        if (array_key_exists('is_in_intro_offer_period', $this->rawData)) {
            $this->isInIntroOfferPeriod = filter_var(
                $this->rawData['is_in_intro_offer_period'],
                FILTER_VALIDATE_BOOLEAN
            );
        }

        if (array_key_exists('purchase_date_ms', $this->rawData)) {
            $this->purchaseDate = Carbon::createFromTimestampUTC(
                (int) round((int) $this->rawData['purchase_date_ms'] / 1000)
            );
        }

        if (array_key_exists('original_purchase_date_ms', $this->rawData)) {
            $this->originalPurchaseDate = Carbon::createFromTimestampUTC(
                (int) round((int) $this->rawData['original_purchase_date_ms'] / 1000)
            );
        }

        if (array_key_exists('expires_date_ms', $this->rawData)) {
            $this->expiresDate = Carbon::createFromTimestampUTC((int) round((int) $this->rawData['expires_date_ms'] / 1000));
        } elseif (array_key_exists('expires_date', $this->rawData) && is_numeric($this->rawData['expires_date'])) {
            $this->expiresDate = Carbon::createFromTimestampUTC(
                (int) round((int) $this->rawData['expires_date'] / 1000)
            );
        }

        if (array_key_exists('cancellation_date_ms', $this->rawData)) {
            $this->cancellationDate = Carbon::createFromTimestampUTC(
                (int) round((int) $this->rawData['cancellation_date_ms'] / 1000)
            );
        }

        return $this;
    }

    /**
     * @return array<string>|null
     */
    public function getRawResponse(): ?array
    {
        return $this->rawData;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function isTrialPeriod(): ?bool
    {
        return $this->isTrialPeriod;
    }

    public function isInIntroOfferPeriod(): ?bool
    {
        return $this->isInIntroOfferPeriod;
    }

    public function getPromotionalOfferId(): ?string
    {
        return $this->promotionalOfferId;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getWebOrderLineItemId(): ?string
    {
        return $this->webOrderLineItemId;
    }

    public function getTransactionId(): string
    {
        return $this->transactionId;
    }

    public function getOriginalTransactionId(): string
    {
        return $this->originalTransactionId;
    }

    public function getPurchaseDate(): ?Carbon
    {
        return $this->purchaseDate;
    }

    public function getOriginalPurchaseDate(): ?Carbon
    {
        return $this->originalPurchaseDate;
    }

    public function getExpiresDate(): ?Carbon
    {
        return $this->expiresDate;
    }

    public function getCancellationDate(): ?Carbon
    {
        return $this->cancellationDate;
    }

    /**
     * @throws RunTimeException
     */
    public function offsetSet($key, $value): void
    {
        $this->rawData[$key] = $value;
        $this->parseData();
    }

    // phpcs:disable Squiz.Commenting.FunctionComment.MissingParamName
    // phpcs:disable SlevomatCodingStandard.TypeHints.DisallowMixedTypeHint.DisallowedMixedTypeHint
    // phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint
    public function offsetGet($key)
    {
        return $this->rawData[$key];
    }

    public function offsetUnset($key): void
    {
        unset($this->rawData[$key]);
    }


    public function offsetExists($key): bool
    {
        return isset($this->rawData[$key]);
    }

}
