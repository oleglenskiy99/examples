<?php

declare(strict_types=1);

namespace App\ReceiptValidator\iTunes;

class ProductionResponse extends AbstractResponse implements ResponseInterface
{

    public function isProduction(): bool
    {
        return true;
    }

    public function isSandbox(): bool
    {
        return false;
    }

}
