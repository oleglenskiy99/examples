<?php

declare(strict_types=1);

namespace App\ReceiptValidator\iTunes;

use Carbon\Carbon;
use ReceiptValidator\RunTimeException;

// phpcs:disable SlevomatCodingStandard.Classes.SuperfluousAbstractClassNaming.SuperfluousPrefix
abstract class AbstractResponse
{

    protected int $resultCode;
    protected int $bundleId;
    protected string $appItemId;
    protected ?Carbon $originalPurchaseDate;
    protected ?Carbon $requestDate;
    protected ?Carbon $receiptCreationDate;

    /**
     * @var array
     */
    protected array $receipt = [];

    protected string $latestReceipt;

    /**
     * @var array
     */
    protected array $latestReceiptInfo = [];

    /**
     * @var array
     */
    protected array $purchases = [];

    /**
     * @var array
     */
    protected array $pendingRenewalInfo = [];

    /**
     * @var ?array
     */
    protected ?array $rawData;
    protected bool $isRetryable = false;

    /**
     * @throws RunTimeException
     */
    public function __construct(?array $data = null)
    {
        $this->rawData = $data;
        $this->parseData();
    }

    /**
     * @return $this
     * @throws RunTimeException
     */
    // phpcs:disable SlevomatCodingStandard.Namespaces.UnusedUses.MismatchingCaseSensitivity
    public function parseData()
    {
        if (!is_array($this->rawData)) {
            throw new RuntimeException('Response must be an array');
        }

        if (!array_key_exists('status', $this->rawData)) {
            $this->resultCode = ResponseInterface::RESULT_DATA_MALFORMED;

            return $this;
        }
        $this->resultCode = $this->rawData['status'];

        // ios > 7 receipt validation
        if ($this->isIOS7StyleReceipt()) {
            $this->parseIOS7StyleReceipt();
        } elseif ($this->isIOS6StyleReceipt()) {
            $this->parseIOS6StyleReceipt();
        }

        return $this;
    }

    public function getResultCode(): int
    {
        return $this->resultCode;
    }

    public function setResultCode(int $code): void
    {
        $this->resultCode = $code;
    }

    /**
     * @return array
     */
    public function getPurchases(): array
    {
        return $this->purchases;
    }

    /**
     * @return array
     */
    public function getReceipt(): array
    {
        return $this->receipt;
    }

    /**
     * @return array
     */
    public function getLatestReceiptInfo(): array
    {
        return $this->latestReceiptInfo;
    }

    public function getLatestReceipt(): ?string
    {
        return $this->latestReceipt;
    }

    public function getBundleId(): int
    {
        return $this->bundleId;
    }

    public function getAppItemId(): string
    {
        return $this->appItemId;
    }

    public function getOriginalPurchaseDate(): ?Carbon
    {
        return $this->originalPurchaseDate;
    }

    public function getRequestDate(): ?Carbon
    {
        return $this->requestDate;
    }

    /**
     * @throws RunTimeException
     */
    // phpcs:disable SlevomatCodingStandard.Namespaces.UnusedUses.MismatchingCaseSensitivity

    public function getReceiptCreationDate(): ?Carbon
    {
        return $this->receiptCreationDate;
    }

    /**
     * @return array
     */
    public function getPendingRenewalInfo(): array
    {
        return $this->pendingRenewalInfo;
    }

    /**
     * @return array|string[]|null
     */
    public function getRawData()
    {
        return $this->rawData;
    }

    public function isValid(): bool
    {
        return $this->resultCode === ResponseInterface::RESULT_OK
            || $this->resultCode === ResponseInterface::RESULT_RECEIPT_VALID_BUT_SUB_EXPIRED;
    }

    public function isRetryable(): bool
    {
        return $this->isRetryable;
    }

    protected function isIOS7StyleReceipt(): bool
    {
        return array_key_exists('receipt', $this->rawData)
            && is_array($this->rawData['receipt'])
            && array_key_exists('in_app', $this->rawData['receipt'])
            && is_array($this->rawData['receipt']['in_app']);
    }

    /**
     * Collect data for iOS >= 7.0 receipt.
     *
     * @throws RunTimeException
     */
    protected function parseIOS7StyleReceipt(): void
    {
        $this->receipt = $this->rawData['receipt'];
        $this->appItemId = $this->rawData['receipt']['app_item_id'];
        $this->purchases = [];

        if (array_key_exists('original_purchase_date_ms', (array) $this->rawData['receipt'])) {
            $this->originalPurchaseDate = Carbon::createFromTimestampUTC(
                (int) round($this->rawData['receipt']['original_purchase_date_ms'] / 1000)
            );
        }

        if (array_key_exists('request_date_ms', (array) $this->rawData['receipt'])) {
            $this->requestDate = Carbon::createFromTimestampUTC(
                (int) round($this->rawData['receipt']['request_date_ms'] / 1000)
            );
        }

        if (array_key_exists('receipt_creation_date_ms', (array) $this->rawData['receipt'])) {
            $this->receiptCreationDate = Carbon::createFromTimestampUTC(
                (int) round($this->rawData['receipt']['receipt_creation_date_ms'] / 1000)
            );
        }
        foreach ($this->rawData['receipt']['in_app'] as $purchase_item_data) {
            $this->purchases[] = new PurchaseItem($purchase_item_data);
        }

        if (array_key_exists('bundle_id', (array) $this->rawData['receipt'])) {
            $this->bundleId = $this->rawData['receipt']['bundle_id'];
        }

        if (array_key_exists('latest_receipt_info', (array) $this->rawData)) {
            $this->latestReceiptInfo = array_map(
                function ($data) {
                    return new PurchaseItem((array) $data);
                },
                (array) $this->rawData['latest_receipt_info']
            );

            usort(
                $this->latestReceiptInfo,
                function (PurchaseItem $a, PurchaseItem $b) {
                    return $b->getPurchaseDate()->timestamp - $a->getPurchaseDate()->timestamp;
                }
            );
        }

        if (array_key_exists('latest_receipt', (array) $this->rawData)) {
            $this->latestReceipt = $this->rawData['latest_receipt'];
        }

        if (array_key_exists('pending_renewal_info', (array) $this->rawData)) {
            $this->pendingRenewalInfo = array_map(
                function ($data) {
                    return new PendingRenewalInfo((array) $data);
                },
                (array) $this->rawData['pending_renewal_info']
            );
        }

        if (array_key_exists('is-retryable', $this->rawData)) {
            $this->isRetryable = true;
        }
    }

    protected function isIOS6StyleReceipt(): bool
    {
        return !$this->isIOS7StyleReceipt() && array_key_exists('receipt', $this->rawData);
    }

    /**
     * Collect data for iOS <= 6.0 receipt.
     *
     * @throws RunTimeException
     */
    protected function parseIOS6StyleReceipt(): void
    {
        $this->receipt = (array) $this->rawData['receipt'];
        $this->purchases = [];
        $this->purchases[] = new PurchaseItem((array) $this->rawData['receipt']);

        if (array_key_exists('bid', (array) $this->rawData['receipt'])) {
            $this->bundleId = $this->rawData['receipt']['bid'];
        }
    }

}
