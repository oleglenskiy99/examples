<?php

declare(strict_types=1);

namespace App\ReceiptValidator\Amazon;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use ReceiptValidator\RunTimeException;

class Validator
{

    public const ENDPOINT_SANDBOX = 'http://localhost:8080/RVSSandbox/';
    public const ENDPOINT_PRODUCTION = 'https://appstore-sdk.amazon.com/version/1.0/verifyReceiptId/';

    protected string $endpoint;
    protected ?HttpClient $client = null;
    protected ?string $userId = null;
    protected ?string $receiptId = null;
    protected ?string $developerSecret = null;

    /**
     * @throws RunTimeException
     */
    // phpcs:disable Squiz.Strings.DoubleQuoteUsage.ContainsVar
    public function __construct(string $endpoint = self::ENDPOINT_PRODUCTION)
    {
        if ($endpoint !== self::ENDPOINT_PRODUCTION && $endpoint !== self::ENDPOINT_SANDBOX) {
            throw new RunTimeException("Invalid endpoint '{$endpoint}'");
        }

        $this->endpoint = $endpoint;
    }

    public function setUserId(?string $userId = null): Validator
    {
        $this->userId = $userId;

        return $this;
    }

    public function setReceiptId(?string $receiptId): Validator
    {
        $this->receiptId = $receiptId;

        return $this;
    }

    public function getDeveloperSecret(): ?string
    {
        return $this->developerSecret;
    }

    public function setDeveloperSecret(?string $developerSecret): Validator
    {
        $this->developerSecret = $developerSecret;

        return $this;
    }

    public function getEndpoint(): string
    {
        return $this->endpoint;
    }

    public function setEndpoint(string $endpoint): Validator
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    /**
     * @throws RunTimeException
     * @throws GuzzleException
     * @return Response
     */
    public function validate(): Response
    {
        try {
            $httpResponse = $this->getClient()->request(
                'GET',
                sprintf(
                    'developer/%s/user/%s/receiptId/%s',
                    $this->developerSecret,
                    $this->userId,
                    $this->receiptId
                )
            );

            return new Response(
                $httpResponse->getStatusCode(),
                json_decode((string) $httpResponse->getBody(), true)
            );
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return new Response(
                    $e->getResponse()->getStatusCode(),
                    json_decode((string) $e->getResponse()->getBody(), true)
                );
            }
        }

        return new Response(Response::RESULT_INVALID_RECEIPT);
    }

    protected function getClient(): HttpClient
    {
        if ($this->client === null) {
            $this->client = new HttpClient(['base_uri' => $this->endpoint]);
        }

        return $this->client;
    }

}
