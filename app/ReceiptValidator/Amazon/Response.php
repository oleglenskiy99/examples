<?php

declare(strict_types=1);

namespace App\ReceiptValidator\Amazon;

use App\ReceiptValidator\RunTimeException;

class Response
{

    public const RESULT_OK = 200;
    public const RESULT_INVALID_RECEIPT = 400;
    public const RESULT_INVALID_DEVELOPER_SECRET = 496;
    public const RESULT_INVALID_USER_ID = 497;
    public const RESULT_INTERNAL_ERROR = 500;

    protected int $code;

    /**
     * @var array<string>
     */
    protected array $receipt = [];

    /**
     * purchases info.
     *
     * @var array<PurchaseItem>
     */
    protected $purchases = [];

    /**
     * Response constructor.
     *
     * @param int $httpStatusCode
     * @param array|null $jsonResponse
     * @throws RunTimeException
     */
    public function __construct($httpStatusCode = 200, $jsonResponse = null)
    {
        $this->code = $httpStatusCode;

        if ($jsonResponse !== null) {
            $this->parseJsonResponse($jsonResponse);
        }
    }

    /**
     * Get Result Code.
     *
     * @return int
     */
    public function getResultCode(): int
    {
        return $this->code;
    }

    /**
     * @return array<string>
     */
    public function getReceipt(): array
    {
        return $this->receipt;
    }

    /**
     * Get purchases info.
     *
     * @return PurchaseItem[]
     */
    public function getPurchases(): array
    {
        return $this->purchases;
    }

    /**
     * returns if the receipt is valid or not.
     *
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->code === self::RESULT_OK;
    }

    /**
     * @throws RunTimeException
     */
    // phpcs:disable SlevomatCodingStandard.Namespaces.UnusedUses.MismatchingCaseSensitivity
    public function parseJsonResponse(?array $jsonResponse = null): Response
    {
        if (!is_array($jsonResponse)) {
            throw new RuntimeException('Response must be a scalar value');
        }

        $this->receipt = $jsonResponse;
        $this->purchases = [];
        $this->purchases[] = new PurchaseItem($jsonResponse);

        return $this;
    }

}
