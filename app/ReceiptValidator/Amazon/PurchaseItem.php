<?php

declare(strict_types=1);

namespace App\ReceiptValidator\Amazon;

use Carbon\Carbon;
use ReceiptValidator\RunTimeException;

class PurchaseItem
{

    /**
     * @var array<string>
     */
    protected ?array $rawData = null;
    protected int $quantity;
    protected string $productId;
    protected string $transactionId;
    protected Carbon $purchaseDate;
    protected ?Carbon $cancellationDate;
    protected ?Carbon $renewalDate;
    protected ?array $rawdata;

    /**
     * @throws RunTimeException
     */
    public function __construct(?array $jsonResponse = null)
    {
        $this->rawdata = $jsonResponse;
        if ($this->rawData !== null) {
            $this->parseJsonResponse();
        }
    }

    /**
     * @return array<string>|null
     */
    public function getRawResponse(): ?array
    {
        return $this->rawData;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getTransactionId(): string
    {
        return $this->transactionId;
    }

    public function getPurchaseDate(): Carbon
    {
        return $this->purchaseDate;
    }

    public function getCancellationDate(): ?Carbon
    {
        return $this->cancellationDate;
    }

    public function getRenewalDate(): ?Carbon
    {
        return $this->renewalDate;
    }

    /**
     * @throws RunTimeException
     */
    // phpcs:disable SlevomatCodingStandard.Namespaces.UnusedUses.MismatchingCaseSensitivity
    public function parseJsonResponse(): PurchaseItem
    {
        $jsonResponse = $this->rawData;
        if (!is_array($jsonResponse)) {
            throw new RuntimeException('Response must be a scalar value');
        }

        if (array_key_exists('quantity', $jsonResponse)) {
            $this->quantity = (int) $jsonResponse['quantity'];
        }

        if (array_key_exists('receiptId', $jsonResponse)) {
            $this->transactionId = $jsonResponse['receiptId'];
        }

        if (array_key_exists('productId', $jsonResponse)) {
            $this->productId = $jsonResponse['productId'];
        }

        if (array_key_exists('purchaseDate', $jsonResponse) && !empty($jsonResponse['purchaseDate'])) {
            $this->purchaseDate = Carbon::createFromTimestampUTC(intval(round((int) $jsonResponse['purchaseDate'] / 1000)));
        }

        if (array_key_exists('cancelDate', $jsonResponse) && !empty($jsonResponse['cancelDate'])) {
            $this->cancellationDate = Carbon::createFromTimestampUTC(
                intval(round((int) $jsonResponse['cancelDate'] / 1000))
            );
        }

        if (array_key_exists('renewalDate', $jsonResponse) && !empty($jsonResponse['renewalDate'])) {
            $this->renewalDate = Carbon::createFromTimestampUTC(intval(round((int) $jsonResponse['renewalDate'] / 1000)));
        }

        return $this;
    }

}
