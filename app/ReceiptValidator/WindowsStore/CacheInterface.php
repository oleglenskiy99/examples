<?php

declare(strict_types=1);

namespace App\ReceiptValidator\WindowsStore;

interface CacheInterface
{

    // phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingNativeTypeHint
    // phpcs:disable SlevomatCodingStandard.TypeHints.DisallowMixedTypeHint.DisallowedMixedTypeHint
    public function get(string $key);

    /**
     * @param string $key
     * @param mixed $value
     * @param int $minutes
     * @return void
     */
    // phpcs:disable SlevomatCodingStandard.TypeHints.DisallowMixedTypeHint.DisallowedMixedTypeHint
    public function put(string $key, $value, int $minutes): void;

}
