<?php

declare(strict_types=1);

namespace App\ReceiptValidator\WindowsStore;

use Aws\CacheInterface;
use DOMDocument;
use GuzzleHttp\Client;
use ReceiptValidator\RunTimeException;
use RobRichards\XMLSecLibs\XMLSecEnc;
use RobRichards\XMLSecLibs\XMLSecurityDSig;

class Validator
{

    protected CacheInterface $cache;

    public function __construct(?CacheInterface $cache = null)
    {
        $this->cache = $cache;
    }

    /**
     * @throws RunTimeException
     */
    public function validate(string $receipt): bool
    {
        // Load the receipt that needs to verified as an XML document.
        $dom = new DOMDocument();
        if (@$dom->loadXML($receipt) === false) {
            throw new RunTimeException('Invalid XML');
        }

        // The certificateId attribute is present in the document root, retrieve it.
        $certificateId = $dom->documentElement->getAttribute('CertificateId');
        if (empty($certificateId)) {
            throw new RunTimeException('Missing CertificateId in receipt');
        }

        // Retrieve the certificate from the official site.
        $certificate = $this->retrieveCertificate($certificateId);

        return $this->validateXml($dom, $certificate);
    }

    /**
     * @return resource
     */
    protected function retrieveCertificate(string $certificateId)
    {
        // Retrieve from cache if a cache handler has been set.
        $cacheKey = 'store-receipt-validate.windowsstore.' . $certificateId;
        $certificate = $this->cache->get($cacheKey);

        if ($certificate === null) {
            // We are attempting to retrieve the following url. The getAppReceiptAsync website at
            // http://msdn.microsoft.com/en-us/library/windows/apps/windows.applicationmodel.store.currentapp.getappreceiptasync.aspx
            // lists the following format for the certificate url.
            $certificateUrl = '/fwlink/?LinkId=246509&cid=' . $certificateId;

            // Make an HTTP GET request for the certificate.
            $client = new Client(['base_uri' => 'https://go.microsoft.com']);
            $response = $client->request('GET', $certificateUrl);

            // Retrieve the certificate out of the response.
            $certificate = $response->getBody();

            // Write back to cache.
                $this->cache->set($cacheKey, $certificate, 3600);
        }

        return openssl_x509_read($certificate);
    }

    protected function validateXml(DOMDocument $dom, $certificate): bool
    {
        $secDsig = new XMLSecurityDSig();

        // Locate the signature in the receipt XML.
        $dsig = $secDsig->locateSignature($dom);
        if ($dsig === null) {
            throw new RunTimeException('Cannot locate receipt signature');
        }

        $secDsig->canonicalizeSignedInfo();
        $secDsig->idKeys = ['wsu:Id'];
        $secDsig->idNS = [
            'wsu' => 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd',
        ];

        if (!$secDsig->validateReference()) {
            throw new RunTimeException('Reference validation failed');
        }

        $key = $secDsig->locateKey();
        if ($key === null) {
            throw new RunTimeException('Could not locate key in receipt');
        }

        $keyInfo = XMLSecEnc::staticLocateKeyInfo($key, $dsig);
        if (!$keyInfo->key) {
            $key->loadKey($certificate);
        }

        return $secDsig->verify($key) === 1;
    }

}
