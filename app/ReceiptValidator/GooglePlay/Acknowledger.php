<?php

declare(strict_types=1);

namespace App\ReceiptValidator\GooglePlay;

use Google_Service_AndroidPublisher;
use Google_Service_AndroidPublisher_ProductPurchasesAcknowledgeRequest;
use Google_Service_AndroidPublisher_SubscriptionPurchasesAcknowledgeRequest;
use RunTimeException;
use Throwable;

class Acknowledger
{

    public const ACKNOWLEDGE_STRATEGY_IMPLICIT = 'strategy_implicit';
    public const ACKNOWLEDGE_STRATEGY_EXPLICIT = 'strategy_explicit';
    public const SUBSCRIPTION = 'SUBSCRIPTION';
    public const PRODUCT = 'PRODUCT';

    protected Google_Service_AndroidPublisher $androidPublisherService;
    protected string $packageName;
    protected string $purchaseToken;
    protected string $productId;
    protected string $strategy;

    /**
     * @throws RunTimeException
     */
    // phpcs:disable SlevomatCodingStandard.Namespaces.UnusedUses.MismatchingCaseSensitivity
    public function __construct(
        Google_Service_AndroidPublisher $googleServiceAndroidPublisher,
        string $packageName,
        string $productId,
        string $purchaseToken,
        string $strategy = self::ACKNOWLEDGE_STRATEGY_EXPLICIT
    )
	{
        if (!in_array($strategy, [self::ACKNOWLEDGE_STRATEGY_EXPLICIT, self::ACKNOWLEDGE_STRATEGY_IMPLICIT])) {
            throw new RuntimeException(sprintf('Invalid strategy provided %s', $strategy));
        }

        $this->androidPublisherService = $googleServiceAndroidPublisher;
        $this->packageName = $packageName;
        $this->purchaseToken = $purchaseToken;
        $this->productId = $productId;
        $this->strategy = $strategy;
    }

    // phpcs:disable SlevomatCodingStandard.Namespaces.UnusedUses.MismatchingCaseSensitivity
    public function acknowledge(string $type = self::SUBSCRIPTION, string $developerPayload = ''): bool
    {
        try {
            switch ($type) {
                case self::SUBSCRIPTION:
                    if ($this->strategy === self::ACKNOWLEDGE_STRATEGY_EXPLICIT) {
                        // Here exception might be thrown as previously, so no BC break here
                        $this->androidPublisherService->purchases_subscriptions->acknowledge(
                            $this->packageName,
                            $this->productId,
                            $this->purchaseToken,
                            new Google_Service_AndroidPublisher_SubscriptionPurchasesAcknowledgeRequest(
                                ['developerPayload' => $developerPayload]
                            )
                        );
                    } elseif ($this->strategy === self::ACKNOWLEDGE_STRATEGY_IMPLICIT) {
                        $subscriptionPurchase = $this->androidPublisherService->purchases_subscriptions->get(
                            $this->packageName,
                            $this->productId,
                            $this->purchaseToken
                        );

                        if ($subscriptionPurchase->getAcknowledgementState() !== AbstractResponse::ACKNOWLEDGEMENT_STATE_DONE) {
                            $this->androidPublisherService->purchases_subscriptions->acknowledge(
                                $this->packageName,
                                $this->productId,
                                $this->purchaseToken,
                                new Google_Service_AndroidPublisher_SubscriptionPurchasesAcknowledgeRequest(
                                    ['developerPayload' => $developerPayload]
                                )
                            );
                        }
                    }
                    break;
                case self::PRODUCT:
                    if ($this->strategy === self::ACKNOWLEDGE_STRATEGY_EXPLICIT) {
                        // Here exception might be thrown as previously, so no BC break here
                        $this->androidPublisherService->purchases_products->acknowledge(
                            $this->packageName,
                            $this->productId,
                            $this->purchaseToken,
                            new Google_Service_AndroidPublisher_ProductPurchasesAcknowledgeRequest(
                                ['developerPayload' => $developerPayload]
                            )
                        );
                    } elseif ($this->strategy === self::ACKNOWLEDGE_STRATEGY_IMPLICIT) {
                        $productPurchase = $this->androidPublisherService->purchases_products->get(
                            $this->packageName,
                            $this->productId,
                            $this->purchaseToken
                        );

                        if ($productPurchase->getAcknowledgementState() !== AbstractResponse::ACKNOWLEDGEMENT_STATE_DONE) {
                            $this->androidPublisherService->purchases_products->acknowledge(
                                $this->packageName,
                                $this->productId,
                                $this->purchaseToken,
                                new Google_Service_AndroidPublisher_ProductPurchasesAcknowledgeRequest(
                                    ['developerPayload' => $developerPayload]
                                )
                            );
                        }
                    }
                    break;
                default:
                    throw new \RuntimeException(
                        sprintf(
                            'Invalid type provided : %s expected %s',
                            $type,
                            implode(',', [self::PRODUCT, self::SUBSCRIPTION])
                        )
                    );
            }

            return true;
        } catch (Throwable $e) {
            throw new RuntimeException($e->getCode(), $e->getCode(), $e);
        }
    }

}
