<?php

declare(strict_types=1);

namespace App\ReceiptValidator\GooglePlay;

use Google_Service_AndroidPublisher_ProductPurchase;
use Google_Service_AndroidPublisher_SubscriptionPurchase;

// phpcs:disable SlevomatCodingStandard.Classes.SuperfluousAbstractClassNaming.SuperfluousPrefix
abstract class AbstractResponse
{

    public const CONSUMPTION_STATE_YET_TO_BE_CONSUMED = 0;
    public const CONSUMPTION_STATE_CONSUMED = 1;
    public const PURCHASE_STATE_PURCHASED = 0;
    public const PURCHASE_STATE_CANCELED = 1;
    public const ACKNOWLEDGEMENT_STATE_YET_TO_BE = 0;
    public const ACKNOWLEDGEMENT_STATE_DONE = 1;

    /**
     * @var Google_Service_AndroidPublisher_ProductPurchase|Google_Service_AndroidPublisher_SubscriptionPurchase
     */
    protected $response;

    /**
     * @param Google_Service_AndroidPublisher_ProductPurchase|Google_Service_AndroidPublisher_SubscriptionPurchase $response
     */
    public function __construct($response)
    {
        $this->response = $response;
    }

    /**
     * @return array<string>|string
     */
    public function getDeveloperPayload()
    {
        return $this->response->getDeveloperPayload();
    }

    public function getAcknowledgementState(): int
    {
        return $this->response->acknowledgementState;
    }

    public function isAcknowledged(): bool
    {
        return $this->response->acknowledgementState === self::ACKNOWLEDGEMENT_STATE_DONE;
    }

    public function getKind(): string
    {
        return $this->response->kind;
    }

    /**
     * @return Google_Service_AndroidPublisher_ProductPurchase|Google_Service_AndroidPublisher_SubscriptionPurchase
     */
    public function getRawResponse()
    {
        return $this->response;
    }

}
