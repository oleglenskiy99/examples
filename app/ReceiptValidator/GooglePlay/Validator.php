<?php

declare(strict_types=1);

namespace App\ReceiptValidator\GooglePlay;

use Google_Service_AndroidPublisher;
use ReceiptValidator\GooglePlay\PurchaseResponse;
use ReceiptValidator\GooglePlay\SubscriptionResponse;

/**
 * Class Validator.
 */
class Validator
{

    protected ?Google_Service_AndroidPublisher $androidPublisherService = null;
    protected ?string $packageName = null;
    protected ?string $purchaseToken = null;
    protected ?string $productId = null;
    private bool $validationModePurchase = true;

    public function __construct(
        Google_Service_AndroidPublisher $googleServiceAndroidPublisher,
        bool $validationModePurchase = true
    )
	{
        $this->androidPublisherService = $googleServiceAndroidPublisher;
        $this->validationModePurchase = $validationModePurchase;
    }

    public function setPackageName(string $packageName): Validator
    {
        $this->packageName = $packageName;

        return $this;
    }

    public function setPurchaseToken(string $purchaseToken): Validator
    {
        $this->purchaseToken = $purchaseToken;

        return $this;
    }

    public function setProductId(string $productId): Validator
    {
        $this->productId = $productId;

        return $this;
    }

    public function setValidationModePurchase(bool $validationModePurchase): Validator
    {
        $this->validationModePurchase = $validationModePurchase;

        return $this;
    }

    /**
     * @return PurchaseResponse|SubscriptionResponse
     */
    public function validate()
    {
        return $this->validationModePurchase ? $this->validatePurchase() : $this->validateSubscription();
    }

    public function validatePurchase(): PurchaseResponse
    {
        return new PurchaseResponse(
            $this->androidPublisherService->purchases_products->get(
                $this->packageName,
                $this->productId,
                $this->purchaseToken
            )
        );
    }

    public function validateSubscription(): SubscriptionResponse
    {
        return new SubscriptionResponse(
            $this->androidPublisherService->purchases_subscriptions->get(
                $this->packageName,
                $this->productId,
                $this->purchaseToken
            )
        );
    }

    public function getPublisherService(): ?Google_Service_AndroidPublisher
    {
        return $this->androidPublisherService;
    }

}
