<?php

declare(strict_types=1);

namespace App\ReceiptValidator\GooglePlay;

use Google_Service_AndroidPublisher_ProductPurchase;

class PurchaseResponse extends AbstractResponse
{

    /**
     * @var Google_Service_AndroidPublisher_ProductPurchase
     */
    protected $response;

    /**
     * @var array<string>
     */
    protected array $developerPayload = [];

    public function __construct(Google_Service_AndroidPublisher_ProductPurchase $response)
    {
        parent::__construct($response);

        $this->developerPayload = json_decode($this->response->developerPayload, true);
    }

    public function getConsumptionState(): int
    {
        return $this->response->consumptionState;
    }

    public function getPurchaseTimeMillis(): string
    {
        return $this->response->purchaseTimeMillis;
    }

    /**
     * @return array<string>
     */
    public function getDeveloperPayload(): array
    {
        return $this->developerPayload;
    }

    public function getDeveloperPayloadElement(string $key): string
    {
        return $this->developerPayload[$key] ?? '';
    }

    public function getPurchaseState(): int
    {
        return $this->response->purchaseState;
    }

}
