<?php

declare(strict_types=1);

namespace App\ReceiptValidator\GooglePlay;

use Google_Service_AndroidPublisher_SubscriptionPurchase;

/**
 * Class SubscriptionResponse.
 */
class SubscriptionResponse extends AbstractResponse
{

    /**
     * @var Google_Service_AndroidPublisher_SubscriptionPurchase
     */
    protected $response;

    public function getAutoRenewing(): bool
    {
        return $this->response->getAutoRenewing();
    }

    public function getCancelReason(): ?int
    {
        return $this->response->getCancelReason();
    }

    public function getCountryCode(): string
    {
        return $this->response->getCountryCode();
    }

    public function getPriceAmountMicros(): string
    {
        return $this->response->getPriceAmountMicros();
    }

    public function getPriceCurrencyCode(): string
    {
        return $this->response->getPriceCurrencyCode();
    }

    public function getStartTimeMillis(): string
    {
        return $this->response->getStartTimeMillis();
    }

    public function getExpiryTimeMillis(): string
    {
        return $this->response->getExpiryTimeMillis();
    }

    public function getUserCancellationTimeMillis(): ?string
    {
        return $this->response->getUserCancellationTimeMillis();
    }

    public function getPaymentState(): int
    {
        return $this->response->getPaymentState();
    }

    public function getExpiresDate(): string
    {
        return $this->response->expiryTimeMillis;
    }

}
