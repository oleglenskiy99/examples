<?php

declare(strict_types=1);

namespace App;

use App\Models\User\Info;
use App\Models\Video\Category;
use App\Repository\VideoRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Video
 *
 * @OA\Schema(
 *     description="User Video",
 *     title="User Video",
 * @OA\Xml(
 *         name="Video"
 *     )
 * )
 * @property int $id
 * @property int $user_id
 * @property int $target_id
 * @property int $type
 * @property string $friend_name
 * @property int $category
 * @property string $instruction
 * @property int $deadline
 * @property int $cost
 * @property bool $is_pay
 * @property bool $is_completed
 * @property bool $is_deleted
 * @property bool $is_live
 * @property bool $is_privacy
 * @property string $charge_id
 * @property int $media
 * @property bool $is_viewed
 * @property Carbon $created_at
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and');
 * @method static Video findOrFail(int $id, array $columns = ['*']);
 */
class Video extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_videos';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'target_id',
        'type',
        'instruction',
        'category',
        'cost',
        'charge_id',
        'is_privacy',
        'deadline',
        'is_pay',
        'is_viewed',
        'is_deleted',
        'is_completed',
        'media',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_live' => 'boolean',
        'is_pay' => 'boolean',
        'is_completed' => 'boolean',
        'is_deleted' => 'boolean',
        'is_privacy' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'is_viewed' => 'boolean',
    ];

    public function owner(): HasOne
    {
        return $this->hasOne(Info::class, 'id', 'user_id');
    }

    public function target(): HasOne
    {
        return $this->hasOne(Info::class, 'id', 'target_id');
    }

    public function category(): HasOne
    {
        return $this->hasOne(Category::class, 'id', 'category');
    }

    // phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint

    public function media(): HasOne
    {
        return $this->hasOne(Media::class, 'id', 'media');
    }

    /**
     * @return array
     */
    public static function getById(int $id, string $section = 'incoming'): array
    {
        $item = VideoRepository::builder()->where('id', $id)->first();
        $response = [];
        if ($item) {
            $response = $item->toArray();

            $response['group'] = 'video';
            $response['user'] = $section === 'incoming' ? $item['owner'] : $item['target'];
            $response['section'] = $section;

        }
        return $response;
    }

}
