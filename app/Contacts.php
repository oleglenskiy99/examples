<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Contacts
 *
 * @OA\Schema(
 *     description="Contacts Model",
 *     title="Contacts Model"
 * )
 * @property int $id
 * @property int $owner_id
 * @property int $target_id
 * @property bool $is_approved
 * @property bool $is_deleted
 */
class Contacts extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_contacts';

    /**
     * @var array<string>
     */
    protected $casts = [
        'is_approved' => 'boolean',
        'is_deleted' => 'boolean',
    ];

    public function user(): HasOne
	{
        return $this->hasOne('\App\Models\User\Info', 'id', 'target_id');
    }

    public function owner(): HasOne
	{
        return $this->hasOne('\App\Models\User\Info', 'id', 'owner_id');
    }

    public static function isContact(int $ownerId, int $targetId): bool
	{
        return  self::where('owner_id', $ownerId)->where('target_id', $targetId)->count() === 1;
    }

}
