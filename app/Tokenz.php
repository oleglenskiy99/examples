<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and');
 * @method static Tokenz|null find($id, $columns = ['*']);
 */
class Tokenz extends Model
{

    public bool $isAuthorized = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    public Tokenz $tokenz;

    /**
     * @var string
     */
    protected $table = 'module_users_tokens';

    /**
     * @var array
     */
    protected $casts = [
        'is_online' => 'boolean',
        'access_token' => 'string',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'access_token',
        'timestamp',
        'expires',
        'call_id',
        'application_version',
        'device_model',
        'device_type',
        'device_os',
        'device_os_version',
        'city_name',
        'region_name',
        'country_name',
        'device_token',
        'session_id',
        'is_online',
    ];

    public function __construct(array $attributes = [], ?string $accessToken = null)
    {
        parent::__construct($attributes);

        if (is_null($accessToken)) {
            return;
        }

        $this->tokenz = self::where('access_token', $accessToken)->first();

        $this->isAuthorized = !is_null($this->tokenz);
    }

    // phpcs:disable SlevomatCodingStandard.Variables.DisallowSuperGlobalVariable.DisallowedSuperGlobalVariable

    public static function findByAccessToken(string $token): ?Tokenz
    {
        return static::where('access_token', $token)->first();
    }

    /**
     * @return array
     */
    public static function getDeviceTokens(int $userId): array
    {
        $tokens = static::select('id', 'session_id', 'access_token', 'device_token')
            ->with('settings')
            ->addSelect(
                DB::raw(
                    '(SELECT SUM(unread_messages_count) FROM module_chats_users WHERE module_chats_users.user_id = module_users_tokens.user_id AND unread_messages_count > 0 ) as badge'
                )
            )
            ->where('user_id', $userId)
            ->whereRaw('device_token IS NOT NULL')
            ->get();

        return $tokens->toArray();
    }

    /**
     * @return array
     */
    public static function getChatMembersTokens(int $chatId): array
    {
        $members = DB::table('module_chats_users')->select('user_id')->where('dialog_id', $chatId)
            ->where('is_muted', 0)->get()->toArray();

        return static::select('id', 'user_id', 'session_id', 'access_token', 'device_token')
            ->addSelect(
                DB::raw(
                    '(SELECT SUM(unread_messages_count) FROM module_chats_users WHERE module_chats_users.user_id = module_users_tokens.user_id AND unread_messages_count > 0 ) as badge'
                )
            )
            ->with('settings')
            ->whereIn('user_id', array_column($members, 'user_id'))
            ->whereRaw('device_token IS NOT NULL')
            ->get()->toArray();
    }

    // phpcs:disable SlevomatCodingStandard.Classes.ClassStructure.IncorrectGroupOrder

    public function detect(): void
    {
        $this->ip = getClientIP();

        $agent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/Linux/', $agent)) {
            $os = 'Linux';
            if (preg_match('/Android/', $agent)) {
                $os = 'Android';
            }
        } elseif (preg_match('/Windows/', $agent)) {
            $os = 'Windows';
        } elseif (preg_match('/Mac/', $agent)) {
            $os = 'Mac';
            if (preg_match('/iPhone/', $agent)) {
                $os = 'iPhone';
            }
        } elseif (preg_match('/Apple/', $agent)) {
            $os = 'Apple';
        } else {
            $os = 'UnKnown';
        }

        $location = GeoIP::detect();

        $this->city_name = $location['cityName'];
        $this->region_name = $location['regionName'];
        $this->country_name = $location['countryName'];

        $headers = apacheRequestHeaders();

        $this->device = array_key_exists('device', $headers) ? $headers['device'] : $os;
        $this->app_version = array_key_exists('app_version', $headers) ? $headers['app_version'] : null;
        $this->op_system = array_key_exists('op_system', $headers) ? $headers['op_system'] : null;
        $this->op_version = array_key_exists('op_version', $headers) ? str_replace('_', '.', $headers['op_version']) : null;
        $this->device_token = array_key_exists('device_token', $headers) ? $headers['device_token'] : null;
    }

}
