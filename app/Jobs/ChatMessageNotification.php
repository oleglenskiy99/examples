<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Tokenz;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Throwable;

class ChatMessageNotification implements ShouldQueue
{

    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected array $message;
    protected int $peerId;

    public function __construct(array $message, int $peerId)
    {
        $this->message = $message;
        $this->peerId = $peerId;
    }

    public function handle(): void
    {
        $devices = Tokenz::getDeviceTokens($this->peerId);

        foreach ($devices as $device) {

            if ($this->getBoolean($device['settings'], 'can_display_chat_notification', true) === false) {
                return;
            }

            $body = '';

            $title = $this->getBoolean(
                $device['settings'],
                'can_display_chat_title',
                true
            )
                ? $this->message['user']['firstName'] . ' ' . $this->message['user']['lastName']
                : 'Новое сообщение';

            if ($this->getBoolean($device['settings'], 'can_display_chat_body', true)) {
                $formatter = $this->validChatMessageText();
                $body = $formatter['text'];
            }

            $config = $this->getDefaultApnsConfig();
            $config['payload']['aps']['badge'] = (int) $device['badge'];

            try {
                $this->send(['title' => $title, 'body' => $body], $config, $device['device_token']);
            } catch (Throwable $ex) {
                Log::error($ex->getMessage());
            }
        }
    }

    /**
     * @param array<string> $array
     * @param string $key
     * @param bool $default
     * @return bool
     */
    private function getBoolean(array $array, string $key, bool $default): bool
    {
        if (empty($array)) {
            return $default;
        }

        if (array_key_exists($key, $array)) {
            return true;
        }

        return $default;
    }

    /**
     * @return array
     */
    private function validChatMessageText(): array
    {
        if ($this->message['type'] === 'voice') {
            return ['text' => 'Голосовое сообщение', 'image' => null];
        }

        if (trim($this->message['text']) === '') {

            if (array_key_exists('attachments', $this->message)) {

                $text = '';
                $image = null;

                $attachment = $this->message['attachments'][0];

                switch ($attachment['type']) {
                    case 'image':
                        $text = 'Изображение';
                        break;
                    case 'video':
                        $text = 'Изображение';
                        break;
                }

                $image = $attachment['thumb'];

                return ['text' => $text, 'image' => $image];

            }

        }

        return ['text' => $this->message['text'], 'image' => null];
    }

    /**
     * @return array
     */
    private function getDefaultApnsConfig(): array
    {
        return [
            'payload' => [
                'aps' => [
                    'sound' => 'default',
                ],
            ],
        ];
    }

    private function send($arrayData, $config, $token): void
    {
        $messaging = app('firebase.messaging');

        /** @psalm-suppress UndefinedClass * */
        $message = CloudMessage::new();
        /** @psalm-suppress UndefinedClass * */
        $notification = Notification::fromArray($arrayData);

        $message = $message->withTarget('token', $token)
            ->withNotification($notification)
            ->withApnsConfig($config);

        $messaging->send($message);
    }

}
