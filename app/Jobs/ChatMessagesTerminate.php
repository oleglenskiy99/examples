<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Models\Chats\Members;
use App\Models\Chats\Messages;
use App\Services\PaymentServiceInterface;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Throwable;

class ChatMessagesTerminate implements ShouldQueue
{

    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected int $dialogId;
    protected int $userId;

    public function __construct(int $dialogId, int $userId)
    {
        $this->dialogId = $dialogId;
        $this->userId = $userId;
    }

    public function handle(): void
    {
        $payments = app(PaymentServiceInterface::class);
        $messages = Messages::getUnreplyMessagesByChat($this->dialogId)->where('user_id', '!=', $this->userId)->get();
        $balance = 0;
        $user = User::getUserById($this->userId);

        foreach ($messages as $msg) {

            if ($msg->charge_id) {
                try {
                    $payments->capturePayment($user, $msg->charge_id);
                    $msg->is_has_reply = 1;
                    $msg->save();
                    $balance += $msg->cost;
                } catch (Throwable $exception) {
                    Log::error($exception->getMessage());
                }
            } else {
                $msg->is_has_reply = 1;
                $msg->save();
            }

        }

        if ($balance <= 0) {
            return;
        }

        Members::where('dialog_id', $this->dialogId)->where('user_id', $this->userId)->update(
            ['balance_actual' => 0]
        );
    }

}
