<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Media;
use Aws\Exception\MultipartUploadException;
use Aws\S3\MultipartUploader;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UploadS3Job implements ShouldQueue
{

    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected int $mediaId;
    protected string $filename;

    public function __construct(int $mediaId, string $filename)
    {
        $this->mediaId = $mediaId;
        $this->filename = $filename;
    }

    public function handle(): void
    {
        $filePath = storage_path('app') . '/' . $this->filename;
        $disk = Storage::disk('s3');
        $uploader = new MultipartUploader($disk->getDriver()->getAdapter()->getClient(), fopen($filePath, 'r+'), [
            'bucket' => config('filesystems.disks.s3.bucket'),
            'key' => $this->filename,
        ]);

        try {
            $uploader->upload();
            //записываем размер азгруженного
            $media = Media::find($this->mediaId);
            if ($media) {
                $media->url = $this->filename;
                $media->size = Storage::disk('s3')->size($this->filename);
                $media->save();
            }

        } catch (MultipartUploadException $e) {
            Log::error($e->getMessage());
            //если ошибка, то удалаяем медиа
            Media::find($this->mediaId)->delete();

        }

        File::delete($filePath);
    }

}
