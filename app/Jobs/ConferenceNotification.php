<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Models\Conference;
use Emitter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Throwable;

class ConferenceNotification implements ShouldQueue
{

    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected string $type;

    /**
     * @var array
     */
    protected array $deviceTokens;

    /**
     * @var array
     */
    protected array $data;
    protected Conference $conference;

    /**
     * @var array
     */
    protected array $users = [];

    /**
     * @param string $type
     * @param array $data
     * @param Conference $conference
     * @param array $tokens
     * @param array $users
     */
    public function __construct(string $type, array $data, Conference $conference, array $tokens, array $users = [])
    {
        $this->type = $type;
        $this->data = $data;
        $this->deviceTokens = $tokens;
        $this->conference = $conference;
        $this->users = $users;
    }

    public function handle(): void
    {
        $messaging = app('firebase.messaging');
        /** @psalm-suppress UndefinedClass * */
        $notification = Notification::fromArray($this->data);
        /** @psalm-suppress UndefinedClass * */
        $message = CloudMessage::new();

        $message = $message
            ->withNotification($notification)
            ->withApnsConfig($this->getDefaultApnsConfig())
            ->withData([
                'type' => 'conference',
                'conference' => json_encode([
                    'id' => $this->conference->id,
                    'slug' => $this->conference->slug,
                ])]);

        $offset = 0;

        try {
            do {
                $members = array_slice($this->deviceTokens, $offset, 499);
                $messaging->sendMulticast($message, $members);
                $offset += 499;
            } while (count($members) > 500);
        } catch (Throwable $ex) {
            Log::error($ex->getMessage());
        }

        $emitter = new Emitter();

        foreach ($this->users as $id) {
            $emitter->to(sha1($id))->emit($this->type, [
                'notification' => $this->data,
                'type' => 'conference',
                'conference' => $this->conference->id,
            ]);
        }
    }

    /**
     * @return array
     */
    protected function getDefaultApnsConfig(): array
    {
        return [
            'headers' => [
                'apns-priority' => '10',
            ],
            'payload' => [
                'aps' => [
                    'sound' => 'default',
                ],
            ],
        ];
    }

}
