<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Models\Chats\Chat;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ChatGroupNotification implements ShouldQueue
{

    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected array $message;
    protected Chat $chat;

    public function __construct(Chat $chat, array $message)
    {
        $this->message = $message;
        $this->chat = $chat;
    }

    public function handle(): void
    {
        $this->validChatMessageText();
    }

    /**
     * @return array
     */
    private function validChatMessageText(): array
    {
        if ($this->message['type'] === 'voice') {
            return ['text' => 'Голосовое сообщение', 'image' => null];
        }

        $text = strip_tags($this->message['text']);

        if (mb_strlen($text) > 155) {
            $text = trim(mb_substr($text, 0, 155)) . '...';
        }

        if (trim($this->message['text']) === '') {

            if (array_key_exists('attachments', $this->message)) {

                $text = '';
                $image = null;

                $attachment = $this->message['attachments'][0];

                switch ($attachment['type']) {
                    case 'image':
                        $text = 'Изображение';
                        break;
                    case 'video':
                        $text = 'Изображение';
                        break;
                }

                $image = $attachment['thumb'];

                return ['text' => $text, 'image' => $image];

            }

        }

        if (array_key_exists('attachments', $this->message)) {

            $image = null;
            $attachment = $this->message['attachments'][0];

            switch ($attachment['type']) {
                case 'image':
                    $text = '🖼️ ' . $text;
                    break;
                case 'video':
                    $text = '📹 ' . $text;
                    break;
            }

            $image = $attachment['thumb'];

            return ['text' => $text, 'image' => $image];

        }

        return ['text' => $text, 'image' => null];
    }

}
