<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Events\Transaction\CharityDonated;
use App\ExternalServices\Pledge\Donations;
use App\Models\User\Founds;
use App\Services\PayoutServiceInterface;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class DonateCharity implements ShouldQueue
{

    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var Founds
     */
    protected $found;

    protected float $amount;

    public function __construct(User $user, Founds $found, float $amount)
    {
        $this->afterCommit = true;
        $this->user = $user;
        $this->found = $found;
        $this->amount = $amount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PayoutServiceInterface $payments, Donations $donations): void
    {
        DB::transaction(function () use ($payments, $donations): void {
            $amount = $this->amount * $this->found->percent / 100;
            // TODO: сохранять лог пожертвований в module_users_founds_donations
            $payments->registerCharityPayout($this->user, $amount);
            $donations->create($this->user, $this->found->external_id, (string) $amount);
            CharityDonated::dispatch($this->found, $amount);
        });
    }

}
