<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Tokenz;
use App\Video;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
//use Notification;
use Throwable;

class Notifications implements ShouldQueue
{

    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected string $type;

    /**
     * @var array
     */
    protected array $data;

    /**
     * @param string $type
     * @param array $data
     */
    public function __construct(string $type, array $data)
    {
        $this->type = $type;
        $this->data = $data;
    }

    public function handle(): void
    {
        $devices = Tokenz::getDeviceTokens((int) $this->data['target']);
        $data = [];

        switch ($this->type) {

            case 'video:reply':
                $notification = Notification::fromArray([
                    'title' => 'Вам отправлен видео-ответ.',
                    'body' => sprintf(
                        '%s %s отправил ответ на ваш видео запрос.',
                        $this->data['from']->info->firstName,
                        $this->data['from']->info->lastName
                    ),
                ]);

                $config = $this->getDefaultApnsConfig();

                $data = [
                    'title' => 'Вам отправлен видео-ответ.',
                    'body' => sprintf(
                        '%s %s отправил ответ на ваш видео запрос.',
                        $this->data['from']->info->firstName,
                        $this->data['from']->info->lastName
                    ),
                    'video' => Video::getById($this->data['id'], 'outgoing'),
                    'type' => $this->type,
                ];

                break;

            case 'video:request':
                $notification = Notification::fromArray([
                    'title' => 'У вас новый запрос',
                    'body' => sprintf(
                        '%s %s отправил Вам запрос на запись видео.',
                        $this->data['from']->info->firstName,
                        $this->data['from']->info->lastName
                    ),
                ]);

                $config = $this->getDefaultApnsConfig();
                $data = [
                    'title' => 'У вас новый запрос',
                    'body' => sprintf(
                        '%s %s отправил Вам запрос на запись видео.',
                        $this->data['from']->info->firstName,
                        $this->data['from']->info->lastName
                    ),
                    'video' => Video::getById((int) $this->data['id']),
                    'type' => $this->type,
                ];

                break;

        }

        foreach ($devices as $device) {
            try {
                $this->send($notification, $config, $data, (string) $device['device_token']);
            } catch (Throwable $ex) {
                Log::error($ex->getMessage());
            }
        }
    }

    /**
     * @return array
     */
    private function getDefaultApnsConfig(): array
    {
        return [
            'headers' => [
                'apns-priority' => '10',
            ],
            'payload' => [
                'aps' => [
                    'sound' => 'default',
                    'content_available' => false,
                ],
            ],
        ];
    }

    private function send(Notification $notification, array $config, array $data, string $token): void
    {
        $messaging = app('firebase.messaging');

        $message = CloudMessage::new();

        $message = $message->withTarget('token', $token)
            ->withData(['type' => $this->type, 'content' => json_encode($data)])
            ->withNotification($notification)
            ->withApnsConfig($config);

        $messaging->send($message);
    }

}
