<?php

declare(strict_types=1);

use App\SocketIO\Events;
use App\Tokenz;
use Channel\Server as ChannelServer;
use PHPSocketIO\SocketIO;
use Workerman\Worker;

require __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../../public/index.php';

ini_set('display_errors', 'on');

$context = [
    'allowedHeaders' => ['Authorization'],
];

$io = new SocketIO(1337, $context);

// =======Channel server========
// phpcs:disable SlevomatCodingStandard.Variables.UnusedVariable.UnusedVariable
$channel = new ChannelServer('127.0.0.1');

// ========ChannelAdapter========
$io->on('workerStart', function () use ($io): void {
    $io->adapter('\PHPSocketIO\ChannelAdapter');
});


$io->on('connection', function ($socket) use ($io): void {
    if (array_key_exists('authtoken', $socket->request->_query)) {
        $authtoken = $socket->request->_query['authtoken'];
    } elseif (array_key_exists('authorization', $socket->request->_query)) {
        $authtoken = $socket->request->_query['authorization'];
    } else {
        return;
    }

    if (!$authtoken) {
        return;
    }

    $authorization = Tokenz::findByAccessToken($authtoken);

    if (!$authorization) {
        return;
    }

    session(['user_id' => $authorization->user_id]);

    try {
        $socket->user_id = $authorization->user_id;
        $socket->token_id = $authorization->id;
        $socket->authtoken = $authtoken;
        $socket->calltoken = $authorization->call_id;
        $e = new Events($socket, $io);
        $e->afterAuthorizedConnection();
    } catch (Throwable $ex) {
        echo $ex->getMessage() . PHP_EOL;
    }

    // chats
    $e->addEventLinstiner('chats:created', ['\App\SocketIO\Events\Chats', 'created']);
    $e->addEventLinstiner('request:chats:update', ['\App\SocketIO\Events\Chats', 'update']);
    $e->addEventLinstiner('request:chats:details', ['\App\SocketIO\Events\Chats', 'details']);
    $e->addEventLinstiner('request:chat:subscribe', ['\App\SocketIO\Events\Chats', 'subscribe']);

    // messages
    $e->addEventLinstiner('chat:message:get', ['\App\SocketIO\Events\Messages', 'get']);
    $e->addEventLinstiner('chat:message:vector', ['\App\SocketIO\Events\Messages', 'vector']);
    $e->addEventLinstiner('chat:message:history', ['\App\SocketIO\Events\Messages', 'getHistory']);
    $e->addEventLinstiner('chat:message:state', ['\App\SocketIO\Events\Messages', 'state']);
    $e->addEventLinstiner('chat:message:sent', ['\App\SocketIO\Events\Messages', 'sent']);
    $e->addEventLinstiner('chat:message:pay', ['\App\SocketIO\Events\Messages', 'pay']);
    $e->addEventLinstiner('chat:message:read', ['\App\SocketIO\Events\Messages', 'read']);
    $e->addEventLinstiner('chat:message:view', ['\App\SocketIO\Events\Messages', 'view']);
    $e->addEventLinstiner('chat:message:edit', ['\App\SocketIO\Events\Messages', 'edit']);
    $e->addEventLinstiner('chat:message:delete', ['\App\SocketIO\Events\Messages', 'delete']);

    // video requests
    $e->addEventLinstiner('video:request:get', ['\App\SocketIO\Events\Video', 'get']);
    $e->addEventLinstiner('video:request:create', ['\App\SocketIO\Events\Video', 'create']);
    $e->addEventLinstiner('video:request:reply', ['\App\SocketIO\Events\Video', 'reply']);
    $e->addEventLinstiner('video:request:cancel', ['\App\SocketIO\Events\Video', 'cancel']);
    $e->addEventLinstiner('video:request:reject', ['\App\SocketIO\Events\Video', 'reject']);

    // contacts
    $e->addEventLinstiner('contacts:add', ['\App\SocketIO\Events\Contacts', 'add']);
    $e->addEventLinstiner('contacts:cancel', ['\App\SocketIO\Events\Contacts', 'cancel']);

    // finance
    $e->addEventLinstiner('finance:charges', ['\App\SocketIO\Events\Finance', 'charges']);

    // utils
    $e->addEventLinstiner('utils:link:preview', ['\App\SocketIO\Events\Utils', 'linkPreview']);
    $e->addEventLinstiner('utils:check:username', ['\App\SocketIO\Events\Utils', 'checkUsername']);
});


Worker::runAll();
