<?php

declare(strict_types=1);

namespace App\SocketIO;

use App\Contacts;
use App\Crypto;
use App\Helpers\KeyMessages;
use App\Models\Chats\Members;
use App\Models\User\Sessions;
use App\SocketIO\Events\Event;
use App\Tokenz;
use App\User;
use Illuminate\Support\Facades\Log;
use Throwable;

// phpcs:disable SlevomatCodingStandard.TypeHints.PropertyTypeHint.MissingAnyTypeHint
class Events
{

    public $socket;
    public $io;

    public function __construct(object $socket, $io)
    {
        $this->socket = $socket;
        $this->io = $io;
    }

    public function afterAuthorizedConnection(): void
    {
        User::getUserById($this->socket->user_id)->update([
            'guuid' => $this->socket->id,
            'is_online' => true,
        ]);

        Tokenz::findByAccessToken($this->socket->authtoken)->update([
            'session_id' => $this->socket->id,
            'is_online' => true,
        ]);

        $self = $this;

        $this->online();

        $this->socket->on('online', function () use ($self): void {
            echo 'online event ';
            $self->online();
        });

        $this->socket->on('offline', function () use ($self): void {
            $self->offline();
        });

        $this->socket->on('disconnect', function () use ($self): void {
            $self->afterSocketDisconnect();
        });
    }

    public function online(): void
    {
        $this->socket->session_id = Sessions::insertGetId([
            'user_id' => $this->socket->user_id,
            'token_id' => $this->socket->token_id,
            'session_id' => $this->socket->id,
            'start_at' => time(),
        ]);

        $this->socket->join(KeyMessages::toChatCryptoInt($this->socket->user_id));
        $this->socket->join($this->socket->id);
        $this->socket->join($this->socket->calltoken);

        foreach ($this->getAuthorizedUserContact() as $id) {
            $this->dispatch(
                'user:online',
                ['id' => $this->socket->user_id, 'timestamp' => time()],
                KeyMessages::toChatCryptoInt((int) $id)
            );
        }

        foreach ($this->getAuthorizedUserDialogs() as $id) {
            $this->dispatch(
                sprintf('%s:peer:online', $id),
                [
                    'id' => $id,
                    'user_id' => $this->socket->user_id,
                    'timestamp' => time(),
                ],
                sprintf('chat:%s', $id)
            );
            $this->socket->join(sprintf('chat:%s', $id));
        }

        $this->io->to($this->socket->id)->emit('session:started', [
            'session_id' => $this->socket->session_id,
        ]);

        echo 'online' . PHP_EOL;
    }

    public function dispatch($event, $data, $room = null): void
    {
        if ($room) {
            $this->io->to($room)->emit($event, $data);
        } else {
            $this->io->emit($event, $data);
        }
    }

    public function offline(): void
    {
        Sessions::where('id', $this->socket->session_id)->update([
            'end_at' => time(),
        ]);

        User::getUserById($this->socket->user_id)->update([
            'is_online' => false,
        ]);

        Tokenz::findByAccessToken($this->socket->authtoken)->update([
            'is_online' => false,
        ]);

        foreach ($this->getAuthorizedUserContact() as $id) {
            $this->dispatch(
                'user:offline',
                ['id' => $this->socket->user_id, 'timestamp' => time()],
                KeyMessages::toChatCryptoInt((int) $id)
            );
        }

        foreach ($this->getAuthorizedUserDialogs() as $id) {
            $this->dispatch(
                sprintf('%s:peer:offline', $id),
                [
                    'id' => $this->socket->user_id,
                    'timestamp' => time(),
                ],
                sprintf('chat:%s', $id)
            );
        }

        echo 'offline' . PHP_EOL;
    }

    public function afterSocketDisconnect(): void
    {
        User::getUserById($this->socket->user_id)->update([
            'guuid' => null,
            'is_online' => 0,
        ]);

        Tokenz::findByAccessToken($this->socket->authtoken)->update([
            'session_id' => null,
            'is_online' => 0,
        ]);

        foreach ($this->getAuthorizedUserContact() as $id) {
            $this->dispatch(
                'user:offline',
                [
                    'id' => $this->socket->user_id,
                    'timestamp' => time(),
                ],
                KeyMessages::toChatCryptoInt((int) $id)
            );
        }

        foreach ($this->getAuthorizedUserDialogs() as $id) {
            $this->dispatch(
                sprintf('%s:peer:offline', $id),
                [
                    'id' => $id,
                    'user_id' => $this->socket->user_id,
                    'timestamp' => time(),
                ],
                sprintf('chat:%s', $id)
            );
        }

        Sessions::where('id', $this->socket->session_id)->update([
            'end_at' => time(),
        ]);

        echo 'disconnect' . PHP_EOL;
    }

    public function addEventLinstiner(string $event, $usrfn): void
    {
        $self = $this;

        $this->socket->on($event, function ($data = [], $fn = null) use ($usrfn, $self): void {
            try {
                if (array_key_exists('payload', $data)) {
                    $data = Crypto::decrypt($data['payload'], $self->socket->authtoken);
                }
                $data = (object) $data;
                call_user_func_array($usrfn, [$data, $self->socket, $self->io, $fn]);
            } catch (Throwable $ex) {

                $fn(Event::failResponse($self->socket->authtoken, [
                    'message' => $ex->getMessage(),
                ]));

                $traceError = $ex->getTraceAsString() . PHP_EOL;
                $traceError .= $ex->getMessage() . PHP_EOL;
                $traceError .= $ex->getFile() . PHP_EOL;
                $traceError .= $ex->getLine() . PHP_EOL;

                Log::error($traceError);
            }
        });
    }

    /**
     * @return array<int>
     */
    private function getAuthorizedUserContact(): array
    {
        $result = Contacts::select('target_id')->where('owner_id', $this->socket->user_id)
            ->get()->toArray();
        return array_column($result, 'target_id');
    }

    /**
     * @return array<int>
     */
    private function getAuthorizedUserDialogs(): array
    {
        $result = Members::select('dialog_id')
            ->where('user_id', $this->socket->user_id)
            ->where('is_deleted', 0)
            ->where('is_blocked', 0)
            ->get()->toArray();
        return array_column($result, 'dialog_id');
    }

}
