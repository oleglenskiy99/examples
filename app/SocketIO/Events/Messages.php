<?php

declare(strict_types=1);

namespace App\SocketIO\Events;

use App\Errors;
use App\Helpers\KeyMessages;
use App\Models\Chats\Chat;
use App\Models\Chats\Members;
use App\Models\Chats\Messages as Message;
use App\Repository\Chats\MembersRepository;
use App\Repository\Chats\MessagesRepository;
use App\Repository\User\PricesRepository;
use App\Services\V1\MessageService;
use Closure;

class Messages extends Event
{

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function get(object $request, object $socket, $io, Closure $fn): void
    {
        $chat = Chat::where('id', (int) $request->id)->first();

        $messages = MessagesRepository::get((int) $chat->id, $socket->user_id)
            ->orderBy('id', 'desc')
            ->limit(100)
            ->get();

        $fn(self::okResponse($socket->authtoken, array_reverse($messages->toArray())));
    }

    public static function vector(object $request, object $socket, $io, Closure $fn): void
    {
        $top = MessagesRepository::get((int) $request->c, $socket->user_id)
            ->where('id', '<', (int) $request->i)
            ->orderBy('id', 'desc')
            ->limit($request->l)->get()->toArray();

        $middle = MessagesRepository::get((int) $request->c, $socket->user_id)
            ->where('id', '>=', (int) $request->i)
            ->limit($request->l)->get()->toArray();

        $fn([
            'top' => $top,
            'middle' => $middle,
        ]);
    }

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function getHistory(object $request, object $socket, $io, Closure $fn): void
    {
        $limit = isset($request->exclude) ? $request->limit : round((int) $request->limit / 2);

        $offset_id = isset($request->offset_id) ? (int) $request->offset_id : 0;

        $top = MessagesRepository::get((int) $request->id, $socket->user_id)
            ->where('id', '<', $offset_id)
            ->orderBy('id', 'desc')
            ->limit($limit)->get();

        $middle = isset($request->exclude)
            ? []
            : MessagesRepository::get((int) $request->id, $socket->user_id)
                ->where('id', '>', $offset_id)
                ->limit($limit)->get();

        $fn(self::okResponse($socket->authtoken, [
            'top' => array_reverse($top->toArray()),
            'middle' => isset($request->exclude) ? [] : $middle->toArray(),
        ]));
    }

    public static function state(object $request, object $socket, $io, Closure $fn): void
    {
        $message = Message::select(['id', 'message', 'is_read', 'is_paid', 'is_edit', 'is_read', 'views'])
            ->where('id', (int) $request->id)
            ->where('is_deleted', false)
            ->whereRaw(
                sprintf(
                    'id NOT IN (SELECT message_id FROM module_chats_messages_deleted WHERE user_id = %s)',
                    $socket->user_id
                )
            )
            ->first();

        if ($message) {
            $fn(self::okResponse($socket->authtoken, $message));
        } else {
            $fn(self::failResponse($socket->authtoken));
        }
    }

    public static function sent(object $request, object $socket, $io, Closure $fn): void
    {
        if (isset($request->total_upload_count)) {
            unset($request->total_upload_count);
        }
        $currentTime = time();
        if (isset($request->time)) {
            $currentTime = strtotime('now ' . $request->time);
            unset($request->time);
        }

        /** @var Chat $chat */
        $chat = Chat::where('id', (int) $request->dialog_id)->first();

        if ($chat === null) {
            $request->error = ['code' => 103, 'message' => __('errors.request_resource_error')];
            $io->to(KeyMessages::toChatCrypto($socket->user_id))->emit(KeyMessages::toEmitMessageResponse($request->dialog_id), $request);
            $fn(self::failResponse($socket->authtoken), json_decode(json_encode($request), true));
            return;
        }

        /** @var Members $peer */
        $peer = MembersRepository::peer($socket->user_id, $chat->id)->first();

        $message = new Message();

        if ($chat->expert_id === $socket->user_id) {
            //is expert
            $message->isNeedPayment = false;
            $message->isNeedPayout = true;
            $message->is_paid = true;
            $message->cost = 0;
        } else {
            $userDefaultMessagePrice = PricesRepository::getPriceObjectForUserType(
                $chat->expert_id,
                PricesRepository::CATEGORY_MESSAGE
            )['value'];

            // We take amount that was sent from front-end side or default user amount per message
            $amount = ($request->cost ? $request->cost->value : $userDefaultMessagePrice );
            // If the transferred or default value of the amount is zero,
            // then we return an error so that there is no possible to sending free messages
            if ( $amount <= 0) {
                $request->error = ['code' => 5, 'message' => __('errors.wrong_request')];
                $io->to(KeyMessages::toChatCrypto($socket->user_id))->emit(
                    KeyMessages::toEmitMessageResponse($chat->id),
                    $request
                );
                $fn(self::failResponse($socket->authtoken), json_decode(json_encode($request), true));
                return;
            }
            $request->cost->value = $amount;

            $message->isNeedPayment = true;
            $message->isNeedPayout = false;
            $message->is_paid = false;
            $message->cost = (float) $amount;
        }

        $message->dialog_id = $chat->id;
        $message->user_id = $socket->user_id;
        $message->reply = isset($request->reply) ? $request->reply->id : null;
        $message->guuid = $request->guuid;
        $message->message = $request->text;
        $message->type = $request->type;
        $message->timestamp = $currentTime;
        $message->save();

        $request->id = $message->id;
        $request = self::messageService()->setAttachment($request);

        if ($message->isNeedPayout) {
            if (self::messageService()->payoutMessages($chat->id, $socket->user_id) === false) {
                $request->error = [
                    'code' => 103,
                    'message' => Errors::get(103),
                ];
                $fn(self::failResponse($socket->authtoken, json_decode(json_encode($request), true)));
                return;
            }
            self::log($socket, 'chat:message:reply', $message->id);
        }

        if ($message->isNeedPayment) {
            $request->charge_id = self::messageService()->paymentMessages($message);
            if ( $request->charge_id === null) {
                $request->error = [
                    'code' => 103,
                    'message' => Errors::get(103),
                ];
                $fn(self::failResponse($socket->authtoken, json_decode(json_encode($request), true)));
                return;
            } else {
                $request->is_paid = true;
                self::log($socket, 'chat:message', $message->id);

                $peer->increment('unread_messages_count', 1);
                $io->to(KeyMessages::toChatDialog($chat->id))->emit(
                    KeyMessages::toEmitMessageSent($chat->id),
                    $request
                );
            }
            self::messageService()->notify($message->toArray(), $peer->user_id);
            $fn(self::okResponse($socket->authtoken, ['message' => $request]));
        } else {
            $peer->increment('unread_messages_count', 1);
            $io->to(KeyMessages::toChatDialog($chat->id))->emit(
                KeyMessages::toEmitMessageSent($chat->id),
                $request
            );
            self::messageService()->notify($message->toArray(), $peer->user_id);
            $fn(self::okResponse($socket->authtoken, ['message' => $request]));
        }
    }

    public static function pay(object $request, object $socket, $io, Closure $fn): void
    {
        /** @var Message $message */
        $message = Message::where('id', (int) $request->id)->first();

        /** @var Chat $chat */
        $chat = Chat::where('id', $message->dialog_id)->first();

        /** @var Members $peer */
        $peer = MembersRepository::peer($socket->user_id, $chat->id)->first();

        if ($message->is_paid) {
            self::messageService()->notify($message->toArray(), $peer->user_id);
            $fn(self::okResponse($socket->authtoken, []));
            return;
        }
        $chargeId = self::messageService()->paymentMessages($message);
        if ( $chargeId === null) {
            $fn(self::failResponse($socket->authtoken, [
                'code' => 103,
                'message' => Errors::get(103),
            ]));
            return;
        }
        self::log($socket, 'chat:message', $message->id);
        if (isset($message->error)) {
            unset($message->error);
        }
        //after payment refresh message object
        $messageRes = Message::where('id', $message->id)->first();
        $io->to(KeyMessages::toChatDialog($chat->id))->emit(
            KeyMessages::toEmitMessageSent($chat->id),
            $messageRes->toArray()
        );
        self::messageService()->notify($messageRes->toArray(), $peer->id);
        $fn(self::okResponse($socket->authtoken, []));
    }

    public static function read(object $request, object $socket, $io, Closure $fn): void
    {
        $ids = explode(',', $request->ids);

        MessagesRepository::setRead($ids, (int) $request->chat, $socket->user_id);

        $io->to(KeyMessages::toChatDialog($request->chat))->emit(KeyMessages::toEmitMessageRead($request->chat), [
            'ids' => explode(',', $request->ids),
            'user_id' => $socket->user_id,
            'chat' => $request->chat,
        ]);
        $fn(self::okResponse($socket->authtoken, []));
    }

    public static function view(object $request, object $socket, $io, Closure $fn): void
    {
        $ids = explode(',', $request->ids);

        MessagesRepository::setView($ids, (int) $request->chat, $socket->user_id);

        $io->to(KeyMessages::toChatDialog($request->chat))->emit(KeyMessages::toEmitMessageView($request->chat), [
            'ids' => explode(',', $request->ids),
            'user_id' => $socket->user_id,
            'chat' => $request->chat,
        ]);
        $fn(self::okResponse($socket->authtoken, []));
    }

    public static function edit(object $request, object $socket, $io, Closure $fn): void
    {
        MessagesRepository::setEdit((int) $request->id, $request->text);

        $io->to(KeyMessages::toChatDialog($request->chat))->emit(KeyMessages::toEmitMessageEdit($request->chat), [
            'id' => $request->id,
            'text' => $request->text,
        ]);
        $fn(self::okResponse($socket->authtoken, []));
    }

    public static function delete(object $request, object $socket, $io, Closure $fn): void
    {
        // Delete message from all chat users
        $deleted = self::messageService()->deleteOther((array) $request->ids);
        // phpcs:disable SlevomatCodingStandard.ControlStructures.EarlyExit.EarlyExitNotUsed
        if ($deleted !== null && count($deleted) > 0) {
            // Sending event on front-end side
            $io->to(KeyMessages::toChatDialog($request->chat))->emit(KeyMessages::toEmitMessageDelete($request->chat), [
                'ids' => $deleted,
            ]);
        } else {
            $request->error = ['code' => 104, 'message' => __('errors.cancel_payment_error')];
            $io->to(KeyMessages::toChatCrypto($socket->user_id))->emit(KeyMessages::toEmitMessageResponse($request->chat), $request);
            $fn(self::failResponse($socket->authtoken));
            return;
        }
        $fn(self::okResponse($socket->authtoken, []));
    }

    private static function messageService(): MessageService
    {
        return app(MessageService::class);
    }

}
