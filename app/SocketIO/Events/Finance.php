<?php

declare(strict_types=1);

namespace App\SocketIO\Events;

use App\Models\User\Charges;
use App\User;
use Closure;

class Finance extends Event
{

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function charges(object $request, object $socket, $io, Closure $fn): void
    {
        $charges = Charges::where('customer', User::select('stripe_customer_id')
            ->where('id', $socket->user_id)
            ->value('stripe_customer_id'))
                    ->orderBy('created', 'DESC')
            ->paginate(30, ['*'], 'next_max_id');

        $fn(self::okResponse($socket->authtoken, [
            'total_count' => $charges->total(),
            'is_more_available' => $charges->hasMorePages(),
            'next_max_id' => $charges->hasMorePages() ? encrypt($charges->currentPage() + 1) : null,
            'items' => $charges->items(),
        ]));
    }

}
