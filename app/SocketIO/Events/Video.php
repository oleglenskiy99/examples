<?php

declare(strict_types=1);

namespace App\SocketIO\Events;

use App\Events\Notify\CancelRequestVideo;
use App\Events\Notify\NewRequestVideo;
use App\Events\Notify\NotApprovedRequest;
use App\Events\Notify\ReplyRequestVideo;
use App\Jobs\Notifications;
use App\Repository\User\UsersRepository;
use App\Repository\VideoRepository;
use App\Services\PaymentServiceInterface;
use App\User;
use App\Video as VideoModel;
use Closure;
use Throwable;

class Video extends Event
{

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function get(object $request, object $socket, $io, Closure $fn): void
    {
        $user_id = $socket->user_id;
        $builder = VideoRepository::builder();
        $builder = $builder->where('user_id', $user_id)->orWhere(function ($q) use ($user_id): void {
            $q->where('target_id', $user_id)->where('is_completed', false)->where('is_deleted', false);
        });

        $videos = $builder->get()->toArray();
        foreach ($videos as $k => $video) {
            $video['group'] = 'video';

            if ($video['user_id'] === $user_id) {
                $video['user'] = $video['target'];
                $video['section'] = 'outgoing';
            } else {
                $video['user'] = $video['owner'];
                $video['section'] = 'incoming';
            }
            unset($video['target']);
            unset($video['owner']);
            $videos[$k] = $video;
        }

        $fn(self::okResponse($socket->authtoken, $videos));
    }

    public static function reject(object $request, object $socket, $io, Closure $fn): void
    {
        $video = VideoModel::select('*')
            ->where('id', $request->id)
            ->where('target_id', $socket->user_id)
            ->first();

        if ($video === null) {
            $fn(self::failResponse($socket->authtoken, [
                'message' => __('errors.resource_not_found'),
            ], 17));
            return;
        }

        if ($video->is_completed) {
            $fn(self::failResponse($socket->authtoken), [
                'message' => __('errors.video_completed_cancel'),
            ], -1);
            return;
        }

        if ($video->is_deleted) {
            $fn(self::failResponse($socket->authtoken, [
                'message' => __('errors.video_deleted_cancel'),
            ], 17));
            return;
        }

        if ($video->charge_id !== null) {
            try {
                self::getPayments()->cancelPayment(User::getUserById($video->user_id), $video->charge_id);
            } catch (Throwable $exception) {
                $fn(
                    self::failResponse($socket->authtoken, [
                        'message' => __('errors.cancel_payment_error'),
                    ], 104)
                );
            }
        }
        $video->update([
            'is_deleted' => true,
        ]);

        self::dispatch($io, 'video:rejected', [
            'id' => $video->id,
        ], sha1((string) $video->user_id));

        //update user Event;
        NotApprovedRequest::dispatch($video->user_id, $video->target_id);

        self::log($socket, 'video:reject', $video->id);

        $fn(self::okResponse($socket->authtoken));
    }

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function cancel(object $request, object $socket, $io, Closure $fn): void
    {
        $video = VideoModel::select('*')
            ->where('id', $request->id)
            ->where('user_id', $socket->user_id)
            ->first();

        if ($video === null) {
            $fn(self::failResponse($socket->authtoken, [
                'message' => __('errors.resource_not_found'),
            ], 17));
            return;
        }

        if ($video->is_completed) {
            $fn(self::failResponse($socket->authtoken), [
                'message' => __('errors.video_completed_cancel'),
            ], -1);
            return;
        }

        if ($video->is_deleted) {
            $fn(self::failResponse($socket->authtoken, [
                'message' => __('errors.video_deleted_cancel'),
            ], 17));
            return;
        }

        if ($video->charge_id !== null) {
            try {
                self::getPayments()->cancelPayment(User::getUserById($video->user_id), $video->charge_id);
            } catch (Throwable $exception) {
                $fn(
                    self::failResponse($socket->authtoken, [
                        'message' => __('errors.cancel_payment_error') . ' ' . $exception->getMessage(),
                    ], 104)
                );
            }
        }
        $video->update([
            'is_deleted' => true,
        ]);

        $io->to(sha1((string) $video->target_id))->emit('video:canceled', ['id' => $video->id]);

        //Event CancelRequest;
        CancelRequestVideo::dispatch($video->target_id, $video->user_id);

        $fn(self::okResponse($socket->authtoken));

        self::log($socket, 'video:cancel', $video->id);
    }

    public static function reply(object $request, object $socket, $io, Closure $fn): void
    {
        $video = VideoModel::select('*')
            ->where('id', $request->id)
            ->where('is_completed', false)
            ->first();

        if ($video === null) {
            $fn(self::failResponse($socket->authtoken, [
                'message' => __('errors.resource_not_found'),
            ], 17));
            return;
        }

        try {
            self::getPayments()->capturePayment(User::getUserById($video->target_id), $video->charge_id);
        } catch (Throwable $exception) {
            $fn(self::failResponse($socket->authtoken, [
                'message' => __('errors.payment_error') . $exception->getMessage(),
            ], 103));
            return;
        }

        $video->update([
            'is_viewed' => true,
            'is_completed' => true,
            'media' => $request->media,
        ]);

        $io->to(sha1((string) $video->user_id))->emit('video:reply', ['video' => VideoModel::getById($video->id)]);

        $job = (new Notifications('video:reply', [
            'id' => $video->id,
            'from' => UsersRepository::getUserProfile((string) $socket->user_id),
            'target' => $video->user_id,
        ]));
        dispatch($job);
        //Event ReplyRequest;
        ReplyRequestVideo::dispatch($video->user_id, $video->target_id);

        $fn(self::okResponse($socket->authtoken));

        self::log($socket, 'video:reply', $video->id);
    }

    public static function create(object $request, object $socket, $io, Closure $fn): void
    {
        $user = User::getUserById($socket->user_id);

        $video = new VideoModel();
        $video->user_id = $socket->user_id;
        $video->type = $request->type;
        $video->target_id = (int) $request->target_id;

        if (isset($request->category)) {
            $video->category = (int) $request->category;
        }
        $video->instruction = $request->instruction;
        $video->deadline = strtotime('1 hour');
        $amount = $request->cost ?? self::getPayments()->getCategoryPrice(
            $user,
            PaymentServiceInterface::CATEGORY_VIDEO
        );

        if ( $amount <= 0) {
            $fn(self::failResponse($socket->authtoken), ['message' => __('errors.wrong_request')], 5);
            return;
        }

        $video->cost = (int) $amount;

        try {
            $video->charge_id = self::getPayments()->createPayment(
                $user,
                true,
                PaymentServiceInterface::CATEGORY_VIDEO,
                PaymentServiceInterface::CURRENCY_USD,
                $amount
            );
            $video->is_pay = true;
            $video->is_privacy = $request->is_privacy;
            $video->save();
        } catch (Throwable $exception) {
            $fn(self::failResponse($socket->authtoken), ['amount' => $amount, 'message' => $exception->getMessage()], 103);
            return;
        }

        $io->to(sha1((string) $video->target_id))->emit('video:request', ['video' => VideoModel::getById($video->id)]);

        $job = (new Notifications('video:request', [
            'id' => $video->id,
            'from' => User::getUserById($socket->user_id),
            'target' => $video->target_id,
        ]));
        dispatch($job);
        //Event NewRequestVideo;
        NewRequestVideo::dispatch($video->target_id, $video->user_id);

        $result = VideoModel::getById($video->id, 'outgoing');

        $fn(self::okResponse($socket->authtoken, $result));

        self::log($socket, 'video:create', $video->id);
    }

    private static function getPayments(): PaymentServiceInterface
    {
        return app(PaymentServiceInterface::class);
    }

}
