<?php

declare(strict_types=1);

namespace App\SocketIO\Events;

use App\User;
use App\Utils\LinkPreview;
use App\Utils\LinkPreview\Models\VideoLink;
use Closure;
use Throwable;

class Utils extends Event
{

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function checkUsername(object $request, object $socket, $io, Closure $fn): void
	{
        $user = User::where('domain', strtolower($request->domain))->where('id', '!=', $socket->user_id)->first();

        if ($user) {
			$fn(self::failResponse($socket->authtoken));
            return;
        }

        $fn(self::okResponse($socket->authtoken));
    }

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function linkPreview(object $request, object $socket, $io, Closure $fn): void
	{
        $linksData = [];

        foreach ($request as $link) {
            try {

                $linkPreview = new LinkPreview($link);
                $parsed = $linkPreview->getParsed();

                foreach ($parsed as $linkData) {

                        $item = [
                            'title' => $linkData->getTitle(),
                            'description' => $linkData->getDescription(),
                            'image' => $linkData->getImage(),
                            'url' => $link,
                            'realurl' => $linkData->getRealUrl(),
                        ];

                        if ( $linkData instanceof VideoLink) {
                            $item['video'] = [
                                'id' => $linkData->getVideoId(),
                                'video' => $linkData->getVideo(),
                                'embedcode' => $linkData->getEmbedCode(),
                            ];
                        }

                        $linksData[] = $item;

                }

            } catch (Throwable $ex) {
                $fn(self::failResponse($socket->authtoken));
            }
        }

        $fn(self::okResponse($socket->authtoken, $linksData));
    }

}
