<?php

declare(strict_types=1);

namespace App\SocketIO\Events;

use App\Services\V1\ContactService;
use Closure;

class Contacts extends Event
{

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function add(object $request, object $socket, $io, Closure $fn): void
    {
        $contact = self::getContacts()->add((int) $request->id, $socket->user_id);
        $fn(self::okResponse($socket->authtoken, $contact));
    }

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function cancel(object $request, object $socket, $io, Closure $fn): void
    {
        self::getContacts()->cancel((int) $request->id);
        $fn(self::okResponse($socket->authtoken));
    }


    private static function getContacts(): ContactService
    {
        return app(ContactService::class);
    }

}
