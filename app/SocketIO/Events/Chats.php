<?php

declare(strict_types=1);

namespace App\SocketIO\Events;

use App\Crypto;
use App\Events\Notify\NewRequest;
use App\Helpers\KeyMessages;
use App\Models\Chats\Members;
use App\Repository\Chats\ChatsRepository;
use Closure;

class Chats
{

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function created(object $request, object $socket, $io, Closure $fn): void
	{
        /** @var Members $peer */
        $peer = Members::where([
            ['user_id', '!=', $socket->user_id],
            ['dialog_id', $request->chat->id],
        ])->first();

        $itemForPeerUser = ChatsRepository::getItemById($peer->dialog_id, $peer->user_id);

        $io->to(KeyMessages::toChatCryptoInt($peer->user_id))->emit('chats:created', $itemForPeerUser);
        $io->to(KeyMessages::toChatCryptoInt($socket->user_id))->emit('chats:created', $request->chat);

        NewRequest::dispatch($peer->user_id, $socket->user_id);
    }

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function update(object $request, object $socket, $io, Closure $fn): void
	{
        $items = ChatsRepository::items($socket->user_id, 0, false);

        $response = Crypto::encrypt(json_encode([
            'pinned' => [],
            'items' => $items,
        ]), $socket->authtoken);

        $fn([
            'payload' => $response,
        ]);
    }

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function details(object $request, object $socket, $io, Closure $fn): void
	{
        $chats = [];

        foreach ($request->ids as $id) {
            $item = ChatsRepository::getItemById($id, $socket->user_id);
            $chats[$id] = $item;
        }

        $io->to($socket->id)->emit('chats:details', $chats);
    }

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function subscribe(object $request, object $socket, $io, Closure $fn): void
	{
        $chat_id = $request->chat_id;
        $socket->join(
            sprintf('chat:%s', $chat_id)
        );
    }

}
