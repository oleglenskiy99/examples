<?php

declare(strict_types=1);

namespace App\SocketIO\Events;

use App\Crypto;
use App\Errors;
use App\Models\User\Sessions\Activity;
use Illuminate\Support\Facades\DB;

class Event
{

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function log($socket, $action, $source_id = -1): void
	{
        Activity::store($action, $socket->user_id, $socket->session_id, $source_id);
    }

    /**
     * @return array<string>
     */
    public static function getAuthorizedUserDialogs(int $userId): array
	{
        $result = DB::table('module_chats_users')
            ->select('dialog_id')
            ->where('user_id', $userId)
            ->where('is_deleted', 0)
            ->where(
                'is_blocked',
                0
            )->get()->toArray();

        return array_column($result, 'dialog_id');
    }

    /**
     * @return array<string>
     */
    public static function getAuthorizedUserContact(int $userId): array
	{
        $result = DB::table('module_users_contacts')
            ->select('target_id')
            ->where('owner_id', $userId)
            ->get()->toArray();

        return array_column($result, 'target_id');
    }

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public static function dispatch($io, $event, $data, $room = null): void
	{
        if ( $room) {
            $io->to($room)->emit($event, $data);
        } else {
            $io->emit($event, $data);
        }
    }

    /**
     * @return array<string>
     */
    public static function okResponse(string $passphrase, array $data = []): array
	{
        $payload = Crypto::encrypt(json_encode([
            'status' => 'ok',
            'result' => $data,
        ]), $passphrase);

        return ['payload' => $payload];
    }

    /**
     * @return array<string>
     */
    public static function failResponse(string $passphrase, array $data = [], int $code = 1): array
	{
        if (empty($data['message'])) {
            if ( Errors::get($code)) {
                $data['message'] = Errors::get($code);
                if ( $code === 1) {
                    $data['hiden'] = true;
                }
            }
        }

        $data['code'] = $code;
        $data['status'] = 'fail';

        $payload = Crypto::encrypt(json_encode(array_reverse($data)), $passphrase);

        return ['payload' => $payload];
    }

}
