<?php

declare(strict_types=1);

namespace App\Repository\Requests;

use App\User;
use App\Video;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class VideoRepository
{

    public static function getList(User $user): Collection
	{
        return Video::whereHas('target')
            ->whereHas('owner')
            ->with(['target', 'owner'])
            ->with('media')
            ->where('is_deleted', false)
            ->where('user_id', $user->id)
            ->orWhere(function ($q) use ($user): void {
                $q->where([
                    [ 'target_id', $user->id ],
                    [ 'is_completed', false ],
                    [ 'is_deleted', false ],
                ]);
            })
            ->orderBy('updated_at', 'DESC')
            ->get()
            ->each(function ($video) use ($user): void {
                $video->makeHidden([
                    'owner',
                    'target',
                    'user_id',
                    'target_id',
                    'deadline',
                    'type',
                    'friend_name',
                    'category',
                    'is_pay',
                    'is_completed',
                    'is_deleted',
                    'is_live',
                    'is_privacy',
                    'charge_id',
                ]);

                if ( $video->target->id === $user->id) {
                    $video->setAttribute('user', $video->owner);
                } else {
                    $video->setAttribute('user', $video->target);
                }
            })->map(function ($video) use ($user) {
                return [
                    'id' => $video->id,
                    'user' => [
                        'id' => $video->user->id,
                        'full_name' => trim($video->user->firstName . ' ' . $video->user->lastName),
                        'photo' => $video->user->photo,
                    ],
                    'message' => $video->instruction,
                    'media' => $video->media,
                    'timestamp' => $video->updated_at,
                    'cost' => $video->cost,
                    'outgoing' => $video->user_id === $user->id,
                    'is_responded' => isset($video->media),
                    'is_viewed' => $video->is_viewed,
                    'days_left' => Carbon::parse($video->created_at)->addDays(6)->longAbsoluteDiffForHumans(),
                    'type' => 'video',
                ];
            });
    }

    public static function getById(User $user, int $id): Video
	{
        return static::getList($user)->firstWhere('id', $id);
    }

}
