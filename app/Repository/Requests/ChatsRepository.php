<?php

declare(strict_types=1);

namespace App\Repository\Requests;

use App\Models\Chats\Chat;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class ChatsRepository
{

    // phpcs:disable Squiz.Arrays.ArrayDeclaration.NoKeySpecified
    public static function getList(User $user): Collection
	{
        return Chat::whereHas('last_message', function ($q) use ($user): void {
            $q->where('is_paid', true)->orWhere('user_id', $user->id)->where('is_deleted', false);
        })
            ->whereHas('info', function ($q) use ($user): void {
                $q->where('user_id', $user->id)->where('is_deleted', false);
            })
            ->with([
                'last_message' => function ($message): void {
                    $message->whereHas('user')->with([
                        'user' => function ($user): void {
                            $user->select('id', 'firstName', 'lastName', 'photo');
                        },
                    ])->orderBy('id', 'DESC');
                },
                'creator',
                'expert',
                'user' => function ($q) use ($user): void {
                    $q->join('module_users', 'user_id', 'module_users.id')
                        ->join('module_users_about', 'user_id', 'module_users_about.id')
                        ->where('user_id', '!=', $user->id);
                },
                'info' => function ($info) use ($user): void {
                    $info->where('user_id', $user->id);
                },
            ])
            ->get()
            ->each(function ($item): void {
                $item->setAttribute('is_pinned', $item->getIsPinnedAttribute());
                $item->setAttribute('is_muted', $item->getIsMutedAttribute());
                $item->setAttribute('unread_count', $item->getUnreadCountAttribute());
                $item->setAttribute('balance', $item->getBalanceActualAttribute());
                $item->setAttribute('peer', $item->getPeerAttribute());

                $item->makeHidden(['info', 'creator', 'expert']);
            })
            ->map(function ($chat) use ($user) {
                return [
                    'id' => $chat->id,
                    'user' => [
                        'id' => $chat->peer['id'],
                        'full_name' => $chat->peer['name'],
                        'photo' => $chat->peer['image'],
                    ],
                    'title' => $chat->title,
                    'message' => $chat->last_message->text,
                    'timestamp' => $chat->last_message->timestamp,
                    'cost' => $chat->last_message->cost,
                    'outgoing' => $chat->creator_id === $user->id,
                    'is_responded' => $chat->last_message->user->id !== $user->id,
                    'is_viewed' => $chat->last_message->views > 0,
                    'days_left' => Carbon::parse($chat->last_message->timestamp)->addDays(6)->longAbsoluteDiffForHumans(),
                    'type' => 'chat',
                ];
            });
    }

    public static function getById(User $user, int $id): Chat
	{
        return static::getList($user)->firstWhere('id', $id);
    }

}
