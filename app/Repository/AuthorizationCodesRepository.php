<?php

declare(strict_types=1);

namespace App\Repository;

use App\Models\AuthorizationCodes;

class AuthorizationCodesRepository
{

    /**
     * Create a new code for authorizathion.
     *
     * @param string $codestring
     * @return AuthorizationCodes
     */
    public static function createCode(string $codestring): AuthorizationCodes
    {
        $codeRand = getRandCode($codestring);

        $codes = new AuthorizationCodes();
        $codes->identification = $codeRand['identification'];
        $codes->additional = $codestring;
        $codes->code = $codeRand['code'];
        $codes->save();

        return $codes;
    }

    /**
     * Create a new code for authorizathion.
     *
     * @param array $params
     * @return \App\Models\AuthorizationCodes || null
     */
    public static function getCode(array $params): ?AuthorizationCodes
    {
        $codes = AuthorizationCodes::select('identification', 'additional', 'code');

        if (isset($params['identification']) && !empty($params['identification'])) {
            $codes = $codes->where('identification', $params['identification']);
        }
        if (isset($params['additional']) && !empty($params['additional'])) {
            $codes = $codes->where('additional', $params['additional']);
        }
        if (isset($params['code']) && !empty($params['code'])) {
            $codes = $codes->where('code', $params['code']);
        }

        return $codes->first();
    }

}
