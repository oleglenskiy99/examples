<?php

declare(strict_types=1);

namespace App\Repository\User;

use App\Events\User\RegisterUserEvent;
use App\Events\User\SetCreatorUserEvent;
use App\Events\User\UnsetCreatorUserEvent;
use App\Exceptions\API\EmailAddressExistsException;
use App\Helpers\SocialUserData;
use App\Helpers\UserHelper;
use App\Models\User\Rating;
use App\Roles;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Throwable;

class UsersRepository
{

    public function getUserById(int $id): User
    {
        return User::where('id', $id)->firstOrFail();
    }

    public function createNewUserByEmailPassword(string $email, string $password): User
    {
        $user = User::where('email', 'ILIKE', $email)->first();

        if ( $user) {
            throw new EmailAddressExistsException();
        }

        /** @var User $user */
        $user = User::create([
            'email' => $email,
            'password' => Hash::make($password),
            'balance' => 0,
            'is_blocked' => false,
            'is_verify' => true,
            'is_email_verify' => false,
            'onboarding' => 'name',
            'last_activity' => time(),
        ]);

        $user->update([
            'domain' => UserHelper::getDomainName($user->id),
        ]);

        RegisterUserEvent::dispatch($user->id);

        return $user;
    }

    public function createNewUserBySocialNetwork(SocialUserData $userSocialData): User
    {
        /** @var User $user */
        $user = User::create([
            'email' => $userSocialData->getEmail(),
            'balance' => 0,
            'is_blocked' => false,
            'is_verify' => true,
            'is_email_verify' => !empty($userSocialData->getEmail()),
            'onboarding' => 'name',
            'last_activity' => time(),
        ]);

        $user->update([
            'domain' => UserHelper::getDomainName($user->id),
        ]);

        $user->info()->update([
            'firstName' => $userSocialData->getFirstName(),
            'lastName' => $userSocialData->getLastName(),
        ]);

        $user->setAttribute('isNewUser', true);

        RegisterUserEvent::dispatch($user->id);

        return $user;
    }

    public static function setRoleCreator(int $userId): void
    {
        $user = self::builderFiltered()->where('module_users.id', $userId)->first();

        if ($user && self::isSetRoleUserByName($user, UserHelper::CREATOR_ROLE_NAME) === false) {
                $res = [
                    'is_message_set_price' => false,
                    'is_vide_set_price' => false,
                    'soc_media_list' => [],
                    'category_list' => [],
                    'country_list' => [$user->info->country],
                ];
                $user->setRelation('prices', $user->prices->keyBy('key'));
                if (isset($user->prices['message']) && $user->prices['message']->value > 0) {
                    $res['is_message_set_price'] = true;
                }

                if (isset($user->prices['video']) && $user->prices['video']->value > 0) {
                    $res['is_vide_set_price'] = true;
                }

                $linkeds = $user->linkeds()->get()->toArray();
                if (count($linkeds) <= 0) {
                    return;
                }
                $res['soc_media_list'] = array_column($linkeds, 'network_id');

                $categories = $user->categories()->get()->toArray();
                if (count($categories) <= 0) {
                    return;
                }
                $res['category_list'] = array_column($categories, 'id');

                self::setRoleUserByName($user, UserHelper::CREATOR_ROLE_NAME);
                SetCreatorUserEvent::dispatch($user->id, $res);
        } elseif ($user === null) {
            $user = User::getUserById($userId);
            if ($user && self::isSetRoleUserByName($user, UserHelper::CREATOR_ROLE_NAME)) {
                $res = [
                    'is_message_set_price' => false,
                    'is_vide_set_price' => false,
                    'soc_media_list' => [],
                    'category_list' => [],
                    'country_list' => [$user->info->country],
                ];
                $user->setRelation('prices', $user->prices->keyBy('key'));
                if (isset($user->prices['message']) && $user->prices['message']->value > 0) {
                    $res['is_message_set_price'] = true;
                }

                if (isset($user->prices['video']) && $user->prices['video']->value > 0) {
                    $res['is_vide_set_price'] = true;
                }

                $linkeds = $user->linkeds()->get()->toArray();
                if (count($linkeds) > 0) {
                    $res['soc_media_list'] = array_column($linkeds, 'network_id');
                }

                $categories = $user->categories()->get()->toArray();
                if (count($categories) > 0) {
                    $res['category_list'] = array_column($categories, 'id');
                }
                self::unsetRoleUserByName($user, UserHelper::CREATOR_ROLE_NAME);
                UnsetCreatorUserEvent::dispatch($user->id, $res);
            }
        }
    }

    public static function builderFiltered(?int $userId = null, ?int $industryId = null): Builder
    {
        $builder = User::select([
            'module_users.id',
            'firstName',
            'lastName',
            'domain',
            'photo',
            'biography',
            'is_online',
            'is_verify',
            'last_activity',
            'country',
        ])
            ->join('module_users_about', 'module_users_about.id', 'module_users.id')
            ->whereNotNull('country')
            ->whereHas('linkeds', function ($linked): void {
                $linked->where('is_deleted', false);
            })
            ->has('prices')->with('prices')
            ->whereNotNull('photo')
            ->where('biography', '!=', '');

        if ($userId !== null) {
            $builder = $builder->where('module_users.id', '!=', $userId);
        }
        if ($industryId !== null) {
            $builder = $builder->whereHas('categories', function ($q) use ($industryId): void {
                $q->where('module_categories.id', '=', $industryId);
            })->with('categories');
        } else {
            $builder = $builder->has('categories')->with('categories');
        }

        return $builder;
    }

    public static function getUserProfile(string $id, bool $self = false): ?User
    {
        $builder = User::select([
            'module_users.id',
            'domain',
            'firstName',
            'lastName',
            'photo',
            'biography',
            'is_online',
            'last_activity',
            'country',
            '_hsid',
            'onboarding',
            'pseudonym',
        ])
            ->join('module_users_about', 'module_users_about.id', 'module_users.id')
            ->withCount('fans')
            ->withCount('reviews')
            ->with('country')
            ->with('categories')
            ->with([
                'medias' => function ($q): void {
                    $q->orderBy('id', 'DESC');
                },
            ]);

        //if id is only number maybe domain or id
        if (preg_match('/^([0-9])+$/', $id)) {
            $builder = $builder->where(function ($q) use ($id): void {
                $q->where('domain', $id)->orWhere('module_users.id', (int) $id);
            });
        } else {
            $builder = $builder->where('domain', $id);
        }

        if ($self) {
            $builder = $builder->addSelect(['withdraw', 'balance'])->with('cards');
        }

        try {
            $user = $builder->firstOrFail();
            $user->setRelation('prices', $user->prices->keyBy('key'));
            $user->setAttribute('accounts', $user->getLinkedAccounts());
            $user->setAttribute('price', self::getMiddlePrice($user));
            if (!$self) {
                $user->setRelation('acr', $user->fans()->where('sender', session('user_id'))->first());
            }
            return $user;
        } catch (Throwable $exception) {
            return null;
        }
    }

    public static function search(string $q): Builder
    {
        //ормируем поисковую строки вида
        // мгышс:*|music:*&test:*|еуые   каждое слово с измененой раскладкой
        $qa = explode(' ', str_replace('+', ' ', $q));
        $q = '';
        foreach ($qa as $str) {
            if (!empty($str)) {
                $strl = str_replace("'", '', $str);
                $strl = empty($strl) ? '' : $strl . ':*';
                $strt = textSwitcher($str, 2);
                $strt = empty($strt) ? '' : $strt . ':*';
                if (empty($strl) && !empty($strt)) {
                    $q = '(' . $strt . ') & ';
                } elseif (!empty($strl) && empty($strt)) {
                    $q = '(' . $strl . ') & ';
                } else {
                    $q = '(' . $strl . '|' . $strt . ') & ';
                }
            }
        }
        //убираем последний И
        $q = substr($q, 0, -3);

        $vectorString = "setweight(to_tsvector('english',coalesce(\"firstName\",'') || ' ' || coalesce(\"lastName\",'') || ' ' || \"biography\"), 'A')";
        $vectorString .= " || setweight(to_tsvector('english',coalesce(\"domain\",'')), 'B')";
        $vectorString .= " || setweight(to_tsvector('english',coalesce(\"name_ru\",'') || ' ' || coalesce(\"name_en\",'')), 'C')";

        $items_se = DB::select(
            sprintf(
                "select \"module_users\".\"id\", \"domain\", \"firstName\", \"lastName\", \"biography\", \"name_ru\", \"name_en\"
                from \"module_users\"
                inner join \"module_users_about\" on \"module_users_about\".\"id\" = \"module_users\".\"id\"
                left join \"module_users_categories\" ON \"module_users_categories\".\"user_id\" = \"module_users\".\"id\"
                left join \"module_categories\" ON \"module_categories\".\"id\" = \"module_users_categories\".\"category_id\"
                where %s @@ to_tsquery('english','%s') order by ts_rank(%s, to_tsquery('english','%s')) DESC limit 50",
                $vectorString,
                $q,
                $vectorString,
                $q
            )
        );

        $ids = [];
        for ($i = 0; $i < count($items_se); $i++) {
            $id = $items_se[$i]->id;
            if (!in_array($id, $ids)) {
                $ids[] = (int) $items_se[$i]->id;
            }
        }

        return User::select([
            'module_users.id',
            'domain',
            'firstName',
            'lastName',
            'biography',
            'country',
            'photo',
        ])
            ->join('module_users_about', 'module_users_about.id', 'module_users.id')
            ->whereIn('module_users.id', $ids)
            ->orderByRaw(DB::raw(sprintf('array_position(ARRAY[%s]::int[], "module_users"."id"::int)', implode(',', $ids))));
    }

    public static function getMiddlePrice(User $user): ?float
    {
        $price = $user->prices()->where('value', '>', 0)->get();
        if ($price === null) {
            return null;
        }
        return $price->min('value');
    }

    public static function getRating(User $user): float
    {
        $rating = $user->rating()->first();
        if ($rating === null) {
            return 0;
        }
        return (float) $rating->rating;
    }

    public static function setRating(User $user): void
    {
        $reviews = $user->reviews()->get();
        $rating = 0;
        if ($reviews && $reviews->count() > 0) {
            $maxRatingValue  = 5;
            $a = 0.2 * $maxRatingValue;
            $a2 = 0.8 * $maxRatingValue;
            $b = 0.2;
            $result = 0;
            foreach ($reviews as $review) {
                //считаем рейтинг каждого проголосовавшего
                $userrating = User::getUserById($review->owner_id);
                $ratValue = $a * exp(-$b * $review->value);
                //force
                $ratSkill = $a2 * exp(-$b * $userrating->rating);
                $result += log((exp($a2) - 5) / 6 * $ratSkill + 1) + $ratValue;
            }
            $result /= $reviews->count();
            $rating = $result;
        }
        Rating::updateOrCreate(['user_id' => $user->id], ['rating' => $rating]);
    }

    private static function isSetRoleUserByName(User $user, string $rolename): bool
    {
        $role = Roles::getRoleByName($rolename);
        if ($role) {
            if ($user->roles()->where('id', $role->id)->first()) {
                return true;
            }
        }
        return false;
    }

    private static function setRoleUserByName(User $user, string $rolename): void
    {
        $role = Roles::getRoleByName($rolename);
        if ($role) {
            $user->roles()->attach($role->id);
        }
    }

    private static function unsetRoleUserByName(User $user, string $rolename): void
    {
        $role = Roles::getRoleByName($rolename);
        if ($role) {
            $user->roles()->detach($role->id);
        }
    }

}
