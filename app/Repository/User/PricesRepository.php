<?php

declare(strict_types=1);

namespace App\Repository\User;

use App\Models\User\Prices;

class PricesRepository
{

    public const CATEGORY_MESSAGE = 'message';
    public const CATEGORY_VIDEO = 'video';

    public static function savePrice(
        int $userId,
        string $key,
        float $value,
        ?string $appleGoodId = null
    ): ?Prices
    {
        //if there is a price, then need to save it as the previous price
        $oldprice_sum = 0;
        $oldprice = Prices::where('user_id', $userId)->where('key', $key)->first();
        if ($oldprice && $oldprice->value > 0) {
            $oldprice_sum = $oldprice->value;
        }

        return Prices::updateOrCreate([
            'user_id' => $userId,
            'key' => $key,
        ], [
            'key' => $key,
            'value' => $value,
            'prev' => $oldprice_sum,
            'apple_good_id' => $appleGoodId,
            'ticker' => 'USD',
        ]);
    }

    public static function getPriceObjectForUserType(int $id, string $type): array
    {
        $prices = Prices::where('user_id', $id)->where('key', $type)->first();
        if ($prices === null) {
            return ['value' => 0, 'ticker' => 'USD'];
        }
        return $prices->toArray();
    }

}
