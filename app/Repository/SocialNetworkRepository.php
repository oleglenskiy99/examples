<?php

declare(strict_types=1);

namespace App\Repository;

use App\Helpers\SocialUserData;
use App\Models\Network;
use App\Models\User\Linked;
use App\User;
use Laravel\Socialite\Facades\Socialite;

class SocialNetworkRepository
{

    public function getSocialAccountForUser(int $id, string $external_user_id, User $user): ?Linked
    {
        return Linked::where([
            ['network_id', $id],
            ['external_id', $external_user_id ],
            ['user_id', $user->id ],
        ])->firstOrFail();
    }

    public function getSocialAccountByNetworkId(int $id, string $external_user_id): ?Linked
    {
		return Linked::where([
            ['network_id', $id ],
            ['external_id', $external_user_id ],
		])->firstOrFail();
    }

    public function getUserBySocialEmail(string $email): ?User
    {
        return User::where('email', $email)->first();
    }

    public function getUserBySocialNetwork(int $id, string $external_id): ?User
    {
        $socialAccount = $this->getSocialAccountByNetworkId($id, $external_id);
        return User::where('id', $socialAccount->user_id)->firstOrFail();
    }

    public function createUserSocialAccount(User $user, SocialUserData $userSocialData): Linked
    {
        return Linked::create([
            'network_id' => $userSocialData->getNetworkId(),
            'user_id' => $user->id,
            'external_id' => $userSocialData->getId(),
            'username' => $userSocialData->getUsername(),
            'photo' => $userSocialData->getPhoto(),
            'followers_count' => 0,
            'is_deleted' => false,
        ]);
    }

    public function updateSocialAccountData(Linked $socialAccount, SocialUserData $userSocialData): Linked
    {
        $socialAccount->update([
            'username' => $userSocialData->getUsername(),
            'is_deleted' => false,
        ]);

        return $socialAccount;
    }

    public function getUserData(string $provider): object
    {
        if ($provider === 'instagram') {
            /** @psalm-suppress UndefinedInterfaceMethod */
            return Socialite::driver('instagrambasic')->stateless()->user();
        }

        if ($provider === 'vk') {
            /** @psalm-suppress UndefinedInterfaceMethod */
            return  Socialite::driver('vkontakte')->stateless()->user();
        }

        /** @psalm-suppress UndefinedInterfaceMethod */
        return Socialite::driver($provider)->stateless()->user();
    }

    public function attachSocialNetworkData(object $userSocial, string $provider): ?SocialUserData
    {
        $network = Network::where('provider', $provider)->firstOrFail();
        $accountItem = new SocialUserData();

        switch ($provider) {
            case 'facebook':
                return $accountItem
                    ->setId((string) $userSocial->id)
                    ->setNetworkId($network->id)
                    ->setEmail($userSocial->email)
                    ->setUsername($userSocial->nickname ?: $userSocial->name)
                    ->setFirstName($userSocial->name)
                    ->setLastName('')
                    ->setPhoto($userSocial->avatar);

            case 'google':
                $given_name = $userSocial->user['given_name'] ?? '';
                $family_name = $userSocial->user['family_name'] ?? '';
                $username = $userSocial->user['name'] ?? '';

                return $accountItem
                    ->setId((string) $userSocial->id)
                    ->setNetworkId($network->id)
                    ->setEmail($userSocial->email)
                    ->setUsername($username)
                    ->setFirstName($given_name)
                    ->setLastName($family_name)
                    ->setPhoto($userSocial->avatar);

            case 'twitch':
            case 'vk':
                return $accountItem
                    ->setId((string) $userSocial->id)
                    ->setNetworkId($network->id)
                    ->setEmail($userSocial->email)
                    ->setUsername($userSocial->nickname)
                    ->setFirstName($userSocial->name)
                    ->setLastName('')
                    ->setPhoto($userSocial->avatar);

            case 'instagram':
                return $accountItem
                    ->setId((string) $userSocial->id)
                    ->setNetworkId($network->id)
                    ->setEmail(null)
                    ->setUsername($userSocial->nickname)
                    ->setFirstName($userSocial->name)
                    ->setLastName('')
                    ->setPhoto('');

            case 'tiktok':
                return $accountItem
                    // The unique identification of the user across different apps for the same developer.
                    // For example, if a partner has X number of clients, it will get X number of open_id
                    // for the same TikTok user, but one persistent union_id for the particular user.
                    ->setId((string) $userSocial->union_id)
                    ->setNetworkId($network->id)
                    ->setEmail(null)
                    ->setUsername($userSocial->name)
                    ->setFirstName($userSocial->name)
                    ->setLastName('')
                    ->setPhoto('')
                    ->setUnionId($userSocial->union_id);

            case 'linkedin':
                return $accountItem
                    ->setId((string) $userSocial->id)
                    ->setNetworkId($network->id)
                    ->setEmail($userSocial->email)
                    ->setUsername($userSocial->name)
                    ->setFirstName($userSocial->name)
                    ->setLastName('')
                    ->setPhoto($userSocial->avatar);

        }

        return null;
    }

}
