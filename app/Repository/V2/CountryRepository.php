<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\Models\V2\Country;
use Illuminate\Database\Eloquent\Collection;

class CountryRepository
{

    public function findAll(): Collection
    {
        return Country::all();
    }

    public function findById(int $id): ?Country
    {
        return Country::where('id', $id)->first();
    }

}
