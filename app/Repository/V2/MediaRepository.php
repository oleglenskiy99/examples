<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\Models\V2\Media;
use Illuminate\Database\Eloquent\Collection;

class MediaRepository
{

    public function getMediaByIds(array $ids): Collection
    {
        return Media::whereIn('id', $ids)->get();
    }

}
