<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\Models\V2\Categories;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository
{

    public function findAll(): Collection
    {
        return Categories::all();
    }

    public function getPopularCategories(): Collection
    {
        return Categories::limit(10)->get();
    }

}
