<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\Models\V2\UsersPrices;
use Illuminate\Database\Eloquent\Collection;

class PriceRepository
{

    public function findByUserId(int $id): Collection
    {
        return UsersPrices::where([
            'user_id' => $id,
        ])->get();
    }

    public function findByUserIdAndName(int $id, string $name): ?UsersPrices
    {
        return UsersPrices::where([
            'user_id' => $id,
            'key' => $name,
        ])->first();
    }

}
