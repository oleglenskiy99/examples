<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\Models\V2\UsersEvent;

class UsersEventRepository
{

    public function findByUserIdEventId(int $userId, int $eventId): ?UsersEvent
    {
        return UsersEvent::where([
            'user_id' => $userId,
            'event_id' => $eventId,
        ])->first();
    }

}
