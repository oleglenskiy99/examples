<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\Models\V2\UsersCategories;
use Illuminate\Database\Eloquent\Collection;

class UsersCategoriesRepository
{

    public function findAllUserId(int $userId): Collection
    {
        return UsersCategories::where([
            'user_id' => $userId,
        ])->get();
    }

}
