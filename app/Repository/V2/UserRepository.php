<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\DTO\Request\UserFilter;
use App\Models\V2\User;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class UserRepository
{

    public function findUserByNickname(string $nickname, bool $with_me = true): ?User
    {
        $user = User::where('domain', $nickname)->first();

        if (!$with_me && $user && $user->isMe()) {
            return null;
        }

        return $user;
    }

    public function findUserByEmail(string $email, bool $with_me = true): ?User
    {
        $user = User::where('email', $email)->first();

        if (!$with_me && $user && $user->isMe()) {
            return null;
        }

        return $user;
    }

    public function findById(int $id): ?User
    {
        return User::where('id', $id)->first();
    }

    public function findByFilter(UserFilter $filter): LengthAwarePaginator
    {
        $query = User::leftJoin(
            'module_users_about',
            'module_users_about.id',
            '=',
            'module_users.id'
        )->leftJoin(
            'module_users_categories',
            'module_users_categories.user_id',
            '=',
            'module_users.id'
        )->leftJoin(
            'module_users_prices',
            'module_users_prices.user_id',
            '=',
            'module_users.id'
        )->leftJoin(
            'module_users_reviews',
            'module_users_reviews.owner_id',
            '=',
            'module_users.id'
        )->leftJoin(
            'module_users_events',
            'module_users_events.user_id',
            '=',
            'module_users.id'
        )->select([
            'module_users.id',
            'module_users.domain',
            'follow.sender as followId',
            'following.taker as followingId',
        ])->groupBy('module_users.id', 'follow.sender', 'following.taker');

        if ($filter->getCategoryIds()) {
            $query->whereIn('module_users_categories.category_id', $filter->getCategoryIds());
        }

        if ($filter->getCountryIds()) {
            $query->whereIn('module_users_about.country', $filter->getCountryIds());
        }

        if ($filter->isChatRequestAvailable()) {
            $query->where('module_users_prices.key', '=', 'chats');
        }

        if ($filter->isAudioRequestAvailable()) {
            $query->where('module_users_prices.key', '=', 'audio');
        }

        if ($filter->isVideoRequestAvailable()) {
            $query->where('module_users_prices.key', '=', 'video');
        }

        if ($filter->isImageRequestAvailable()) {
            $query->where('module_users_prices.key', '=', 'image');
        }

        if ($filter->getPriceFrom()) {
            $query->where('module_users_prices.value', '>=', $filter->getPriceFrom());
        }

        if ($filter->getPriceTo()) {
            $query->where('module_users_prices.value', '<=', $filter->getPriceTo());
        }

        if ($filter->getRatingsFrom()) {
            $query->havingRaw('AVG(module_users_reviews.value) >= ?', [$filter->getRatingsFrom()]);
        }

        if ($filter->getRatingsTo()) {
            $query->havingRaw('AVG(module_users_reviews.value) <= ?', [$filter->getRatingsTo()]);
        }

        if ($filter->isVerification()) {
            $query->where('module_users.is_email_verify', '=', $filter->isVerification());
        }

        if ($filter->getEventId()) {
            $query->where('module_users_events.event_id', $filter->getEventId());
        }

        if ($filter->getEventUserTypeId()) {
            $query->where('module_users_events.user_type_id', $filter->getEventUserTypeId());
        }

        if ($filter->getOnlyFilled()) {
            $query
                ->whereRaw('module_users_about.biography IS NOT NULL AND LENGTH(module_users_about.biography) > 0')
                ->whereRaw('module_users_about.photo IS NOT NULL AND LENGTH(module_users_about.photo) > 0')
                ->whereExists(function ($query): void {
                    $query->select(DB::raw(1))->from('module_users_prices')->whereColumn('module_users_prices.user_id', 'module_users.id');
                });
        }

        if ($filter->getSearchText()) {
            $query->where(function ($query) use ($filter): void {
                $query->where(
                    'module_users_about.firstName',
                    'ILIKE',
                    sprintf('%%%s%%', $filter->getSearchText())
                )->orWhere(
                    'module_users_about.lastName',
                    'ILIKE',
                    sprintf('%%%s%%', $filter->getSearchText())
                )->orWhere(
                    'module_users.domain',
                    'ILIKE',
                    sprintf('%%%s%%', $filter->getSearchText())
                )->orWhere(
                    'module_users_prices.key',
                    'ILIKE',
                    sprintf('%%%s%%', $filter->getSearchText())
                )->orWhere(
                    'module_users_about.biography',
                    'ILIKE',
                    sprintf('%%%s%%', $filter->getSearchText())
                );
            });
        }

        $currentUserId = $filter->getCurrentUserId();
        $query->leftJoin(
            'module_users_contacts_requests as follow',
            function (JoinClause $join) use ($currentUserId): void {
                $join->on('follow.taker', '=', 'module_users.id');
                $join->where('follow.sender', $currentUserId);
            }
        );

        $query->leftJoin(
            'module_users_contacts_requests as following',
            function (JoinClause $join) use ($currentUserId): void {
                $join->on('following.sender', '=', 'module_users.id');
                $join->where('following.taker', $currentUserId);
            }
        );

        $query->orderByRaw('AVG(module_users_reviews.value), module_users.id');

        return $query->paginate($filter->getCountOfItems(), ['*'], 'next_max_id', $filter->getPage());
    }

    public function findUserByIdOrNickname(int $currentUserId, ?int $userId, ?string $nickname): ?User
    {
        $query = null;
        if ($userId !== null && $nickname !== null) {
            $query = User::where('module_users.id', $userId)->orWhere('domain', $nickname);
        } elseif ($userId !== null) {
            $query = User::where('module_users.id', $userId);
        } elseif ($nickname !== null) {
            $query = User::where('domain', $nickname);
        }

        if ($query !== null) {
            $query->select([
                '*',
                'module_users.id',
                'follow.sender as followId',
                'following.taker as followingId',
            ]);
            $query->leftJoin(
                'module_users_contacts_requests as follow',
                function (JoinClause $join) use ($currentUserId): void {
                    $join->on('follow.taker', '=', 'module_users.id');
                    $join->where('follow.sender', $currentUserId);
                }
            );

            $query->leftJoin(
                'module_users_contacts_requests as following',
                function (JoinClause $join) use ($currentUserId): void {
                    $join->on('following.sender', '=', 'module_users.id');
                    $join->where('following.taker', $currentUserId);
                }
            );
        }

        return $query ? $query->first() : null;
    }

    public function findUserByPhone(?string $phone): ?User
    {
        return User::where('phone', $phone)->first();
    }

}
