<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\DTO\Request\ChatHistoryRequest;
use App\Models\V2\Chat;
use App\Models\V2\ChatAttachments;
use App\Models\V2\ChatMessages;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class MessagesRepository
{

    public function get(ChatHistoryRequest $chatHistory): LengthAwarePaginator
    {
        return ChatMessages::with(['replyMessage', 'attachments'])
            ->where('dialog_id', $chatHistory->getChatId())
            ->orderBy('id', 'desc')
            ->paginate(
                $chatHistory->getCountOfItems(),
                ['*'],
                'page',
                $chatHistory->getPage()
            );
    }


    public function getPaidMessagesWithoutReply(Chat $chat): Collection
    {
        return ChatMessages::select('id', 'user_id', 'cost', 'charge_id')
            ->where(DB::raw('COALESCE(is_has_reply::boolean, false)'), false)
            ->where('is_paid', 1)
            ->where('cost', '>', 0)
            ->where('dialog_id', $chat->id)
            ->where('is_deleted', 0);
    }

    public function addMediaToMessage(Collection $medias, ChatMessages $message): void
    {
        foreach ($medias as $media) {
            ChatAttachments::create([
                'dialog_id' => $message->dialog_id,
                'message_id' => $message->id,
                'media_id' => $media->id,
                'type' => $media->type,
            ]);
        }
    }

}
