<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\Models\V2\User;
use App\Models\V2\UsersView;
use Illuminate\Pagination\LengthAwarePaginator;

class UsersViewsRepository
{

    public function findLastViewedByUserId(int $userId, int $page, int $countOfItems): LengthAwarePaginator
    {
        $user = User::where('id', $userId)->first();

        return $user->viewedUsers()->latest()->paginate($countOfItems, ['*'], 'next_max_id', $page);
    }

    public function updateOrCreateByUserAndViewed(int $userId, int $viewedUserId): UsersView
    {
        $exists = UsersView::where('user_id', $userId)->where('viewed_user_id', $viewedUserId)->first();

        if ($exists) {
            $exists->touch();
            return $exists;
        }

        $userView = new UsersView();
        $userView->user_id = $userId;
        $userView->viewed_user_id = $viewedUserId;
        $userView->save();
        return $userView;
    }

}
