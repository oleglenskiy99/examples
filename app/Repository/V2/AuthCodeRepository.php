<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\Models\V2\AuthorizationCode;

class AuthCodeRepository
{

    public function deleteAllByEmail(string $email): void
    {
        AuthorizationCode::where('additional', $email)->delete();
    }

    public function findByEmailAndCode(string $email, int $code): ?AuthorizationCode
    {
        return AuthorizationCode::where('additional', $email)->where('code', $code)->first();
    }

}
