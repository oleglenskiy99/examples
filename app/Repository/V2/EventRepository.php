<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\Models\V2\Event;
use Illuminate\Database\Eloquent\Collection;

class EventRepository
{

    public function findAll(): Collection
    {
        return Event::all();
    }

    public function findById(int $id): ?Event
    {
        return Event::where('id', $id)->first();
    }

}
