<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\Models\V2\UsersContactsRequests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class UsersContactsRequestsRepository
{

    public function findFollowersByUserId(int $userId, int $page, int $countOfItems): LengthAwarePaginator
    {
        return UsersContactsRequests::select([
            '*',
            DB::raw('(
                select 1
                from module_users_contacts_requests as r
                where r.sender = module_users_contacts_requests.taker and r.taker = module_users_contacts_requests.sender
                limit 1
            ) as respond'),
        ])
            ->where('sender', $userId)
            ->paginate($countOfItems, ['*'], 'next_max_id', $page);
    }

    public function findFollowingsByUserId(int $userId, int $page, int $countOfItems): LengthAwarePaginator
    {
        return UsersContactsRequests::select([
            '*',
            DB::raw('(
                select 1
                from module_users_contacts_requests as r
                where r.sender = module_users_contacts_requests.taker and r.taker = module_users_contacts_requests.sender
                limit 1
            ) as respond'),
        ])
            ->where('taker', $userId)
            ->paginate($countOfItems, ['*'], 'next_max_id', $page);
    }

    public function findByOwnerIdFollowerId(int $ownerId, int $followerId): ?UsersContactsRequests
    {
        return UsersContactsRequests::where([
            'taker' => $ownerId,
            'sender' => $followerId,
        ])->first();
    }

}
