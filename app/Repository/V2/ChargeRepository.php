<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\DTO\Request\ChargeFilter;
use App\Models\V2\UsersCharge;
use Illuminate\Pagination\LengthAwarePaginator;

class ChargeRepository
{

    public function findByFilterAndPaymentId(ChargeFilter $filter, ?string $payment_id): LengthAwarePaginator
    {
        $query = UsersCharge::where('customer', $payment_id);

        return $query->paginate($filter->getCountOfItems(), ['*'], 'next_max_id', $filter->getPage());
    }

}
