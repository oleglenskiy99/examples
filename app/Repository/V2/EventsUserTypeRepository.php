<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\Models\V2\EventsUserType;
use Illuminate\Database\Eloquent\Collection;

class EventsUserTypeRepository
{

    public function findAll(): Collection
    {
        return EventsUserType::all();
    }

    public function findById(int $id): ?EventsUserType
    {
        return EventsUserType::where('id', $id)->first();
    }

    public function findByName(string $name): ?EventsUserType
    {
        return EventsUserType::where('name', $name)->first();
    }

    public function findOrCreateByName(string $name): EventsUserType
    {
        return EventsUserType::firstOrCreate(['name' => $name]);
    }

}
