<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\DTO\Request\ConnectionsRequest;
use App\Enums\RequestsSectionsEnum;
use App\Models\V2\Chat;
use App\Models\V2\ChatMembers;
use App\Models\V2\User;
use App\Services\V2\UserService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ChatsRepository
{

    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function getChatsForCurrentUser(ConnectionsRequest $connectionsRequest): Collection
    {
        $user = $this->userService->getCurrentAuthUser();

        $chats = $this->defaultChatsBuilder($user)
            ->where(function (Builder $q) use ($user): void {
                $q->where('creator_id', $user->id)->orWhere('expert_id', $user->id);
            })
            ->groupBy(['id', 'expert_id', 'creator_id']);

        if ( $connectionsRequest->hasIsOnlyNew()) {
            $chats->whereHas('memberInfo', function (Builder $q): void {
                $q->where('unread_messages_count', '>', 0)->orWhere('balance_actual', '>', 0);
            });
        }

        return $chats->get();
    }

    public function getChatsBetweenUsers(ConnectionsRequest $connectionsRequest): Collection
    {
        $user = $this->userService->getCurrentAuthUser();

        $chats = $this->defaultChatsBuilder($user)->where(function (Builder $q) use ($user, $connectionsRequest): void {
			switch ( $connectionsRequest->getSection()) {
				case RequestsSectionsEnum::INCOMING:
					$q->where([
						['creator_id', $connectionsRequest->getUserId() ],
						['expert_id', $user->id ],
					]);
					break;
				case RequestsSectionsEnum::OUTGOING:
					$q->where([
						['expert_id', $connectionsRequest->getUserId() ],
						['creator_id', $user->id ],
					]);
					break;
				default:
					$q->where([
						['creator_id', $user->id ],
						['expert_id', $connectionsRequest->getUserId() ],
					])->orWhere([
						['creator_id', $connectionsRequest->getUserId() ],
						['expert_id', $user->id ],
					]);

					break;
			}
        });

        if ( $connectionsRequest->hasIsOnlyNew()) {
            $chats->whereHas('memberInfo', function (Builder $q): void {
                $q->where('unread_messages_count', '>', 0)->orWhere('balance_actual', '>', 0);
            });
        }

        return $chats->get();
    }

    public function getMemberByUserChatId(User $user, int $chatId): ?ChatMembers
    {
        return ChatMembers::where([
            ['user_id', $user->id ],
            ['dialog_id', $chatId ],
        ])->first();
    }

    public function getChatById(int $id): Chat
    {
        return Chat::where('id', $id)->firstOrFail();
    }


    private function defaultChatsBuilder(User $user): Builder
    {
        return Chat::select('module_chats.id', 'title', 'expert_id', 'creator_id')
            ->whereHas('message', function (Builder $q) use ($user): void {
                $q->where('is_paid', true)->orWhere('user_id', $user->id)->where('is_deleted', false);
            })->with([
                'memberInfo' => function (HasOne $q) use ($user): void {
                    $q->where('user_id', $user->id);
                },
            ])->with('message')->orderBy('updated_at', 'DESC');
    }

}
