<?php

declare(strict_types=1);

namespace App\Repository\V2;

use App\DTO\Common\Price;
use App\DTO\Response\Finance\StripeCheckoutSession;
use App\DTO\Response\Objects\Card;
use App\DTO\Response\User\UserFinances;
use App\Models\V2\User;
use App\Models\V2\UsersCards;
use App\Services\V2\UserService;
use Illuminate\Auth\Access\AuthorizationException;
use Stripe\Checkout\Session;
use Stripe\Exception\ApiErrorException;
use Stripe\StripeClient;

class FinancesRepository
{

    private StripeClient $stripeClient;
    private UserService $userService;

    public function __construct(StripeClient $stripeClient, UserService $userService)
    {
        $this->stripeClient = $stripeClient;
        $this->userService = $userService;
    }

    public function createPaymentSession(int $amount): StripeCheckoutSession
    {
        $user = $this->userService->getCurrentAuthUser();

        $this->ensureStripeCustomerId($user);

        try {
            $product = $this->stripeClient->products->retrieve('top_up_user_balance');
        } catch (ApiErrorException $exception) {
            $product = $this->stripeClient->products->create([
                'id' => 'top_up_user_balance',
                'name' => 'Top up balance',
            ]);
        }

        $price = $this->stripeClient->prices->create([
            'currency' => 'usd',
            'product' => $product->id,
            'unit_amount' => $amount * 100,
        ]);

        $session = $this->stripeClient->checkout->sessions->create([
            'mode' => 'payment',
            'success_url' => config('app.front_url'),
            'cancel_url' => config('app.front_url'),
            'client_reference_id' => $user->id,
            'customer' => $user->stripe_customer_id,
            'line_items' => [
                [
                    'price' => $price->id,
                    'quantity' => 1,
                ],
            ],
        ]);

        return $this->mapCheckoutSession($session);
    }

    /**
     * @return StripeCheckoutSession
     * @throws AuthorizationException
     * @throws ApiErrorException
     */
    public function createCreditCardAttachSession(): StripeCheckoutSession
    {
        $user = $this->userService->getCurrentAuthUser();

        $this->ensureStripeCustomerId($user);

        $session = $this->stripeClient->checkout->sessions->create([
            'mode' => 'setup',
            'success_url' => config('app.front_url'),
            'cancel_url' => config('app.front_url'),
            'client_reference_id' => $user->id,
            'customer' => $user->stripe_customer_id,
            'payment_method_types' => ['card'],
        ]);

        return $this->mapCheckoutSession($session);
    }

    public function mapDbToUserFinances(User $user): UserFinances
    {
        $object = new UserFinances();
        $balance = new Price();
        $withdraw = new Price();

        /**
         * @psalm-suppress UndefinedMagicPropertyFetch
         */
        return $object
            ->setForSpending(
                $balance->setAmount($user->balance)->setCurrency('usd')
            )
            ->setForWithdrawal(
                $withdraw->setAmount($user->withdraw)->setCurrency('usd')
            );
    }

    public function mapDbToCard(UsersCards $card): Card
    {
        $cardObject = new Card();

        return $cardObject
            ->setId($card->id)
            ->setLast4($card->last4)
            ->setFunding($card->funding)
            ->setBrand($card->brand)
            ->setExpMonth($card->exp_month)
            ->setExpYear($card->exp_year);
    }

    public function mapCheckoutSession(Session $session): StripeCheckoutSession
    {
        $stripeCheckoutSession = new StripeCheckoutSession();

        return $stripeCheckoutSession
            ->setId($session->id)
            ->setObject($session->object)
            ->setCustomer($session->customer)
            ->setCurrency($session->currency)
            ->setMode($session->mode)
            ->setExpiresAt($session->expires_at)
            ->setStatus($session->status)
            ->setFormUrl($session->url);
    }

    public function findCard(int $card): Card
    {
        return $this->mapDbToCard(UsersCards::find($card));
    }
    /**
     * @param User $user
     * @return void
     * @throws ApiErrorException
     */
    private function ensureStripeCustomerId(User $user): void
	{
        if ( $user->stripe_customer_id === null) {
            $user->stripe_customer_id = $this->stripeClient->customers->create()->id;
        }

        if ( $user->isDirty('stripe_customer_id')) {
            $user->save();
        }
    }

}
