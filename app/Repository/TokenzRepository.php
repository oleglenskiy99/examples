<?php

declare(strict_types=1);

namespace App\Repository;

use App\DTO\Response\JwtTokenStructure;
use App\GeoIP;
use App\Helpers\UTMRedisKey;
use App\Tokenz;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Throwable;
use Tymon\JWTAuth\JWTGuard;

class TokenzRepository
{

    private JWTGuard $JWTGuard;

    public function __construct(JWTGuard $JWTGuard)
    {
        $this->JWTGuard = $JWTGuard;
    }

    public function revokeCurrent(?User $user, string $token): void
    {
        try {
            $this->JWTGuard->setToken($token)->logout();
        } catch (Throwable $exception) {
            // Old auth tokens
        }

        Tokenz::where([
            ['user_id', $user->id],
            ['access_token', $token],
        ])->delete();
    }

    public function revokeAll(?User $user, string $token): void
    {
        $sessions = Tokenz::where([
            ['user_id', $user->id],
            ['access_token', '!=', $token],
        ])->get();

        foreach ($sessions as $session) {
            try {
                $this->JWTGuard->setToken($session->access_token)->logout();
            } catch (Throwable $exception) {
                // Old auth tokens
            }

            $session->delete();
        }
    }

    public function revokeById(?User $user, int $id): void
    {
        $session = Tokenz::where([
            ['id', $id ],
            ['user_id', $user->id],
        ])->first();

        if (!$session) {
            return;
        }

        try {
            $this->JWTGuard->setToken($session->access_token)->logout();
        } catch (Throwable $exception) {
            // Old auth tokens
        }

        $session->delete();
    }

    public function storeUserSession(User $user, JwtTokenStructure $tokenStructure): Tokenz
    {
        $request = request();

        if ($request && $request->has('utm') && $request->get('utm') !== null) {
            $timeexpires = config('cache.token.redisexpires');
            Redis::setex(UTMRedisKey::UTM_REDIS_KEY . (string) $user->id, $timeexpires, $request->get('utm'));
        }

        return Tokenz::create(array_merge([
            'user_id' => $user->id,
            'access_token' => User::getAccessToken(),
            'timestamp' => time(),
            'expires' => $tokenStructure->getExpiresIn(),
        ], $this->getUserAgentInfo()));
    }

    public function getUserAgentInfo(): array
    {
        $agent = request()->userAgent();
        $device = session()->pull('device') ?? request()->toArray();
        $location = GeoIP::detect();

        if (preg_match('/Linux/', $agent)) {
            $os = 'Linux';
            if (preg_match('/Android/', $agent)) {
                $os = 'Android';
            }
        } elseif (preg_match('/Windows/', $agent)) {
            $os = 'Windows';
        } elseif (preg_match('/Mac/', $agent)) {
            $os = 'Mac';
            if (preg_match('/iPhone/', $agent)) {
                $os = 'iPhone';
            }
        } elseif (preg_match('/Apple/', $agent)) {
            $os = 'Apple';
        } else {
            $os = 'UnKnown';
        }

        return [
            'application_version' => $device['application_version'] ?? null,
            'device_model' => $device['device_model'] ?? null,
            'device_type' => $device['device_type'] ?? null,
            'device_os' => $device['device_os'] ?? $os,
            'device_os_version' => $device['device_os_version'] ?? null,
            'city_name' => $location['cityName'],
            'region_name' => $location['regionName'],
            'country_name' => $location['countryName'],
        ];
    }


    public static function getUserByToken(string $access_token): ?User
    {
        $token = Tokenz::where('access_token', $access_token)->firstOrFail();
        return User::where('id', $token->user_id)->firstOrFail();
    }

    public static function afterComplete(User $user, ?Request $request = null): Tokenz
    {
        $tokenz = new Tokenz();
        $tokenz->user_id = $user->id;

        $tokenz->access_token = User::getAccessToken();
        $tokenz->timestamp = time();
        $tokenz->expires = config('cache.token.expires');
        $tokenz->call_id = User::getAccessToken();

        $tokenz->detect();
        $tokenz->save();

        if ($request && isset($request->utm) && $request->utm !== null) {
			$timeexpires = config('cache.token.redisexpires');
			Redis::setex(UTMRedisKey::UTM_REDIS_KEY . (string) $user->id, $timeexpires, $request->utm);
        }

        return $tokenz;
    }

}
