<?php

declare(strict_types=1);

namespace App\Repository\Chats;

use App\Models\Chats\Attachments;
use Illuminate\Support\Facades\DB;

class AttachmentsRepository
{

    // phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint
    public static function builder($query = null, int $userId = 0)
	{
        return Attachments::whereHas('message', function ($q) use ($query, $userId): void {
            if ( $query) {
                $q->where('is_deleted', 0)->where(function ($q) use ($userId): void {
                    $q->where('is_paid', 1)->orWhere('user_id', $userId)->orWhere('cost', 0);
                })->where('message', 'LIKE', sprintf('%%%s%%', $query));
            } else {
                $q->where('is_deleted', 0)->where(function ($q) use ($userId): void {
                    $q->where('is_paid', 1)->orWhere('user_id', $userId)->orWhere('cost', 0);
                });
            }
        })->with([
            'message' => function ($message) use ($query): void {
				$message->select(
                    'id',
                    'user_id',
                    'dialog_id',
                    'timestamp',
                    'type',
                    'guuid',
                    'charge_id',
                    'cost',
                    'is_paid',
                    'is_edit',
                    'is_read',
                    'views',
                    'service',
                    'reply',
                    'message'
				);

				if ( $query) {

					$message->addSelect(DB::raw(sprintf("IF (
                    LOCATE('%s', message) < 50,
                    CONCAT(SUBSTRING(message, 1, 200),'...'),
                    CONCAT('...',SUBSTRING(message, LOCATE('%s', message) - 50, 200),'...')
                ) AS text", $query, $query)));

				} else {

					$message->with([
                        'attachments' => function ($q): void {
							$q->where('module_chats_messages_attach.type', 'image')->orWhere(
                                'module_chats_messages_attach.type',
                                'video'
							)->orWhere(
                                'module_chats_messages_attach.type',
                                'file'
							);
						}])
					->with([
                        'audio' => function ($q): void {
							$q->where('module_chats_messages_attach.type', 'voice');
						}]);

				}

				$message->whereHas('user')->with([
                    'user' => function ($query): void {
						$query->select('id', 'firstName', 'lastName', 'photo');
					}]);
			}])->whereHas('chat', function ($q): void {
				$q->where('is_saved_messages', 0);
			})->with([
                'chat' => function ($q) use ($userId): void {
						$q->with([
                            'user' => function ($query) use ($userId): void {
								$query
                                    ->select(
                                        'user_id as id',
                                        'dialog_id',
                                        'firstName',
                                        'lastName',
                                        'domain',
                                        'biography',
                                        'photo',
                                        'is_online',
                                        'last_activity'
                                    )
                                    ->join('module_users', 'user_id', 'module_users.id')
                                    ->join('module_users_about', 'user_id', 'module_users_about.id')
                                    ->where('user_id', '!=', $userId);
							}]);
				}])->orderBy('message_id', 'DESC');
    }

}
