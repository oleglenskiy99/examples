<?php

declare(strict_types=1);

namespace App\Repository\Chats;

use App\Models\Chats\Members;
use Illuminate\Database\Eloquent\Builder;

class MembersRepository
{

    public static function itself(int $user_id, int $chat_id): Builder
    {
        return Members::where('user_id', $user_id)->where('dialog_id', $chat_id);
    }

    public static function peer(int $user_id, int $chat_id): Builder
    {
        return Members::where('user_id', '!=', $user_id)->where('dialog_id', $chat_id);
    }

    public static function allMembers(int $chat_id): Builder
    {
        return Members::where('dialog_id', $chat_id);
    }

}
