<?php

declare(strict_types=1);

namespace App\Repository\Chats;

use App\Models\Chats\Chat;
use App\Models\Chats\Members;
use App\Repository\User\PricesRepository;
use App\Repository\User\UsersRepository;

class ChatsRepository
{

    /**
     * @param int $chatId
     * @param int $userId
     * @return array
     */
    public static function getById(int $chatId, int $userId = -1): ?array
    {
        return self::getItemById($chatId, $userId);
    }

    public static function getItemById(int $chatId, int $userId): ?array
    {
        $items = self::items(
            $userId,
            0,
            false,
            $chatId
        );
        if ($items === null) {
            return null;
        }
        return $items;
    }

    public static function items(
        int $userId,
        int $offset = 0,
        bool $archived = false,
        ?int $specificId = null
    ): ?array
    {
        $chats = Chat::select(
            'id',
            'title',
            'image',
            'is_group',
            'is_saved_messages',
            'description',
            'creator_id',
            'is_call'
        )
            ->with([
                'last_message' => function ($query) use ($userId): void {
                    $query->select([
                        'id',
                        'user_id',
                        'author as from',
                        'dialog_id',
                        'timestamp',
                        'type',
                        'guuid',
                        'charge_id',
                        'cost',
                        'is_paid',
                        'is_edit',
                        'is_read',
                        'views',
                        'reply',
                        'message',
                    ])
                        ->with([
                            'attachments' => function ($q): void {
                                $q->select([
                                    'id as media_id',
                                    'module_medias.type',
                                    'size',
                                    'width',
                                    'height',
                                    'duration',
                                    'fileName',
                                    'extension',
                                    'thumb',
                                    'placeholder',
                                    'url',
                                ])->where(
                                    function ($where): void {
                                        $where->where('module_chats_messages_attach.type', 'image')->orWhere(
                                            'module_chats_messages_attach.type',
                                            'video'
                                        );
                                    }
                                )->where('inherited', 0);
                            },
                        ])
                        ->with([
                            'reply' => function ($q): void {
                                $q->select('id', 'dialog_id', 'message', 'type', 'user_id')->whereHas('user')->with([
                                    'user' => function ($query): void {
                                        $query->select('id', 'firstName', 'lastName', 'photo');
                                    },
                                ])->with([
                                    'attachments' => function ($q): void {
                                        $q->select([
                                            'id',
                                            'id as media_id',
                                            'module_medias.type',
                                            'size',
                                            'width',
                                            'height',
                                            'duration',
                                            'fileName',
                                            'extension',
                                            'thumb',
                                            'placeholder',
                                            'url',
                                        ])->where(
                                            function ($where): void {
                                                $where->where('module_chats_messages_attach.type', 'image')->orWhere(
                                                    'module_chats_messages_attach.type',
                                                    'video'
                                                );
                                            }
                                        )->where('inherited', 0);
                                    },
                                ])->with([
                                    'file' => function ($q): void {
                                        $q->select([
                                            'id',
                                            'id as media_id',
                                            'module_medias.type',
                                            'size',
                                            'width',
                                            'height',
                                            'duration',
                                            'fileName',
                                            'extension',
                                            'thumb',
                                            'placeholder',
                                            'url',
                                        ])->where(
                                            'module_chats_messages_attach.type',
                                            'file'
                                        )->where(
                                            'inherited',
                                            0
                                        );
                                    },
                                ])->with([
                                    'audio' => function ($q): void {
                                        $q->select([
                                            'id',
                                            'id as media_id',
                                            'module_medias.type',
                                            'size',
                                            'duration',
                                            'fileName',
                                            'extension',
                                            'url',
                                        ])->where(
                                            'module_chats_messages_attach.type',
                                            'voice'
                                        )->where(
                                            'inherited',
                                            0
                                        );
                                    },
                                ]);
                            },
                        ])
                        ->with([
                            'file' => function ($q): void {
                                $q->select([
                                    'id',
                                    'id as media_id',
                                    'module_medias.type',
                                    'size',
                                    'width',
                                    'height',
                                    'duration',
                                    'fileName',
                                    'extension',
                                    'thumb',
                                    'placeholder',
                                    'url',
                                ])->where(
                                    'module_chats_messages_attach.type',
                                    'file'
                                )->where(
                                    'inherited',
                                    0
                                );
                            },
                        ])
                        ->with([
                            'audio' => function ($q): void {
                                $q->select([
                                    'id',
                                    'id as media_id',
                                    'module_medias.type',
                                    'size',
                                    'duration',
                                    'fileName',
                                    'extension',
                                    'url',
                                ])->where(
                                    'module_chats_messages_attach.type',
                                    'voice'
                                )->where(
                                    'inherited',
                                    0
                                );
                            },
                        ])
                        ->with([
                            'external' => function ($query): void {
                                $query->select('data', 'message_id')->where('type', 'link');
                            },
                        ])
                        ->whereHas('user')->with([
                            'user' => function ($query): void {
                                $query->select('id', 'firstName', 'lastName', 'photo');
                            },
                        ])->where(function ($query) use ($userId): void {
                            if ($userId) {
                                $query->where('is_paid', 1)->orWhere('user_id', $userId);
                            }
                        })
                        ->where('is_deleted', false)
                        ->whereRaw(
                            sprintf(
                                'id not in (SELECT message_id from module_chats_messages_deleted WHERE user_id = %s and module_chats_messages_deleted.dialog_id = module_chats_messages.dialog_id )',
                                $userId
                            )
                        )
                        ->orderBy('id', 'DESC');
                },
            ])
            ->with([
                'user' => function ($query) use ($userId): void {
                    $query
                        ->select(['user_id', 'dialog_id', 'firstName', 'lastName', 'domain', 'photo', 'is_online', 'last_activity'])
                        ->join('module_users', 'user_id', 'module_users.id')
                        ->join('module_users_about', 'user_id', 'module_users_about.id')
                        ->where('user_id', '!=', $userId);
                },
            ])
            ->with([
                'info' => function ($q) use ($userId): void {
                    $q->where('user_id', $userId);
                },
            ]);

        $chats = $archived === true ? $chats->whereRaw(
            sprintf(
                'module_chats.id in ( SELECT module_chats_archive.dialog_id FROM module_chats_archive WHERE module_chats_archive.user_id = %s )',
                $userId
            )
        ) : $chats->whereRaw(
            sprintf(
                'module_chats.id not in ( SELECT module_chats_archive.dialog_id FROM module_chats_archive WHERE module_chats_archive.user_id = %s )',
                $userId
            )
        );

        if ($specificId) {
            $chats = $chats->where('id', $specificId);
            $items = $chats->first();
            if ($items === null) {
                return null;
            }
            return self::setChatItem($items);
        }

        $chats = $chats->whereHas('last_message', function ($query) use ($userId): void {
            $query->where(function ($q) use ($userId): void {
                $q->where('is_paid', 1)->orWhere('user_id', $userId);
            })->where('is_deleted', 0);
        })
            ->whereRaw(
                sprintf(
                    'module_chats.id in ( SELECT module_chats_users.dialog_id FROM module_chats_users WHERE user_id = %s and is_deleted = FALSE )',
                    $userId
                )
            )
            ->orderBy('last_update_timestamp', 'desc')
            ->offset($offset);

        $items = $chats->get();
        if ($items === null) {
            return null;
        }
        $result = [];
        foreach ($items as $item) {
            $result[] = self::setChatItem($item);
        }
        return $result;
    }

    public static function isExists(int $u1, int $u2, bool $isCallChat = false): ?int
    {
		$UserChats = Members::select('dialog_id')
            ->where('user_id', $u2)->get()->toArray();

        $dialog = Members::select('dialog_id')
            ->join('module_chats', 'dialog_id', 'module_chats.id')
            ->where('user_id', $u1)
            ->where('is_group', false)
            ->where('is_call', $isCallChat)
            ->whereIn('dialog_id', array_column($UserChats, 'dialog_id'))
            ->first();

        if ($dialog !== null) {
            return $dialog->dialog_id;
        }

        return null;
    }

    private static function setChatItem(Chat $chat): array
    {
        $item = $chat->toArray();
        $item['balance_actual'] = $chat->info->balance_actual;
        $item['unread_count'] = $chat->info->unread_messages_count;
        $item['is_expert'] = $chat->info->user_id !== $chat->creator_id;
        if ($chat->is_saved_messages) {
            $item['peer'] = [
                'id' => $chat->id,
                'type' => 'saved',
                'name' => '',
                'photo' => null,
            ];
        } else {
            $item['peer'] = [
                'id' => $chat->user->user_id,
                'type' => 'user',
                'name' => trim($chat->user->firstName . ' ' . $chat->user->lastName),
                'domain' => $chat->user->domain,
                'image' => $chat->user->photo,
                'is_online' => $chat->user->is_online,
            ];
            $item['price'] = PricesRepository::getPriceObjectForUserType($chat->user->user_id, PricesRepository::CATEGORY_MESSAGE);
            $user = UsersRepository::getUserProfile((string) $chat->user->user_id, false);
            if ($user !== null) {
                $result = $user->toArray();
                foreach ($user->getMutatedAttributes() as $key) {
                    if ( !array_key_exists($key, $result)) {
                        $result[$key] = $user->{$key};
                    }
                }
                $item['user'] = $result;
            }

        }
        unset($item['info']);
        return $item;
    }

}
