<?php

declare(strict_types=1);

namespace App\Repository\Chats;

use App\Models\Chats\Messages as Message;
use Illuminate\Database\Eloquent\Builder;
use Throwable;

class MessagesRepository
{

    public static function get(int $chatId, int $userId): Builder
    {
        return Message::builder()
            ->where('dialog_id', $chatId)
            ->whereDoesntHave('deletedmessages', function ($q) use ($userId): void {
                $q->where('user_id', $userId);
            });
    }

    public static function setRead(array $ids, int $chatId, int $userId): bool
    {
        $member = MembersRepository::itself($userId, $chatId)->first();
        //get last or other not read messages in current chat
        $notReadIds = Message::where('dialog_id', $chatId)->where('is_read', false)->get()->pluck('id')->toArray();
        $ids = array_keys(array_flip(array_merge($ids, $notReadIds)));

        try {
            Message::whereIn('id', $ids)
                ->update([
                    'is_read' => true,
                ]);

        } catch (Throwable $ex) {
            return false;
        }

        $member->actualDecrement(count($ids));

        return true;
    }

    public static function setView(array $ids, int $chatId, int $userId): bool
    {
        $member = MembersRepository::itself($userId, $chatId)->first();

        try {
            Message::whereIn('id', $ids)
                ->update([
                    'is_read' => true,
                ])->increment('views', 1);
        } catch (Throwable $ex) {
            return false;
        }

        $member->actualDecrement(count($ids));

        return true;
    }

    public static function setEdit(int $id, string $text): bool
    {
        try {
            Message::where('id', $id)
                ->update([
                    'message' => $text,
                    'is_edit' => true,
                ]);
        } catch (Throwable $ex) {
            return false;
        }

        return true;
    }

}
