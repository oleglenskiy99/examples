<?php

declare(strict_types=1);

namespace App\Repository;

use App\Video;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class VideoRepository
{

    public static function builder(): Builder
    {
        $locale = App::getLocale();
        return Video::select([
            'id',
            'type',
            'user_id',
            'target_id',
            'friend_name',
            'category',
            'instruction',
            'deadline',
            'cost',
            'is_live',
            'is_privacy',
            'created_at',
            'updated_at',
            'is_viewed',
            'is_completed',
            'media',
            'updated_at',
        ])
            ->addSelect(DB::raw('id as guuid'))
            ->whereHas('target')
            ->whereHas('owner')
            ->where('is_deleted', false)->with([
                'owner' => function ($user): void {
                    $user->select('id', 'firstName', 'lastName', 'photo');
                }])->with([
                    'target' => function ($user): void {
						$user->select('id', 'firstName', 'lastName', 'photo');
					}])->with([
                        'category' => function ($q) use ($locale): void {
								$q->select(
                                    'module_videos_categories.id',
                                    sprintf('name_%s as name', $locale),
                                    'icon'
								);
						}])->with('media')->orderBy(
                            'updated_at',
                            'DESC'
                        );
    }

}
