<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Events\Notify\CancelRequestVideo;
use App\Events\Notify\NewRequestVideo;
use App\Events\Notify\ReplyRequestVideo;
use App\Jobs\Notifications;
use App\Repository\VideoRepository;
use App\Services\PaymentServiceInterface;
use App\User;
use App\Video;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Throwable;

class Requests extends ApiController
{

    protected PaymentServiceInterface $payments;

    public function __construct(PaymentServiceInterface $payments)
    {
        parent::__construct();

        $this->payments = $payments;
    }

    public function getVideo(Request $request): JsonResponse
    {
        $builder = VideoRepository::builder();
        if ($request->id) {
            $builder = $builder->where('id', $request->get('id'));
        } else {
            $user_id = $this->userId;
            $builder = $builder->where('user_id', $user_id)->orWhere(function ($q) use ($user_id): void {
                $q->where('target_id', $user_id)->where('is_completed', false)->where('is_deleted', false);
            });
        }

        $videos = $builder->get()->toArray();
        foreach ($videos as $k => $video) {
            $video['group'] = 'video';

            if ($video['user_id'] === $user_id) {
                $video['user'] = $video['target'];
                $video['section'] = 'outgoing';
            } else {
                $video['user'] = $video['owner'];
                $video['section'] = 'incoming';
            }
            unset($video['target']);
            unset($video['owner']);
            $videos[$k] = $video;
        }

        return $this->displayOk($videos);
    }

    public function videoCancelReject(Request $request, string $type): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        try {
            $video = Video::findOrFail($request->get('id'));
        } catch (Throwable $exception) {
            /* TODO Add Error Code */
            return $this->displayError([]);
        }

        /** @psalm-suppress UndefinedMagicPropertyFetch * */
        if ($video->is_completed) {
            return $this->displayError(['message' => __('errors.video_completed_cancel')]);
        }
        /** @psalm-suppress UndefinedMagicPropertyFetch * */
        if ($video->is_deleted) {
            return $this->displayError(['message' => __('errors.video_removed_cancel')]);
        }

        /** @psalm-suppress UndefinedMagicPropertyFetch * */
        if ($video->charge_id !== null) {
            try {
                switch ($type) {
                    case 'cancel':
                        /** @psalm-suppress UndefinedMagicPropertyFetch * */
                        $this->payments->cancelPayment($this->current, $video->charge_id);
                        break;
                    case 'reject':
                        /** @psalm-suppress UndefinedMagicPropertyFetch * */
                        $this->payments->cancelPayment(User::getUserById($video->user_id), $video->charge_id);
                        break;
                }
            } catch (Throwable $exception) {
                return $this->displayError([]);
            }
        }
        $video->update([
            'is_deleted' => true,
        ]);
        //Event CancelRequest;
        /** @psalm-suppress UndefinedMagicPropertyFetch * */
        CancelRequestVideo::dispatch($video->user_id, $video->target_id);

        return $this->displayOk([]);
    }

    public function videoReply(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required',
            'media_id' => 'required',
        ]);

        $video = VideoRepository::builder()
            ->where('id', $request->get('id'))
            ->where('is_completed', false)
            ->first();

        if ($video === null) {
            return $this->displayError([]);
        }

        try {
            $this->payments->capturePayment(User::getUserById($video->target_id), $video->charge_id);
        } catch (Throwable $exception) {
            return $this->displayError([]);
        }

        $video->update([
            'is_viewed' => true,
            'is_completed' => true,
            'media' => $request->get('media'),
        ]);
        //Event ReplyRequest;
        ReplyRequestVideo::dispatch($video->user_id, $video->target_id);

        return $this->displayOk([]);
    }

    public function video(Request $request): JsonResponse
    {
        $this->validate($request, [
            'category' => [
                'required',
                'integer',
            ],
            'message' => [
                'required',
            ],
            'target_id' => [
                'required',
            ],
        ]);

        $amount = $request->has('cost') ?? $this->payments->getCategoryPrice(
            User::getUserById((int) $request->get('target_id')),
            PaymentServiceInterface::CATEGORY_VIDEO
        );

        $video = new Video();
        $video->user_id = $this->userId;
        $video->type = -1;
        $video->target_id = $request->get('target_id');

        if (isset($request->category)) {
            $video->category = $request->get('category');
        }
        $video->instruction = $request->get('message');
        $video->deadline = Carbon::now()->addDays(5)->timestamp;
        $video->cost = (int) $amount;

        try {
            $video->charge_id = $this->payments->createPayment(
                User::getUserById($video->user_id),
                true,
                PaymentServiceInterface::CATEGORY_VIDEO,
                PaymentServiceInterface::CURRENCY_USD,
                $amount
            );
            $video->is_pay = true;
            $video->is_privacy = true;
            $video->save();
        } catch (Throwable $exception) {
            return $this->displayError([
                'amount' => $amount,
            ], 103);
        }

        dispatch(
            (new Notifications('video:request', [
                'id' => $video->id,
                'from' => User::getUserById($this->userId),
                'target' => $request->get('target_id'),
            ]))
        );

        NewRequestVideo::dispatch(
            $video->target_id,
            $video->user_id
        );

        $result = Video::getById($video->id, 'outgoing');

        return $this->displayOk($result);
    }

    public function videoViewed(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        try {
            $video = Video::findOrFail($request->get('id'));
            $video->update([
                'is_viewed' => true,
            ]);
            return $this->displayOk([]);
        } catch (Throwable $exception) {
            return $this->displayError([]);
        }
    }

}
