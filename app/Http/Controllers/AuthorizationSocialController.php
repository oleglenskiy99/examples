<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Repository\TokenzRepository;
use App\Services\V1\SocialNetworkService;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Laravel\Socialite\Facades\Socialite;
use Throwable;

// phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint
class AuthorizationSocialController extends Controller
{

    private SocialNetworkService $socialNetworkService;

    public function __construct(SocialNetworkService $socialNetworkService)
    {
        parent::__construct();

        $this->socialNetworkService = $socialNetworkService;
    }

    /**
     * @OA\Get(
     *     path="/auth.{provider}",
     *     summary="Socialite redirect to provider",
     *     tags={"Socialite"},
     *     description="Socialite redirect to provider",
     *     @OA\Parameter(
     *         name="provider",
     *         in="query",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *     ),
     * )
     */
    public function redirect(Request $request, string $provider)
    {
        if (!config()->has('services.' . $provider)) {
            return $this->displayError([], 9);
        }

        $this->socialNetworkService->putSession($request);

        if ($provider === 'instagram') {
            return Socialite::driver('instagrambasic')->redirect();
        }
        if ($provider === 'vk') {
            return Socialite::driver('vkontakte')->redirect();
        }

        return Socialite::driver($provider)->redirect();
    }

    /**
     * @OA\Post(
     *     path="/api/methods/social/add.{provider}",
     *     security={{"bearerAuth":{}}},
     *     summary="Add social network for user",
     *     description="Add social network for user",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Parameter(
     *         name="provider",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function socialAdd(Request $request, string $provider)
    {
        if (!config()->has('services.' . $provider)) {
            return $this->displayError([], 9);
        }

        if (!$this->accessToken) {
            return $this->displayError([], 9);
        }

        $this->socialNetworkService->putSession($request, $this->accessToken);

        if ($provider === 'instagram') {
            return Socialite::driver('instagrambasic')->stateless()->redirect();
        }
        if ($provider === 'vk') {
            return Socialite::driver('vkontakte')->stateless()->redirect();
        }

        return Socialite::driver($provider)->stateless()->redirect();
    }

    /**
     * @OA\Post(
     *     path="/auth/{provider}/delete",
     *     description="Delete social network for user",
     * @OA\Parameter(
     *         name="provider",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function deleteCallback(string $provider)
    {
        if (!config()->has('services.' . $provider)) {
            return $this->displayError([], 9);
        }

        if (!$this->accessToken) {
            return $this->displayError([], 9);
        }

        try {
            if ($provider === 'instagram') {
                $userSocial = Socialite::driver('instagrambasic')->stateless()->user();
            } elseif ($provider === 'vk') {
                $userSocial = Socialite::driver('vkontakte')->stateless()->user();
            } else {
                $userSocial = Socialite::driver($provider)->stateless()->user();
            }
        } catch (Throwable $e) {
            return $this->displayError([
                'message' => $e->getMessage(),
            ], 1);
        }

        $response = $this->socialNetworkService->socialDelete($userSocial, $provider, $this->userId);
        // config(app.front_url)
        return view('callback', array_merge($response, ['url' => '*']));
    }

    /**
     * @OA\Post(
     *     path="/auth/{provider}/callback",
     *     description="Callback for social network",
     * @OA\Parameter(
     *         name="provider",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function callback(string $provider): View
    {
        // получаем данные из соц. сети
        $userSocial = $this->socialNetworkService->getSocialNetworkUser($provider);
        // формируем объект с единой структурой
        $socialUserData = $this->socialNetworkService->getUserSocialData($userSocial, $provider);

        // Если получен токен, то это привязка соц. сети
        if (!session()->has('s_token')) {
            // если токен не был передан, то это авторизация
            $user = $this->socialNetworkService->authUserWithSocialNetwork($socialUserData);
            // возвращаем токен
            return $this->socialNetworkService->authSuccessResponse(
                $user
            );
        }

        try {
            // пытаемся прикрепить социальную сеть за пользователем
            $socialAccount = $this->socialNetworkService->attachSocialNetworkToUser(
                TokenzRepository::getUserByToken(
                    session()->get('s_token')
                ),
                $socialUserData
            );

            if ( $socialAccount === null) {
                return $this->socialNetworkService->returnFailResponse(
                    __('errors.network_exist'),
                    1
                );
            }

            return $this->socialNetworkService->addAccountResponse($socialAccount);

        } catch (Throwable $exception) {
            return $this->socialNetworkService->returnFailResponse(
                $exception->getMessage(),
                $exception->getCode()
            );
        }
    }

}
