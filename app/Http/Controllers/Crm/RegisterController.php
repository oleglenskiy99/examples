<?php

declare(strict_types=1);

namespace App\Http\Controllers\Crm;

use App\Helpers\CacheKey;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

// phpcs:disable Squiz.Strings.DoubleQuoteUsage.ContainsVar
class RegisterController extends Controller
{

    public function login(Request $request): string
    {
        $bearer = $request->bearerToken();

        $token = Hash::make($bearer . uniqid($request->get('user_id')));

        Cache::store('redis')->put(CacheKey::CRM_USER_KEY . ":{$token}", $request->get('user_id'), config('cache.token.crm_user_token'));

        return $token;
    }

    public function test(): string
    {
        return 'test succes';
    }

}
