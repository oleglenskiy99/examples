<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Repository\TokenzRepository;
use App\Services\V2\AuthService;
use App\Tokenz;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SessionsController extends ApiController
{

    private AuthService $authService;
    private TokenzRepository $tokenzRepository;

    public function __construct(AuthService $authService, TokenzRepository $tokenzRepository)
    {
        $this->authService = $authService;
        $this->tokenzRepository = $tokenzRepository;
    }

    /**
     * @OA\Get (
     *     path="/api/methods/sessions.get",
     *     summary="sessions.get",
     *     security={{"bearerAuth":{}}},
     *     operationId="sessions.get",
     *     description="sessions.get",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function get(): JsonResponse
    {
        $sessions = Tokenz::select(
            'id as session_id',
            'application_version',
            'device_model',
            'device_type',
            'device_os',
            'device_os_version',
            'device_manufacturer',
            'device',
            'ip',
            'city_name',
            'region_name',
            'country_name',
            'is_online',
            'timestamp'
        )
            ->where('user_id', $this->userId)
            ->where('access_token', '!=', $this->accessToken)->orderBy('timestamp', 'DESC')->get();

        $current = Tokenz::select(
            'id as session_id',
            'application_version',
            'device_model',
            'device_type',
            'device_os',
            'device_os_version',
            'device_manufacturer',
            'device',
            'ip',
            'city_name',
            'region_name',
            'country_name',
            'is_online',
            'timestamp'
        )
            ->where('access_token', $this->accessToken)->first();

        $sessions = $sessions ? $sessions->toArray() : [];

        $current = $current ? $current->toArray() : [];

        return $this->displayOk([
            'current' => $current,
            'active' => $sessions,
        ]);
    }

    /**
     * @OA\Post (
     *     path="/api/methods/sessions.revokeCurrent",
     *     summary="sessions.revokeCurrent",
     *     security={{"bearerAuth":{}}},
     *     operationId="sessions.revokeCurrent",
     *     description="sessions.revokeCurrent",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="devicePushToken",
     *         in="query",
     *         required=true,
     *         description="string",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function revokeCurrent(): JsonResponse
    {
        $this->tokenzRepository->revokeCurrent($this->current, $this->accessToken);
        return $this->displayOk([]);
    }

    public function revoke(Request $request): JsonResponse
    {
        $this->validate($request, ['id' => 'required:integer']);
        $this->tokenzRepository->revokeById($this->current, (int) $request->get('id'));
        return $this->displayOk([]);
    }

    public function revokeAll(): JsonResponse
    {
        $this->tokenzRepository->revokeAll($this->current, $this->accessToken);
        return $this->displayOk([]);
    }


    /**
     * @OA\Post(
     *     path="/api/methods/auth.refresh",
     *     summary="Refresh",
     *     tags={"Authentication"},
     *     security={ {"bearer_token": {} }},
     *     operationId="refresh",
     *     description="refresh",
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/ForbiddenException"
     *         ),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/UnauthorizedException"
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(
     *              ref="#/components/schemas/JwtTokenStructure"
     *         )
     *     ),
     * )
     */
    public function refresh(): JsonResponse
    {
        $jwt = $this->authService->refreshByToken($this->accessToken);

        return $this->displayOk([
            'access_token' => $jwt->getAccessToken(),
            'expires' => $jwt->getExpiresIn(),
            'token_type' => $jwt->getTokenType(),
        ]);
    }

}
