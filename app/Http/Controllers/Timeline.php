<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Timeline\Likes;
use App\Models\Timeline\Media;
use App\Models\Timeline\Post;
use Google\Service\Compute\Tags;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Timeline extends Controller
{

    /**
     * @OA\Post (
     *     path="/api/methods/timeline.publish",
     *     summary="timeline.publish",
     *     security={{"bearerAuth":{}}},
     *     operationId="timeline.publish",
     *     description="timeline.publish",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="type",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Parameter(
     *         name="media[]",
     *         in="query",
     *         required=true,
     *         description="array|integer",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function publish(Request $request): JsonResponse
    {
        $this->validate($request, [
            'media' => 'required',
            'type' => 'required',
        ]);

        $post = new Post();
        $post->user_id = $this->userId;
        $post->type = $request->type;
        $post->caption = $request->caption ?: '';
        $post->timestamp = time();
        $post->save();

        foreach ($request->media as $media_id) {

            $media = new Media();
            $media->post_id = $post->id;
            $media->media_id = $media_id;
            $media->save();

        }

        return $this->displayOk([]);
    }

    /**
     * @OA\Post (
     *     path="/api/methods/timeline.Delete",
     *     summary="timeline.Delete",
     *     security={{"bearerAuth":{}}},
     *     operationId="timeline.Delete",
     *     description="timeline.Delete",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="post_id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function delete(Request $request): JsonResponse
    {
        if (!$this->isAuthorized) {
            return $this->displayError([], 4);
        }

        $this->validate($request, [
            'post_id' => 'required:integer',
        ]);

        Post::where('id', $request->post_id)->where('user_id', $this->userId)->update(['is_deleted' => 1]);

        return $this->displayOk([]);
    }

    /**
     * @OA\Post (
     *     path="/api/methods/timeline.edit",
     *     summary="timeline.edit",
     *     security={{"bearerAuth":{}}},
     *     operationId="timeline.edit",
     *     description="timeline.edit",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="post_id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Parameter(
     *         name="caption",
     *         in="query",
     *         required=true,
     *         description="string перед текстом должен стоять символ #",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function edit(Request $request): JsonResponse
    {
        if (!$this->isAuthorized) {
            return $this->displayError([], 4);
        }

        $this->validate($request, [
            'post_id' => 'required:integer',
        ]);

        Post::where('id', $request->post_id)->where('user_id', $this->userId)
            ->update(['caption' => $request->caption]);

        /** @psalm-suppress UndefinedMethod * */
        Tags::assign($request->post_id, 'media', $request->caption);

        return $this->displayOk([]);
    }

    /**
     * @OA\Post (
     *     path="/api/methods/timeline.like",
     *     summary="timeline.like",
     *     security={{"bearerAuth":{}}},
     *     operationId="timeline.like",
     *     description="timeline.like",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="post_id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function like(Request $request): JsonResponse
    {
        if (!$this->isAuthorized) {
            return $this->displayError([], 4);
        }

        $this->validate($request, [
            'post_id' => 'required:integer',
        ]);

        $like = Likes::where('post_id', $request->post_id)->where('user_id', $this->userId)->first();

        if (is_null($like)) {
            Likes::create(['post_id' => $request->post_id, 'user_id' => $this->userId]);
        }

        return $this->displayOk([]);
    }

    /**
     * @OA\Post (
     *     path="/api/methods/timeline.unlike",
     *     summary="timeline.unlike",
     *     security={{"bearerAuth":{}}},
     *     operationId="timeline.unlike",
     *     description="timeline.unlike",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="post_id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function unlike(Request $request): JsonResponse
    {
        if (!$this->isAuthorized) {
            return $this->displayError([], 4);
        }

        $this->validate($request, [
            'post_id' => 'required:integer',
        ]);

        Likes::where('post_id', $request->post_id)->where('user_id', $this->userId)->delete();

        return $this->displayOk([]);
    }

    /**
     * @OA\Get(
     *     path="/api/methods/timeline.get",
     *     summary="timeline.get",
     *     security={{"bearerAuth":{}}},
     *     operationId="timeline.get",
     *     description="timeline.get",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="owner_id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function get(Request $request): JsonResponse
    {
        $this->validate($request, ['owner_id' => 'required:integer']);

        $media = Post::builder()
            ->where('user_id', $request->owner_id)
            ->paginate(18, ['*'], 'next_max_id');

        $items = $media->items();

        foreach ($items as $k => $item) {

            $item = $item->toArray();

            foreach ($item['media'] as $key => $value) {
                $item[$key] = $value;
            }

            unset($item['media']);
            $items[$k] = $item;
        }

        return $this->displayOk([
            'total_count' => $media->total(),
            'is_more_available' => $media->hasMorePages(),
            'next_max_id' => $media->hasMorePages() ? encrypt($media->currentPage() + 1) : null,
            'items' => $items,
        ]);
    }

    /**
     * @OA\Get(
     *     path="/api/methods/timeline.getNext",
     *     summary="timeline.getNext",
     *     security={{"bearerAuth":{}}},
     *     operationId="timeline.getNext",
     *     description="timeline.getNext",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="owner_id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function getNext(Request $request): JsonResponse
    {
        $this->validate($request, ['owner_id' => 'required:integer']);

        $media = Post::builder()
            ->where('user_id', $request->owner_id)
            ->paginate(12, ['*'], 'next_max_id');

        return $this->displayOk([
            'total_count' => $media->total(),
            'is_more_available' => $media->hasMorePages(),
            'next_max_id' => $media->hasMorePages() ? $media->currentPage() + 1 : null,
            'items' => $media->items(),
        ]);
    }

}
