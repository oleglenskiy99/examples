<?php

declare(strict_types=1);

namespace App\Http\Controllers;

// phpcs:disable Squiz.Strings.DoubleQuoteUsage.ContainsVar
use Illuminate\Http\JsonResponse;

class GetPrivacyTermsController extends Controller
{

    public function get(string $callstring): JsonResponse
    {
        // Options
        $secret_token = 'secret_ClI5Iy2NgaVv1rr3axZUUKYmWWyyN2vKporqm8ZN84O';
        $notion_version = '2021-08-16';
        $url = '';

        switch ($callstring) {
            case 'terms':
                $url = 'https://api.notion.com/v1/blocks/4be8b91cfdf4423da803b576fd921b9b/children';
                break;
            case 'policy':
                $url = 'https://api.notion.com/v1/blocks/5d493bf09fe0425488678d80aaafc659/children';
                break;
        }

        if (empty($url)) {
            return $this->displayError([
                'message' => __('errors.resource_not_found'),
            ], 17);
        }
        $data = $this->getData($url, $secret_token, $notion_version);
        return $this->displayOk((array) $data);
    }

    private function getData(string $url, string $token, string $version): string
    {
        // Init curl
        $ch = curl_init();
        // Set curl options
        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                "Authorization: Bearer {$token}",
                "Notion-Version: {$version}",
            ],
        ]);
        $response = curl_exec($ch);
        $response = json_decode($response);
        if (@$response->next_cursor) {
            $extra = $this->getData($url . '?start_cursor=' . $response->next_cursor, $token, $version);
            $response = (object) array_merge((array) $response->results, (array) $extra);
        }

        curl_close($ch);
        return $response;
    }

}
