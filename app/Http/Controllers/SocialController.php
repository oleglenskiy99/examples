<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Network;
use App\Models\User\Linked;
use Illuminate\Http\JsonResponse;

class SocialController extends ApiController
{

    /**
     * @OA\Post(
     *     path="/api/methods/social/del.{provider}",
     *     security={{"bearerAuth":{}}},
     *     summary="Delete social network for user",
     *     description="Delete social network for user",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Parameter(
     *         name="provider",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function socialDel(string $provider): JsonResponse
    {
        $network = Network::where('provider', $provider)->where('is_deleted', false)->first();

        if ($network === null) {
            return $this->displayError([], 9);
        }

        $linked = Linked::where('network_id', $network->id)
            ->where('user_id', $this->userId)
            ->where('is_deleted', false)
            ->first();

        if ($linked === null) {
            return $this->displayError([], 10);
        }

        $linked->is_deleted = true;
        $linked->save();

        return $this->displayOk([]);
    }

}
