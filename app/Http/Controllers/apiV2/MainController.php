<?php

declare(strict_types=1);

namespace App\Http\Controllers\apiV2;

use App\Errors;
use App\Tokenz;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class MainController extends Controller
{

    public User $user;
    public int $userId;

    /**
     * @param string $method
     * @param array $parameters
     */
    // phpcs:disable Squiz.Commenting.FunctionComment.TypeHintMissing
    public function callAction($method, $parameters): Response
    {
        try {
            $this->user = $this->getUser();
            if ($this->user) {
                $this->userId = $this->user->id;
            }
            return parent::callAction($method, $parameters);
        } catch (Throwable $exception) {

            if ($exception instanceof ValidationException) {
                return $this->displayError([
                    'params' => $exception->errors(),
                ], 100, Response::HTTP_BAD_REQUEST);
            }

            return $this->displayError([
                // TODO Can be removed later
                '_debug' => $exception->getMessage(),
            ], 1, Response::HTTP_INTERNAL_SERVER_ERROR);

        }
    }

    // phpcs:disable SlevomatCodingStandard.TypeHints.DisallowMixedTypeHint.DisallowedMixedTypeHint
    /**
     * @return mixed
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function getUser()
    {
        // JWT
        $authorizationToken = request()->headers->get('Authorization')
            // OLD
            ?? request()->headers->get('authorization')
            // Query Param
            ?? request()->get('access_token')
            // Query Param
            ?? request()->get('token');

        $token = Tokenz::where('access_token', $authorizationToken)->first();

        if ($token) {
            return User::find($token->userId);
        }

        return null;
    }

    public function displayError(array $data = [], int $errCode = 1, int $statusCode = Response::HTTP_OK): JsonResponse
    {
        if (empty($data['message'])) {
            $data['message'] = Errors::get($errCode);
        }

        return response()->json(
            array_merge([
                'status' => 'fail',
            ], $data),
            $statusCode
        );
    }

    public function validate(Request $request, array $rules = []): void
    {
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            throw ValidationException::withMessages(
                $validator->errors()->messages()
            );
        }
    }

    public function displayOk(array $data = [], int $statusCode = Response::HTTP_OK): JsonResponse
    {
        return response()->json(
            array_merge([
                'status' => 'ok',
            ], [
                'result' => $data,
            ]),
            $statusCode
        );
    }

}
