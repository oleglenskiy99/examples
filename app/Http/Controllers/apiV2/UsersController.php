<?php

declare(strict_types=1);

namespace App\Http\Controllers\apiV2;

use App\Events\ConfirmEmailEvent;
use App\Events\User\UpdateUserEvent;
use App\Models\User\Categories;
use App\Models\User\Info;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends MainController
{

    /**
     * @OA\Post(
     *     path="/api/methods/user.update",
     *     summary="user get",
     *     security={{"bearerAuth":{}}},
     *     operationId="user.set",
     *     description="Update user profile information",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="firstName",
     *         in="query",
     *         required=false,
     *         description="User first name",
     *     ),
     * @OA\Parameter(
     *         name="lastName",
     *         in="query",
     *         required=false,
     *         description="User last name",
     *     ),
     * @OA\Parameter(
     *         name="country",
     *         in="query",
     *         required=false,
     *         description="Country id",
     *     ),
     * @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=false,
     *         description="User email for change",
     *     ),
     * @OA\Parameter(
     *         name="categories",
     *         in="query",
     *         required=false,
     *         description="Array of categories ids for update",
     *     ),
     *
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     * @OA\Property (property="status",type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function updateUserProfile(Request $request): JsonResponse
    {
        $this->validate($request, [
            'firstName' => 'required_without_all:lastName,biography,categories,country,email',
        ]);

        $user = Info::find(config()->get('user_id'));
        if ($user !== null) {
            $user = new Info();
            $user->id = config()->get('user_id');
        }

        if ($request->has('firstName')) {
            $user->firstName = $request->get('firstName');
        }

        if ($request->has('lastName')) {
            $user->lastName = $request->get('lastName') ?? '';
        }

        if ($request->has('biography')) {
            $user->biography = $request->get('biography') ?? '';
        }

        if ($request->has('country')) {
            $user->country = $request->get('country');
        }

        if ($request->has('categories')) {
            Categories::where('user_id', config()->get('user_id'))->delete();
            $inserts = [];
            foreach ($request->categories as $category) {
                $inserts[] = ['user_id' => $this->userId, 'category_id' => $category];
            }
            Categories::insert($inserts);
        }
        /** @psalm-suppress UndefinedThisPropertyFetch * */
        if ($request->has('email') && (mb_strtolower(trim($this->current->email)) !== mb_strtolower(trim($request->get('email'))))) {

            $this->validate($request, [
                'email' => 'email',
            ]);

            DB::table('module_user_pd_archives')->insert([
                'user_id' => $this->current->id,
                'value' => $this->current->email,
                'type' => 'email',
            ]);

            $this->current->update([
                'email' => $request->get('email'),
                'is_email_verify' => false,
            ]);

            ConfirmEmailEvent::dispatch(config()->get('user_id'));

        }

        $user->save();

        UpdateUserEvent::dispatch(config()->get('user_id'));

        return $this->displayOk([]);
    }

}
