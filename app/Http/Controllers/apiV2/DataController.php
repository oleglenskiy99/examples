<?php

declare(strict_types=1);

namespace App\Http\Controllers\apiV2;

use App;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class DataController extends MainController
{

    public function getData(): JsonResponse
    {
        $response = [];
        $locale = App::getLocale();

        $response['country'] = DB::table('module_countries')->select(['id', 'name', 'phonecode', 'sortname'])->get()->toArray();
        $response['networks'] = DB::table('module_networks')->get()->toArray();
        $response['industry'] = DB::table('module_categories')->select(
            ['id', sprintf('name_%s as name', $locale), 'default']
        )->get()->toArray();
        $response['category'] = DB::table('module_channels_categories')->select(
            ['id', sprintf('name_%s as name', $locale)]
        )->get()->toArray();
        $response['twideo'] = DB::table('module_videos_categories')->select(
            ['id', sprintf('name_%s as name', $locale), 'icon']
        )->get()->toArray();

        return $this->displayOk($response);
    }

}
