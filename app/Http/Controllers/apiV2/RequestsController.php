<?php

declare(strict_types=1);

namespace App\Http\Controllers\apiV2;

use App\Events\Notify\NewRequestVideo;
use App\Jobs\Notifications;
use App\Models\Chats\Chat;
use App\Models\Chats\Members;
use App\Models\Chats\Messages;
use App\Repository\Requests\ChatsRepository;
use App\Repository\Requests\VideoRepository;
use App\Services\PaymentServiceInterface;
use App\User;
use App\Video;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class RequestsController extends MainController
{

    protected PaymentServiceInterface $payments;

    public function __construct(PaymentServiceInterface $payments)
    {
        $this->payments = $payments;
    }


    public function getRequests(): JsonResponse
    {
        $user = $this->user;

        $requests = collect([]);

        $videos = VideoRepository::getList($user);
        $chats = ChatsRepository::getList($user);

        $requests->merge($videos)->merge($chats);

        $result = $requests->merge($videos)->merge($chats)->sortByDesc('timestamp')->toArray();

        return $this->displayOk(
            array_values($result)
        );
    }

    public function createVideoRequest(Request $request): JsonResponse
    {
        $this->validate($request, [
            'message' => [
                'required',
            ],
            'target_id' => [
                'required',
                'integer',
            ],
        ]);

        $amount = intval($request->get('cost'));

        $defaultPrice = $this->payments->getCategoryPrice(
            User::find($request->get('target_id')),
            PaymentServiceInterface::CATEGORY_VIDEO
        );

        if ($amount < $defaultPrice) {
            $amount = $defaultPrice;
        }

        try {
            $charge_id = $this->payments->createPayment(
                $this->user,
                true,
                PaymentServiceInterface::CATEGORY_VIDEO,
                PaymentServiceInterface::CURRENCY_USD,
                $amount
            );
        } catch (Throwable $exception) {
            return $this->displayError([
                'amount' => $amount,
                'message' => $exception->getMessage(),
            ], 103, 402);
        }

        $video = Video::create([
            'user_id' => config()->get('user_id'),
            'target_id' => $request->get('target_id'),
            // TODO make nullable or drop
            'type' => -1,
            'instruction' => $request->get('message'),
            // TODO make nullable or drop
            'category' => 9,
            'cost' => $amount,
            'charge_id' => $charge_id,
            // TODO make nullable or drop
            'deadline' => strtotime('1 hour'),
            'is_privacy' => true,
            'is_pay' => true,
        ]);

        dispatch(
            (new Notifications('video:request', [
                'id' => $video->id,
                'from' => User::getUserById(config()->get('user_id')),
                'target' => $request->get('target_id'),
            ]))
        );

        NewRequestVideo::dispatch(
            $video->target_id,
            $video->user_id
        );

        $result = VideoRepository::getById($this->user, $video->id);

        return $this->displayOk((array) $result);
    }

    public function createPrivateChatRequest(Request $request): JsonResponse
    {
        $this->validate($request, [
            'title' => 'required',
            'message' => 'required',
            'target_id' => 'required|integer',
        ]);

        try {

            $chat = Chat::where([
                ['expert_id', $request->get('target_id')],
                ['creator_id', config()->get('user_id')],
            ])->orWhere([
                ['creator_id', $request->get('target_id')],
                ['expert_id', config()->get('user_id')],
            ])->firstOrFail();

        } catch (Throwable $exception) {

            $chat = Chat::create([
                'title' => $request->get('title'),
                'creator_id' => config()->get('user_id'),
                'expert_id' => $request->get('target_id'),
            ]);

            Members::insert([
                ['dialog_id' => $chat->id, 'user_id' => $chat->creator_id],
                ['dialog_id' => $chat->id, 'user_id' => $chat->expert_id],
            ]);

        }

        $amount = intval($request->get('cost'));

        $defaultPrice = $this->payments->getCategoryPrice(
            User::find($chat->expert_id),
            PaymentServiceInterface::CATEGORY_MESSAGE
        );

        if ($amount < $defaultPrice) {
            $amount = $defaultPrice;
        }

        try {
            $charge_id = $this->payments->createPayment(
                $this->user,
                true,
                PaymentServiceInterface::CATEGORY_MESSAGE,
                PaymentServiceInterface::CURRENCY_USD,
                $amount
            );
        } catch (Throwable $exception) {
            return $this->displayError([
                'amount' => $amount,
            ], 103, 402);
        }

        Messages::create([
            'dialog_id' => $chat->id,
            'user_id' => config()->get('user_id'),
            'message' => $request->get('message'),
            'type' => 'message',
            'timestamp' => time(),
            'is_paid' => true,
            'cost' => $amount,
            'charge_id' => $charge_id,
        ]);

        $chat = ChatsRepository::getById($this->user, $chat->id);

        return $this->displayOk((array) $chat);
    }

}
