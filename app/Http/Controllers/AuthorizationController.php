<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Events\User\RegisterUserEvent;
use App\Helpers\SMSTexts;
use App\Helpers\UserHelper;
use App\Models\User\Info as UserInfo;
use App\Repository\AuthorizationCodesRepository;
use App\Repository\TokenzRepository;
use App\Services\SendSMSServiceInterface;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthorizationController extends Controller
{

    public function process(Request $request): void
    {
        if ($request->identification) {
            $this->complete($request);
        } else {
            $this->twofactor($request);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/methods/auth",
     *     summary="Two Factor Authorization",
     *     operationId="auth",
     *     description="Номер телефона пользователя, который начинается с +7 или 7.",
     * @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="string",
     *                  title="i"
     *              )
     *         ),
     *     ),
     * )
     */
    public function twofactor(Request $request, ?SendSMSServiceInterface $serviceSMS = null): JsonResponse
    {
        $this->validate($request, [
            'phone' => 'required',
        ]);

        $user = User::getUserByPhone($request->phone);

        if ($user && $user->is_blocked) {
            return $this->displayError([
                'message' => 'This phone number exists in another user account and user is blocked',
            ], 9);
        }

        $codes = AuthorizationCodesRepository::createCode($request->phone);

        $message = __(SMSTexts::VERIFIED_TEXT) . ' ' . $codes->code;

        $messageID = $serviceSMS->send($request->phone, $message);

        if ($messageID === null) {
            return $this->displayError([
                'message' => __('errors.authorization'),
            ], 1);
        }

        return $this->displayOk([
            'i' => $codes->identification,
            's' => $messageID,
            'n' => is_null($user),
        ]);
    }

    /**
     * @OA\Get(
     *     path="/api/methods/auth.confirm",
     *     summary="Two Factor Authorization",
     *     operationId="auth.confirm",
     *     description="Метод запроса кода подтверждения, который будет отправлен на переданный номер телефона.",
     * @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(
     *                          type="object",
     * @OA\Property(
     *                              property="access_token",
     *                              type="integer",
     *                              description="Ключ доступа, который необходимо передавать при каждом запросе к API."
     *                          ),
     * @OA\Property(
     *                              property="call_id",
     *                              type="string",
     *                              description="Идентификатор пользователя для P2P вещания, должен использоваться, как ID для подключения к медиа серверу."
     *                          ),
     * @OA\Property(
     *                              property="created",
     *                              type="datetime",
     *                              description="Время создания ключа доступа в UNIX формате."
     *                          ),
     * @OA\Property(
     *                              property="expires",
     *                              type="string",
     *                              description="Время жизни ключа доступа."
     *                          ),
     * @OA\Property(
     *                              property="user",
     *                              type="object",
     *                              ref="#/components/schemas/User"
     *                          ),
     *                      ),
     *                  )
     *              ),
     *     ),
     * )
     */
    public function confirm(Request $request): JsonResponse
    {
        $this->validate($request, [
            'phone' => 'required',
            'i' => 'required:string',
            'c' => 'required:integer',
            'utm' => 'string',
        ]);

        $user = User::getUserByPhone($request->phone);

        if ($user === null) {
            $user = new User();

            $user->phone = $request->phone;
            $user->balance = 0;
            $user->is_blocked = 0;
            $user->is_verify = 1;
            $user->last_activity = time();
            $user->onboarding = 'name';
            $user->save();
            $user->update(['domain' => UserHelper::getDomainName($user->id)]);

            UserInfo::where('id', $user->id)->update([
                'firstName' => $request->name,
                'lastName' => '',
            ]);
            //register new user Event;
            RegisterUserEvent::dispatch($user->id);
        }

        $access = AuthorizationCodesRepository::getCode([
            'identification' => $request->i ?? '',
            'additional' => '',
            'code' => $request->c,
        ]);

        if ($access === null) {
            return $this->displayError([
                'i' => $request->i,
                'c' => $request->c,
            ], 11);
        }

        $tokens = TokenzRepository::afterComplete($user, $request);

        return $this->displayOk([
            'access_token' => $tokens->access_token,
            'call_id' => $tokens->call_id,
            'created' => $tokens->timestamp,
            'expires' => $tokens->expires,
            'user' => $user->toArray(),
        ]);
    }

}
