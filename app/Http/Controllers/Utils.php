<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\V1\UsersService;
use App\Services\V1\UtilsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Utils extends Controller
{

    private UtilsService $utilsService;
    private UsersService $usersService;

    public function __construct(UtilsService $utilsService, UsersService $userService)
    {
        parent::__construct();

        $this->utilsService = $utilsService;
        $this->usersService = $userService;
    }
    /**
     * @OA\Get(
     *     path="/api/methods/user.get",
     *     summary="user get",
     *     security={{"bearerAuth":{}}},
     *     operationId="user.get",
     *     description="get User",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="id",
     *         in="query",
     *         required=false,
     *         description="nothing|user id|domain name",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function getUser(Request $request): JsonResponse
    {
        if ($request->has('id')) {
            $result = $this->usersService->getUserProfile((string) $request->id);
        } elseif ($request->has('domain')) {
            $result = $this->usersService->getUserProfile((string) $request->domain, true);
        } else {
            $result = $this->usersService->getUserProfile((string) $this->userId, true);
        }
        if ($result === null) {
            return $this->displayError([], 8);
        }
        return $this->displayOk([
            'user' => $result,
        ]);
    }

    /**
     * @OA\Get (
     *     path="/api/methods/database.get",
     *     summary="utils.getDatabases",
     *     operationId="database.get",
     *     description="get reference bases",
     * @OA\Parameter(
     *         name="fields",
     *         in="query",
     *         required=true,
     *         description="string fields country,industry,networks,category,twideo,prices",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function getDatabases(Request $request): JsonResponse
    {
        $this->validate($request, ['fields' => 'required:string']);

        if ($request->fields === null) {
            $this->undefinedValueError($request, 'fields');
        }

        $fields = explode(',', $request->fields);
        $response = $this->utilsService->getDatabases($fields);

        return $this->displayOk($response);
    }

    /**
     * @OA\Get (
     *     path="/api/methods/utils.checkDomainName",
     *     summary="utils.checkDomainName",
     *     security={{"bearerAuth":{}}},
     *     operationId="utils.checkDomainName",
     *     description="utils.checkDomainName",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="domain",
     *         in="query",
     *         required=true,
     *         description="string",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function checkDomainAvailable(Request $request): JsonResponse
    {
        $this->validate($request, [
            'domain' => 'required:filled',
        ]);

        $isUserDomainAvailable = $this->utilsService->isUserDomainAvailable(
            $request->get('domain'),
            $this->userId
        );

        if ( $isUserDomainAvailable) {
            return $this->displayOk([]);
        }

        return $this->displayError([
            'isBackground' => true,
        ], 13);
    }

    /**
     * @OA\Get (
     *     path="/api/methods/utils.getLocation",
     *     summary="utils.getLocation",
     *     security={{"bearerAuth":{}}},
     *     operationId="utils.getLocation",
     *     description="utils.getLocation",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function location(): JsonResponse
    {
        return $this->displayOk([
            'location' => $this->utilsService->getLocation(),
        ]);
    }

    /**
     * @OA\Get (
     *     path="/api/methods/utils.getIp",
     *     summary="utils.getIp",
     *     security={{"bearerAuth":{}}},
     *     operationId="utils.getIp",
     *     description="utils.getIp",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    // phpcs:disable Squiz.Strings.DoubleQuoteUsage.ContainsVar
    // phpcs:disable PSR2.Methods.FunctionCallSignature.CloseBracketLine
    public function geoIp(): JsonResponse
    {
        return $this->displayOk($this->utilsService->getGeoIP());
    }

}
