<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Helpers\Chats\AttachmentsHelper;
use App\Helpers\KeyMessages;
use App\Models\Chats\Chat;
use App\Models\Chats\Members;
use App\Models\Chats\Messages;
use App\Repository\Chats\AttachmentsRepository;
use App\Repository\Chats\ChatsRepository;
use App\Repository\Chats\MessagesRepository;
use App\Services\V1\ChatsService;
use App\Services\V1\MessageService;
use Emitter;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Throwable;

class ChatsController extends ApiController
{

    private ChatsService $chatsService;
    private MessageService $messageService;

    public function __construct(ChatsService $chatsService, MessageService $messageService)
    {
        parent::__construct();

        $this->chatsService = $chatsService;
        $this->messageService = $messageService;
    }

    /**
     * @OA\Get(
     *     path="/api/methods/chats.getById",
     *     summary="chats getById",
     *     security={{"bearerAuth":{}}},
     *     operationId="chats.getById",
     *     description="unsubscribe",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function getById(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required:integer',
        ]);

        $result = $this->chatsService->getById($this->userId, (int) $request->id);

        if ($result === null) {
            return $this->displayError();
        }

        return $this->displayOk($result);
    }

    /**
     * @OA\Get(
     *     path="/api/methods/chats.getList",
     *     summary="chats getList",
     *     security={{"bearerAuth":{}}},
     *     operationId="chats.getList",
     *     description="getList",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function lists(): JsonResponse
    {
        $items = ChatsRepository::items($this->userId, 0, false);

        return $this->displayOk([
            'pinned' => [],
            'items' => $items,
            'archived' => [],
        ]);
    }

    /**
     * @OA\Get(
     *     path="/api/methods/chats.get",
     *     summary="chats get",
     *     security={{"bearerAuth":{}}},
     *     operationId="chats.get",
     *     description="get get",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="user_id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function get(Request $request): JsonResponse
    {
        $this->validate($request, [
            'user_id' => 'required:integer',
        ]);

        $result = $this->chatsService->getChat($this->userId, (int) $request->user_id);

        if ($result === null) {
            return $this->displayError();
        }

        return $this->displayOk([
            'id' => $result,
        ]);
    }

    public function getMediaAround(Request $request): JsonResponse
    {
        $attachments = AttachmentsRepository::builder()->with('media')->where('media_id', '<', $request->media_id)->where(
            'dialog_id',
            $request->dialog_id
        )->where(
            function ($q): void {
                $q->where('type', 'image')->orWhere('type', 'video');
            }
        )->paginate(50, ['*'], 'next_max_id');

        $history = AttachmentsRepository::builder()->with('media')->where('media_id', '>=', $request->media_id)->where(
            'dialog_id',
            $request->dialog_id
        )->where(
            function ($q): void {
                $q->where('type', 'image')->orWhere('type', 'video');
            }
        )->get()->toArray();

        return $this->displayOk([
            'total_count' => $attachments->total() + count($history),
            'index' => count($history) - 1,
            'is_more_available' => $attachments->hasMorePages(),
            'next_max_id' => $attachments->hasMorePages() ? encrypt($attachments->currentPage() + 1) : null,
            'items' => array_merge(AttachmentsHelper::format($history), AttachmentsHelper::format($attachments->items())),
        ]);
    }

    public function loadMoreAttachments(Request $request): JsonResponse
    {
        if ($request->next_max_id) {
            Paginator::currentPageResolver(function () use ($request) {
                try {
                    $currentPage = decrypt($request->next_max_id);
                } catch (DecryptException $ex) {
                    return $this->displayError(['message' => $ex->getMessage()], 5);
                }

                return $currentPage;
            });
        }

        switch ($request->section) {
            case 'media':
                $data = AttachmentsRepository::builder()->with('media')->where('dialog_id', $request->id)->where(function ($q): void {
                    $q->where('type', 'image')->orWhere('type', 'video');
                })->paginate(50, ['*'], 'next_max_id');

                break;

            case 'files':
                $data = AttachmentsRepository::builder()->with('media')->where('dialog_id', $request->id)->where('type', 'file')->paginate(
                    50,
                    ['*'],
                    'next_max_id'
                );
                break;
            case 'links':
                $data = AttachmentsRepository::builder()->with('media')->where('dialog_id', $request->id)->where('type', 'link')->paginate(
                    50,
                    ['*'],
                    'next_max_id'
                );
                break;
            case 'voices':
                $data = AttachmentsRepository::builder()->with('media')->where('dialog_id', $request->id)->where('type', 'voice')->paginate(
                    50,
                    ['*'],
                    'next_max_id'
                );
                break;
        }

        switch ($request->section) {
            case 'media':
                $items = AttachmentsHelper::format($data->items());
                break;
            case 'files':
                $items = AttachmentsHelper::formatFiles($data->items());
                break;
            case 'links':
                $items = AttachmentsHelper::formatLinks($data->items());
                break;
            case 'voices':
                $items = AttachmentsHelper::formatVoices($data->items());
                break;
        }

        return $this->displayOk([
            'total_count' => $data->total(),
            'is_more_available' => $data->hasMorePages(),
            'next_max_id' => $data->hasMorePages() ? encrypt($data->currentPage() + 1) : null,
            'items' => $items,
        ]);
    }

    public function getAllMedia(Request $request): JsonResponse
    {
        if ($request->next_max_id) {
            Paginator::currentPageResolver(function () use ($request) {
                try {
                    $currentPage = decrypt($request->next_max_id);
                } catch (DecryptException $ex) {
                    return $this->displayError(['message' => $ex->getMessage()], 5);
                }

                return $currentPage;
            });

            $this->validate($request, [
                'section' => 'required',
            ]);

            $sections = [$request->section];
        } else {
            $sections = $request->sections && count(
                $request->sections
            ) > 0
                ? $request->sections
                : ['chats', 'media', 'files', 'voices', 'links'];
        }

        return $this->displayOk(
            $this->chatsService->getAllMedia($sections, $this->userId)
        );
    }

    public function getAllChatMedia(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        if ($request->next_max_id) {
            Paginator::currentPageResolver(function () use ($request) {
                try {
                    $currentPage = decrypt($request->next_max_id);
                } catch (DecryptException $ex) {
                    return $this->displayError(['message' => $ex->getMessage()], 5);
                }

                return $currentPage;
            });

            $this->validate($request, [
                'section' => 'required',
            ]);

            $sections = [$request->section];
        } else {
            $sections = $request->sections && count(
                $request->sections
            ) > 0
                ? $request->sections
                : ['chats', 'media', 'files', 'voices', 'links'];
        }

        return $this->displayOk(
            $this->chatsService->getAllChatMedia($sections, (int) $request->id)
        );
    }

    public function getChatMedia(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        return $this->displayOk(
            $this->chatsService->getChatMedia((int) $request->id, $this->userId, $request->offset_id, $request->last_stored_id)
        );
    }

    /**
     * @OA\Get(
     *     path="/api/methods/chats.getHistory",
     *     summary="chats get",
     *     security={{"bearerAuth":{}}},
     *     operationId="chats.get",
     *     description="get get",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Parameter(
     *         name="offset_id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function getHistory(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required|numeric|filled',
            // 9715
            'offset_id' => 'required|numeric|filled',
            'limit' => 'required|numeric|filled',
        ]);

        $limit = $request->exclude ? $request->limit : round($request->limit / 2);

        $chat = Chat::where('id', $request->id)->first();

        if ($chat === null) {
            return $this->displayError(['isBackground' => true], 15);
        }

        $offset_id = $request->offset_id ?: 0;

        $top = MessagesRepository::get((int) $request->id, $this->userId)
            ->where('id', '<', $offset_id)
            ->orderBy('id', 'desc')
            ->limit($limit)->get();

        $middle = $request->exclude
            ? []
            : MessagesRepository::get((int) $request->id, $this->userId)
            ->where('id', '>=', $offset_id)
            ->limit($limit)->get();

        foreach ($top as $k => $item) {
            $top[$k] = Messages::format($item);
        }

        foreach ($middle as $k => $item) {
            $middle[$k] = Messages::format($item);
        }

        return $this->displayOk([
            'top' => array_reverse($top->toArray()),
            'middle' => $request->exclude ? [] : $middle->toArray(),
        ]);
    }

    /**
     * @OA\Get(
     *     path="/api/methods/chats.getMoreMessages",
     *     summary="chats get",
     *     security={{"bearerAuth":{}}},
     *     operationId="chats.get",
     *     description="get get",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Parameter(
     *         name="offset_id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function getMoreMessages(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required|numeric|filled',
            'offset_id' => 'required|numeric|filled',
            'limit' => 'required|numeric|filled',
        ]);

        $chat = Chat::where('id', $request->id)->first();

        if ($chat === null) {
            return $this->displayError(['isBackground' => true], 15);
        }

        $middle = MessagesRepository::get($request->id, $this->userId)
            ->where('id', '>', $request->offset_id)
            ->limit($request->limit)->get();

        foreach ($middle as $k => $item) {
            try {
                $middle[$k] = Messages::format($item);
            } catch (Throwable $ex) {
                echo $ex->getMessage();
            }
        }

        return $this->displayOk([
            'items' => $middle,
        ]);
    }

    /**
     * @OA\Post(
     *     path="/api/methods/chats.deleteMessage",
     *     summary="Delete messages from chat",
     *     operationId="chats",
     *     description="Delete messages from chat",
     * @OA\Parameter(
     *         name="ids",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Parameter(
     *         name="dialog_id",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="string",
     *                  title="i"
     *              )
     *         ),
     *     ),
     * )
     */
    public function deleteMessage(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required|array',
            'dialog_id' => 'required|numeric|filled',
        ]);
        /* TODO to service */

        $deleted_ids = $this->messageService->deleteForUser($request->ids, $request->dialog_id, $this->userId);

        if (count($deleted_ids) === 0) {
            return $this->displayError([], 6);
        }

        $emitter = new Emitter();

        $emitter->to(KeyMessages::toChatDialog($request->dialog_id))->emit(
            KeyMessages::toEmitMessageDelete($request->dialog_id),
            [
                'ids' => $deleted_ids,
                'dialog_id' => $request->dialog_id,
            ]
        );

        return $this->displayOk([]);
    }

    /**
     * @OA\Post(
     *     path="/api/methods/chats.deleteMessages",
     *     summary="Delete messages from chat",
     *     operationId="chats",
     *     description="Delete messages from chat",
     * @OA\Parameter(
     *         name="ids",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Parameter(
     *         name="dialog_id",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="string",
     *                  title="i"
     *              )
     *         ),
     *     ),
     * )
     */
    public function deleteMessages(Request $request): JsonResponse
    {
        $this->validate($request, [
            'ids' => 'required|array',
            'dialog_id' => 'required|numeric|filled',
        ]);
        $deleted_ids = $this->messageService->deleteOther($request->ids);

        if (count($deleted_ids) === 0) {
            return $this->displayError([], 6);
        }

        /* TODO to service */
        $emitter = new Emitter();
        $emitter->to(KeyMessages::toChatDialog($request->dialog_id))->emit(
            KeyMessages::toEmitMessageDelete($request->dialog_id),
            [
                'ids' => $deleted_ids,
                'dialog_id' => $request->dialog_id,
            ]
        );

        return $this->displayOk([]);
    }

    /**
     * @OA\Post(
     *     path="/api/methods/chats.pin",
     *     summary="chats pin",
     *     security={{"bearerAuth":{}}},
     *     operationId="chats.pin",
     *     description="set pin",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="ids[]",
     *         in="query",
     *         required=true,
     *         description="array",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function setPinUserChat(Request $request): JsonResponse
    {
        $this->validate($request, [
            'ids' => 'required|array',
        ]);

        Members::whereIn('dialog_id', $request->ids)
            ->where('user_id', $this->userId)
            ->update(['is_pinned' => 1]);

        return $this->displayOk([]);
    }

    /**
     * @OA\Post(
     *     path="/api/methods/chats.unpin",
     *     summary="chats pin",
     *     security={{"bearerAuth":{}}},
     *     operationId="chats.unpin",
     *     description="set unpin",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="ids[]",
     *         in="query",
     *         required=true,
     *         description="array",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function setUnPinUserChat(Request $request): JsonResponse
    {
        Members::whereIn('dialog_id', $request->ids)
            ->where('user_id', $this->userId)
            ->update(['is_pinned' => 0]);

        return $this->displayOk([]);
    }

    /**
     * @OA\Post(
     *     path="/api/methods/chats.markAllRead",
     *     summary="chats markAllRead",
     *     security={{"bearerAuth":{}}},
     *     operationId="chats.markAllRead",
     *     description="get markAllRead",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="ids[]",
     *         in="query",
     *         required=true,
     *         description="array",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function markAllRead(Request $request): JsonResponse
    {
        $this->validate($request, [
            'ids' => 'required|array',
        ]);

        Members::whereIn('dialog_id', $request->ids)->where('user_id', $this->userId)->update([
            'unread_messages_count' => 0,
        ]);

        return $this->displayOk([]);
    }

}
