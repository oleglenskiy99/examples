<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\DTO\Request\ChatCreate;
use App\DTO\Request\ChatHistoryRequest;
use App\DTO\Request\SendMessage;
use App\Http\Requests\V2\CreateChatRequest;
use App\Http\Requests\V2\MessagesHistoryRequest;
use App\Http\Requests\V2\SendMessageRequest;
use App\Services\V2\ChatsService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ChatsController
{

    private ChatsService $chatsService;
    private SerializerInterface $serializer;
    private DenormalizerInterface $denormalizer;

    public function __construct(
        ChatsService $chatsService,
        SerializerInterface $serializer,
        DenormalizerInterface $denormalizer
    )
    {
        $this->chatsService = $chatsService;
        $this->serializer = $serializer;
        $this->denormalizer = $denormalizer;
    }


    /**
     * @OA\Get(
     *     path="/api/v2/chats/history",
     *     summary="Chats",
     *     tags={"Chats"},
     *     security={ {"bearer_token": {} }},
     *     description="Get chat messages",
     *     @OA\Parameter(
     *         name="chatId",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="Current chat id",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="Current page number",
     *         )
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response (
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/ChatMessageItem"),
     *         )
     *     ),
     * )
     */
    public function getHistory(MessagesHistoryRequest $request): JsonResponse
    {
        $chatHistory = $this->denormalizer->denormalize(
            $request->query(),
            ChatHistoryRequest::class,
            JsonEncoder::FORMAT
        );

        $data = $this->chatsService->getChatHistory($chatHistory);
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

    /**
     * @OA\Post(
     *     path="/api/v2/chats/create",
     *     summary="Chats",
     *     tags={"Chats"},
     *     security={ {"bearer_token": {} }},
     *     description="Create new chat with user",
     *     @OA\Parameter(
     *         name="title",
     *         in="query",
     *         @OA\Schema(
     *            type="string",
     *            example="Chat title",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="userId",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="User Id",
     *         )
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response (
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Schema(ref="#/components/schemas/ChatItem"),
     *         )
     *     ),
     * )
     */
    public function create(CreateChatRequest $request): JsonResponse
    {
        $chatCreate = $this->denormalizer->denormalize(
            $request->query(),
            ChatCreate::class,
            JsonEncoder::FORMAT
        );

        $data = $this->chatsService->create($chatCreate);
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }


    /**
     * @OA\Post(
     *     path="/api/v2/chats/send",
     *     summary="Chats",
     *     tags={"Chats"},
     *     security={ {"bearer_token": {} }},
     *     description="Send message to chat",
     *     @OA\Parameter(
     *         name="chatId",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="123",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="text",
     *         in="query",
     *         @OA\Schema(
     *            type="string",
     *            example="Message text",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="mediaIds",
     *         in="query",
     *         @OA\Items(
     *              type="array",
     *              example="[1,2,3,4,5]",
     *              @OA\Items()
     *          ),
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response (
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Schema(ref="#/components/schemas/ChatMessageItem"),
     *         )
     *     ),
     * )
     */
    public function sendMessage(SendMessageRequest $sendMessageRequest): JsonResponse
    {
        $sendMessage = $this->denormalizer->denormalize(
            $sendMessageRequest->query(),
            SendMessage::class,
            JsonEncoder::FORMAT
        );

        $data = $this->chatsService->sendChatMessage($sendMessage);
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
