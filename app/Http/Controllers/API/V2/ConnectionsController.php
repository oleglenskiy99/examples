<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\DTO\Request\ConnectionsRequest;
use App\Http\Requests\V2\ConnectionsGetRequest;
use App\Services\V2\ConnectionsService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ConnectionsController
{

    private ConnectionsService $connectionsService;
    private SerializerInterface $serializer;
    private DenormalizerInterface $denormalizer;

    public function __construct(
        ConnectionsService $connectionsService,
        SerializerInterface $serializer,
        DenormalizerInterface $denormalizer
    )
    {
        $this->connectionsService = $connectionsService;
        $this->serializer = $serializer;
        $this->denormalizer = $denormalizer;
    }

    /**
     * @OA\Get (
     *     path="/api/v2/connections",
     *     summary="Get my connections",
     *     tags={"Connections"},
     *     security={ {"bearer_token": {} }},
     *     description="Get connections for current user",
     *     @OA\Parameter(
     *         name="isOnlyNew",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="1",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/ConnectionsList")),
     *     ),
     * )
     */
    public function getAll(): JsonResponse
    {
        $connectionsRequest = $this->denormalizer->denormalize(
            request()->query(),
            ConnectionsRequest::class,
            JsonEncoder::FORMAT
        );

        $data = $this->connectionsService->getCurrentUserConnections($connectionsRequest);
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }


    /**
     * @OA\Get (
     *     path="/api/v2/connections/with-user",
     *     summary="Get connections with user",
     *     tags={"Connections"},
     *     security={ {"bearer_token": {} }},
     *     description="Get connections between current and provided users",
     *     @OA\Parameter(
     *         name="userId",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="1",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="section",
     *         in="query",
     *         description="1 - incoming, 2 - outgoing",
     *         @OA\Schema(
     *            type="int",
     *            example="1",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/ConnectionsList")),
     *     ),
     * )
     */
    public function getByUser(ConnectionsGetRequest $request): JsonResponse
    {
        $connectionsRequest = $this->denormalizer->denormalize(
            $request->query(),
            ConnectionsRequest::class,
            JsonEncoder::FORMAT
        );

        $data = $this->connectionsService->getWithUserById($connectionsRequest);
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
