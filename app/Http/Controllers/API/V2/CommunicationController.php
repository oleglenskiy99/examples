<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\DTO\Request\AttachCommunication;
use App\DTO\Request\ViewedUser;
use App\Http\Requests\V2\UserViewedRequest;
use App\Services\V2\CommunicationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class CommunicationController
{

    private SerializerInterface $serializer;
    private CommunicationService $communicationService;
    private DenormalizerInterface $denormalizer;

    public function __construct(
        SerializerInterface $serializer,
        CommunicationService $communicationService,
        DenormalizerInterface $denormalizer
    )
    {
        $this->serializer = $serializer;
        $this->communicationService = $communicationService;
        $this->denormalizer = $denormalizer;
    }

    /**
     * @OA\Get(
     *     path="/api/v2/communication/user",
     *     summary="Communication",
     *     tags={"Communication"},
     *     security={ {"bearer_token": {} }},
     *     description="Get all communications for the user",
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response (
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Communication"),
     *         )
     *     ),
     * )
     */
    public function getByCurrentUser(): JsonResponse
    {
        $communications = $this->communicationService->getAllByCurrentUser();
        $response = $this->serializer->serialize($communications, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

    /**
     * @OA\Post (
     *     path="/api/v2/communication/attach",
     *     summary="Communication",
     *     tags={"Communication"},
     *     security={ {"bearer_token": {} }},
     *     description="Attach communication to currentuser",
     *     @OA\RequestBody(
     *          description="Ids of communicatiosns",
     *          @OA\JsonContent(ref="#/components/schemas/AttachCommunication"),
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response (
     *         response=201,
     *         description="Ok",
     *     ),
     * )
     */
    public function attach(Request $request): JsonResponse
    {
        $attachCategory = $this->serializer->deserialize(
            $request->getContent(),
            AttachCommunication::class,
            JsonEncoder::FORMAT
        );
        $this->communicationService->attacheToCurrentUser($attachCategory);
        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }


    /**
     * @OA\Get (
     *     path="/api/v2/communication/user-viewed",
     *     summary="Communication",
     *     tags={"Communication"},
     *     security={ {"bearer_token": {} }},
     *     description="Add user to recently viewed",
     *     @OA\Parameter(
     *         name="userId",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="1",
     *         )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response (
     *         response=200,
     *         description="Ok",
     *     ),
     * )
     */
    public function userViewed(UserViewedRequest $request): JsonResponse
    {
        $viewedUser = $this->denormalizer->denormalize(
            $request->query(),
            ViewedUser::class,
            JsonEncoder::FORMAT
        );

        $this->communicationService->addUserViewed($viewedUser);
        return new JsonResponse(null, JsonResponse::HTTP_OK);
    }

}
