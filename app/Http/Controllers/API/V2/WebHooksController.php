<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\Http\Controllers\Controller;
use App\Services\Payment\StripeWebhooksService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class WebHooksController extends Controller
{

    private StripeWebhooksService $stripeWebhooksService;

    public function __construct(StripeWebhooksService $stripeWebhooksService)
    {
        $this->stripeWebhooksService = $stripeWebhooksService;
    }

    public function stripe(Request $request): JsonResponse
	{
        $this->stripeWebhooksService->handleStripeEvent($request->getContent());
        return new JsonResponse([], JsonResponse::HTTP_OK, [], 0, false);
    }

}
