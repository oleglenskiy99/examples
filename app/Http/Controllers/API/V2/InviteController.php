<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\DTO\Request\GetInviteCodeRequest;
use App\Http\Requests\V2\ApplyInviteCodeRequest;
use App\Http\Requests\V2\CheckInviteCodeRequest;
use App\Services\V2\InviteService;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class InviteController
{

    private SerializerInterface $serializer;
    private InviteService $inviteService;
    private DenormalizerInterface $denormalizer;

    public function __construct(SerializerInterface $serializer, InviteService $inviteService, DenormalizerInterface $denormalizer)
    {
        $this->serializer = $serializer;
        $this->inviteService = $inviteService;
        $this->denormalizer = $denormalizer;
    }

    /**
     * @OA\Get (
     *     path="/api/v2/invites/check",
     *     summary="Check invite code",
     *     tags={"Invites"},
     *     description="Check invite code",
     *     @OA\Parameter(
     *         name="inviteCode",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="210",
     *         )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Not found",
     *         @OA\JsonContent(ref="#/components/schemas/NotFoundException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="{ is_active: true }",
     *     ),
     * )
     */
    public function check(CheckInviteCodeRequest $request): JsonResponse
    {
        $inviteCode = $this->denormalizer->denormalize(
            $request->query(),
            GetInviteCodeRequest::class,
            JsonEncoder::FORMAT
        );

        $data = $this->inviteService->check($inviteCode);

        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

    /**
     * @OA\Get (
     *     path="/api/v2/invites/apply",
     *     summary="Apply invite code",
     *     tags={"Invites"},
     *     security={ {"bearer_token": {} }},
     *     description="Apply invite code",
     *     @OA\Parameter(
     *         name="inviteCode",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="210",
     *         )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Not found",
     *         @OA\JsonContent(ref="#/components/schemas/NotFoundException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *     ),
     * )
     */
    public function apply(ApplyInviteCodeRequest $request): JsonResponse
    {
        $inviteCode = $this->denormalizer->denormalize(
            $request->query(),
            GetInviteCodeRequest::class,
            JsonEncoder::FORMAT
        );

        $data = $this->inviteService->apply($inviteCode);

        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
