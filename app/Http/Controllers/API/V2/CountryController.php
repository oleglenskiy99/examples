<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\Http\Controllers\Controller;
use App\Services\V2\CountryService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class CountryController extends Controller
{

    private SerializerInterface $serializer;
    private CountryService $countryService;

    public function __construct(SerializerInterface $serializer, CountryService $countryService)
    {
        $this->serializer = $serializer;
        $this->countryService = $countryService;
    }

    /**
     * @OA\Get (
     *     path="/api/v2/countries",
     *     summary="Get countries",
     *     tags={"Country"},
     *     description="get countries",
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/CountryItem"),
     *     ),
     * )
     */
    public function getAll(): JsonResponse
    {
        $data = $this->countryService->getCountries();

        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
