<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\DTO\Request\ChargeFilter;
use App\Services\V2\ChargeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ChargeController
{

    private ChargeService $chargeService;
    private SerializerInterface $serializer;
    private DenormalizerInterface $denormalizer;

    public function __construct(ChargeService $chargeService, SerializerInterface $serializer, DenormalizerInterface $denormalizer)
    {
        $this->chargeService = $chargeService;
        $this->serializer = $serializer;
        $this->denormalizer = $denormalizer;
    }


    /**
     * @OA\Get (
     *     path="/api/v2/charges",
     *     summary="Get charges",
     *     tags={"Charges"},
     *     security={ {"bearer_token": {} }},
     *     description="get charges",
     *     @OA\Parameter(
     *         name="countOfItems",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="10",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="1",
     *         )
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/ChargeList"),
     *     ),
     * )
     */
    public function getAll(Request $request): JsonResponse
    {
        $filter = $this->denormalizer->denormalize(
            $request->query(),
            ChargeFilter::class,
            JsonEncoder::FORMAT
        );

        $data = $this->chargeService->getChargesByFilter($filter);
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
