<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\Services\Payment\StripePaymentService;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class CardsController
{

    private StripePaymentService $stripePaymentService;
    private SerializerInterface $serializer;

    public function __construct(StripePaymentService $stripePaymentService, SerializerInterface $serializer)
    {
        $this->stripePaymentService = $stripePaymentService;
        $this->serializer = $serializer;
    }

    /**
     * @OA\Post (
     *     path="/finances/card/attach",
     *     summary="Card add form",
     *     tags={"Finance"},
     *     description="Create credit card add session and retrieve form url",
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/StripeCheckoutSession")
     *     ),
     * )
     */
    public function creditCardAttach(): JsonResponse
    {
        $data = $this->stripePaymentService->createCreditCardAttachSession();
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
