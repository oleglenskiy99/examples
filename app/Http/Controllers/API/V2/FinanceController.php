<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\DTO\Request\AddPayout;
use Illuminate\Http\JsonResponse;
use App\Services\V2\FinanceService;
use App\Http\Requests\V2\AddPayoutRequest;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class FinanceController
{

    private SerializerInterface $serializer;
    private FinanceService $financeService;

    public function __construct(SerializerInterface $serializer, FinanceService $financeService)
    {
        $this->serializer = $serializer;
        $this->financeService = $financeService;
    }

    public function addPayout(AddPayoutRequest $request)
    {
        $addPayout = $this->serializer->deserialize(
            $request->getContent(),
            AddPayout::class,
            JsonEncoder::FORMAT
        );
        $result = $this->financeService->addPayout($addPayout);
        $response = $this->serializer->serialize($result, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
