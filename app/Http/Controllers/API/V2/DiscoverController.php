<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\DTO\Request\UserFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\V2\UserFilterRequest;
use App\Services\V2\DiscoverService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class DiscoverController extends Controller
{

    private DiscoverService $discoverService;
    private SerializerInterface $serializer;
    private DenormalizerInterface $denormalizer;

    public function __construct(DiscoverService $discoverService, SerializerInterface $serializer, DenormalizerInterface $denormalizer)
    {
        $this->discoverService = $discoverService;
        $this->serializer = $serializer;
        $this->denormalizer = $denormalizer;
    }

    /**
     * @OA\Get (
     *     path="/api/v2/discover",
     *     summary="Get discover screen",
     *     tags={"Discover"},
     *     security={ {"bearer_token": {} }},
     *     description="get discover screen",
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/DiscoverItems")),
     *     ),
     * )
     */
    public function getAll(): JsonResponse
    {
        $data = $this->discoverService->getDiscover();
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }


    /**
     * @OA\Get (
     *     path="/api/v2/discover/specially-for-you",
     *     summary="Get discover specially for you",
     *     tags={"Discover"},
     *     security={ {"bearer_token": {} }},
     *     description="Get discover specially for you",
     *     @OA\Parameter(
     *         name="countOfItems",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="10",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="1",
     *         )
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/UserList"),
     *     ),
     * )
     */
    public function speciallyForYou(UserFilterRequest $request): JsonResponse
    {
        $filter = $this->denormalizer->denormalize(
            $request->query(),
            UserFilter::class,
            JsonEncoder::FORMAT
        );

        $data = $this->discoverService->speciallyForYou($filter);
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
