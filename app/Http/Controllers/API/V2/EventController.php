<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\Services\V2\EventService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class EventController
{

    private SerializerInterface $serializer;
    private EventService $eventService;

    public function __construct(SerializerInterface $serializer, EventService $eventService)
    {
        $this->serializer = $serializer;
        $this->eventService = $eventService;
    }


    /**
     * @OA\Get (
     *     path="/api/v2/events",
     *     summary="Get events",
     *     tags={"Events"},
     *     description="get events",
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/EventItem")),
     *     ),
     * )
     */
    public function getAll(): JsonResponse
    {
        $data = $this->eventService->getEvents();

        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
