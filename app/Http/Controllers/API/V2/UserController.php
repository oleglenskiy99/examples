<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\DTO\Request\ChangeCountry;
use App\DTO\Request\ChangePassword;
use App\DTO\Request\RegistrationUser;
use App\DTO\Request\UpdateUser;
use App\DTO\Request\UserFilter;
use App\Http\Requests\V2\CountryChangeRequest;
use App\Http\Requests\V2\GetUserRequest;
use App\Http\Requests\V2\PasswordChangeRequest;
use App\Http\Requests\V2\RegistrationUserRequest;
use App\Http\Requests\V2\UpdateUserRequest;
use App\Http\Requests\V2\UserFilterRequest;
use App\Services\V2\UserService;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class UserController
{

    private UserService $userService;
    private SerializerInterface $serializer;
    private DenormalizerInterface $denormalizer;

    public function __construct(
        UserService $userService,
        SerializerInterface $serializer,
        DenormalizerInterface $denormalizer
    )
    {
        $this->userService = $userService;
        $this->serializer = $serializer;
        $this->denormalizer = $denormalizer;
    }

    /**
     * @OA\Patch (
     *     path="/api/v2/user/change-password",
     *     summary="Change password",
     *     tags={"User"},
     *     security={ {"bearer_token": {} }},
     *     description="change password",
     *     @OA\RequestBody(
     *          description="Country body",
     *          @OA\JsonContent(ref="#/components/schemas/ChangePassword"),
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid password",
     *         @OA\JsonContent(ref="#/components/schemas/InvalidPasswordException"),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Ok",
     *     ),
     * )
     */
    public function changePassword(PasswordChangeRequest $request): JsonResponse
    {
        $changePassword = $this->serializer->deserialize(
            $request->getContent(),
            ChangePassword::class,
            JsonEncoder::FORMAT
        );

        $this->userService->changePassword($changePassword);
        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }

    /**
     * @OA\Patch (
     *     path="/api/v2/user/change-country",
     *     summary="Change country",
     *     tags={"User"},
     *     security={ {"bearer_token": {} }},
     *     description="change country",
     *     @OA\RequestBody(
     *          description="Country body",
     *          @OA\JsonContent(ref="#/components/schemas/ChangeCountry"),
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Country is not exists",
     *         @OA\JsonContent(ref="#/components/schemas/NotFoundException"),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Ok",
     *     ),
     * )
     */
    public function changeCountry(CountryChangeRequest $request): JsonResponse
    {
        $changeCountry = $this->serializer->deserialize(
            $request->getContent(),
            ChangeCountry::class,
            JsonEncoder::FORMAT
        );

        $this->userService->changeCountry($changeCountry);
        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }

    /**
     * @OA\Post (
     *     path="/api/v2/user",
     *     summary="Create user",
     *     tags={"User"},
     *     description="Create user",
     *     @OA\RequestBody(
     *          description="User body",
     *          @OA\JsonContent(ref="#/components/schemas/RegistrationUser"),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Validation errors",
     *         @OA\JsonContent(ref="#/components/schemas/ValidationException"),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Ok"
     *     ),
     * )
     */
    public function create(RegistrationUserRequest $request): JsonResponse
    {
        $userBody = $this->serializer->deserialize(
            $request->getContent(),
            RegistrationUser::class,
            JsonEncoder::FORMAT
        );
        $this->userService->create($userBody);
        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }

    /**
     * @OA\Get (
     *     path="/api/v2/user",
     *     summary="Get user",
     *     tags={"User"},
     *     security={ {"bearer_token": {} }},
     *     description="get user",
     *     @OA\Parameter(
     *         name="userId",
     *         in="query"
     *     ),
     *     @OA\Parameter(
     *         name="nickname",
     *         in="query"
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/UserItem"),
     *     ),
     * )
     */
    public function get(GetUserRequest $request): JsonResponse
    {
        $data = $this->userService->getUserByIdOrNickname(
            $request->get('userId') ? (int) $request->get('userId') : null,
            $request->get('nickname')
        );

        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

    /**
     * @OA\Get (
     *     path="/api/v2/user/all",
     *     summary="Get users",
     *     tags={"User"},
     *     security={ {"bearer_token": {} }},
     *     description="get users",
     *     @OA\Parameter(
     *         name="categoryIds[]",
     *         in="query",
     *         @OA\Schema(
     *            type="array",
     *            @OA\Items(
     *                example="115",
     *                type="int",
     *            ),
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="countryIds[]",
     *         in="query",
     *         @OA\Schema(
     *            type="array",
     *            @OA\Items(
     *                example="115",
     *                type="int",
     *            ),
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ratingsFrom",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="2",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ratingsTo",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="5",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="isChatRequestAvailable",
     *         in="query",
     *         @OA\Schema(
     *            type="bool",
     *            example="1",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="isAudioRequestAvailable",
     *         in="query",
     *         @OA\Schema(
     *            type="bool",
     *            example="1",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="isVideoRequestAvailable",
     *         in="query",
     *         @OA\Schema(
     *            type="bool",
     *            example="1",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="isImageRequestAvailable",
     *         in="query",
     *         @OA\Schema(
     *            type="bool",
     *            example="1",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="priceFrom",
     *         in="query",
     *         @OA\Schema(
     *            type="number",
     *            example="10",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="priceTo",
     *         in="query",
     *         @OA\Schema(
     *            type="number",
     *            example="100",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="isVerification",
     *         in="query",
     *         @OA\Schema(
     *            type="bool",
     *            example="1",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="searchText",
     *         in="query",
     *         @OA\Schema(
     *            type="string",
     *            example="Full text search field",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="countOfItems",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="10",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="1",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="eventId",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="1",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="eventUserTypeId",
     *         in="query",
     *         @OA\Schema(
     *            type="int",
     *            example="1",
     *         )
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/UserList"),
     *     ),
     * )
     */
    public function getAll(UserFilterRequest $request): JsonResponse
    {
        $filter = $this->denormalizer->denormalize(
            $request->query(),
            UserFilter::class,
            JsonEncoder::FORMAT
        );

        $data = $this->userService->getUserByFilter($filter);
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

    /**
     * @OA\Put (
     *     path="/api/v2/user",
     *     summary="Update user",
     *     tags={"User"},
     *     security={ {"bearer_token": {} }},
     *     description="update user",
     *     @OA\RequestBody(
     *          description="User body",
     *          @OA\JsonContent(ref="#/components/schemas/UpdateUser"),
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Ok"
     *     ),
     * )
     */
    public function update(UpdateUserRequest $request): JsonResponse
    {
        $updateUserBody = $this->serializer->deserialize(
            $request->getContent(),
            UpdateUser::class,
            JsonEncoder::FORMAT
        );

        $this->userService->update($updateUserBody);
        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }

}
