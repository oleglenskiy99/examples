<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\DTO\Request\GetFollowers;
use App\Http\Requests\V2\FollowersGerRequest;
use App\Services\V2\FollowersService;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class FollowingController
{

    private FollowersService $followersService;
    private SerializerInterface $serializer;
    private DenormalizerInterface $denormalizer;

    public function __construct(
        FollowersService $followersService,
        SerializerInterface $serializer,
        DenormalizerInterface $denormalizer
    )
    {
        $this->followersService = $followersService;
        $this->serializer = $serializer;
        $this->denormalizer = $denormalizer;
    }

    /**
     * @OA\Get(
     *     path="/api/v2/following",
     *     summary="Followings",
     *     tags={"Following"},
     *     security={ {"bearer_token": {} }},
     *     description="Get all foolowings for an user",
     *     @OA\Parameter(
     *         name="userId",
     *         in="query",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         name="count",
     *         in="query",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         required=true,
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/UserList")
     *     ),
     * )
     */
    public function getAll(FollowersGerRequest $request): JsonResponse
    {
        $getFollowers = $this->denormalizer->denormalize(
            $request->query(),
            GetFollowers::class,
            JsonEncoder::FORMAT
        );
        $data = $this->followersService->getFollowingsByUserId($getFollowers);
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
