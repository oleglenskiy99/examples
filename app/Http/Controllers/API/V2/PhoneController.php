<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\DTO\Request\AddPhone;
use App\DTO\Request\ConfirmPhone;
use App\Http\Requests\V2\AddPhoneRequest;
use App\Http\Requests\V2\ConfirmPhoneRequest;
use App\Services\V2\PhoneService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class PhoneController
{

    private SerializerInterface $serializer;
    private PhoneService $phoneService;

    public function __construct(SerializerInterface $serializer, PhoneService $phoneService)
    {
        $this->serializer = $serializer;
        $this->phoneService = $phoneService;
    }

    /**
     * @OA\Post(
     *     path="/api/v2/user/addPhone",
     *     summary="User",
     *     tags={"User"},
     *     security={ {"bearer_token": {} }},
     *     description="",
     *     @OA\RequestBody(
     *          description="Add phone body",
     *          @OA\JsonContent(ref="#/components/schemas/AddPhone"),
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response (
     *         response=200,
     *         description="{'i':'string', 'n':'false', 's':'string'}"
     *     ),
     * )
     */
    public function addPhone(AddPhoneRequest $request): JsonResponse
    {
        $addPhone = $this->serializer->deserialize(
            $request->getContent(),
            AddPhone::class,
            JsonEncoder::FORMAT
        );
        $result = $this->phoneService->addPhone($addPhone);
        $response = $this->serializer->serialize($result, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

    /**
     * @OA\Post(
     *     path="/api/v2/user/confirmPhone",
     *     summary="User",
     *     tags={"User"},
     *     security={ {"bearer_token": {} }},
     *     description="",
     *     @OA\RequestBody(
     *          description="Confirm phone body",
     *          @OA\JsonContent(ref="#/components/schemas/ConfirmPhone"),
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response (
     *         response=200,
     *         description="Ok"
     *     ),
     * )
     */
    public function confirmPhone(ConfirmPhoneRequest $request): JsonResponse
    {
        $conformPhone = $this->serializer->deserialize(
            $request->getContent(),
            ConfirmPhone::class,
            JsonEncoder::FORMAT
        );
        $result = $this->phoneService->confirmPhone($conformPhone);
        $response = $this->serializer->serialize($result, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
