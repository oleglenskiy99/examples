<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\DTO\Request\CheckEmailVerification;
use App\DTO\Request\CheckPasswordReset;
use App\DTO\Request\Login;
use App\DTO\Request\SendEmailVerification;
use App\DTO\Request\SendPasswordReset;
use App\DTO\Response\Logout;
use App\DTO\Response\StatusMessage;
use App\Services\V2\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class AuthController
{

    private SerializerInterface $serializer;
    private AuthService $authService;

    public function __construct(SerializerInterface $serializer, AuthService $authService)
    {
        $this->serializer = $serializer;
        $this->authService = $authService;
    }

    /**
     * @OA\Post(
     *     path="/api/v2/auth/login",
     *     summary="Login",
     *     tags={"Authentication"},
     *     operationId="login",
     *     description="Sing in",
     *     @OA\RequestBody(
     *          description="Country body",
     *          @OA\JsonContent(ref="#/components/schemas/Login")
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/JwtTokenStructure")
     *     ),
     * )
     */
    public function login(Request $request): JsonResponse
    {
        $login = $this->serializer->deserialize($request->getContent(), Login::class, JsonEncoder::FORMAT);

        $token = $this->authService->login($login);
        $response = $this->serializer->serialize($token, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }


    /**
     * @OA\Post(
     *     path="/api/v2/auth/logout",
     *     summary="Logout",
     *     tags={"Authentication"},
     *     security={ {"bearer_token": {} }},
     *     operationId="logout",
     *     description="Logout",
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/Logout")
     *     ),
     * )
     */
    public function logout(): JsonResponse
    {
        $this->authService->logout();

        $data = new Logout();
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

    /**
     * @OA\Post(
     *     path="/api/v2/auth/refresh",
     *     summary="Refresh",
     *     tags={"Authentication"},
     *     security={ {"bearer_token": {} }},
     *     operationId="refresh",
     *     description="refresh",
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/ForbiddenException"
     *         ),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/UnauthorizedException"
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(
     *              ref="#/components/schemas/JwtTokenStructure"
     *         )
     *     ),
     * )
     */
    public function refresh(): JsonResponse
    {
        $data = $this->authService->refresh();
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }


    /**
     * @OA\Post(
     *     path="/api/v2/auth/send-email-verification",
     *     summary="Send email verification",
     *     tags={"Authentication"},
     *     description="Send email verification",
     *     @OA\RequestBody(
     *          description="Email",
     *          @OA\JsonContent(ref="#/components/schemas/SendEmailVerification")
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="User is not exists",
     *         @OA\JsonContent(ref="#/components/schemas/NotFoundException"),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Ok",
     *     ),
     * )
     */
    public function sendEmailVerificationCode(Request $request): JsonResponse
    {
        $emailVerification = $this->serializer->deserialize($request->getContent(), SendEmailVerification::class, JsonEncoder::FORMAT);

        $this->authService->sendEmailVerification($emailVerification);

        $message = (new StatusMessage())->setMessage('The code has been sent to your email');
        $response = $this->serializer->serialize($message, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

    /**
     * @OA\Post(
     *     path="/api/v2/auth/check-email-verification",
     *     summary="Check email verification",
     *     tags={"Authentication"},
     *     description="Check email verification",
     *     @OA\RequestBody(
     *          description="Email",
     *          @OA\JsonContent(ref="#/components/schemas/CheckEmailVerification")
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="User|Code is not exists",
     *         @OA\JsonContent(ref="#/components/schemas/NotFoundException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *     ),
     * )
     */
    public function checkEmailVerificationCode(Request $request): JsonResponse
    {
        $emailVerification = $this->serializer->deserialize($request->getContent(), CheckEmailVerification::class, JsonEncoder::FORMAT);

        $this->authService->checkEmailVerification($emailVerification);

        $message = (new StatusMessage())->setMessage('Your email has been verified');
        $response = $this->serializer->serialize($message, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

    /**
     * @OA\Post(
     *     path="/api/v2/auth/send-password-reset",
     *     summary="Send password reset",
     *     tags={"Authentication"},
     *     description="Send password reset",
     *     @OA\RequestBody(
     *          description="Password",
     *          @OA\JsonContent(ref="#/components/schemas/SendPasswordReset")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *     ),
     * )
     */
    public function sendPasswordReset(Request $request): JsonResponse
    {
        $passwordReset = $this->serializer->deserialize($request->getContent(), SendPasswordReset::class, JsonEncoder::FORMAT);

        $this->authService->sendPasswordReset($passwordReset);

        $message = (new StatusMessage())->setMessage('The code has been sent to your email');
        $response = $this->serializer->serialize($message, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

    /**
     * @OA\Post(
     *     path="/api/v2/auth/check-password-reset",
     *     summary="Check password reset",
     *     tags={"Authentication"},
     *     description="Check password reset",
     *     @OA\RequestBody(
     *          description="New password",
     *          @OA\JsonContent(ref="#/components/schemas/CheckPasswordReset")
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="User|Code is not exists",
     *         @OA\JsonContent(ref="#/components/schemas/NotFoundException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *     ),
     * )
     */
    public function checkPasswordReset(Request $request): JsonResponse
    {
        $passwordReset = $this->serializer->deserialize($request->getContent(), CheckPasswordReset::class, JsonEncoder::FORMAT);

        $this->authService->checkPasswordReset($passwordReset);

        $message = (new StatusMessage())->setMessage('Your password has been changed');
        $response = $this->serializer->serialize($message, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
