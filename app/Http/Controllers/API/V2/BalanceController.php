<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\Http\Requests\V2\TopUpBalanceRequest;
use App\Services\Payment\StripePaymentService;
use App\Services\V2\FinanceService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class BalanceController
{

    private FinanceService $financeService;
    private SerializerInterface $serializer;
    private StripePaymentService $stripePaymentService;

    public function __construct(
        FinanceService $financeService,
        SerializerInterface $serializer,
        StripePaymentService $stripePaymentService
    )
    {
        $this->financeService = $financeService;
        $this->serializer = $serializer;
        $this->stripePaymentService = $stripePaymentService;
    }


    /**
     * @OA\Get (
     *     path="/api/v2/finances/balances",
     *     summary="Get user balances",
     *     tags={"Finance"},
     *     security={ {"bearer_token": {} }},
     *     description="Returns available user balances",
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid password",
     *         @OA\JsonContent(ref="#/components/schemas/InvalidPasswordException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/UserFinances"),
     *     ),
     * )
     */
    public function getUserBalances(): JsonResponse
    {
        $data = $this->financeService->getCurrentUserBalances();
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

    /**
     * @OA\Post (
     *     path="/api/v2/finances/balances/top-up",
     *     summary="Create payment session",
     *     tags={"Finance"},
     *     security={ {"bearer_token": {} }},
     *     description="Returns stripe checkout session",
     *     @OA\Parameter(
     *         name="amount",
     *         in="query",
     *         required=true
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid password",
     *         @OA\JsonContent(ref="#/components/schemas/InvalidPasswordException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/StripeCheckoutSession"),
     *     ),
     * )
     */
    public function topUpUserBalance(TopUpBalanceRequest $request): JsonResponse
    {
        $data = $this->stripePaymentService->createPaymentSession((int) $request->get('amount'));
        $response = $this->serializer->serialize($data, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
