<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\Http\Controllers\Controller;
use App\Http\Requests\V2\UploadRequest;
use App\Services\V2\UploadService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class UploadController extends Controller
{

    private UploadService $uploadService;

    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer, UploadService $uploadService)
    {
        $this->uploadService = $uploadService;
        $this->serializer = $serializer;
    }

    /**
     * @OA\Post (
     *     path="/api/v2/upload",
     *     summary="Upload file",
     *     tags={"Upload"},
     *     security={ {"bearer_token": {} }},
     *     description="upload file",
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="file to upload",
     *                     property="file",
     *                     type="string",
     *                     format="binary",
     *                 ),
     *                 @OA\Property(
     *                     property="tag",
     *                     description="tag",
     *                     type="string",
     *                     example="avatars",
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *     ),
     * )
     */
    public function upload(UploadRequest $request): JsonResponse
    {
        $upload = $this->uploadService->upload($request);

        $response = $this->serializer->serialize($upload, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

}
