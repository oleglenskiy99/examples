<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V2;

use App\DTO\Request\AttachCategory;
use App\Services\V2\CategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class CategoryController
{

    private SerializerInterface $serializer;
    private CategoryService $categoryService;

    public function __construct(SerializerInterface $serializer, CategoryService $categoryService)
    {
        $this->serializer = $serializer;
        $this->categoryService = $categoryService;
    }

    /**
     * @OA\Get(
     *     path="/api/v2/category",
     *     summary="Category",
     *     tags={"Category"},
     *     security={ {"bearer_token": {} }},
     *     description="Get all categories",
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response (
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Category"),
     *         )
     *     ),
     * )
     */
    public function getAll(): JsonResponse
    {
        $categories = $this->categoryService->getAll();
        $response = $this->serializer->serialize($categories, JsonEncoder::FORMAT);
        return new JsonResponse($response, JsonResponse::HTTP_OK, [], 0, true);
    }

    /**
     * @OA\Post (
     *     path="/api/v2/category/attach",
     *     summary="Category",
     *     tags={"Category"},
     *     security={ {"bearer_token": {} }},
     *     description="Attach categories to currentuser",
     *     @OA\RequestBody(
     *          description="Ids of categories",
     *          @OA\JsonContent(ref="#/components/schemas/AttachCategory"),
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Forbidden",
     *         @OA\JsonContent(ref="#/components/schemas/ForbiddenException"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/UnauthorizedException"),
     *     ),
     *     @OA\Response (
     *         response=201,
     *         description="Ok",
     *     ),
     * )
     */
    public function attach(Request $request): JsonResponse
    {
        $attachCategory = $this->serializer->deserialize(
            $request->getContent(),
            AttachCategory::class,
            JsonEncoder::FORMAT
        );
        $this->categoryService->attacheCategoryToCurrentUser($attachCategory);
        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }

}
