<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Helpers\Chats\AttachmentsHelper;
use App\Models\Chats\Chat;
use App\Models\Chats\Members;
use App\Models\Chats\Messages;
use App\Repository\Chats\AttachmentsRepository;
use App\Services\V1\UsersService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// phpcs:disable Squiz.Arrays.ArrayDeclaration.IndexNoNewline
// phpcs:disable Squiz.Strings.DoubleQuoteUsage.ContainsVar
// phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint
class Search extends ApiController
{

    private UsersService $usersService;

    public function __construct(UsersService $usersService)
    {
        parent::__construct();

        $this->usersService = $usersService;
    }

    public function users(Request $request): JsonResponse
    {
        $this->validate($request, [
            'q' => ['required', 'string', 'filled', 'min:3'],
        ]);

        if (!empty($request->next_max_id)) {
            if ($this->usersService->setPaginatePageUser($request->next_max_id) === false) {
                return $this->displayError([], 5);
            }
        }
        $industry = null;
        if (!empty($request->industry)) {
            $industry = (int) $request->industry;
        }

        return $this->displayOk(
            $this->usersService->getSearchPaginatedUsers(
                (string) $request->q,
                $this->userId,
                $industry
            )
        );
    }

    public function categories(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required:integer',
            'q' => [
                'required',
                'string',
                'filled',
            ],
        ]);

        $users = $this->usersService->getSearchUsers((string) $request->q, $this->userId, (int) $request->id, 20);
        $users = $users->toArray();

        return $this->displayOk([
            'is_more_available' => count($users) === 20,
            'items' => $users,
        ]);
    }

    public function chats(Request $request): JsonResponse
    {
        $this->validate($request, [
            'q' => [
                'required',
                'string',
                'filled',
            ],
        ]);

        $request->q = mb_strtolower($request->q);

        $user_dialog_ids = Members::select('dialog_id')->where('user_id', $this->userId)->get()->toArray();

        $dialogs = Chat::select('id', 'title', 'image', 'is_group')
            ->whereHas('user', function ($q) use ($request): void {
                $q->join('module_users', 'user_id', 'module_users.id')
                    ->join('module_users_about', 'user_id', 'module_users_about.id')->where(function ($q) use ($request): void {
                        $q->where('firstName', 'like', "{$request->q}%")->orWhere('lastName', 'like', "{$request->q}%");
                    });
            })
            ->with([
                'user' => function ($query): void {
                    $query
                        ->select('user_id', 'dialog_id', 'firstName', 'lastName', 'domain', 'photo', 'is_online', 'last_activity')
                        ->join('module_users', 'user_id', 'module_users.id')
                        ->join('module_users_about', 'user_id', 'module_users_about.id')
                        ->where('user_id', '!=', session('user_id'));
                },
            ])
            ->whereIn('id', array_column($user_dialog_ids, 'dialog_id'))->where('is_group', 0)
            ->get()
            ->toArray();

        $groups = Chat::select('id', 'title', 'image', 'is_group')
            ->where(DB::raw('LOWER(title)'), 'like', "%{$request->q}%")
            ->whereIn('id', array_column($user_dialog_ids, 'dialog_id'))
            ->get()
            ->toArray();

        $chats = [];

        foreach (array_merge($dialogs, $groups) as $instance) {
            $chat = [
                'id' => $instance['id'],
            ];

            if ($instance['is_group']) {
                $chat['peer'] = [
                    'id' => $instance['id'],
                    'name' => $instance['title'],
                    'image' => $instance['image'],
                    'type' => 'group',
                ];
            } elseif (array_key_exists('user', $instance)) {
                $chat['peer'] = [
                    'id' => $instance['user']['user_id'],
                    'domain' => $instance['user']['domain'],
                    'name' => $instance['user']['firstName'] . ' ' . $instance['user']['lastName'],
                    'image' => $instance['user']['photo'],
                    'type' => 'user',
                ];
            }

            $chats[] = $chat;
        }

        $messages = Messages::builder()->addSelect(
            DB::raw(
                "IF (
                        LOCATE('{$request->q}', message) < 50,
                        CONCAT(SUBSTRING(message, 1, 200),'...'),
                        CONCAT('...',SUBSTRING(message, LOCATE('{$request->q}', message) - 50, 200),'...')
                    ) AS text"
            )
        )->whereHas('chat', function ($q): void {
            $q->where('is_saved_messages', 0);
        })->with([
            'chat' => function ($q): void {
                $q->select('module_chats.id as id', 'title', 'image', 'is_group', 'creator_id')->with([
                    'user' => function ($query): void {
                        $query
                            ->select('user_id', 'dialog_id', 'firstName', 'lastName', 'photo', 'is_online', 'last_activity')
                            ->join('module_users', 'user_id', 'module_users.id')
                            ->join('module_users_about', 'user_id', 'module_users_about.id')
                            ->where('user_id', '!=', session('user_id'));
                    },
                ]);
            },
        ])->where('message', 'LIKE', "%{$request->q}%")->where('type', 'message')
            ->whereIn('dialog_id', array_column($user_dialog_ids, 'dialog_id'))->orderBy('id', 'DESC')->paginate(51, ['*'], 'next_max_id');

        $attachments = AttachmentsRepository::builder($request->q, $this->userId)->with('media')->whereRaw(
            "dialog_id in (SELECT dialog_id from module_chats_users WHERE user_id = {$this->userId})"
        )->where(
            function ($q): void {
                $q->where('type', 'image')->orWhere('type', 'video');
            }
        )->groupBy('message_id')->paginate(51, ['*'], 'next_max_id');

        $items = $messages->items();

        foreach ($items as $k => $message) {
            $items[$k] = Messages::format($message, true);
        }

        foreach ($items as $k => $message) {
            unset($message['chat']['user']);
            $items[$k] = $message;
        }

        return $this->displayOk([

            'chats' => [
                'items' => $chats,
            ],

            'messages' => [
                'total_count' => $messages->total(),
                'is_more_available' => $messages->hasMorePages(),
                'next_max_id' => $messages->hasMorePages() ? encrypt($messages->currentPage() + 1) : null,
                'items' => $items,
            ],
            'media' => [
                'total_count' => $attachments->total(),
                'is_more_available' => $attachments->hasMorePages(),
                'next_max_id' => $attachments->hasMorePages() ? encrypt($attachments->currentPage() + 1) : null,
                'items' => AttachmentsHelper::format($attachments->items(), true),
            ],
        ]);
    }

    public function messages(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required',
            'q' => [
                'required',
                'string',
                'filled',
            ],
        ]);

        $chats = Chat::where('id', $request->id)->first();
        if ($chats === null) {
            return $this->displayError(['isBackground' => true], 15);
        }

        if ($request->ids) {
            $q = mb_strtolower($request->q);

            $messages = Messages::select('id')->where(DB::raw('LOWER(message)'), 'LIKE', "%{$q}%")->where('type', 'message')
                ->where('dialog_id', $request->id)
                ->orderBy('id', 'ASC')->get()->toArray();

            return $this->displayOk(array_column($messages, 'id'));
        }

        $messages = Messages::builder()->addSelect(
            DB::raw(
                "IF (
                            LOCATE('{$request->q}', message) < 50,
                            CONCAT(SUBSTRING(message, 1, 200),'...'),
                            CONCAT('...',SUBSTRING(message, LOCATE('{$request->q}', message) - 50, 200),'...')
                        ) AS text"
            )
        )->whereHas('chat', function ($q): void {
            $q->where('is_saved_messages', 0);
        })->with([
            'chat' => function ($q): void {
                $q->select('module_chats.id as id', 'title', 'image', 'is_group', 'creator_id')->with([
                    'user' => function ($query): void {
                        $query
                            ->select('user_id', 'dialog_id', 'firstName', 'lastName', 'photo', 'is_online', 'last_activity')
                            ->join('module_users', 'user_id', 'module_users.id')
                            ->join('module_users_about', 'user_id', 'module_users_about.id')
                            ->where('user_id', '!=', session('user_id'));
                    },
                ]);
            },
        ])
            ->where('message', 'LIKE', "%{$request->q}%")->where('type', 'message')
            ->where('dialog_id', $request->id)
            ->orderBy('id', 'DESC')
            ->paginate(51, ['*'], 'next_max_id');

        $items = $messages->items();

        foreach ($items as $k => $message) {
            $items[$k] = Messages::format($message, true);
        }

        return $this->displayOk([
            'total_count' => $messages->total(),
            'is_more_available' => $messages->hasMorePages(),
            'next_max_id' => $messages->hasMorePages() ? encrypt($messages->currentPage() + 1) : null,
            'items' => $items,
        ]);
    }

    /**
     * @OA\Get (
     *     path="/api/methods/search",
     *     summary="search",
     *     operationId="search",
     *     description="Поиск пользователей",
     * @OA\Parameter(
     *         name="q",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function __invoke(Request $request): JsonResponse
    {
        $this->validate($request, [
            'q' => [
                'required',
                'string',
                'filled',
            ],
        ]);

        $users = $this->usersService->getSearchUsers($request->q, $this->userId);

        return $this->displayOk([
            'users' => [
                'total_count' => $users->total(),
                'items' => $users->items(),
            ],
        ]);
    }

}
