<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Events\ResetPasswordProfile;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PasswordController extends ApiController
{

    /**
     * @OA\Post(
     *     path="/api/methods/password.reset",
     *     security={{"bearerAuth":{}}},
     *     summary="Password reset",
     *     operationId="password.reset",
     *     description="Change password feom profile edit",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function resetPassword(Request $request): JsonResponse
    {
        $this->validate($request, [
            'password' => 'required',
        ]);

        $user = User::getUserById($this->userId);

        if ($user === null) {
            return $this->displayError([
                'message' => 'user not found',
            ], 1);
        }

        $user->password = Hash::make($request->password);
        $user->save();

        ResetPasswordProfile::dispatch($user->id);

        return $this->displayOk([]);
    }

}
