<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\User\Linked;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

// phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint
// phpcs:disable Squiz.Strings.DoubleQuoteUsage.ContainsVar
// phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingNativeTypeHint
class Callbacks extends Controller
{

    public function fBAuthCallback(Request $request)
	{
        $endpoint = 'https://graph.facebook.com/v8.0/oauth/access_token';

        $host = $request->getHttpHost();

        $user_id = $request->state;

        $data = [
            'client_id' => 485966015538780,
            'redirect_uri' => "https://{$host}/api/methods/cbfb/",
            'client_secret' => '3b771774bc338626deac6d32b8166dc8',
            'code' => $request->code,
		];

        $response = $this->getHttpRequest($endpoint, $data);

        $user = $this->getHttpRequest('https://graph.facebook.com/me', [
            'access_token' => $response->access_token,
            'fields' => 'id, name, picture',
        ]);

        $linked = Linked::where('network_id', 2)->where('user_id', $user_id)->first();

        if (empty($linked)) {
            $linked = new Linked();
        }

        $linked->network_id = 2;
        $linked->user_id = $user_id;
        $linked->id = $user->id;
        $linked->username = $user->name;
        $linked->followers_count = 0;

        $linked->save();

        return redirect()->to('/success.html')->send();
    }

    public function iGAuthCallback(Request $request)
	{
        $endpoint = 'https://api.instagram.com/oauth/access_token';
        $user_id = $request->state;
        $host = $request->getHttpHost();

        $data = [
            'client_id' => '1244579435879568',
            'client_secret' => 'fa09e0e0cb5ef1ec450dd24e5ccceb17',
            'grant_type' => 'authorization_code',
            'redirect_uri' => "https://{$host}/api/methods/cbig/",
            'code' => str_replace('#_', '', $request->code),
        ];

        $response = $this->getHttpRequest($endpoint, $data, 'POST');

        $user = $this->getHttpRequest("https://graph.instagram.com/{$response->user_id}", [
            'fields' => 'id,username',
            'access_token' => $response->access_token,
        ]);

        $linked = Linked::where('network_id', 3)->where('user_id', $user_id)->first();

        if (empty($linked)) {
            $linked = new Linked();
        }

        $linked->network_id = 3;
        $linked->user_id = $user_id;
        $linked->id = $user->id;
        $linked->username = $user->username;
        $linked->followers_count = 0;

        $linked->photo = null;

        $linked->save();

        return redirect()->to('/success.html')->send();
    }

    public function vKAuthCallback(Request $request)
	{
        $endpoint = 'https://oauth.vk.com/access_token';
        $host = $request->getHttpHost();
        $data = [
            'client_id' => 6763421,
            'client_secret' => 'g39MeBjOE1tc0vKIf6dp',
            'redirect_uri' => "https://{$host}/api/methods/cbvk/",
            'code' => $request->code,
		];

        $user_id = $request->state;

        $response = $this->getHttpRequest($endpoint, $data);

        if (!$response->access_token) {
            return;
        }

        $user = $this->getHttpRequest('https://api.vk.com/method/users.get', [
            'v' => '5.52',
            'access_token' => $response->access_token,
            'fields' => 'verified, domain, photo_200, photo_400_orig, counters',
        ])->response[0];

        $linked = Linked::where('network_id', 1)->where('user_id', $user_id)->first();

        if (empty($linked)) {
            $linked = new Linked();
        }

        $linked->network_id = 1;
        $linked->user_id = $user_id;
        $linked->id = $user->id;
        $linked->username = $user->domain;
        $linked->followers_count = $user->counters->followers + $user->counters->friends;

        $fileName = sha1($linked->id . '_vk_small') . '.jpg';
        Storage::disk('s3')->put("users-linked-photos/{$fileName}", file_get_contents($user->photo_200), 'public');

        $linked->photo = Storage::disk('s3')->url("users-linked-photos/{$fileName}");

        $linked->save();

        return redirect()->to('/success.html')->send();
    }

    public function tWAuthCallback(Request $request)
	{
        $endpoint = 'https://id.twitch.tv/oauth2/token';

        $host = $request->getHttpHost();

        $data = [
            'client_id' => 'lfns34c4g69vh0imuy56ua8dsdkbgr',
            'client_secret' => 'aoxtif4uioecbin9q0tpl67ebsspbq',
            'code' => $request->code,
            'grant_type' => 'authorization_code',
            'redirect_uri' => "https://{$host}/api/methods/cbtw/",

		];

        $response = $this->getHttpRequest($endpoint, $data, 'POST');

        $user_id = $request->state;
        $access_token = $response->access_token;

        if ( !$access_token) {
            return;
        }

        $client = new Client();

        $userResponse = $client->request('GET', 'https://api.twitch.tv/kraken/user', [
            'query' => [],
            'headers' => [
                'Client-ID' => 'lfns34c4g69vh0imuy56ua8dsdkbgr',
                'Accept' => 'application/vnd.twitchtv.v5+json',
                'Authorization' => "OAuth {$access_token}",
            ],
        ]);

        $user = json_decode($userResponse->getBody()->getContents());

        $linked = Linked::where('network_id', 8)->where('user_id', $user_id)->first();

        if ( empty($linked)) {
            $linked = new Linked();
        }

        $linked->network_id = 8;
        $linked->user_id = $user_id;
        $linked->id = $user->_id;
        $linked->username = $user->display_name;

        $fileName = sha1($linked->id . '_tw_small') . '.jpg';
        Storage::disk('s3')->put("users-linked-photos/{$fileName}", file_get_contents($user->logo), 'public');

        $linked->photo = Storage::disk('s3')->url("users-linked-photos/{$fileName}");

        $linked->save();

        return redirect()->to('/success.html')->send();
    }

    private function getHttpRequest($endpoint, $params, $method = 'GET')
	{
        $client = new Client();

        $data = $method === 'GET' ? ['query' => $params] : ['form_params' => $params];

        try {
            $response = $client->request($method, $endpoint, $data);
        } catch (RequestException $ex) {
            print_r(json_decode($ex->getResponse()->getBody()->getContents(), true));
            exit;
        }

        return json_decode($response->getBody()->getContents());
    }

}
