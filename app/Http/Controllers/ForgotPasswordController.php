<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Events\ResetPassword;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ForgotPasswordController extends Controller
{

    public function forgot(Request $request): JsonResponse
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $user = User::getUserByEmail($request->email);

        if ($user === null) {
            return $this->displayError([
                'message' => 'This email is not exists',
            ], 1);
        }

        $code = rand(100000, 999999);
        $identification = sha1(time() . md5((string) $code) . $request->email);

        DB::table('module_authorization_codes')->insert([
            'identification' => $identification,
            'additional' => $request->email,
            'code' => intval($code),
        ]);

        //event
        ResetPassword::dispatch($user->id, $code);

        return $this->displayOk([
            'i' => $identification,
            'n' => false,
        ]);
    }

    public function confirm(Request $request): JsonResponse
    {
        $this->validate($request, [
            'password' => 'required',
            'i' => 'required:string',
            'c' => 'required:integer',
        ]);

        $access = DB::table('module_authorization_codes')
            ->where('identification', '=', $request->i)
            ->where('code', $request->c)
            ->first();

        if ($access) {
            $user = User::getUserByEmail($access->additional);

            if ($user === null) {
                return $this->displayError([
                    'message' => __('errors.invalid_code'),
                ], 1);
            }

            $user->password = Hash::make($request->password);
            $user->save();

            return $this->displayOk([]);

        }

        return $this->displayError([
            'message' => __('errors.invalid_code'),
        ], 1);
    }

}
