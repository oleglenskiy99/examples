<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\User\Cards;
use App\Services\Payment\FallbackPayoutService;
use App\Services\Payment\PaymentException;
use App\Services\Payment\StripePaymentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\SetupIntent;
use Throwable;

class Finance extends ApiController
{

    /**
     * @var StripePaymentService
     */
    protected $payments;

    /**
     * @var FallbackPayoutService
     */
    protected $payouts;

    public function __construct(StripePaymentService $payments, FallbackPayoutService $payouts)
    {
        parent::__construct();

        $this->payments = $payments;
        $this->payouts = $payouts;
    }

    public function checkPaymentStatus(Request $request): JsonResponse
    {
        $this->validate($request, [
            'payment_intent' => 'required',
        ]);

        $intent = $this->payments->checkPaymentStatus($this->current, $request->payment_intent);
        return $intent->status === PaymentIntent::STATUS_SUCCEEDED
            ? $this->displayOk([])
            : $this->displayError([
                'intent' => $intent,
                'action' => $intent->next_action,
            ]);
    }


    public function registryCard(Request $request): JsonResponse
    {
        $this->validate($request, [
            'customer_token' => 'required',
            'account_token' => 'sometimes',
        ]);

        $user = $this->current;
        // TODO: отдавать новый, одноразовый токен
        $returnUrl = action([self::class, 'registryCardStatus'], ['access_token' => $this->accessToken]);

        try {
            $intent = $this->payments->addCreditCard($user, $request->customer_token, $request->account_token, $returnUrl);
            return $intent->status === SetupIntent::STATUS_SUCCEEDED
                ? $this->displayOk([])
                : $this->displayError([
                    'intent' => $intent,
                    'action' => $intent->next_action,
                ]);
        } catch (ApiErrorException $exception) {
            return $this->displayError([
                'message' => $exception->getMessage(),
                'gateway_request_id' => $exception->getRequestId(),
                'gateway_reject_code' => $exception->getStripeCode(),
            ]);
        } catch (Throwable $exception) {
            return $this->displayError([
                'message' => $exception->getMessage(),
            ]);
        }
    }

    public function registryCardStatus(Request $request): void
    {
        $this->validate($request, [
            'setup_intent' => 'required',
        ]);

        if ($this->payments->checkCreditCardStatus($this->current, $request->setup_intent)) {
            echo 'OK';
        }
    }

    public function addBankAccount(Request $request): JsonResponse
    {
        $this->validate($request, [
            'token' => 'required',
        ]);

        $user = $this->current;

        try {
            $this->payments->addBankAccount($user, $request->token);
            return $this->displayOk([]);
        } catch (ApiErrorException $exception) {
            return $this->displayError([
                'message' => $exception->getMessage(),
                'gateway_request_id' => $exception->getRequestId(),
                'gateway_reject_code' => $exception->getStripeCode(),
            ]);
        } catch (Throwable $exception) {
            return $this->displayError([
                'message' => $exception->getMessage(),
            ]);
        }
    }

    public function balanceUpdate(Request $request): JsonResponse
    {
        $this->validate($request, [
            'card' => 'required',
            'amount' => 'required',
        ]);

        $user = $this->current;
        $card = Cards::find($request->card);
        if ($card === null) {
            return $this->displayError(['message' => 'Card haven\'t been found']);
        }

        try {
            $this->payments->updateBalance($user, $card, 'usd', (int) $request->amount);
            return $this->displayOk([
                'balance' => $user->balance,
            ]);
        } catch (Throwable $exception) {
            return $this->displayError([
                'message' => $exception->getMessage(),
            ]);
        }
    }

    public function removeCard(Request $request): JsonResponse
    {
        $this->validate($request, [
            'card' => 'required',
        ]);

        $user = $this->current;
        $card = Cards::find($request->card);

        try {
            if ($card) {
                $this->payments->removeCreditCard($user, $card);
            }
            return $this->displayOk([]);
        } catch (Throwable $exception) {
            return $this->displayError([
                'message' => $exception->getMessage(),
            ]);
        }
    }

    public function payout(Request $request): JsonResponse
    {
        $this->validate($request, [
            'card' => 'required',
            'amount' => 'required',
        ]);

        $user = $this->current;
        $card = Cards::find($request->card);
        if ($card === null) {
            return $this->displayError([]);
        }

        try {
            $refreshUrl = url('/');
            $returnUrl = url('/');
            $kyc = $this->payments->checkAccount($user, $refreshUrl, $returnUrl);
            if ($kyc !== null) {
                return $this->displayError(['kyc' => $kyc]);
            }
            $this->payments->createPayout($user, $card, (float) $request->amount);
            return $this->displayOk([]);
        } catch (PaymentException $exception) {
            return $this->displayError([
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
            ]);
        } catch (ApiErrorException $exception) {
            return $this->displayError([
                'code' => $exception->getStripeCode(),
                'error' => $exception->getError(),
                'message' => $exception->getMessage(),
            ]);
        } catch (Throwable $exception) {
            return $this->displayError([
                'message' => $exception->getMessage(),
            ]);
        }
    }

    public function payoutFallback(Request $request): JsonResponse
    {
        $this->validate($request, [
            'account' => 'required',
            'amount' => 'required',
        ]);

        $user = $this->current;

        try {
            $this->payouts->createPayout($user, $request->account, (float) $request->amount);
            return $this->displayOk([]);
        } catch (Throwable $exception) {
            return $this->displayError(['message' => $exception->getMessage()]);
        }
    }

}
