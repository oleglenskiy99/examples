<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Throwable;

class ApiController extends Controller
{

    /**
     * {@inheritDoc}
     *
     * @see \App\Http\Controllers\Controller::callAction()
     */
    public function callAction($method, $parameters)
    {
        $request = request();
        if (!$request->attributes->has('session')) {
            return $this->displayError(['isBackground' => true], 4);
        }

        try {
            return parent::callAction($method, $parameters);
        } catch (Throwable $exception) {
            return response()->json([
                'status' => 'fail',
                'message' => $exception->getMessage(),
            ]);
        }
    }

}
