<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\DTO\Request\Login;
use App\DTO\Request\RegistrationUser;
use App\Http\Requests\V1\AuthRequests;
use App\Http\Requests\V1\SignUpRequest;
use App\Services\V1\UsersService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

class AuthorizationEmailController extends Controller
{

    private UsersService $usersService;
    private SerializerInterface $serializer;

    public function __construct(UsersService $usersService, SerializerInterface $serializer)
    {
        $this->usersService = $usersService;
        $this->serializer = $serializer;
    }

    /**
     * @OA\Post(
     *     path="/api/methods/auth.signin",
     *     summary="Email authorization",
     *     operationId="auth.signin",
     *     description="Проверяет авторизацию по Email",
     * @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function authorization(AuthRequests $request): JsonResponse
    {
        $userAuthData = new Login();
        $userAuthData->setEmail($request->get('email'))->setPassword($request->get('password'));

        $userAuth = $this->usersService->authUserWithEmailPassword($userAuthData);

        if ($userAuth === null) {
            return $this->displayError([
                'message' => __('errors.authorization'),
            ], 1);
        }

        return $this->displayOk([
            'access_token' => $userAuth->getAccessToken(),
            'jwt_token' => $userAuth->getJwtToken(),
            'jwt_expires' => $userAuth->getJwtExpires(),
            'jwt_token_type' => $userAuth->getJwtTokenType(),
        ]);
    }

    /**
     * @OA\Post(
     *     path="/api/methods/auth.signup",
     *     summary="Email registration",
     *     operationId="auth.signup",
     *     description="Регистрируемся с помощью Email",
     * @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function registration(SignUpRequest $request): JsonResponse
    {
        $registrationUser = new RegistrationUser();
        $registrationUser->setEmail($request->get('email'))
            ->setPassword($request->get('password'));

        try {
            $userAuth = $this->usersService->createUserWithEmailPassword($registrationUser);
        } catch (Throwable $exception) {
            return $this->displayError([
                'message' => $exception->getMessage(),
            ], (int) $exception->getCode());
        }

        return $this->displayOk([
            'access_token' => $userAuth->getAccessToken(),
            'jwt_token' => $userAuth->getJwtToken(),
            'jwt_expires' => $userAuth->getJwtExpires(),
            'jwt_token_type' => $userAuth->getJwtTokenType(),
        ]);
    }

}
