<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\V1\UploadServices;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Upload extends ApiController
{

    private UploadServices $uploadServices;

    public function __construct(UploadServices $uploadServices)
    {
        parent::__construct();

        $this->uploadServices = $uploadServices;
    }

    public function media(Request $request): JsonResponse
    {
        $this->validate($request, [
            'object' => ['required', 'file'],
        ]);

        $file = $request->file('object');

        if ($request->type) {
            $fileMimeType = $request->type;
            if ($request->voice) {
                $fileMimeType = 'audio';
            }
        } else {
            $fileMimeType = $request->voice ? 'audio' : explode('/', $file->getMimeType())[0];
        }

        switch ($fileMimeType) {
            case 'image':
                $store = $this->uploadServices->processImageUpload($file);
                break;
            case 'video':
                $store = $this->uploadServices->processVideoUpload($file, $request->width, $request->height);
                break;
            case 'audio':
                $store = $this->uploadServices->voice($file);
                break;
            default:
                $store = $this->uploadServices->processFileUpload($request);
        }

        return $this->displayOk($store);
    }

    public function getmedia(Request $request): JsonResponse
    {
        $this->validate($request, [
            'media' => 'required',
        ]);

        return $this->displayOk([
            'urls3' => $this->uploadServices->getMedia($request->media),
        ]);
    }

}
