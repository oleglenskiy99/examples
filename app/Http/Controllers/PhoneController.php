<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Events\User\UpdateUserEvent;
use App\Helpers\SMSTexts;
use App\Models\Contacts\ContactArchives;
use App\Repository\AuthorizationCodesRepository;
use App\Services\SendSMSServiceInterface;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PhoneController extends ApiController
{

    public function addNumber(Request $request, SendSMSServiceInterface $serviceSMS): JsonResponse
    {
        $this->validate($request, [
            'phone' => 'required',
        ]);

        $user = User::getUserByPhone($request->phone);

        if ($user) {
            return $this->displayError([
                'message' => 'This phone number exists in another user account',
            ], 1);
        }

        $codes = AuthorizationCodesRepository::createCode($request->phone);

        $message = __(SMSTexts::VERIFIED_TEXT) . ' ' . $codes->code;

        $messageID = $serviceSMS->send($request->phone, $message);

        if ($messageID === null) {
            return $this->displayError([
                'message' => __('errors.authorization'),
            ], 1);
        }

        return $this->displayOk([
            'i' => $codes->identification,
            's' => $messageID,
            'n' => false,
        ]);
    }

    public function delNumber(): JsonResponse
    {
        $user = User::getUserById($this->userId);

        if ($user === null) {
            return $this->displayError([
                'message' => 'user not found',
            ], 1);
        }

        ContactArchives::insert([
            'user_id' => $user->id,
            'value' => $user->phone,
            'type' => 'phone',
        ]);

        $user->phone = null;
        $user->save();

        UpdateUserEvent::dispatch($user->id);

        return $this->displayOk([]);
    }

    public function confirm(Request $request): JsonResponse
    {
        $this->validate($request, [
            'phone' => 'required',
            'i' => 'required:string',
            'c' => 'required:integer',
        ]);

        $user = User::getUserByPhone($request->phone);

        if ($user) {
            return $this->displayError([
                'message' => 'This phone number exists in another user account',
            ], 1);
        }

        $access = AuthorizationCodesRepository::getCode([
            'identification' => $request->i ?? '',
            'additional' => '',
            'code' => $request->c,
        ]);

        if ($access === null) {
            return $this->displayError([
                'i' => $request->i,
                'c' => $request->c,
            ], 11);
        }
        $user = User::getUserById($this->userId);

        $user->phone = $request->phone;
        $user->save();

        UpdateUserEvent::dispatch($this->userId);

        return $this->displayOk([]);
    }

}
