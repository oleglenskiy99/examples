<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\DTO\Request\ReviewUser;
use App\Events\User\UpdateUserEvent;
use App\Helpers\UserHelper;
use App\Http\Requests\V1\PriceChangeRequest;
use App\Models\User\Categories;
use App\Models\User\Info;
use App\Repository\User\PricesRepository;
use App\Services\V1\UsersService;
use App\Services\V1\UtilsService;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

class UserController extends ApiController
{

    private UsersService $usersService;
    private UtilsService $utilsService;
    private SerializerInterface $serializer;
    private DenormalizerInterface $denormalizer;

    public function __construct(
        UtilsService $utilsService,
        UsersService $usersService,
        SerializerInterface $serializer,
        DenormalizerInterface $denormalizer
    )
    {
        parent::__construct();

        $this->usersService = $usersService;
        $this->utilsService = $utilsService;
        $this->serializer = $serializer;
        $this->denormalizer = $denormalizer;
    }

    /**
     * @OA\Get(
     *     path="/api/methods/user.getByIds",
     *     summary="user getByIds",
     *     security={{"bearerAuth":{}}},
     *     operationId="user.getByIds",
     *     description="get getByIds",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="ids[]",
     *         in="query",
     *         required=true,
     *         description="array|integer",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function getByIds(Request $request): JsonResponse
    {
        /* TODO Нужно изменить все валидаторы, чтобы был return в случае ошибки, вместо exit */

        $this->validate($request, [
            'ids' => 'required|array',
        ]);

        $users = User::getByIds($request->ids);

        return $this->displayOk($users);
    }

    /**
     * @OA\Post (
     *     path="/api/methods/user.set",
     *     summary="user set",
     *     security={{"bearerAuth":{}}},
     *     operationId="user.set",
     *     description="Update user profile information",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="firstName",
     *         in="query",
     *         required=false,
     *         description="user first name",
     *     ),
     * @OA\Parameter(
     *         name="pseudonym",
     *         in="query",
     *         required=false,
     *         description="is user pseudonym",
     *     ),
     * @OA\Parameter(
     *         name="lastName",
     *         in="query",
     *         required=false,
     *         description="user last name",
     *     ),
     * @OA\Parameter(
     *         name="biography",
     *         in="query",
     *         required=false,
     *         description="string",
     *     ),
     * @OA\Parameter(
     *         name="industry",
     *         in="query",
     *         required=false,
     *         description="integer",
     *     ),
     * @OA\Parameter(
     *         name="country",
     *         in="query",
     *         required=false,
     *         description="country id",
     *     ),
     * @OA\Parameter(
     *         name="onboarding",
     *         in="query",
     *         required=false,
     *         description="is onboarding step",
     *     ),
     * @OA\Parameter(
     *         name="domain",
     *         in="query",
     *         required=false,
     *         description="string",
     *     ),
     * @OA\Parameter(
     *         name="categories",
     *         in="query",
     *         required=false,
     *         description="Array of categories ids for update",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     * @OA\Property (property="status",type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function update(Request $request): JsonResponse
	{
        $user = Info::find($this->userId);
        if ($user === null) {
            $user = new Info();
            $user->id = $this->userId;
        }

        if ($request->has('firstName')) {
            $user->firstName = $request->firstName;
        }
        if ($request->has('lastName')) {
            $user->lastName = $request->lastName;
        }
        if ($request->has('biography')) {
            $user->biography = $request->biography;
        }
        if ($request->has('pseudonym')) {
            $user->pseudonym = $request->pseudonym;
        }
        if ($request->has('industry')) {
            $user->industry = $request->industry;
        }
        if ($request->has('country')) {
            $user->country = $request->country;
        }

        $user->save();

        if ($request->has('domain')) {

            $isUserDomainAvailable = $this->utilsService->isUserDomainAvailable($request->get('domain'), $this->userId);

            if ($isUserDomainAvailable === false) {
                return $this->displayError([], 13);
            }

            User::getUserById($this->userId)->update([
                'domain' => UserHelper::getDomainNameWithOutAt($request->domain),
            ]);

        }

        if ($request->has('onboarding')) {

            User::getUserById($this->userId)->update([
                'onboarding' => $request->onboarding,
            ]);
        }

        if ( $request->has('categories')) {
            Categories::where('user_id', $this->userId)->delete();
            $inserts = [];
            foreach ($request->categories as $category) {
                $inserts[] = ['user_id' => $this->userId, 'category_id' => $category];
            }
            Categories::insert($inserts);
        }

        $user->save();

        UpdateUserEvent::dispatch($this->userId);

        return $this->displayOk([]);
    }

    public function addMedia(Request $request): JsonResponse
	{
        $this->validate($request, [
            'id' => [
                'required',
                'integer',
            ],
        ]);

        if ($this->usersService->addMedia((int) $request->id, $this->userId) !== null) {
            return $this->displayOk([]);
        }
        return $this->displayError([], 8);
    }

    public function removeMedia(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => [
                'required',
                'integer',
            ],
        ]);

        $this->usersService->removeMedia((int) $request->id, $this->userId);

        return $this->displayOk([]);
    }

    /**
     * @OA\Post (
     *     path="/api/methods/user.removePhoto",
     *     summary="user.removePhoto",
     *     security={{"bearerAuth":{}}},
     *     operationId="user.removePhoto",
     *     description="user.removePhoto",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function removePhoto(): JsonResponse
    {
        $this->usersService->removePhoto($this->userId);
        return $this->displayOk([]);
    }


    /**
     * @OA\Post (
     *     path="/api/methods/user.setDomain",
     *     summary="user.setDomain",
     *     security={{"bearerAuth":{}}},
     *     operationId="user.setDomain",
     *     description="user.setDomain",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="domain",
     *         in="query",
     *         required=true,
     *         description="string",
     *     ),
     * @OA\Parameter(
     *         name="onboarding",
     *         in="query",
     *         required=false,
     *         description="string",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function setDomain(Request $request): JsonResponse
	{
        /* TODO Нужно изменить все валидаторы, переписать на Request */

        $this->validate($request, [
            'domain' => [
                'required',
                'regex:/[0-9a-z][-_.0-9a-z]{5,15}/i',
                'unique:App\User,domain',
            ],
        ]);

        $domain = UserHelper::getDomainNameWithOutAt((string) $request->get('domain'));

        /** @psalm-suppress InvalidMethodCall * */
        $this->current->update(['domain' => $domain]);

        if ($request->has('onboarding')) {
            User::getUserById($this->userId)->update(['onboarding' => $request->onboarding]);
        }

        return $this->displayOk([]);
    }


    /**
     * @OA\Post (
     *     path="/api/methods/user.setPrice",
     *     summary="user.setPrice",
     *     security={{"bearerAuth":{}}},
     *     operationId="user.setPrice",
     *     description="user.setPrice",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="key",
     *         in="query",
     *         required=true,
     *         description="string",
     *     ),
     * @OA\Parameter(
     *         name="apple_good_id",
     *         in="query",
     *         required=false,
     *         description="boolean",
     *     ),
     * @OA\Parameter(
     *         name="value",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Parameter(
     *         name="onboarding",
     *         in="query",
     *         required=false,
     *         description="string",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function setPrice(PriceChangeRequest $request): JsonResponse
	{
        PricesRepository::savePrice($this->userId, $request->key, (float) $request->value, $request->apple_good_id);

        //for onboarding process
        if ($request->has('onboarding')) {
            $this->current->update(['onboarding' => $request->onboarding]);
        }

        UpdateUserEvent::dispatch($this->userId);

        return $this->displayOk([]);
    }

    /**
     * @OA\Post (
     *     path="/api/methods/user.review",
     *     summary="user.review",
     *     security={{"bearerAuth":{}}},
     *     operationId="user.review",
     *     description="user.review",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="target_id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Parameter(
     *         name="service_type",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Parameter(
     *         name="value",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Parameter(
     *         name="service_id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function makeReview(Request $request): JsonResponse
	{
        $getReviewUser = $this->denormalizer->denormalize(
            $request->query(),
            ReviewUser::class,
            JsonEncoder::FORMAT
        );

        $getReviewUser->setTargetId((int) $request->target_id);
        $getReviewUser->setServiceId((int) $request->service_id);
        $getReviewUser->setValue((int) $request->value);
        $getReviewUser->setCaption((string) $request->caption);
        $getReviewUser->setServiceType((string) $request->service_type);

        try {
            $this->usersService->setReview($getReviewUser, $this->userId);
        } catch (Throwable $ex) {
            return $this->displayError([
                'message' => $ex->getMessage(),
            ], 9);
        }

        return $this->displayOk([]);
    }

}
