<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\V1\UsersService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

// phpcs:disable Squiz.Strings.DoubleQuoteUsage.ContainsVar
class Explore extends Controller
{

    private UsersService $usersService;

    public function __construct(UsersService $usersService)
    {
        parent::__construct();

        $this->usersService = $usersService;
    }

    /**
     * @OA\Post (
     *     path="/api/methods/explore.next",
     *     summary="explore.next",
     *     security={{"bearerAuth":{}}},
     *     operationId="explore.next",
     *     description="explore.next",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Parameter(
     *         name="next_max_id",
     *         in="query",
     *         required=true,
     *         description="integer",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function peopleNext(Request $request): JsonResponse
    {
        $this->validate($request, ['next_max_id' => 'required:string']);

        if ($this->usersService->setPaginatePageUser($request->next_max_id) === false) {
            return $this->displayError([], 5);
        }
        return self::people($request);
    }

    /**
     * @OA\Get (
     *     path="/api/methods/explore",
     *     summary="explore",
     *     security={{"bearerAuth":{}}},
     *     operationId="explore",
     *     description="explore",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public function people(Request $request): JsonResponse
    {
        return $this->displayOk([
            'people' => $this->usersService->getSortedPaginatedUsers($this->userId),
        ]);
    }

    /**
     * @OA\Get (
     *     path="/api/methods/explore.industries",
     *     summary="explore",
     *     security={{"bearerAuth":{}}},
     *     operationId="explore.industries",
     *     description="get not empty categories",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *         description="token",
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function industries(): JsonResponse
    {
        return $this->displayOk($this->usersService->getSortedCategories($this->userId));
    }

     /**
      * @OA\Get (
      *     path="/api/methods/explore.industry",
      *     summary="explore",
      *     security={{"bearerAuth":{}}},
      *     operationId="explore.industry",
      *     description="get sorted users in category",
      * @OA\Parameter(
      *         name="access_token",
      *         in="query",
      *         required=true,
      *         description="token",
      *     ),
      * @OA\Parameter(
      *         name="id",
      *         in="query",
      *         required=true,
      *         description="category id",
      *     ),
      * @OA\Response(
      *         response=200,
      *         description="Ok",
      * @OA\MediaType(
      *              mediaType="application/json",
      * @OA\Schema(
      *                  type="array",
      * @OA\Items(type="string"),
      *              )
      *         ),
      *     ),
      * )
      */
    public function industry(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required',
        ]);
        if (!empty($request->next_max_id)) {
            if ($this->usersService->setPaginatePageUser($request->next_max_id) === false) {
                return $this->displayError([], 5);
            }
        }
        return $this->displayOk($this->usersService->getSortedPaginatedUsers($this->userId, (int) $request->id));
    }

}
