<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\V1\ContactService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ContactsController extends ApiController
{

    private ContactService $contactService;

    public function __construct(ContactService $contactService)
    {
        parent::__construct();

        $this->contactService = $contactService;
    }

    /**
     * @OA\Post(
     *     path="/api/methods/contacts.add",
     *     security={{"bearerAuth":{}}},
     *     summary="Add contact",
     *     operationId="contacts.add",
     *     description="Add followers contact",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Parameter(
     *         name="id",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function add(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required:integer',
        ]);
        return $this->displayOk($this->contactService->add((int) $request->id, $this->userId));
    }

    /**
     * @OA\Post(
     *     path="/api/methods/contacts.remove",
     *     security={{"bearerAuth":{}}},
     *     summary="Remove contact",
     *     operationId="contacts.remove",
     *     description="Remove followers contact",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Parameter(
     *         name="id",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     *         ),
     *     ),
     * )
     */
    public function remove(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required:integer',
        ]);

        $this->contactService->cancel($request->id);
        return $this->displayOk([]);
	}

}
