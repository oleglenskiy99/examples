<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Crypto;
use App\Errors;
use App\Tokenz;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;

/**
 * @OA\Info(title="Paysenger API", version="1.0")
 */
// phpcs:disable Squiz.Strings.DoubleQuoteUsage.ContainsVar
class Controller extends BaseController
{

    private const ACCESS_TOKEN = 'access_token';
    private const SESSION = 'session';

    /**
     * @var array<string>
     */
    public array $headers = [];
    public ?string $accessToken = null;
    public bool $isAuthorized = false;
    public ?Tokenz $session = null;
    public ?int $userId = null;
    public ?User $current = null;

    public function __construct()
    {
        if (!function_exists('apacheRequestHeaders')) {
            return;
        }
        $this->headers = apacheRequestHeaders();
    }

    /**
     * {@inheritDoc}
     *
     * @see \Illuminate\Routing\Controller::callAction()
     */
    public function callAction($method, $parameters)
    {
        $request = request();

        $this->accessToken = $request->attributes->get(self::ACCESS_TOKEN);
        $this->session = $request->attributes->get(self::SESSION, null);

        if ($this->session !== null) {
            $this->isAuthorized = true;
            $this->userId = $this->session->user_id;
            session(['user_id' => $this->userId]);
            $this->current = User::getUserById($this->userId);
            if ($this->current) {
                auth()->login($this->current);
                $this->current->update(['last_activity' => time()]);
            }
        }

        return parent::callAction($method, $parameters);
    }

    public function undefinedValueError(Request $request, $param): void
    {
        $params = [];

        foreach ($request->all() as $k => $v) {
            $params[] = ['key' => $k, 'value' => $v];
        }

        echo response()->json([
            'responseStatus' => 'error',
            'responseErrorCode' => 100,
            'responseData' => [
                'error_message' => "One of the parameters specified was missing or invalid: {$param} is undefined",
                'request_params' => $params,
            ],
        ])->content();
    }

    public function invalidParamType(Request $request, $param, $needly): void
    {
        $params = [];

        foreach ($request->all() as $k => $v) {
            $params[] = ['key' => $k, 'value' => $v];
        }

        $currentType = gettype($request->$param);

        echo response()->json([
            'responseStatus' => 'error',
            'responseErrorCode' => 100,
            'responseData' => [
                'error_message' => "One of the parameters have invalid type: {$param} is {$currentType}, but must be {$needly}.",
                'request_params' => $params,
            ],
        ])->content();
    }

    public function validate(Request $request, $rules = []): void
    {
        $validator = Validator::make($request->all(), $rules);

        if (!$validator->fails()) {
            return;
        }

        $this->displayError([
            'fields' => $validator->errors()->messages(),
        ], 100);
    }

    public function displayError(?array $data = [], int $code = 1): JsonResponse
    {
        if (empty($data['message'])) {
            if (Errors::get($code)) {
                $data['message'] = Errors::get($code);
                if ($code === 1) {
                    $data['isBackground'] = true;
                }
            }
        }

        $data['code'] = $code;
        $data['status'] = 'fail';

        if (array_key_exists('Content-Secure', $this->headers) && $this->accessToken) {
            $data = json_decode(Crypto::encrypt(json_encode($data), $this->accessToken));
        }
        return response()->json($data);
    }

    public function displayOk(array $data): JsonResponse
    {
        $response = [
            'status' => 'ok',
            'result' => $data,
        ];

        if (array_key_exists('Content-Secure', $this->headers) && $this->accessToken) {
            $response = json_decode(Crypto::encrypt(json_encode($response), $this->accessToken));
        }

        return response()->json($response);
    }

}
