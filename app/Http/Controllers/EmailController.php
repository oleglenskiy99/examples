<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Events\ConfirmEmailEvent;
use App\Events\User\UpdateUserEvent;
use App\Models\Contacts\ContactArchives;
use App\Repository\AuthorizationCodesRepository;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EmailController extends ApiController
{

    /**
     * @OA\Post(
     *     path="/api/methods/email.change",
     *     security={{"bearerAuth":{}}},
     *     summary="Email change",
     *     operationId="email.change",
     *     description="Отправка письма для изменения почты",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function changemail(Request $request): JsonResponse
    {
        $this->validate($request, [
            'email' => 'required|filled|email',
        ]);

        $user = User::getUserById($this->userId);

        if ($user === null) {
            return $this->displayError([
                'message' => 'User not found',
            ], 9);
        }

        $userCheck = User::getUserByEmail($request->email);

        if ($userCheck && $userCheck->id !== $user->id) {
            return $this->displayError([
                'message' => 'This email exists in another user account',
            ], 1);
        }

        ContactArchives::insert([
            'user_id' => $user->id,
            'value' => $user->email,
            'type' => 'email',
        ]);

        $user->email = $request->email;
        $user->is_email_verify = false;
        $user->save();

        //update user Event;
        UpdateUserEvent::dispatch($user->id);

        $codes = AuthorizationCodesRepository::createCode(strval($user->id));

        ConfirmEmailEvent::dispatch($user->id, $codes->code);

        return $this->displayOk(['i' => $codes->identification]);
    }

    /**
     * @OA\Post(
     *     path="/api/methods/email.confirm",
     *     security={{"bearerAuth":{}}},
     *     summary="Email confirm",
     *     operationId="email.confirm",
     *     description="Отправка письма для подтверждения почты",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function confirm(): JsonResponse
    {
        $user = User::getUserById($this->userId);

        if ($user === null) {
            return $this->displayError([
                'message' => 'User not found',
            ], 9);
        }

        $codes = AuthorizationCodesRepository::createCode(strval($user->id));

        ConfirmEmailEvent::dispatch($user->id, $codes->code);

        return $this->displayOk(['i' => $codes->identification]);
    }

    /**
     * @OA\Post(
     *     path="/api/methods/email.checkconfirm",
     *     security={{"bearerAuth":{}}},
     *     summary="Email check confirm",
     *     operationId="email.checkconfirm",
     *     description="Check and confirm email",
     * @OA\Parameter(
     *         name="access_token",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Parameter(
     *         name="i",
     *         in="query",
     *         required=false,
     *     ),
     * @OA\Parameter(
     *         name="code",
     *         in="query",
     *         required=true,
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Ok",
     * @OA\MediaType(
     *              mediaType="application/json",
     * @OA\Schema(
     *                  type="array",
     * @OA\Items(type="string"),
     *              )
     *         ),
     *     ),
     * )
     */
    public function checkconfirm(Request $request): JsonResponse
    {
        $this->validate($request, [
            'code' => 'required:integer',
            'i' => 'string',
        ]);

        $access = AuthorizationCodesRepository::getCode([
            'identification' => $request->i ?? '',
            'additional' => strval($this->userId),
            'code' => $request->code,
        ]);

        $user = User::getUserById($this->userId);

        if (!($access && $user && $user->id === intval($access->additional))) {
            return $this->displayError([
                'message' => __('errors.invalid_code'),
            ], 1);
        }

        $user->is_email_verify = true;
        $user->last_activity = time();
        $user->save();

        //update user Event;
        UpdateUserEvent::dispatch($user->id);

        return $this->displayOk(['i' => $access->identification]);
    }

}
