<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Tokenz;
use Closure;
use Illuminate\Http\Request;

class Session
{

    public const ACCESS_TOKEN = 'access_token';
    public const SESSION = 'session';

    public function handle(Request $request, Closure $next): object
    {
        $accessToken = $request->query->get('access_token', null);
        $accessToken ??= $request->headers->get('Authorization', null);
        $accessToken ??= $request->headers->get('X-Authorization', null);
        $request->attributes->set(self::ACCESS_TOKEN, $accessToken);

        if ($accessToken !== null) {
            $session = Tokenz::findByAccessToken((string) $accessToken);
            if ($session !== null) {
                $request->attributes->set(self::SESSION, $session);
            }
        }
        return $next($request);
    }

}
