<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Tokenz;
use App\User;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Symfony\Component\HttpFoundation\Response;
use Throwable;
use Tymon\JWTAuth\JWTAuth;

class Authenticate extends Middleware
{

    private JWTAuth $JWTAuth;

    public function __construct(JWTAuth $JWTAuth)
    {
        $this->JWTAuth = $JWTAuth;
    }

    // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
    public function handle($request, Closure $next, ...$guards): object
    {
        //TODO When we fully transfer to V2 API, need to delete that class
        if (strpos($request->getPathInfo(), 'v2') !== false) {
            return $next($request);
        }

        // JWT
        $authorizationToken = $request->headers->get('Authorization')
            // OLD
            ?? $request->headers->get('authorization')
            // Query Param
            ?? $request->get('access_token')
            // Query Param
            ?? $request->get('token');

        if (!$authorizationToken) {
            return response()->json([
                'status' => 'fail',
            ], Response::HTTP_UNAUTHORIZED);
        }

        try {

            $user = $this->JWTAuth->setToken($authorizationToken)->user();

            if (!$user) {
                // If used old authorization token
                $token = Tokenz::where('access_token', $authorizationToken)->firstOrFail();
                $user = User::getUserById($token->user_id);
            }
            /** @psalm-suppress UndefinedMagicPropertyFetch * */
            /** @psalm-suppress NoInterfaceProperties */
            session(['user_id' => $user->id]);
            /** @psalm-suppress UndefinedMagicPropertyFetch * */
            /** @psalm-suppress NoInterfaceProperties */
            if ($user->is_blocked) {
                return response()->json([
                    'status' => 'fail',
                ], Response::HTTP_FORBIDDEN);
            }

        } catch (Throwable $exception) {
            return response()->json([
                '_debug' => $exception->getMessage(),
            ], Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }

}
