<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Hardware
{

    public const DEVICE = 'device';
    public const APP_VERSION = 'app_version';
    public const OP_SYSTEM = 'op_system';
    public const OP_VERSION = 'op_version';
    public const DEVICE_TOKEN = 'device_token';

    public function handle(Request $request, Closure $next): object
    {
        $request->attributes->set(
            self::DEVICE,
            $request->headers->get(self::DEVICE, $this->detectByUserAgent($request->userAgent() ?? ''))
        );
        foreach ([
            self::APP_VERSION,
            self::OP_SYSTEM,
            self::DEVICE_TOKEN,
        ] as $header) {
            $request->attributes->set($header, $request->headers->get($header));
        }
        $version = null;
        if ($request->headers->has(self::OP_VERSION)) {
            $version = str_replace('_', '.', $request->headers->get(self::OP_VERSION));
        }
        $request->attributes->set(self::OP_VERSION, $version);
        return $next($request);
    }

    protected function detectByUserAgent(string $userAgent = ''): string
    {
        $os = 'Unknown';
        $matches = [];
        if (preg_match('/((?!.*Android)Linux|Android|Windows|(?!.*iPhone)Mac|iPhone|Apple|Unknown)/i', $userAgent, $matches)) {
            [, $os] = $matches;
        }
        return $os;
    }

}
