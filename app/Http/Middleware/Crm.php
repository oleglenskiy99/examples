<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Helpers\CacheKey;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Crm
{

    public function __construct(Request $request)
    {
        if ($request->bearerToken() !== env('JWT_PUBLIC_KEY')) {
            throw new HttpException(401, 'Unauthorized');
        }

        if ($request->route()->uri() !== 'crm/login') {
            self::authenticate($request);
        }
    }

    public function handle(Request $request, Closure $next): object
    {
        //todo сделать логер запросов
        return $next($request);
    }

    // phpcs:disable Squiz.Strings.DoubleQuoteUsage.ContainsVar
    // phpcs:disable PEAR.WhiteSpace.ObjectOperatorIndent.Incorrect
    protected function authenticate(Request $request): void
    {
        if (Cache::store('redis')
                ->get(CacheKey::CRM_USER_KEY . ":{$request->get('user')}") !== $request->get('user_id')
        ) {
            throw new HttpException(403, 'permission denied');
        }
    }

}
