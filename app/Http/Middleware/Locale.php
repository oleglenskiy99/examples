<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class Locale
{

	public function handle(Request $request, Closure $next): object
	{
		$locale = '';

		$headers = apacheRequestHeaders();

		if ( array_key_exists('Locale', $headers)) {
			$locale = $headers['Locale'];
		}

		if ( $request->get('locale') !== null) {
			$locale = $request->get('locale');
		}

		$locale = mb_strtolower($locale);

		if ( !in_array($locale, ['en', 'ru'])) {
			$locale = config('app.locale');
		}

		App::setLocale($locale);

		return $next($request);
	}

}
