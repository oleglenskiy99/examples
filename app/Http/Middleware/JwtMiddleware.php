<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\DTO\Exception\UnauthorizedException;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\JWTAuth;

class JwtMiddleware
{

    private JWTAuth $JWTAuth;
    private SerializerInterface $serializer;

    public function __construct(JWTAuth $JWTAuth, SerializerInterface $serializer)
    {
        $this->JWTAuth = $JWTAuth;
        $this->serializer = $serializer;
    }


    public function handle(Request $request, Closure $next): object
    {
        $token = str_replace('Bearer ', '', $request->header('Authorization'));

        try {
            $this->JWTAuth->setToken($token);
            if (!$this->JWTAuth->getPayload()) {
                return $this->buildExceptionResponse('Unauthorized');
            }
        } catch (TokenExpiredException $e) {
            return $this->buildExceptionResponse('Token is expired');
        } catch (TokenInvalidException $e) {
            return $this->buildExceptionResponse('Invalid token');
        } catch (JWTException $e) {
            return $this->buildExceptionResponse('Incorrect token ');
        }

        return $next($request);
    }

    private function buildExceptionResponse(string $message): JsonResponse
    {
        $response = new UnauthorizedException();
        $response
            ->setCode(403)
            ->setMessage($message)
        ;
        $data = $this->serializer->serialize($response, JsonEncoder::FORMAT);
        return new JsonResponse($data, JsonResponse::HTTP_UNAUTHORIZED, [], 0, true);
    }

}
