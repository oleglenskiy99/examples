<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Cors
{

    public function handle(Request $request, Closure $next): object
    {
        $response = $next($request);
        $SymfonyRedirectResponse = 'Symfony\Component\HttpFoundation\RedirectResponse';

        if ($response instanceof $SymfonyRedirectResponse) {
            return $response;
        }

        $response
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS')
            ->header('Access-Control-Allow-Headers', '*')
            ->header('Access-Control-Max-Age', '1728000');

        return $response;
    }

}
