<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Illuminate\Http\Middleware\TrustProxies as Middleware;
use Illuminate\Http\Request;

// phpcs:disable PEAR.Formatting.MultiLineAssignment.EqualSignLine
class TrustProxies extends Middleware
{

    /**
     * The trusted proxies for this application.
     *
     * @var array<string>|string|null
     */
    protected $proxies;

    /**
     * @var int
     */
    protected $headers = Request::HEADER_X_FORWARDED_AWS_ELB;

}
