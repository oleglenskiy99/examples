<?php

declare(strict_types=1);

namespace App\Http\Middleware\Calls;

use App\Http\Middleware\BaseMiddleware;
use Closure;
use Illuminate\Http\Request;

class CallAddMinutesMiddleware extends BaseMiddleware
{

    public function handle(Request $request, Closure $next): Closure
    {
        if (!$request->id) {
            $this->undefinedValueError($request, 'id');
        }

        if (!$request->minutes) {
            $this->undefinedValueError($request, 'minutes');
        }

        if (!$request->source) {
            $this->undefinedValueError($request, 'source');
        }

        return $next($request);
    }

}
