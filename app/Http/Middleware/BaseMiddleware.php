<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Illuminate\Http\Request;

class BaseMiddleware
{

    public function undefinedValueError(Request $request, string $param): void
    {
        $params = [];

        foreach ($request->all() as $k => $v) {
            $params[] = ['key' => $k, 'value' => $v];
        }

        echo response()->json([
            'responseStatus' => 'error',
            'responseErrorCode' => 100,
            'responseData' => [
                'error_message' => sprintf(
                    'One of the parameters specified was missing or invalid: %s is undefined',
                    $param
                ),
                'request_params' => $params,
            ],
        ])->content();

        die;
    }

}
