<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\GeoIP;
use Closure;
use Illuminate\Http\Request;

class Location
{

    public const ATTRIBUTE = 'location';

    public function handle(Request $request, Closure $next): object
    {
        $location = GeoIP::detect();
        $request->attributes->set(self::ATTRIBUTE, [
            'country' => $location['countryName'],
            'region' => $location['regionName'],
            'city' => $location['cityName'],
            'ip' => $request->getClientIp(),
        ]);
        return $next($request);
    }

}
