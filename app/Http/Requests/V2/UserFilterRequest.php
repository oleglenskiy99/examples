<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class UserFilterRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'categoryIds'    => 'array',
            'categoryIds.*'  => 'int',
            'countryIds'    => 'array',
            'countryIds.*'  => 'int',
            'ratingsFrom' => 'int',
            'ratingsTo' => 'int',
            'isChatRequestAvailable' => 'boolean',
            'isAudioRequestAvailable' => 'boolean',
            'isVideoRequestAvailable' => 'boolean',
            'isImageRequestAvailable' => 'boolean',
            'priceFrom' => 'int',
            'priceTo' => 'int',
            'isVerification' => 'boolean',
            'searchText' => 'string',
            'countOfItems' => 'int',
            'page' => 'int',
            'eventId' => 'int',
        ];
    }

}
