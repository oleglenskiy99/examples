<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class AddPayoutRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'card' => 'required',
            'amount' => 'required',
        ];
    }

}
