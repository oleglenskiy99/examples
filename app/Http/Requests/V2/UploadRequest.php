<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class UploadRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'tag' => 'required|string',
            'file' => 'required|file',
        ];
    }

}
