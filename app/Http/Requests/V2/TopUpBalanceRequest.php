<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class TopUpBalanceRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'amount' => 'required|int|min:10',
        ];
    }

}
