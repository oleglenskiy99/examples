<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class CountryChangeRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'country_id' => 'required|int',
        ];
    }

}
