<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class SubscribeRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'userId' => 'int|isExistValidation',
            'isSubscribe' => 'required',
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'userId.isExistValidation' => 'The user by id not exist',
        ];
    }

}
