<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class UpdateUserRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'photo' => 'string|nullable',
            'firstName' => 'required|string',
            'lastName' => 'string',
            'nickname' => 'required|uniqueField|string',
            'description' => 'string',
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'nickname.uniqueField' => 'The nickname already exist',
        ];
    }

}
