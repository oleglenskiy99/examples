<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class FollowersGerRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'userId' => 'required|int',
            'count' => 'int',
            'page' => 'int',
        ];
    }

}
