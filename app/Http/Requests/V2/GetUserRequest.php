<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class GetUserRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'userId' => 'int',
            'nickname' => 'string|min:3|max:30',
        ];
    }

}
