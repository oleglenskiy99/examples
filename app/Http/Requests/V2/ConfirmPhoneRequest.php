<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class ConfirmPhoneRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'phone' => 'required',
            'i' => 'required:string',
            'c' => 'required:integer',
        ];
    }

}
