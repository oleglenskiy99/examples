<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class UserViewedRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'userId' => 'required|int|exists:module_users,id',
        ];
    }

}
