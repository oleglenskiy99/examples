<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class RegistrationUserRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|min:6|max:20',
            'nickname' => 'required|uniqueField|string|min:6|max:30',
            'email' => 'required|email|uniqueField',
            'password' => 'required|string|min:6|max:20',
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'nickname.uniqueField' => 'The nickname already exist',
            'email.uniqueField' => 'The email already exist',
        ];
    }

}
