<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

abstract class ApiRequest extends FormRequest
{

    public function failedValidation(Validator $validator): HttpResponseException
    {
        throw new HttpResponseException(new JsonResponse([
            'message'   => 'Validation errors',
            'data'      => $validator->errors(),
        ], JsonResponse::HTTP_BAD_REQUEST));
    }

}
