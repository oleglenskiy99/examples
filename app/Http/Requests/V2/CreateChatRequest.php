<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class CreateChatRequest extends ApiRequest
{

    public function rules(): array
    {
        return [
            'title' => 'required',
            'userId' => 'required|int',
        ];
    }

}
