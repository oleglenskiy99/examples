<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class MessagesHistoryRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'chatId' => 'required|int',
        ];
    }

}
