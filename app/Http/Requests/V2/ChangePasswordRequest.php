<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class ChangePasswordRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'new_password' => 'required|string|min:6|max:20',
        ];
    }

}
