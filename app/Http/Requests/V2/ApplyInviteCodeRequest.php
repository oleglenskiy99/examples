<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class ApplyInviteCodeRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'inviteCode' => 'required|string',
        ];
    }

}
