<?php

declare(strict_types=1);

namespace App\Http\Requests\V2;

class PasswordChangeRequest extends ApiRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'oldPassword' => 'required|string|min:6|max:40',
            'newPassword' => 'required|string|min:6|max:40',
        ];
    }

}
