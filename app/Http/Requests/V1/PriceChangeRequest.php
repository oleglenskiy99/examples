<?php

declare(strict_types=1);

namespace App\Http\Requests\V1;

use App\Http\Requests\V2\ApiRequest;
use Illuminate\Validation\Rule;

class PriceChangeRequest extends ApiRequest
{

    /**
     * @return array
     */
    public function rules(): array
	{
        return [
            'key' => [
                'required',
                Rule::in(['message', 'video' ]),
            ],
            'value' => [
                'required',
                'integer',
                'min:1',
            ],
        ];
    }

}
