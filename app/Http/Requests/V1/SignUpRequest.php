<?php

declare(strict_types=1);

namespace App\Http\Requests\V1;

use App\Http\Requests\V2\ApiRequest;

class SignUpRequest extends ApiRequest
{

    public function rules(): array
    {
        return [
            'email' => 'required|filled|email',
            'password' => 'required|filled',
            'utm' => 'string',
        ];
    }

}
