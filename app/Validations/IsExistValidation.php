<?php

declare(strict_types=1);

namespace App\Validations;

use App\Repository\V2\UserRepository;

class IsExistValidation
{

    private UserRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function validate(string $value): bool
    {
        return $this->repository->findById((int) $value) !== null;
    }

}
