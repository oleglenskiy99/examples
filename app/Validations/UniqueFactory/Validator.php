<?php

declare(strict_types=1);

namespace App\Validations\UniqueFactory;

interface Validator
{

    public function validate(string $value): bool;

}
