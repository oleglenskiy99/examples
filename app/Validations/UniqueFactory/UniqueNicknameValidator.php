<?php

declare(strict_types=1);

namespace App\Validations\UniqueFactory;

use App\Repository\V2\UserRepository;

class UniqueNicknameValidator implements Validator
{

    private UserRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function validate(string $value): bool
    {
        return $this->repository->findUserByNickname($value, false) === null;
    }

}
