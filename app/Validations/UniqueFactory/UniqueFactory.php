<?php

declare(strict_types=1);

namespace App\Validations\UniqueFactory;

use App\Enums\UniqueValidatorEnum;
use InvalidArgumentException;

class UniqueFactory
{

    private UniqueNicknameValidator $nicknameValidator;
    private UniqueEmailValidator $emailValidator;

    public function __construct(UniqueNicknameValidator $nicknameValidator, UniqueEmailValidator $emailValidator)
    {
        $this->nicknameValidator = $nicknameValidator;
        $this->emailValidator = $emailValidator;
    }

    public function createValidator(string $attribute): Validator
    {
        switch ($attribute) {
            case UniqueValidatorEnum::EMAIL:
                return $this->emailValidator;
            case UniqueValidatorEnum::NICKNAME:
                return $this->nicknameValidator;
            default:
                throw new InvalidArgumentException();
        }
    }

}
