<?php

declare(strict_types=1);

namespace App\Console;

use App\Console\Commands\PaymentsRefundMessages;
use App\Console\Commands\PaymentsRefundVideo;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    /**
     * @var array<string>
     */
    protected $commands = [
        PaymentsRefundMessages::class,
        PaymentsRefundVideo::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
		$schedule->command('inspire')->everyMinute();

		$schedule->command('payments:refund:video')->hourly();

		$schedule->command('payments:refund:messages')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

}
