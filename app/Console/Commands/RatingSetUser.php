<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Events\User\UpdateReviewsEvent;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class RatingSetUser extends Command
{

    /**
     * @var string
     */
    protected $signature = 'users:rating:set';

    /**
     * @var string
     */
    protected $description = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $query = User::has('reviews');
        $query->chunk(100, function (Collection $users): void {
            $users->map(function ($item) {
                UpdateReviewsEvent::dispatch($item->id);
                return $item->id;
            });
        });
    }

}
