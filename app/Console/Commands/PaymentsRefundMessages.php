<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Events\Chat\MessageDeleted;
use App\Models\Chats\Messages;
use App\Services\PaymentServiceInterface;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Throwable;

class PaymentsRefundMessages extends Command
{

    /**
     * @var string
     */
    protected $signature = 'payments:refund:messages';

    /**
     * @var string
     */
    protected $description = '';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param PaymentServiceInterface $payments - интерфейс сервиса платежных систем, подставляется из DI
     */
    public function handle(PaymentServiceInterface $payments): void
    {
        // Запрос на сообщения с истекшим сроком c зависимостью на чат и создателя
        // Создатель чата будет использован при отмене транзакции
        $query = Messages::with('chat.creator')
            ->where('timestamp', '<', (now()->subHours(env('AUTO_REFUND_TIME', 120))->timestamp))
            ->where('cost', '>', 0)
            ->where('is_paid', true)
            ->where('is_deleted', false)
            ->where('is_has_reply', false);
        // Результаты обрабатываются пакетно, по 100 штук, поступая в callback в коллекции
        $query->chunk(100, function (Collection $messages) use ($payments): void {
            foreach ($messages as $message) {
                try {
                    if ($message->charge_id && $message->chat->creator !== null) {
                        $payments->cancelPayment($message->chat->creator, $message->charge_id);
                    }
                } catch (Throwable $error) {
                    Log::error($error->getMessage());
                }
            }

            // методом toQuery можно получить готовый объект query с первичными ключами всех моделей
            $messages->toQuery()->update([
                'is_deleted' => true,
            ]);
            // Message - единственный параметр конструктора MessageDeleted
            // mapInto передавая каждую в конструктор события преобразует
            // коллекцию моделей в коллекцию событий
            $events = $messages->mapInto(MessageDeleted::class);
            // Методом map каждое событие передается в фасад event
            $events->map('event');
        });
    }

}
