<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ReferenceBook extends Command
{

    /**
     * @var string
     */
    protected $signature = 'reference:create {table?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create reference book';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    // phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint
    public function handle()
    {
        if ($this->argument('table')) {
            if ($this->argument('table') !== 'all') {
                $table = $this->argument('table');
                $class = '\App\Helpers\Reference\Reference' . ucfirst($table);
                $table = new $class();
                return $table::index();
            }

            $files = Storage::disk('helpers')->allFiles();

            foreach ($files as $file) {
                $class = '\App\Helpers\Reference\\' . explode('.', $file)[0];
                $table = new $class();
                $table::index();
            }
            echo 'Все модели обновлены' . PHP_EOL;
            return;
        }

        echo "Укажите имя справочника \n";
    }

}
