<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Events\Notify\CancelRequestVideo;
use App\Events\Video\VideoCancelled;
use App\Helpers\KeyMessages;
use App\Services\PaymentServiceInterface;
use App\User;
use App\Video;
use Emitter;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Throwable;

class PaymentsRefundVideo extends Command
{

    /**
     * @var string
     */
    protected $signature = 'payments:refund:video';

    /**
     * @var string
     */
    protected $description = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(PaymentServiceInterface $payments, Emitter $emitter): void
    {
        $query = Video::with('owner')
            ->where('created_at', '<', now()->subHours(env('AUTO_REFUND_TIME', 120)))
            ->where('is_deleted', false)
            ->where('is_completed', false);
        $query->chunk(100, function (Collection $videos) use ($payments, $emitter): void {
            foreach ($videos as $video) {
                try {
                    if ($video->charge_id && $video->owner !== null) {
                        $user = User::getUserById((int) $video->owner);
                        $payments->cancelPayment($user, $video->charge_id);
                    }
                    $emitter->to(KeyMessages::toChatCrypto($video->target_id))->emit('video:canceled', [
                        'id' => $video->id,
                    ]);
                    CancelRequestVideo::dispatch($video->target_id, $video->user_id);
                } catch (Throwable $exception) {
                    Log::error($exception->getMessage());
                }
            }
            $videos->toQuery()->update([
                'is_deleted' => true,
            ]);
            $events = $videos->mapInto(VideoCancelled::class);
            $events->map('event');
        });
    }

}
