<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Query\Builder;

class Roles extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_roles';

    /**
     * @var array<string>
     */
    protected $fillable = [
        'name',
        'description',
    ];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'module_users_roles', 'role_id', 'user_id');
    }

    public static function builder(): Builder
    {
        return self::select('id', 'name', 'description');
    }

    // phpcs:disable SlevomatCodingStandard.Classes.ClassStructure.IncorrectGroupOrder
    public static function getRoleByName(string $name): ?Roles
	{
        return static::where('name', '=', $name)->first();
    }

}
