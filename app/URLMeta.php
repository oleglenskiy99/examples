<?php

declare(strict_types=1);

namespace App;

use DomDocument;
use DOMXPath;

class URLMeta
{

    private string $url;
    private object $response;

    /**
     * @var array
     */
    private array $standard;

    /**
     * @var array
     */
    private array $og;
    private DOMXPath $xpath;
    private string $errorCode;
    private string $errorResponse;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function parse(): ?object
    {
        $html = $this->crawl();
        if ($html === null) {
            return null;
        }

        $this->response = (object) [
            'title' => '',
            'description' => '',
            'keywords' => (object) [],
            'author' => (object) ['name' => '', 'href' => ''],
            'image' => '',
        ];

        $this->standard = $this->og = [];

        libxml_use_internal_errors(true);
        $doc = new DomDocument();
        /** @psalm-suppress InvalidArgument * */
        $doc->loadHTML($html);
        $this->xpath = new DOMXPath($doc);
        $query = '//*/meta';
        $metas = $this->xpath->query($query);
        if ($metas) {
            foreach ($metas as $meta) {
                /** @psalm-suppress UndefinedMethod * */
                $name = $meta->getAttribute('name');
                /** @psalm-suppress UndefinedMethod * */
                $property = $meta->getAttribute('property');
                /** @psalm-suppress UndefinedMethod * */
                $content = $meta->getAttribute('content');
                if (!empty($name)) {
                    $this->standard[$name] = $content;
                } elseif (!empty($property)) {
                    // can be more than one article:tag
                    if ($property === 'article:tag') {
                        if (isset($this->og['article:tag'])) {
                            $this->og['article:tag'][] = $content;
                        } else {
                            $this->og['article:tag'] = [$content];
                        }
                    }
                    $this->og[$property] = $content;
                }
            }

            $this->getTitle();
            $this->getDescription();
            $this->getKeywords();
            $this->getAuthor();
            $this->getImage();
        } else {
            $this->getTitle();
        }

        return $this->response;
    }

    /**
     * @return bool|string|null
     */
    private function crawl()
    {
        $customFailStrings = [];
        $otherCurlOptions = [];
        for ($i = 0; $i < 3; $i++) {
			$curl_handle = curl_init();
			curl_setopt($curl_handle, CURLOPT_USERAGENT, 'https://github.com/giltotherescue/php-url-meta');
			curl_setopt($curl_handle, CURLOPT_URL, $this->url);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 3);
			curl_setopt($curl_handle, CURLOPT_TIMEOUT, 10);
			curl_setopt($curl_handle, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, 0);

            /** @psalm-suppress InvalidIterator * */
			if (count($otherCurlOptions) > 0) {
				foreach ($otherCurlOptions as $name => $value) {
					curl_setopt($curl_handle, $name, $value);
				}
			}

			$buffer = curl_exec($curl_handle);
			$curlinfo = curl_getinfo($curl_handle);
			curl_close($curl_handle);

			$custom_fail = false;
            /** @psalm-suppress InvalidIterator * */
			if (count($customFailStrings) > 0) {
				foreach ($customFailStrings as $custom_fail_string) {
					if (stristr($buffer, $custom_fail_string)) {
						$custom_fail = true;
						break;
					}
				}
			}

			if (($curlinfo['http_code'] < 400) && ($curlinfo['http_code'] !== 0) && (!$custom_fail)) {
				return $buffer;
			}

			if ($i === 3 - 1) {
				$this->errorCode = $curlinfo['http_code'];
				$this->errorResponse = $buffer;
				return null;
			}
		}

		return null;
    }

    private function getTitle(): void
    {
        if (isset($this->og['og:title'])) {
            $this->response->title = $this->og['og:title'];
        } else {
            $query = '//*/title';
            $titles = $this->xpath->query($query);
            if ($titles) {
                foreach ($titles as $title) {
                    $this->response->title = $title->nodeValue;
                    break;
                }
            }
        }
    }

    private function getDescription(): void
    {
        if (isset($this->og['og:description'])) {
            $this->response->description = $this->og['og:description'];
        } elseif (isset($this->standard['description'])) {
            $this->response->description = $this->standard['description'];
        }
    }

    private function getKeywords(): void
    {
        if (isset($this->standard['keywords'])) {
            $keywords = explode(',', $this->standard['keywords']);
            foreach ($keywords as $k => $v) {
                $keywords[$k] = trim($v);
            }
            $this->response->keywords = (object) $keywords;
        } elseif (isset($this->og['article:tag'])) {
            $this->response->keywords = (object) $this->og['article:tag'];
        }
    }


    // phpcs:disable SlevomatCodingStandard.Functions.DisallowEmptyFunction.EmptyFunction
    // phpcs:disable Squiz.WhiteSpace.ScopeClosingBrace.ContentBefore
    private function getAuthor(): void
    {}

    private function getImage(): void
    {
        if (isset($this->og['og:image'])) {
            $this->response->image = $this->og['og:image'];
        }
    }

}
