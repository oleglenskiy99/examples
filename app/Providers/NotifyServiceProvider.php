<?php

declare(strict_types=1);

namespace App\Providers;

use App\Events\ConfirmEmailEvent;
use App\Events\ResetPassword;
use App\Events\ResetPasswordProfile;
use App\Listeners\ConfirmEmailListener;
use App\Listeners\EventsToEmailListener;
use App\Listeners\EventsToSMSListener;
use App\Listeners\FallbackPayoutMadeListener;
use App\Listeners\ResetPassToEmailListener;
use App\Listeners\ResetPasswordProfileListener;
// TODO
//use App\Listeners\ToUserNotifyListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class NotifyServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $listen = [
	// TODO
	//        UserNotify::class => [
	//            ToUserNotifyListener::class,
	//        ],

        ResetPassword::class => [
            ResetPassToEmailListener::class,
        ],

        ConfirmEmailEvent::class => [
            ConfirmEmailListener::class,
        ],

        ResetPasswordProfile::class => [
            ResetPasswordProfileListener::class,
        ],

    ];

    /**
     * @var array
     */
    protected $subscribe = [
        EventsToEmailListener::class,
        EventsToSMSListener::class,
        FallbackPayoutMadeListener::class,
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot(): void
    {
        parent::boot();
    }

}
