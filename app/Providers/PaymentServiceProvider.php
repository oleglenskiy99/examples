<?php

declare(strict_types=1);

namespace App\Providers;

use App\Listeners\EmitterEventSubscriber;
use App\Repository\V2\FinancesRepository;
use App\Services\Payment\FallbackPaymentService;
use App\Services\Payment\FallbackPayoutService;
use App\Services\Payment\StripePaymentService;
use App\Services\Payment\SystemPaymentService;
use App\Services\PaymentServiceInterface;
use App\Services\PayoutServiceInterface;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Stripe\StripeClient;
use Throwable;

class PaymentServiceProvider extends ServiceProvider
{

    /**
     * Register payment service.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(StripeClient::class, function () {
            return new StripeClient(config('services.stripe'));
        });
        $this->app->singleton(EmitterEventSubscriber::class, function () {
            return new EmitterEventSubscriber();
        });
        $this->app->singleton(StripePaymentService::class, function ($app) {
            return new StripePaymentService(
                $app->make(StripeClient::class),
                config('services.payment.payout_fee'),
                $app->make(FinancesRepository::class)
            );
        });
        $this->app->singleton(FallbackPayoutService::class, function () {
            return new FallbackPayoutService(config('services.payment.payout_fee'));
        });
        $this->app->singleton(PayoutServiceInterface::class, StripePaymentService::class);
        $this->app->singleton(PaymentServiceInterface::class, function ($app) {
            return new FallbackPaymentService(
                $app->make(SystemPaymentService::class),
                $app->make(StripePaymentService::class)
            );
        });
    }

    /**
     * Bootstrap payment service.
     *
     * @return void
     */
    public function boot(): void
    {
        try {
            Event::subscribe($this->app->make(EmitterEventSubscriber::class));
        } catch (Throwable $exception) {
            Log::error($exception);
        }
    }

}
