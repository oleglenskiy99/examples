<?php

declare(strict_types=1);

namespace App\Providers;

use App\Http\Middleware\Crm;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{

    /**
     * The path to the "home" route for your application.
     */
    public const HOME = '/home';

    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    public function boot(): void
    {
        if (env('APP_ENV') === 'production') {
            resolve(UrlGenerator::class)->forceScheme('http');
        }

        parent::boot();
    }

    public function map(): void
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapCrmRoutes();
        $this->mapApiV2Routes();
        $this->mapAuthRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes(): void
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    private function mapApiRoutes(): void
    {
        Route::middleware(SubstituteBindings::class)
            ->prefix('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    private function mapCrmRoutes(): void
    {
        Route::middleware(Crm::class)
            ->prefix('crm')
            ->namespace($this->namespace)
            ->group(base_path('routes/crm.php'));
    }

    private function mapApiV2Routes(): void
    {
        Route::middleware(SubstituteBindings::class)
            ->prefix('api/v2')
            ->namespace($this->namespace)
            ->group(base_path('routes/apiV2.php'));
    }

    private function mapAuthRoutes(): void
    {
        Route::middleware(SubstituteBindings::class)
            ->prefix('api/v2')
            ->namespace($this->namespace)
            ->group(base_path('routes/authV2.php'));
    }

}
