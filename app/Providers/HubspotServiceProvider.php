<?php

declare(strict_types=1);

namespace App\Providers;

use App\Events\User\RegisterUserEvent;
use App\Events\User\UpdateUserEvent;
use App\Listeners\HubspotContactListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class HubspotServiceProvider extends ServiceProvider
{

    /**
     * @var string[][]
     */
    protected $listen = [

        RegisterUserEvent::class => [
            HubspotContactListener::class,
        ],

        UpdateUserEvent::class => [
            HubspotContactListener::class,
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot(): void
    {
        parent::boot();
    }

}
