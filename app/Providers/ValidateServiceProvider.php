<?php

declare(strict_types=1);

namespace App\Providers;

use App\Validations\IsExistValidation;
use App\Validations\UniqueFactory\UniqueFactory;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Factory;

class ValidateServiceProvider extends ServiceProvider
{

    public function boot(): void
    {
        /** @var Factory  $validator */
        $validator = $this->app->get('validator');

        $validator->extend(
            'uniqueField',
            function (string $attribute, string $value) {
				/** @var UniqueFactory $uniqueFactory */
                $uniqueFactory = $this->app->get(UniqueFactory::class);
                $validator = $uniqueFactory->createValidator($attribute);
				return $validator->validate($value);
			},
            'The field already exist'
        );

        // phpcs:disable SlevomatCodingStandard.Functions.UnusedParameter.UnusedParameter
        $validator->extend(
            'isExistValidation',
            function (string $attribute, string $value) {
                /** @var IsExistValidation $isExistValidation */
                $isExistValidation = $this->app->get(IsExistValidation::class);
                return $isExistValidation->validate($value);
            },
            'The field already exist'
        );
    }

}
