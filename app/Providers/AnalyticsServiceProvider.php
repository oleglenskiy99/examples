<?php

declare(strict_types=1);

namespace App\Providers;

use App\Listeners\AnalyticsEventSubscriber;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

class AnalyticsServiceProvider extends ServiceProvider
{

    public function register(): void
    {
        $this->app->singleton(AnalyticsEventSubscriber::class, function () {
            return new AnalyticsEventSubscriber(config('services.analytics'));
        });
    }

    public function boot(): void
    {
        Event::subscribe($this->app->make(AnalyticsEventSubscriber::class));
    }

}
