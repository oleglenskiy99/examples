<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class SerializerServiceProvider extends ServiceProvider
{

    public function register(): void
    {
        $this->app->singleton(SerializerInterface::class, function () {
            $reflectionExtractor = new ReflectionExtractor();
            $phpDocExtractor = new PhpDocExtractor();
            $propertyTypeExtractor = new PropertyInfoExtractor(
                [$reflectionExtractor],
                [$phpDocExtractor, $reflectionExtractor],
                [$phpDocExtractor],
                [$reflectionExtractor],
                [$reflectionExtractor]
            );

            $encoders = [new JsonEncoder()];
            $normalizers = [
                new ArrayDenormalizer(),
                new ObjectNormalizer(
                    null,
                    null,
                    null,
                    $propertyTypeExtractor
                ),
            ];

            return new Serializer($normalizers, $encoders);
        });

        $this->app->singleton(DenormalizerInterface::class, function () {
            $encoders = [new JsonEncoder()];
            $normalizers = [
                new ArrayDenormalizer(),
                new GetSetMethodNormalizer(),
                new PropertyNormalizer(),
                new ObjectNormalizer(
                    null,
                    null,
                    new PropertyAccessor(),
                    new ReflectionExtractor()
                ),
            ];

            return new Serializer($normalizers, $encoders);
        });
    }

}
