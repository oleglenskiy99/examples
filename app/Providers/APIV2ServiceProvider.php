<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Tymon\JWTAuth\JWTGuard;

class APIV2ServiceProvider extends ServiceProvider
{

    public function register(): void
    {
        $this->app->singleton(JWTGuard::class, function (Application $app) {
            return new JWTGuard(
                $app->get('tymon.jwt'),
                $app->get('auth')->createUserProvider('users'),
                $app->get('request')
            );
        });
    }

}
