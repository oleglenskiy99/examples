<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Tymon\JWTAuth\Providers\LumenServiceProvider;

class JwtServiceProvider extends ServiceProvider
{

    public function register(): void
    {
        $this->app->singleton(AuthServiceProvider::class);
        $this->app->singleton(LumenServiceProvider::class);
    }

}
