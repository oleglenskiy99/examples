<?php

declare(strict_types=1);

namespace App\Providers;

use App\Events\User\UpdateReviewsEvent;
use App\Events\User\UpdateUserEvent;
use App\Listeners\UpdateReviewsListener;
use App\Listeners\UserUpdateListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class UserUpdateServiceProvider extends ServiceProvider
{

    /**
     * @var string[][]
     */
    protected $listen = [
        UpdateUserEvent::class => [
            UserUpdateListener::class,
        ],
        UpdateReviewsEvent::class => [
            UpdateReviewsListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot(): void
    {
        parent::boot();
    }

}
