<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use SocialiteProviders\Facebook\FacebookExtendSocialite;
use SocialiteProviders\Google\GoogleExtendSocialite;
use SocialiteProviders\Instagram\InstagramExtendSocialite;
use SocialiteProviders\InstagramBasic\InstagramBasicExtendSocialite;
use SocialiteProviders\LinkedIn\LinkedInExtendSocialite;
use SocialiteProviders\Manager\SocialiteWasCalled;
use SocialiteProviders\TikTok\TikTokExtendSocialite;
use SocialiteProviders\Twitch\TwitchExtendSocialite;
use SocialiteProviders\VKontakte\VKontakteExtendSocialite;

class EventServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $listen = [
        SocialiteWasCalled::class => [
            FacebookExtendSocialite::class . '@handle',
            GoogleExtendSocialite::class . '@handle',
            TwitchExtendSocialite::class . '@handle',
            InstagramExtendSocialite::class . '@handle',
            InstagramBasicExtendSocialite::class . '@handle',
            TikTokExtendSocialite::class . '@handle',
            VKontakteExtendSocialite::class . '@handle',
            LinkedInExtendSocialite::class . '@handle',
        ],
    ];

    public function boot(): void
    {
        parent::boot();
    }

}
