<?php

declare(strict_types=1);

namespace App\Providers;

use App\Services\Mobilipay\MobilipayAPI;
use App\Services\SendSMSServiceInterface;
use Illuminate\Support\ServiceProvider;

class SMSServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(SendSMSServiceInterface::class, MobilipayAPI::class);
    }

}
