<?php

declare(strict_types=1);

namespace App\Helpers\Chats;

class AttachmentsHelper
{

    /**
     * @param array $items
     * @return array
     */
    public static function formatVoices(array $items): array
    {
        $voices = [];
        foreach ($items as $item) {
            $voice = $item['message'];
            $voice['audio'] = $item['media'];
            $voice['date'] = date('F Y', $voice['timestamp']);
            $voice['time'] = date('H:i', $voice['timestamp']);
            $voice['dialog_id'] = $item['dialog_id'];

            if ($item['chat']['is_group']) {
                $voice['chat'] = [
                    'is_group' => true,
                    'title' => $item['chat']['title'],
                ];
            } else {
                $voice['chat'] = $item['message']['user']['id'] === session('user_id') ? [
                    'title' => $item['chat']['user']['firstName'] . ' ' . $item['chat']['user']['lastName'],
                ] : [
                    'title' => $item['message']['user']['firstName'] . ' ' . $item['message']['user']['lastName'],
                ];
            }

            $voices[] = $voice;
        }
        return $voices;
    }

    /**
     * @param array $items
     * @return array
     */
    public static function formatFiles(array $items): array
    {
        $files = [];
        foreach ($items as $item) {

            $file = $item['message'];

            $file['file'] = $item['media'];
            $file['date'] = date('F Y', $file['timestamp']);
            $file['time'] = date('H:i', $file['timestamp']);
            $file['dialog_id'] = $item['dialog_id'];

            if ($item['chat']['is_group']) {
                $file['chat'] = [
                    'is_group' => true,
                    'title' => $item['chat']['title'],
                ];
            } else {

                $file['chat'] = $item['message']['user']['id'] === session('user_id') ? [
                    'title' => $item['chat']['user']['firstName'] . ' ' . $item['chat']['user']['lastName'],
                ] : [
                    'title' => $item['message']['user']['firstName'] . ' ' . $item['message']['user']['lastName'],
                ];

            }

            $files[] = $file;

        }
        return $files;
    }

    /**
     * @param array $items
     * @return array
     */
    public static function formatLinks(array $items): array
    {
        $links = [];
        foreach ($items as $item) {
            $link = $item['message'];
            $link['link'] = json_decode($item['data'], true);
            $link['date'] = date('F Y', $link['timestamp']);
            $link['time'] = date('H:i', $link['timestamp']);

            if ($item['chat']['is_group']) {
                $link['chat'] = [
                    'is_group' => true,
                    'title' => $item['chat']['title'],
                ];
            } else {
                $link['chat'] = $item['message']['user']['id'] === session('user_id') ? [
                    'title' => $item['chat']['user']['firstName'] . ' ' . $item['chat']['user']['lastName'],
                ] : [
                    'title' => $item['message']['user']['firstName'] . ' ' . $item['message']['user']['lastName'],
                ];
            }

            $links[] = $link;
        }
        return $links;
    }

    /**
     * @param array $items
     * @return array
     */
    public static function format(array $items, $withChat = false): array
    {
        $attachments = [];

        foreach ($items as $item) {

            $attachment = $item['media'];
            $attachment['media_id'] = $item['media']['id'];
            $attachment['message_id'] = $item['message_id'];
            $attachment['dialog_id'] = $item['dialog_id'];
            $attachment['text'] = mb_convert_encoding($item['message']['text'], 'UTF-8', 'UTF-8');
            $attachment['timestamp'] = $item['message']['timestamp'];

            if ($withChat) {

                $chat = [
                    'id' => $item['chat']['id'],
                    'creator_id' => $item['chat']['creator_id'],
                ];

                $chat['peer'] = $item['chat']['is_group'] ? [
                    'id' => $item['chat']['id'],
                    'name' => $item['chat']['title'],
                    'image' => $item['chat']['image'],
                    'type' => 'group',
                    'is_online' => 0,
                    'is_owner' => session('user_id') === $item['chat']['creator_id'],
                ] : [
                    'id' => $item['chat']['user']['user_id'],
                    'type' => 'user',
                    'name' => $item['chat']['user']['firstName'] . ' ' . $item['chat']['user']['lastName'],
                    'image' => $item['chat']['user']['photo'],
                    'is_online' => $item['chat']['user']['is_online'],
                ];
                $attachment['source'] = $item['chat'];
                $attachment['chat'] = $chat;

            } else {
                $attachment['user'] = $item['message']['user'];
            }

            $attachment['date'] = date('m Y', $item['message']['timestamp']);
            $attachment['time'] = date('H:i', $item['message']['timestamp']);
            $attachment['other'] = $item['message']['attachments'];

            $attachments[] = $attachment;

        }

        return $attachments;
    }

}
