<?php

declare(strict_types=1);

namespace App\Helpers;

class SocialUserData
{

    private string $id;
    private int $networkId;
    private ?string $email;
    private string $username;
    private ?string $firstName;
    private ?string $lastName;
    private ?string $photo;
    private string $union_id;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): SocialUserData
    {
        $this->id = $id;
        return $this;
    }

    public function getNetworkId(): int
    {
        return $this->networkId;
    }

    public function setNetworkId(int $networkId): SocialUserData
    {
        $this->networkId = $networkId;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): SocialUserData
    {
        $this->email = $email;
        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): SocialUserData
    {
        $this->username = $username;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): SocialUserData
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): SocialUserData
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): SocialUserData
    {
        $this->photo = $photo;
        return $this;
    }

    public function getUnionId(): string
    {
        return $this->union_id;
    }

    public function setUnionId(string $union_id): SocialUserData
    {
        $this->union_id = $union_id;
        return $this;
    }

}
