<?php

declare(strict_types=1);

namespace App\Helpers;

use Illuminate\Support\Carbon;

class CacheKeyMessages
{

    public const CACHE_TIME_MINUTES = 30;
    public const CACHE_CHAT_TTL = 1800;

    public static function chatKey(string $key): string
    {
        return sha1(sprintf('chat:%s', $key));
    }

    public static function chatKeyPeer(string $key): string
    {
        return sha1(sprintf('chat:%s:peer', $key));
    }

    public static function cacheKey(string $key): string
    {
        return sha1($key);
    }

    public static function getCacheTime(): Carbon
    {
        return Carbon::now()->addMinutes(self::CACHE_TIME_MINUTES);
    }

}
