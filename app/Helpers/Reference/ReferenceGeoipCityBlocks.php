<?php

declare(strict_types=1);

namespace App\Helpers\Reference;

use App\Models\GeoIp\CityBlocks;

/**
 * php artisan reference:create geoipCityBlocks
 */
class ReferenceGeoipCityBlocks
{

    public static function index(): void
    {
        $datas = self::getData();

        foreach (array_chunk($datas, 5000) as $data) {
            CityBlocks::upsert($data, ['id'], [
                'city_id',
                'min_ip_address',
                'max_ip_address',
            ]);
        }

        echo 'Модель module_geoip_city_blocks обновлена ' . PHP_EOL;
    }

    /**
     * @return array
     */
    protected static function getData(): array
    {
        $file = file('geoip_city_blocks.txt', FILE_IGNORE_NEW_LINES);
        $data = [];
        foreach ($file as $k => $row) {
            $data[$k] = json_decode($row, true);
        }

        return $data;
    }

}
