<?php

declare(strict_types=1);

namespace App\Helpers\Reference;

use App\Models\NetworksUri;

/**
 * php artisan reference:create networksUri
 */
class ReferenceNetworksUri
{

    public static function index(): void
    {
        NetworksUri::upsert(self::getData(), ['nid'], [
            'uri',
        ]);

        echo 'Модель module_networks_uri обновлена ' . PHP_EOL;
    }

    /**
     * @return array
     */
    protected static function getData(): array
    {
        return [
            ['nid' => 1, 'uri' => 'https://oauth.vk.com/authorize?client_id=6763421&display=mobile&redirect_uri=https://{host}/api/methods/cbvk/&response_type=code&v=5.53&state='],
            ['nid' => 2, 'uri' => 'https://www.facebook.com/v8.0/dialog/oauth?client_id=485966015538780&redirect_uri=https://{host}/api/methods/cbfb/&state='],
            ['nid' => 3, 'uri' => 'https://api.instagram.com/oauth/authorize/?client_id=1244579435879568&scope=user_profile&redirect_uri=https://{host}/api/methods/cbig/&response_type=code&state='],
            ['nid' => 4, 'uri' => 'https://twidy.ru/0auth/external/twitter/?sign='],
            ['nid' => 5, 'uri' => 'https://twidy.ru/0auth/external/youtube/?sign='],
            ['nid' => 6, 'uri' => 'https://www.linkedin.com/oauth/v2/authorization/?client_id=86aplao9kbxh4b&response_type=code&redirect_uri=https://twidy.ru/0auth/callback/linkedin/&state='],
            [
                'nid' => 8,
                'uri' => 'https://id.twitch.tv/oauth2/authorize?client_id=lfns34c4g69vh0imuy56ua8dsdkbgr&redirect_uri=https://{host}/api/methods/cbtw/&response_type=code&scope=user_read&state=',
            ],
        ];
    }

}
