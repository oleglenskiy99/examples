<?php

declare(strict_types=1);

namespace App\Helpers\Reference;

use App\Categories;

/**
 * php artisan reference:create categories
 */
class ReferenceCategories
{

    public static function index(): void
    {
        Categories::upsert(self::getData(), ['id'], ['name_ru', 'name_en', 'default']);

        echo 'Модель module_categories обновлена ' . PHP_EOL;
    }

    /** @return array */
    protected static function getData(): array
    {
        return [
            ['id' => 1, 'name_ru' => 'Музыканты', 'name_en' => 'Music', 'default' => '1'],
            ['id' => 2, 'name_ru' => 'Блогеры', 'name_en' => 'Influencer', 'default' => '1'],
            ['id' => 3, 'name_ru' => 'Актеры', 'name_en' => 'Actors', 'default' => '1'],
            ['id' => 4, 'name_ru' => 'Специалисты', 'name_en' => 'Expert', 'default' => '1'],
            ['id' => 5, 'name_ru' => 'Модели', 'name_en' => 'Model', 'default' => '1'],
            ['id' => 6, 'name_ru' => 'Бизнесмены', 'name_en' => 'Business', 'default' => '1'],
            ['id' => 7, 'name_ru' => 'Финансы', 'name_en' => 'Finance', 'default' => '1'],
            ['id' => 8, 'name_ru' => 'Спорт', 'name_en' => 'Sports', 'default' => '1'],
            ['id' => 9, 'name_ru' => 'Изобразительное искусство', 'name_en' => 'Art', 'default' => '0'],
            ['id' => 10, 'name_ru' => 'Писатель', 'name_en' => 'Writer', 'default' => '1'],
            ['id' => 11, 'name_ru' => 'Красота и здоровье', 'name_en' => 'Beauty and Health', 'default' => '0'],
            ['id' => 12, 'name_ru' => 'Журнализм', 'name_en' => 'Journalism', 'default' => '0'],
            ['id' => 13, 'name_ru' => 'Реклама и маркетинг', 'name_en' => 'Advertising/Marketing', 'default' => '0'],
            ['id' => 14, 'name_ru' => 'Развлечение', 'name_en' => 'Entertainment', 'default' => '0'],
            ['id' => 15, 'name_ru' => 'Образование', 'name_en' => 'Education', 'default' => '0'],
            ['id' => 16, 'name_ru' => 'Юристы', 'name_en' => 'Legal', 'default' => '0'],
            ['id' => 17, 'name_ru' => 'СМИ', 'name_en' => 'Media and News', 'default' => '0'],
            ['id' => 18, 'name_ru' => 'Медицина и здоровье', 'name_en' => 'Medical and Health', 'default' => '0'],
            ['id' => 19, 'name_ru' => 'Недвижимость', 'name_en' => 'Real Estate', 'default' => '0'],
            ['id' => 20, 'name_ru' => 'Наука и технология', 'name_en' => 'Science and Technology ', 'default' => '0'],
            ['id' => 21, 'name_ru' => 'Путешествия', 'name_en' => 'Travel ', 'default' => '0'],
            ['id' => 22, 'name_ru' => 'шоу', 'name_en' => 'Show ', 'default' => '0'],
            ['id' => 23, 'name_ru' => 'ТВ и фильмы', 'name_en' => 'TV and Movies', 'default' => '0'],
            ['id' => 24, 'name_ru' => 'Бренд', 'name_en' => 'Brand', 'default' => '0'],
            ['id' => 25, 'name_ru' => 'Просто для развлечения', 'name_en' => 'Just for Fun', 'default' => '0'],
            ['id' => 26, 'name_ru' => 'Общественный деятель', 'name_en' => 'Public Figure', 'default' => '0'],
            ['id' => 27, 'name_ru' => 'Юмор и комедия', 'name_en' => 'Humor and Comedy', 'default' => '0'],
            ['id' => 28, 'name_ru' => 'Игры', 'name_en' => 'Games', 'default' => '0'],
            ['id' => 29, 'name_ru' => 'Бутик-магазин', 'name_en' => 'Boutique Store', 'default' => '0'],
            ['id' => 30, 'name_ru' => 'Одежда и стиль', 'name_en' => 'Clothing and Style', 'default' => '0'],
            ['id' => 31, 'name_ru' => 'Спикер', 'name_en' => 'Speaker', 'default' => '0'],
            ['id' => 32, 'name_ru' => 'Коучинг', 'name_en' => 'Coaching', 'default' => '0'],
            ['id' => 33, 'name_ru' => 'Фитнес', 'name_en' => 'Fitness', 'default' => '0'],
            ['id' => 34, 'name_ru' => 'Еда', 'name_en' => 'Food and Gastronomy', 'default' => '0'],
            ['id' => 35, 'name_ru' => 'Образ жизни', 'name_en' => 'Lifestyle', 'default' => '1'],
            ['id' => 36, 'name_ru' => 'Личная гигиена', 'name_en' => 'Personal Care', 'default' => '1'],
        ];
    }

}
