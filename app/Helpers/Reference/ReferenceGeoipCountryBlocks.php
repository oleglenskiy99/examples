<?php

declare(strict_types=1);

namespace App\Helpers\Reference;

use App\Models\GeoIp\CountryBlocks;

/**
 * php artisan reference:create geoipCountryBlocks
 */
class ReferenceGeoipCountryBlocks
{

    public static function index(): void
    {
        $datas = self::getData();

        foreach (array_chunk($datas, 5000) as $data) {
            CountryBlocks::upsert($data, ['id'], [
                'country_id',
                'min_ip_address',
                'max_ip_address',
            ]);
        }

        echo 'Модель module_geoip_country_blocks обновлена ' . PHP_EOL;
    }

    /**
     * @return array
     */
    protected static function getData(): array
    {
        $file = file('geoip_country_blocks.txt', FILE_IGNORE_NEW_LINES);
        $data = [];
        foreach ($file as $k => $row) {
            $data[$k] = json_decode($row, true);
        }

        return $data;
    }

}
