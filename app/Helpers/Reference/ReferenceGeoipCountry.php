<?php

declare(strict_types=1);

namespace App\Helpers\Reference;

use App\Models\GeoIp\Country;

/**
 * php artisan reference:create geoipCountry
 */
class ReferenceGeoipCountry
{

    public static function index(): void
    {
        $datas = self::getData();

        foreach (array_chunk($datas, 5000) as $data) {
            Country::upsert($data, ['id'], [
                'code',
                'name_ru',
                'name_en',
                'name_de',
                'name_fr',
                'name_es',
            ]);
        }

        echo 'Модель module_geoip_country обновлена ' . PHP_EOL;
    }

    /**
     * @return array
     */
    protected static function getData(): array
    {
        $file = file('geoip_country.txt', FILE_IGNORE_NEW_LINES);
        $data = [];
        foreach ($file as $k => $row) {
            $data[$k] = json_decode($row, true);
        }

        return $data;
    }

}
