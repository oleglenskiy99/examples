<?php

declare(strict_types=1);

namespace App\Helpers\Reference;

/**
 * php artisan reference:create moduleChannels
 */
class ReferenceModuleChannels
{

    public static function index(): void
    {
        echo 'Модель module_channels обновлена ' . PHP_EOL;
    }

    /**
     * @return array
     */
    protected static function getData(): array
    {
        return [
            [
                'id' => '1',
                'title' => 'Горячие новости',
                'price' => '4',
                'description' => 'Горячие новости – самые актуальные новости России и мира. Фото и видео. Комментарии, мнения и оценки. Происшествия, события шоу-бизнеса, спорта и мира науки. Автор канала: @georgemark0v',
                'category' => '11',
                'apple_good_id' => 'twidy.item.13',
                'creator_id' => 1,
            ],
            [
                'id' => '2',
                'title' => 'Mash',
                'price' => '5',
                'description' => 'Сводка последних новостей в мире на сегодня. Что происходит в мире прямо сейчас. Аналитика мировых событий.',
                'category' => '13',
                'apple_good_id' => 'twidy.item.13',
                'creator_id' => 1,
            ],
            [
                'id' => '3',
                'title' => 'Твой разведчик',
                'price' => '6',
                'description' => 'Проект уже существует более  2-лет и много где зарекомендовал себя,как честный и прибыльный!',
                'category' => '13',
                'apple_good_id' => 'twidy.item.13',
                'creator_id' => 1,
            ],
            ['id' => '4', 'title' => 'HANDICAPGAME', 'price' => '7', 'description' => 'Только победоносные ставки на спорт', 'category' => '13', 'apple_good_id' => 'twidy.item.13', 'creator_id' => 1],
            [
                'id' => '5',
                'title' => 'Yaplakal.com',
                'price' => '8',
                'description' => 'Официальный канал сообщества ЯП (http://www.yaplakal.com). Лучшее с сайта, мемесы и видосы, жара и годнота. Не пропусти!',
                'category' => '11',
                'apple_good_id' => 'twidy.item.13',
                'creator_id' => 1,
            ],
            [
                'id' => '6',
                'title' => 'Улыбатор',
                'price' => '9',
                'description' => '😂 Улыбатор — это развлекательный канал с качественным юмором без пошлости. Смешные картинки, приколы, видеоролики и многое другое. Улыбни свой улыбатор, подпишись! :)',
                'category' => '11',
                'apple_good_id' => 'twidy.item.13',
                'creator_id' => 1,
            ],
            ['id' => '7', 'title' => 'Для всех', 'price' => '10', 'description' => 'Все обо всем', 'category' => '11', 'apple_good_id' => 'twidy.item.13', 'creator_id' => 1],
            [
                'id' => '8',
                'title' => 'Платный для всех',
                'price' => '11',
                'description' => 'Про транспорт Плаваем, летаем, ездим',
                'category' => '1',
                'apple_good_id' => 'twidy.item.13',
                'creator_id' => 1,
            ],
            ['id' => '9', 'title' => 'Приват канал', 'price' => '12', 'description' => 'Все про отдых и веселье', 'category' => '11', 'apple_good_id' => 'twidy.item.13', 'creator_id' => 1],
            ['id' => '10', 'title' => 'Бла', 'price' => '13', 'description' => 'Тестовый', 'category' => '4', 'apple_good_id' => 'twidy.item.13', 'creator_id' => 1],
            ['id' => '11', 'title' => 'FakeNews', 'price' => '14', 'description' => 'Описание канала андроид', 'category' => '4', 'apple_good_id' => 'twidy.item.13', 'creator_id' => 1],
            ['id' => '12', 'title' => 'Marvel', 'price' => '15', 'description' => 'Канал с черным чувством юмора', 'category' => '11', 'apple_good_id' => 'twidy.item.13', 'creator_id' => 1],
            ['id' => '13', 'title' => 'Новый канал', 'price' => '16', 'description' => 'Описание', 'category' => '1', 'apple_good_id' => 'twidy.item.13', 'creator_id' => 1],
            ['id' => '14', 'title' => 'Бесплатный канал', 'price' => '0', 'description' => 'Описание', 'category' => '4', 'apple_good_id' => '', 'creator_id' => 1],
            [
                'id' => '15',
                'title' => 'Не баг, а фича',
                'price' => '0',
                'description' => 'Секреты социальных сетей, уязвимости приложений, анонимность, хакерство, нейронные сети и личная безопасность в интернете.📱',
                'category' => '4',
                'apple_good_id' => '',
                'creator_id' => 1,
            ],
            [
                'id' => '16',
                'title' => 'Инстаграмотность',
                'price' => '0',
                'description' => 'Канал о том, как самостоятельно продвигаться в Instagram. Продвижение без привлечения сторонних специалистов. Получи клиентов уже завтра!',
                'category' => '4',
                'apple_good_id' => '',
                'creator_id' => 1,
            ],
            [
                'id' => '17',
                'title' => 'Computer Science and Programing',
                'price' => '0',
                'description' => 'Channel for who have a passion for Computer Vision, #Deep and #Machine #Learning, #Artificial #Intelligence and Python programming. Up-to-date books, articles or links provided based on #Python.',
                'category' => '4',
                'apple_good_id' => '',
                'creator_id' => 1,
            ],
            ['id' => '18', 'title' => 'Платный веб канал', 'price' => '42', 'description' => 'Описание платного веб канала', 'category' => '13', 'apple_good_id' => 'twidy.item.33', 'creator_id' => 1],
            ['id' => '19', 'title' => 'Тест', 'price' => '7', 'description' => 'Описание', 'category' => '2', 'apple_good_id' => 'twidy.item.6', 'creator_id' => 1],
        ];
    }

}
