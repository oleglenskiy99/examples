<?php

declare(strict_types=1);

namespace App\Helpers\Reference;

use App\Models\Localization;

/**
 * php artisan reference:create localizations
 */
class ReferenceLocalizations
{

    public static function index(): void
    {
        Localization::upsert(self::getData(), ['id'], ['lang', 'name']);

        echo 'Модель module_localizations обновлена ' . PHP_EOL;
    }

    /**
     * @return array
     */
    protected static function getData(): array
    {
        return [
            ['id' => '1', 'lang' => 'ru', 'name' => 'Русский'],
            ['id' => '2', 'lang' => 'en', 'name' => 'English'],
        ];
    }

}
