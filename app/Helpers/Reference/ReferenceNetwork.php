<?php

declare(strict_types=1);

namespace App\Helpers\Reference;

use App\Models\Network;

/**
 * php artisan reference:create network
 */
class ReferenceNetwork
{

    public static function index(): void
    {
        Network::upsert(self::getData(), ['id'], [
            'name',
            'icon',
            'color',
            'is_deleted',
            'provider',
        ]);

        echo 'Модель module_networks обновлена ' . PHP_EOL;
    }

    /**
     * @return array
     */
    protected static function getData(): array
    {
        return [
            ['id' => 1, 'name' => 'VK', 'icon' => 'bd-vk', 'color' => '#4c75a3', 'is_deleted' => 0, 'provider' => 'vk'],
            ['id' => 2, 'name' => 'Facebook', 'icon' => 'bd-facebook', 'color' => '#3b5998', 'is_deleted' => 0, 'provider' => 'facebook'],
            ['id' => 3, 'name' => 'Instagram', 'icon' => 'bd-instagram', 'color' => '#e4405f', 'is_deleted' => 0, 'provider' => 'instagram'],
            ['id' => 4, 'name' => 'Twitter', 'icon' => 'bd-twitter', 'color' => '#1da1f2', 'is_deleted' => 1, 'provider' => null],
            ['id' => 5, 'name' => 'YouTube', 'icon' => 'bd-youtube', 'color' => '#ff0000', 'is_deleted' => 1, 'provider' => null],
            ['id' => 6, 'name' => 'LinkedIn', 'icon' => 'bd-linkedin', 'color' => '#CC181E', 'is_deleted' => 1, 'provider' => null],
            ['id' => 8, 'name' => 'Twitch', 'icon' => 'bd-dribbble', 'color' => '#6441a5', 'is_deleted' => 0, 'provider' => 'twitch'],
            ['id' => 10, 'name' => 'Google', 'icon' => 'bd-google', 'color' => '#4285f4', 'is_deleted' => 0, 'provider' => 'google'],
            ['id' => 11, 'name' => 'TikTok', 'icon' => 'bd-tiktok', 'color' => '#000000', 'is_deleted' => 0, 'provider' => 'tiktok'],
        ];
    }

}
