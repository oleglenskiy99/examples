<?php

declare(strict_types=1);

namespace App\Helpers\Reference;

use App\Models\GeoIp\City;

/**
 * php artisan reference:create geoipCity
 */
class ReferenceGeoipCity
{

    public static function index(): void
    {
        $datas = self::getData();

        foreach (array_chunk($datas, 5000) as $data) {
            City::upsert($data, ['id'], [
                'region_name_ru',
                'city_name_es',
                'city_name_fr',
                'region_name_en',
                'region_name_de',
                'region_name_es',
                'region_name_fr',
                'city_name_ru',
                'city_name_en',
                'city_name_de',
            ]);
        }

        echo 'Модель module_geoip_city обновлена ' . PHP_EOL;
    }

    /**
     * @return array
     */
    protected static function getData(): array
    {
        $file = file('city_geo.txt');
        $data = [];
        foreach ($file as $k => $row) {
            $data[$k] = json_decode($row, true);
        }

        return $data;
    }

}
