<?php

declare(strict_types=1);

namespace App\Helpers;

class UserHelper
{

    public const CREATOR_ROLE_NAME = 'creator';
    public const ONBOARDING_REDIS_KEY = 'onboarding';
    public const PAGINATE_ALL = 50;
    public const PAGINATE_SCROLL = 15;

    public static function getDomainName(int $userId): string
    {
        $set_len = 6;
        $chars = 'abcdefghijklmnopqrstuvwxyz';
        for ($namedomain = (string) $userId; strlen($namedomain) <= $set_len;) {
			$namedomain = $chars[rand(
                0,
                strlen($chars) - 1
			)] . $namedomain;
        }
        return $namedomain;
    }

    public static function getDomainNameWithOutAt(string $name): string
    {
        return strtolower(str_replace('@', '', $name));
    }

}
