<?php

declare(strict_types=1);

namespace App\Helpers;

class SMSTexts
{

	public const VERIFIED_TEXT = 'smstexts.verified_text';
	public const NEW_MESSAGE = 'smstexts.new_message';
	public const NEW_REQUEST_VIDEO = 'smstexts.new_request_video';
	public const NEW_REQUEST = 'smstexts.new_request';
	public const NOT_APPROVED_REQUEST = 'smstexts.not_approved_request';
	public const CANCEL_REQUEST_VIDEO = 'smstexts.cancel_request_video';
	public const REPLY_REQUEST_VIDEO = 'smstexts.reply_request_video';

}
