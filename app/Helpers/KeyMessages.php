<?php

declare(strict_types=1);

namespace App\Helpers;

class KeyMessages
{

    public static function toChatDialog(int $key): string
    {
        return sprintf('chat:%s', $key);
    }

    public static function toChatCrypto(int $key): string
    {
        return sha1((string) $key);
    }

    public static function toChatCryptoInt(int $key): string
    {
        return sha1((string) $key);
    }

    public static function toEmitMessageRead(int $key): string
    {
        return sprintf('%s:message:read', $key);
    }

    public static function toEmitMessageView(int $key): string
    {
        return sprintf('%s:message:view', $key);
    }

    public static function toEmitMessageEdit(int $key): string
    {
        return sprintf('%s:message:edit', $key);
    }

    public static function toEmitMessageDelete(int $key): string
    {
        return sprintf('%s:message:delete', $key);
    }

    public static function toEmitMessageSent(int $key): string
    {
        return sprintf('%s:message:sent', $key);
    }

    public static function toEmitMessageResponse(int $key): string
    {
        return sprintf('%s:message:response', $key);
    }

}
