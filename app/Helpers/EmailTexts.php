<?php

declare(strict_types=1);

namespace App\Helpers;

class EmailTexts
{

	public const CONFIRM_EMAIL_SUBJECT = 'emailtexts.confirm_email_subject';
	public const NEW_MESSAGE_SUBJECT = 'emailtexts.new_message_subject';
	public const NEW_REQUEST_VIDEO_SUBJECT = 'emailtexts.new_request_video_subject';
	public const NEW_REQUEST_SUBJECT = 'emailtexts.new_request_subject';
	public const NOT_APPROVED_REQUEST_SUBJECT = 'emailtexts.not_approved_request_subject';
	public const REPLY_VIDEO_REQUEST_SUBJECT = 'emailtexts.reply_video_request_subject';
	public const CANCEL_VIDEO_REQUEST_SUBJECT = 'emailtexts.cancel_video_request_subject';
    public const FALLBACK_PAYOUT_MADE = 'emailtexts.fallback_payout_made';

}
