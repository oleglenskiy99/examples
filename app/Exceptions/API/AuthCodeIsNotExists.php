<?php

declare(strict_types=1);

namespace App\Exceptions\API;

use Exception;
use Illuminate\Http\JsonResponse;

final class AuthCodeIsNotExists extends Exception
{

    public function __construct()
    {
        parent::__construct(
            'This code is not exists',
            404
        );
    }

    public function render(): JsonResponse
    {
        return new JsonResponse([
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
            'errorCode' => 4,
        ], JsonResponse::HTTP_NOT_FOUND, [], 0);
    }

}
