<?php

declare(strict_types=1);

namespace App\Exceptions\API;

use Exception;

class SocialAccountExistsException extends  Exception
{

    public function __construct()
    {
        parent::__construct(
            __('errors.network_exist'),
            1
        );
    }

}
