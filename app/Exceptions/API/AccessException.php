<?php

declare(strict_types=1);

namespace App\Exceptions\API;

use Exception;
use Illuminate\Http\JsonResponse;

final class AccessException extends Exception
{

    public function __construct()
    {
        parent::__construct(
            'Access Exception',
            400
        );
    }

    public function render(): JsonResponse
    {
        return new JsonResponse([
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
        ], JsonResponse::HTTP_BAD_REQUEST, [], 0);
    }

}
