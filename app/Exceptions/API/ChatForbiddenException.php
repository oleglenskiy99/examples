<?php

declare(strict_types=1);

namespace App\Exceptions\API;

use Exception;
use Illuminate\Http\JsonResponse;

class ChatForbiddenException extends Exception
{

    public function __construct()
    {
        parent::__construct(
            'You are not allowed for this chat',
            403
        );
    }

    public function render(): JsonResponse
    {
        return new JsonResponse([
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
        ], JsonResponse::HTTP_FORBIDDEN, [], 0);
    }

}
