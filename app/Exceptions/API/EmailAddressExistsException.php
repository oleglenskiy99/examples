<?php

declare(strict_types=1);

namespace App\Exceptions\API;

use Exception;

class EmailAddressExistsException extends Exception
{

    public function __construct()
    {
        parent::__construct(
            __('errors.email_already_exists'),
            1
        );
    }

}
