<?php

declare(strict_types=1);

namespace App\Exceptions\API;

use Exception;
use Illuminate\Http\JsonResponse;

final class EmailNeedsVerification extends Exception
{

    public function __construct()
    {
        parent::__construct(
            'Email verification required',
            400
        );
    }

    public function render(): JsonResponse
    {
        return new JsonResponse([
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
            'errorCode' => 1,
        ], JsonResponse::HTTP_BAD_REQUEST, [], 0);
    }

}
