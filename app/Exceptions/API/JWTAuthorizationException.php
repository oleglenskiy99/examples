<?php

declare(strict_types=1);

namespace App\Exceptions\API;

use Exception;
use Illuminate\Http\JsonResponse;

final class JWTAuthorizationException extends Exception
{

    public function __construct()
    {
        parent::__construct(
            'Unauthorized',
            401
        );
    }

    public function render(): JsonResponse
    {
        return new JsonResponse([
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
        ], JsonResponse::HTTP_UNAUTHORIZED, [], 0);
    }

}
