<?php

declare(strict_types=1);

namespace App\Exceptions\API;

use Exception;
use Illuminate\Http\JsonResponse;

final class InviteCodeIsNotActive extends Exception
{

    public function __construct()
    {
        parent::__construct(
            'Invite is not active',
            404
        );
    }

    public function render(): JsonResponse
    {
        return new JsonResponse([
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
        ], JsonResponse::HTTP_NOT_FOUND, [], 0);
    }

}
