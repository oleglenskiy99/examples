<?php

declare(strict_types=1);

namespace App\Exceptions\API;

use Exception;
use Illuminate\Http\JsonResponse;

final class UserIsNotExists extends Exception
{

    public function __construct()
    {
        parent::__construct(
            'User is not exists',
            404
        );
    }

    public function render(): JsonResponse
    {
        return new JsonResponse([
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
        ], JsonResponse::HTTP_NOT_FOUND, [], 0);
    }

}
