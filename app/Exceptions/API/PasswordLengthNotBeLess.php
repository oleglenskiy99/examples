<?php

declare(strict_types=1);

namespace App\Exceptions\API;

use Exception;
use Illuminate\Http\JsonResponse;

final class PasswordLengthNotBeLess extends Exception
{

    public function __construct()
    {
        parent::__construct(
            'password must not be less than 6 characters',
            400
        );
    }

    public function render(): JsonResponse
    {
        return new JsonResponse([
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
            'errorCode' => 2,
        ], JsonResponse::HTTP_BAD_REQUEST, [], 0);
    }

}
