<?php

declare(strict_types=1);

namespace App;

use stdClass;

class Crypto
{

    public static function encrypt(string $value, string $passphrase): string
	{
        $salt = openssl_random_pseudo_bytes(8);
        $salted = '';
        $dx = '';

        while (strlen($salted) < 48) {
            $dx = md5($dx . $passphrase . $salt, true);
            $salted .= $dx;
        }

        $key = substr($salted, 0, 32);
        $iv = substr($salted, 32, 16);

        $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, 1, $iv);
        $data = ['ct' => base64_encode($encrypted_data), 'iv' => bin2hex($iv), 's' => bin2hex($salt)];

        return json_encode($data);
    }

    /**
     * @param string $jsonStr Json stringified value
     * @param string $passphrase Your password
     */
    public static function decrypt(string $jsonStr, string $passphrase): object
    {
        $json = json_decode($jsonStr, true);
        $salt = hex2bin($json['s']);
        $iv = hex2bin($json['iv']);
        $ct = base64_decode($json['ct']);
        $concatedPassphrase = $passphrase . $salt;
        $md5 = [];
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1] . $concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);
        $data = openssl_decrypt($ct, 'aes-256-cbc', $key, 1, $iv);
        $result = json_decode($data);
        if ($result === null) {
			return new stdClass();
        }
        return $result;
    }

}
