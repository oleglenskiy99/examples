<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class GeoIP extends Model
{

    /**
     * @return array<string>
     */
    public static function detect(): array
    {
        $ipAddr = getClientIP();

        if ($ipAddr) {
            $UserAuthRegion = self::getUserCity($ipAddr);
            $UserAuthCountry = self::getUserCountry($ipAddr);
        } else {
            $UserAuthRegion = '';
            $UserAuthCountry = '';
        }

        return [
            'ipAddress' => $ipAddr,
            'countryCode' => $UserAuthCountry ? $UserAuthCountry->code : '',
            'countryName' => $UserAuthCountry ? $UserAuthCountry->name : '',
            'regionName' => $UserAuthRegion ? $UserAuthRegion->region_name : '',
            'cityName' => $UserAuthRegion ? $UserAuthRegion->city_name : '',
        ];
    }

    // phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint
    private static function getUserCountry(string $ip)
    {
        $ip = ip2long($ip);
        $locale = App::getLocale();
        $locale = !empty($locale) ? $locale : 'en';
        return DB::selectOne(
            (string) DB::raw(
                sprintf(
                    'select module_geoip_country.id, code, name_%s as name from module_geoip_country
                                inner join module_geoip_country_blocks on module_geoip_country.id = module_geoip_country_blocks.country_id
                                where %s between min_ip_address and max_ip_address limit 1',
                    $locale,
                    $ip
                )
            )
        );
    }

    // phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint
    private static function getUserCity(string $ip)
    {
        $ip = ip2long($ip);
        $locale = App::getLocale();
        $locale = !empty($locale) ? $locale : 'en';
        return DB::selectOne(
            (string) DB::raw(
                sprintf(
                    'select module_geoip_city.id, region_name_%s as region_name, city_name_%s as city_name
                                from module_geoip_city
                                inner join module_geoip_city_blocks on module_geoip_city.id = module_geoip_city_blocks.city_id
                                where %s between min_ip_address and max_ip_address limit 1',
                    $locale,
                    $locale,
                    $ip
                )
            )
        );
    }

}
