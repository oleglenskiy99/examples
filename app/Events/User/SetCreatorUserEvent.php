<?php

declare(strict_types=1);

namespace App\Events\User;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SetCreatorUserEvent
{

    use Dispatchable;
    use SerializesModels;

    public int $userId;

    /**
     * @var array<string>
     */
    public array $raw;

    public function __construct(int $userId, array $raw)
    {
        $this->userId = $userId;
        $this->raw = $raw;
    }

}
