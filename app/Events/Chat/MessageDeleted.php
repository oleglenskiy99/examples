<?php

declare(strict_types=1);

namespace App\Events\Chat;

use App\Models\Chats\Messages;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MessageDeleted
{

    use Dispatchable;
    use SerializesModels;

    public Messages $message;

    public function __construct(Messages $message)
    {
        $this->message = $message;
    }

}
