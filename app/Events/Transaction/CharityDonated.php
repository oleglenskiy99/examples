<?php

declare(strict_types=1);

namespace App\Events\Transaction;

use App\Models\User\Founds;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CharityDonated
{

    use Dispatchable;
    use SerializesModels;

    /**
     * @var Founds
     */
    public $fond;

    /**
     * @var float
     */
    public $amount;

    public function __construct(Founds $fond, float $amount)
    {
        $this->fond = $fond;
        $this->amount = $amount;
    }

}
