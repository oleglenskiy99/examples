<?php

declare(strict_types=1);

namespace App\Events\Transaction;

use App\Models\User\Cards;
use App\Models\User\Charges;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BalanceRefilled
{

    use Dispatchable;
    use SerializesModels;

    public Cards $card;

    public Charges $charge;

    public float $amount;

    public int $userId;

    public function __construct(Cards $card, Charges $charge, float $amount, int $user_id)
    {
        $this->card = $card;
        $this->charge = $charge;
        $this->amount = $amount;
        $this->userId = $user_id;
    }

}
