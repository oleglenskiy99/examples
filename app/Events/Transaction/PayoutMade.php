<?php

declare(strict_types=1);

namespace App\Events\Transaction;

use App\Models\User\Cards;
use App\Models\User\Charges;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PayoutMade
{

    use Dispatchable;
    use SerializesModels;

    public Cards $card;

    public Charges $charge;

    public float $amount;

    public float $commission;

    public float $donation;

    public int $userId;

    public function __construct(Cards $card, Charges $charge, float $amount, float $commission, float $donation, int $user_id)
    {
        $this->card = $card;
        $this->charge = $charge;
        $this->amount = $amount;
        $this->commission = $commission;
        $this->donation = $donation;
        $this->userId = $user_id;
    }

}
