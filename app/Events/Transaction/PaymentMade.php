<?php

declare(strict_types=1);

namespace App\Events\Transaction;

use App\Models\User\Charges;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PaymentMade
{

    use Dispatchable;
    use SerializesModels;

    public Charges $charge;

    public float $amount;

    public int $userId;

    public function __construct(Charges $charge, float $amount, int $userId)
    {
        $this->charge = $charge;
        $this->amount = $amount;
        $this->userId = $userId;
    }

}
