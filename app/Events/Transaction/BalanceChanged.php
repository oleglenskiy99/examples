<?php

declare(strict_types=1);

namespace App\Events\Transaction;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BalanceChanged
{

    use Dispatchable;
    use SerializesModels;

    public float $balance;

    public float $withdraw;
    public int $userId;

    public function __construct(float $balance, float $withdraw, int $userId)
    {
        $this->balance = $balance;
        $this->withdraw = $withdraw;
        $this->userId = $userId;
    }

}
