<?php

declare(strict_types=1);

namespace App\Events\Transaction;

use App\Models\User\Charges;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ChargeCreated
{

    use Dispatchable;
    use SerializesModels;

    public Charges $charge;
    public int $userId;

    public function __construct(Charges $charge, int $userId)
    {
        $this->charge = $charge;
        $this->userId = $userId;
    }

}
