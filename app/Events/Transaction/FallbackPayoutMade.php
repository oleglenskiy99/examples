<?php

declare(strict_types=1);

namespace App\Events\Transaction;

use App\Models\User\Charges;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class FallbackPayoutMade
{

    use Dispatchable;
    use SerializesModels;

    /**
     * @var array<string>
     */
    public array $account;

    public Charges $charge;

    public float $amount;

    public float $commission;

    public float $donation;

    public int $userId;

    public function __construct(array $account, Charges $charge, float $amount, float $commission, float $donation, int $userId)
    {
        $this->account = $account;
        $this->charge = $charge;
        $this->amount = $amount;
        $this->commission = $commission;
        $this->donation = $donation;
        $this->userId = $userId;
    }

}
