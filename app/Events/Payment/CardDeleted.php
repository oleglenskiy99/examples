<?php

declare(strict_types=1);

namespace App\Events\Payment;

use App\Models\User\Cards;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CardDeleted
{

    use Dispatchable;
    use SerializesModels;

    public Cards $card;
    public int $userId;

    public function __construct(Cards $card, int $userId)
    {
        $this->card = $card;
        $this->userId = $userId;
    }

}
