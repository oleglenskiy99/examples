<?php

declare(strict_types=1);

namespace App\Events\Payment;

use App\DTO\Response\Objects\Card;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PaymentMethodAdded
{

    use Dispatchable;
    use SerializesModels;

    public Card $card;
    public int $userId;

    public function __construct(Card $card, int $userId)
    {
        $this->card = $card;
        $this->userId = $userId;
    }

}
