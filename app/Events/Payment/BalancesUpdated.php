<?php

declare(strict_types=1);

namespace App\Events\Payment;

use App\DTO\Response\User\UserFinances;
use App\Models\V2\User;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BalancesUpdated
{

    use Dispatchable;
    use SerializesModels;

    public User $user;
    public UserFinances $userFinances;

    public function __construct(User $user, UserFinances $userFinances)
    {
        $this->user = $user;
        $this->userFinances = $userFinances;
    }

}
