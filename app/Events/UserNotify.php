<?php

declare(strict_types=1);

namespace App\Events;

use App\User;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserNotify
{

    use Dispatchable;
    use SerializesModels;

    public User $user;

    /**
     * @var array<string>
     */
    public array $data;
    public string $type;

    public function __construct(User $user, array $data, string $type)
    {
        $this->user = $user;
        $this->data = $data;
        $this->type = $type;
    }

}
