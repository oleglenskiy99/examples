<?php

declare(strict_types=1);

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ResetPassword
{

    use Dispatchable;
    use SerializesModels;

    public int $userId;
    public int $code;

    public function __construct(int $userId, int $code)
    {
        $this->userId = $userId;
        $this->code = $code;
    }

}
