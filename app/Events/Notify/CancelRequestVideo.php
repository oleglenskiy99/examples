<?php

declare(strict_types=1);

namespace App\Events\Notify;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CancelRequestVideo
{

    use Dispatchable;
    use SerializesModels;

    public int $toUserId;
    public int $fromUserId;

    public function __construct(int $toUserId, int $fromUserId)
    {
        $this->toUserId = $toUserId;
        $this->fromUserId = $fromUserId;
    }

}
