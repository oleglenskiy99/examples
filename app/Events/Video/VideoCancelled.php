<?php

declare(strict_types=1);

namespace App\Events\Video;

use App\Video;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class VideoCancelled
{

    use Dispatchable;
    use SerializesModels;

    public Video $video;

    public function __construct(Video $video)
    {
        $this->video = $video;
    }

}
