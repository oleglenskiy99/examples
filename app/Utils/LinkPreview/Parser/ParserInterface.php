<?php

declare(strict_types=1);

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Utils\LinkPreview\Parser;

use App\Utils\LinkPreview\Models\LinkInterface;
use App\Utils\LinkPreview\Reader\ReaderInterface;

interface ParserInterface
{

    public function __construct(?ReaderInterface $reader = null, ?LinkInterface $link = null);

    public function getLink(): LinkInterface;
    public function getReader(): ReaderInterface;
    public function isValidParser(): bool;
    public function parseLink(): LinkInterface;
    public function setLink(LinkInterface $link): ParserInterface;
    public function setReader(ReaderInterface $reader): ParserInterface;
    public function __toString(): string;

}
