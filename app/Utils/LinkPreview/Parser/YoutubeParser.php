<?php

declare(strict_types=1);

namespace App\Utils\LinkPreview\Parser;

use App\Utils\LinkPreview\Models\LinkInterface;
use App\Utils\LinkPreview\Models\VideoLink;
use App\Utils\LinkPreview\Reader\GeneralReader;
use App\Utils\LinkPreview\Reader\ReaderInterface;
use DOMDocument;
use DOMElement;

class YoutubeParser implements ParserInterface
{

    public const PATTERN = '/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/';

    private VideoLink $link;

    private ReaderInterface $reader;

    public function __construct(?ReaderInterface $reader = null, ?LinkInterface $link = null)
    {
        if ($reader !== null) {
            $this->setReader($reader);
        } else {
            $this->setReader(new GeneralReader());
        }

        if ($link !== null) {
            $this->setLink($link);
        } else {
            $this->setLink(new VideoLink());
        }
    }

    public function isValidParser(): bool
    {
        $isValid = false;

        $link = $this->getLink();
        $url = $link->getUrl();

        if (preg_match(self::PATTERN, $url, $matches)) {
            $link->setVideoId($matches[1]);
            $isValid = true;
        }

        return $isValid;
    }

    public function getLink(): LinkInterface
    {
        return $this->link;
    }

    public function setLink(LinkInterface $link): YoutubeParser
    {
        $this->link = $link;

        return $this;
    }

    public function parseLink(): LinkInterface
    {
        $this->readLink();
        $link = $this->getLink();
        $htmlData = $this->parseHtml($link->getContent());

        $link->setTitle($htmlData['title'])
            ->setDescription($htmlData['description'])
            ->setImage($htmlData['image'])
            ->setEmbedCode(
                '<iframe id="ytplayer" type="text/html" width="640" height="390" src="http://www.youtube.com/embed/'
                . $link->getVideoId() . '" frameborder="0"/>'
            );

        return $link;
    }

    public function getReader(): ReaderInterface
    {
        return $this->reader;
    }

    public function setReader(ReaderInterface $reader): YoutubeParser
    {
        $this->reader = $reader;

        return $this;
    }

    /**
     * @return array<string>
     */
    protected function parseHtml(string $html): array
    {
        $data = [
            'image' => '',
            'title' => '',
            'description' => '',
        ];

        libxml_use_internal_errors(true);
        $doc = new DOMDocument();
        $doc->loadHTML($html);

        /** @var DOMElement $meta */
        foreach ($doc->getElementsByTagName('meta') as $meta) {
            if ($meta->getAttribute('property') === 'og:image') {
                $data['image'] = $meta->getAttribute('content');
            }

            if ($meta->getAttribute('property') === 'og:title') {
                $data['title'] = $meta->getAttribute('content');
            }

            if ($meta->getAttribute('property') === 'og:description') {
                $data['description'] = $meta->getAttribute('content');
            }
        }

        return $data;
    }

    private function readLink(): void
    {
        $reader = $this->getReader()->setLink($this->getLink());
        $this->setLink($reader->readLink());
    }

    public function __toString(): string
    {
        return 'youtube';
    }

}
