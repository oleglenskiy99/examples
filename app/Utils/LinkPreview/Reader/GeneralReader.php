<?php

declare(strict_types=1);

namespace App\Utils\LinkPreview\Reader;

use App\Utils\LinkPreview\Models\LinkInterface;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\TransferStats;

class GeneralReader implements ReaderInterface
{

    private Client $client;

    private LinkInterface $link;

    public function readLink(): LinkInterface
    {
        $link = $this->getLink();

        $client = $this->getClient();
        $response = $client->request(
            'GET',
            $link->getUrl(),
            [
                'on_stats' => function (TransferStats $stats) use (&$effectiveUrl): void {
                    $effectiveUrl = $stats->getEffectiveUri();
                },
            ]
        );

        $headerContentType = $response->getHeader('content-type');
        $contentType = '';
        if (is_array($headerContentType) && count($headerContentType) > 0) {
            $contentType = current(explode(';', current($headerContentType)));
        }

        $link->setContent((string) $response->getBody())
            ->setContentType($contentType)
            ->setRealUrl($effectiveUrl);

        return $link;
    }

    public function getLink(): LinkInterface
    {
        return $this->link;
    }

    public function setLink(LinkInterface $link): GeneralReader
    {
        $this->link = $link;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client = new Client([RequestOptions::COOKIES => true]);
    }

    public function setClient(Client $client): void
    {
        $this->client = $client;
    }

}
