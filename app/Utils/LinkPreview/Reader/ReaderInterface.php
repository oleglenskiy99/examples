<?php

declare(strict_types=1);

namespace App\Utils\LinkPreview\Reader;

use App\Utils\LinkPreview\Models\LinkInterface;

interface ReaderInterface
{

    public function getLink(): LinkInterface;

    public function readLink(): LinkInterface;

    public function setLink(LinkInterface $link): ReaderInterface;

}
