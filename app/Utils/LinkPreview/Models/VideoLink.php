<?php

declare(strict_types=1);

namespace App\Utils\LinkPreview\Models;

class VideoLink extends Link
{

    private string $embedCode;
    private string $video;
    private string $videoId;

    public function getEmbedCode(): string
    {
        return $this->embedCode;
    }

    public function setEmbedCode(string $embedCode): VideoLink
    {
        $this->embedCode = $embedCode;

        return $this;
    }

    public function getVideo(): string
    {
        return $this->video;
    }

    public function setVideo(string $video): VideoLink
    {
        $this->video = $video;

        return $this;
    }

    public function getVideoId(): string
    {
        return $this->videoId;
    }

    public function setVideoId(string $videoId): void
    {
        $this->videoId = $videoId;
    }

}
