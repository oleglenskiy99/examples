<?php

declare(strict_types=1);

namespace App\Utils\LinkPreview\Models;

interface LinkInterface
{

    public function getContent(): string;
    public function getContentType(): string;
    public function getDescription(): string;
    public function getImage(): string;

    /**
     * @return array<string>
     */
    public function getPictures(): array;
    public function getRealUrl(): string;
    public function getTitle(): string;
    public function getUrl(): string;
    public function setContent(string $content): LinkInterface;
    public function setContentType(string $contentType): LinkInterface;
    public function setDescription(string $description): LinkInterface;
    public function setImage(string $image): LinkInterface;
    public function setPictures(array $pictures): LinkInterface;
    public function setRealUrl(string $realUrl): LinkInterface;
    public function setTitle(string $title): LinkInterface;
    public function setUrl(string $url): LinkInterface;

}
