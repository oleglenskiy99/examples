<?php

declare(strict_types=1);

namespace App\Utils\LinkPreview\Models;

class Link implements LinkInterface
{

    private string $content;
    private string $contentType;
    private string $description;
    private string $image;

    /**
     * @var array<string>
     */
    private array $pictures;
    private string $realUrl;
    private string $title;
    private string $url;

    public function __construct(?string $url = null)
    {
        if ($url !== null) {
            $this->setUrl($url);
        }
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): Link
    {
        $this->content = $content;

        return $this;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    public function setContentType(string $contentType): Link
    {
        $this->contentType = $contentType;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): Link
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $image): Link
    {
        $this->image = $image;

        return $this;
    }

    public function getRealUrl(): string
    {
        return $this->realUrl;
    }

    public function setRealUrl(string $realUrl): Link
    {
        $this->realUrl = $realUrl;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): Link
    {
        $this->title = $title;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): Link
    {
        $this->url = $url;

        return $this;
    }

    public function getPictures(): array
    {
        return $this->pictures;
    }

    /**
     * @param array $pictures
     * @return Link
     */
    public function setPictures(array $pictures): Link
    {
        $this->pictures = $pictures;

        return $this;
    }

}
