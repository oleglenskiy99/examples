<?php

declare(strict_types=1);

namespace App\Utils;

use App\Utils\LinkPreview\Models\LinkInterface;
use App\Utils\LinkPreview\Parser\GeneralParser;
use App\Utils\LinkPreview\Parser\ParserInterface;
use App\Utils\LinkPreview\Parser\YoutubeParser;

class LinkPreview
{

    /**
     * @var array<ParserInterface>
     */
    private array $parsers = [];
    private bool $propagation = false;
    private string $url;

    public function __construct(?string $url = null)
    {
        if ($url !== null) {
            $this->setUrl($url);
        }
    }

    public function addParser(ParserInterface $parser): LinkPreview
    {
        $this->parsers = [(string) $parser => $parser] + $this->parsers;

        return $this;
    }

    /**
     * @return array<LinkInterface>
     */
    public function getParsed(): array
    {
        $parsed = [];

        $parsers = $this->getParsers();
        if (count($parsers) === 0) {
            $this->addDefaultParsers();
        }

        foreach ($this->getParsers() as $name => $parser) {
            $parser->getLink()->setUrl($this->getUrl());

            if (!$parser->isValidParser()) {
                continue;
            }

            $parsed[$name] = $parser->parseLink();

            if (!$this->getPropagation()) {
                break;
            }
        }

        return $parsed;
    }

    /**
     * @return array<ParserInterface>
     */
    public function getParsers(): array
    {
        return $this->parsers;
    }

    public function setParsers(array $parsers): LinkPreview
    {
        $this->parsers = $parsers;

        return $this;
    }

    public function getPropagation(): bool
    {
        return $this->propagation;
    }

    public function setPropagation(bool $propagation): LinkPreview
    {
        $this->propagation = $propagation;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): LinkPreview
    {
        $this->url = $url;

        return $this;
    }

    public function removeParser(string $name): LinkPreview
    {
        if (in_array($name, $this->parsers, false)) {
            unset($this->parsers[$name]);
        }

        return $this;
    }

    protected function addDefaultParsers(): void
    {
        $this->addParser(new GeneralParser());
        $this->addParser(new YoutubeParser());
    }

}
