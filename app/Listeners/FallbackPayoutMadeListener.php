<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\Transaction\FallbackPayoutMade;
use App\Mail\FallbackPayoutMadeReceived;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Mail;

class FallbackPayoutMadeListener implements ShouldQueue
{

    public function subscribe(Dispatcher $events): void
    {
        $events->listen(
            'App\Events\Transaction\FallbackPayoutMade',
            [self::class, 'payoutMade']
        );
    }

    public function payoutMade(FallbackPayoutMade $event): void
    {
        $to_email = explode(',', config('services.fallback_payout.email'));

        if (count($to_email) <= 0) {
            return;
        }
        foreach ($to_email as $email_string) {
            Mail::to($email_string)->send(
                new FallbackPayoutMadeReceived(
                    $event->account,
                    $event->amount,
                    $event->commission,
                    $event->donation,
                    $event->userId
                )
            );
        }
    }

}
