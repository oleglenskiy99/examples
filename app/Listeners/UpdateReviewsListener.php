<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\User\UpdateReviewsEvent;
use App\Repository\User\UsersRepository;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateReviewsListener implements ShouldQueue
{

    public function handle(UpdateReviewsEvent $event): void
    {
        $user = User::getUserById($event->userId);
        if ($user) {
            UsersRepository::setRating($user);
        }
    }

}
