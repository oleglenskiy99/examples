<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\ConfirmEmailEvent;
use App\Helpers\EmailTexts;
use App\Models\User\Info as UserInfo;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class ConfirmEmailListener implements ShouldQueue
{

    public function handle(ConfirmEmailEvent $event): void
    {
        $front_url = config('app.front_url');

        $user = User::getUserById($event->userId);

        if (!$user || empty($user->email)) {
            return;
        }

        $info = UserInfo::where('id', $user->id)->first();
        $username = $info ? ', ' . $info->firstName . ' ' . $info->lastName : '';

        $redirect_url = $front_url . '?confirm=' . $event->code;

        Mail::send('emails.confirm_email', [
            'username' => $username,
            'front_url' => $front_url,
            'redirect_url' => $redirect_url,
            'code' => $event->code,
        ], function ($m) use ($user, $username): void {
            $m->to($user->email, $username)->subject(__(EmailTexts::CONFIRM_EMAIL_SUBJECT));
        });
    }

}
