<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\Notify\CancelRequestVideo;
use App\Events\Notify\NewMessage;
use App\Events\Notify\NewRequest;
use App\Events\Notify\NewRequestVideo;
use App\Events\Notify\NotApprovedRequest;
use App\Events\Notify\ReplyRequestVideo;
use App\Helpers\SMSTexts;
use App\Services\SendSMSServiceInterface;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Events\Dispatcher;

class EventsToSMSListener implements ShouldQueue
{

    public SendSMSServiceInterface $serviceSMS;

    public function __construct(SendSMSServiceInterface $serviceSMS)
    {
        $this->serviceSMS = $serviceSMS;
    }

    public function subscribe(Dispatcher $events): void
    {
        $listeners = [
            NewMessage::class => function (NewMessage $event): void {
                $this->send($event->toUserId, __(SMSTexts::NEW_MESSAGE));
            },
            NewRequestVideo::class => function (NewRequestVideo $event): void {
                $this->send($event->toUserId, __(SMSTexts::NEW_REQUEST_VIDEO));
            },
            NewRequest::class => function (NewRequest $event): void {
                $this->send($event->toUserId, __(SMSTexts::NEW_REQUEST));
            },
            NotApprovedRequest::class => function (NotApprovedRequest $event): void {
                $this->send($event->toUserId, __(SMSTexts::NOT_APPROVED_REQUEST));
            },
            CancelRequestVideo::class => function (CancelRequestVideo $event): void {
                $this->send($event->toUserId, __(SMSTexts::CANCEL_REQUEST_VIDEO));
            },
            ReplyRequestVideo::class => function (ReplyRequestVideo $event): void {
                $this->send($event->toUserId, __(SMSTexts::REPLY_REQUEST_VIDEO));
            },
        ];
        foreach ($listeners as $class => $listener) {
            $events->listen($class, $listener);
        }
    }

    public function send(int $userId, string $message): void
    {
        $user = User::getUserById($userId);
        if ($user && !empty($user->phone)) {
            $this->serviceSMS->send($user->phone, $message);
        }
    }

}
