<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\Notify\CancelRequestVideo;
use App\Events\Notify\NewMessage;
use App\Events\Notify\NewRequest;
use App\Events\Notify\NewRequestVideo;
use App\Events\Notify\NotApprovedRequest;
use App\Events\Notify\ReplyRequestVideo;
use App\Helpers\EmailTexts;
use App\Models\User\Info as UserInfo;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Mail;

class EventsToEmailListener implements ShouldQueue
{

    public function subscribe(Dispatcher $events): void
    {
        $events->listen(
            NewMessage::class,
            [self::class, 'newMessage']
        );
        $events->listen(
            NewRequestVideo::class,
            [self::class, 'NewRequestVideo']
        );
        $events->listen(
            NewRequest::class,
            [self::class, 'newRequest']
        );
        $events->listen(
            NotApprovedRequest::class,
            [self::class, 'NotApprovedRequest']
        );
        $events->listen(
            CancelRequestVideo::class,
            [self::class, 'CancelRequestVideo']
        );
        $events->listen(
            ReplyRequestVideo::class,
            [self::class, 'replyRequestVideo']
        );
    }

    public function newMessage(NewMessage $event): void
    {
        $frontUrl = config('app.front_url');

        $to_user = User::getUserById($event->toUserId);
        $from_user = User::getUserById($event->fromUserId);
        if (!$to_user || empty($to_user->email)) {
            return;
        }

        $info = UserInfo::where('id', $event->toUserId)->first();
        $toUsername = $info ? $info->firstName . ' ' . $info->lastName : '';
        $redirect_url = config('app.url') . '/' . $from_user->domain;

        Mail::send('emails.new_message_1', [
            'front_url' => $frontUrl,
            'redirect_url' => $redirect_url,
        ], function ($m) use ($to_user, $toUsername): void {
            $m->to($to_user->email, $toUsername)->subject(__(EmailTexts::NEW_MESSAGE_SUBJECT));
        });
    }

    public function newRequestVideo(NewRequestVideo $event): void
    {
        $front_url = config('app.front_url');

        $to_user = User::getUserById($event->toUserId);
        if (!$to_user || empty($to_user->email)) {
            return;
        }

        $info = UserInfo::where('id', $event->toUserId)->first();
        $toUsername = $info ? $info->firstName . ' ' . $info->lastName : '';

        Mail::send('emails.new_request_video', [
            'front_url' => $front_url,
        ], function ($m) use ($to_user, $toUsername): void {
            $m->to($to_user->email, $toUsername)->subject(__(EmailTexts::NEW_REQUEST_VIDEO_SUBJECT));
        });
    }

    // phpcs:disable SlevomatCodingStandard.Namespaces.UnusedUses.MismatchingCaseSensitivity
    public function newRequest(newRequest $event): void
    {
        $front_url = config('app.front_url');

        $to_user = User::getUserById($event->toUserId);
        if (!$to_user || empty($to_user->email)) {
            return;
        }

        $info = UserInfo::where('id', $event->toUserId)->first();
        $toUsername = $info ? $info->firstName . ' ' . $info->lastName : '';

        Mail::send('emails.new_request', [
            'front_url' => $front_url,
        ], function ($m) use ($to_user, $toUsername): void {
            $m->to($to_user->email, $toUsername)->subject(__(EmailTexts::NEW_REQUEST_SUBJECT));
        });
    }

    public function notApprovedRequest(NotApprovedRequest $event): void
    {
        $front_url = config('app.front_url');

        $to_user = User::getUserById($event->toUserId);
        if (!$to_user || empty($to_user->email)) {
            return;
        }

        $info = UserInfo::where('id', $event->toUserId)->first();
        $toUsername = $info ? $info->firstName . ' ' . $info->lastName : '';

        Mail::send('emails.not_approved_request', [
            'front_url' => $front_url,
        ], function ($m) use ($to_user, $toUsername): void {
            $m->to($to_user->email, $toUsername)->subject(__(EmailTexts::NOT_APPROVED_REQUEST_SUBJECT));
        });
    }

    public function replyRequestVideo(ReplyRequestVideo $event): void
    {
        $front_url = config('app.front_url');

        $to_user = User::getUserById($event->toUserId);
        if (!$to_user || empty($to_user->email)) {
            return;
        }

        $info = UserInfo::where('id', $event->toUserId)->first();
        $toUsername = $info ? $info->firstName . ' ' . $info->lastName : '';

        Mail::send('emails.video_chat_approved', [
            'front_url' => $front_url,
        ], function ($m) use ($to_user, $toUsername): void {
            $m->to($to_user->email, $toUsername)->subject(__(EmailTexts::REPLY_VIDEO_REQUEST_SUBJECT));
        });
    }

    public function cancelRequestVideo(CancelRequestVideo $event): void
    {
        $front_url = config('app.front_url');

        $to_user = User::getUserById($event->toUserId);
        if (!$to_user || empty($to_user->email)) {
            return;
        }

        $info = UserInfo::where('id', $event->toUserId)->first();
        $toUsername = $info ? $info->firstName . ' ' . $info->lastName : '';

        Mail::send('emails.not_approved_request', [
            'front_url' => $front_url,
        ], function ($m) use ($to_user, $toUsername): void {
            $m->to($to_user->email, $toUsername)->subject(__(EmailTexts::CANCEL_VIDEO_REQUEST_SUBJECT));
        });
    }

}
