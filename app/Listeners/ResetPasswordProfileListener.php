<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Models\User\Info;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class ResetPasswordProfileListener implements ShouldQueue
{

    public function handle(object $event): void
    {
		$frontUrl = config('app.front_url');
		$user = User::find($event->userId);

		if ( !$user || empty($user->email)) {
            return;
        }

        $info = Info::where('id', $event->userId)->first();
        $username = $info ? ', ' . $info->firstName . ' ' . $info->lastName : '';
        Mail::send('emails.password_reset_message', [
            'username' => $username,
            'front_url' => $frontUrl,
        ], function ($m) use ($user, $username): void {
            $m->to($user->email, $username)->subject('Reset password!');
        });
    }

}
