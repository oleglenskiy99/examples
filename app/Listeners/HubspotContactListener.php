<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Services\Hubspot\HubspotContact;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;

class HubspotContactListener implements ShouldQueue
{

    public function handle(object $event): void
    {
        $user = User::getUserById($event->userId);
        if ($user) {
            (new HubspotContact())->contactAPI($user);
        }
    }

}
