<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\Chat\MessageDeleted;
use App\Events\Payment\CardAdded;
use App\Events\Payment\CardDeleted;
use App\Events\Transaction\BalanceRefilled;
use App\Events\Transaction\CharityDonated;
use App\Events\Transaction\FallbackPayoutMade;
use App\Events\Transaction\PaymentMade;
use App\Events\Transaction\PayoutMade;
use App\Events\User\SetCreatorUserEvent;
use App\Events\User\UnsetCreatorUserEvent;
use App\Events\Video\VideoCancelled;
use App\Http\Middleware\Hardware;
use App\Http\Middleware\Location;
use App\Http\Middleware\Session;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Events\Dispatcher;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class AnalyticsEventSubscriber implements ShouldQueue
{

    /**
     * @var array
     */
    protected array $config = [];

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config + config('services.analytics');
    }

    public function subscribe(Dispatcher $events): void
    {
        $listeners = [
            // Добавление кредитной карты
            CardAdded::class => function (CardAdded $event): void {
                $this->send('user:card_add', '', [
                    'type' => $event->card->funding,
                    'number_hash' => sha1($event->card->payment_method),
                    'is_new_card' => true,
                ]);
            },
            // Удаление кредитной карты
            CardDeleted::class => function (CardDeleted $event): void {
                $this->send('user:card_delete', '', [
                    'type' => $event->card->funding,
                    'number_hash' => sha1($event->card->payment_method),
                ]);
            },
            // Пополнение баланса
            BalanceRefilled::class => function (BalanceRefilled $event): void {
                $this->send('user:transaction_balance_refill', '', [
                    'type' => $event->card->funding,
                    'number_hash' => sha1($event->card->payment_method),
                    'charge_id' => $event->charge->id,
                    'amount' => $event->amount,
                ]);
            },
            // Пожертвование в фонд
            CharityDonated::class => function (CharityDonated $event): void {
                $this->send('user:transaction_charity_payout', '', [
                    'fond_id' => $event->fond->external_id,
                    'percent' => $event->fond->percent,
                    'amount' => $event->amount,
                ]);
            },
            // Платеж
            PaymentMade::class => function (PaymentMade $event): void {
                $this->send('user:transaction_transfer_money', '', [
                    'charge_id' => $event->charge->id,
                    'amount' => $event->amount,
                ]);
            },
            // Выплата
            PayoutMade::class => function (PayoutMade $event): void {
                $this->send('user:transaction_payout', '', [
                    'type' => $event->card->funding,
                    'number_hash' => sha1($event->card->payment_method),
                    'charge_id' => $event->charge->id,
                    'amount' => $event->amount,
                    'commission' => $event->commission,
                    'donation' => $event->donation,
                ]);
            },
            // Выплата
            FallbackPayoutMade::class => function (FallbackPayoutMade $event): void {
                $this->send('user:transaction_payout', '', [
                    'type' => 'manual',
                    'number_hash' => sha1($event->account['account_number']),
                    'charge_id' => $event->charge->id,
                    'amount' => $event->amount,
                    'commission' => $event->commission,
                    'donation' => $event->donation,
                ]);
            },
            // Экспирация сообщения чата
            MessageDeleted::class => function (MessageDeleted $event): void {
                $created_at = Date::createFromTimestampUTC($event->message->timestamp);
                $expire_at = Date::createFromTimestampUTC($event->message->timestamp)->addHours(env('AUTO_REFUND_TIME', 1));
                $this->sendAs($event->message->chat->creator_id, 'chat:message_delete', 'cron:payments_refund_messages', [
                    'seller_id' => $event->message->chat->expert_id,
                    'cost' => $event->message->cost,
                    'created_at' => $created_at->timestamp,
                    'expire_at' => $expire_at->timestamp,
                    'is_paid' => $event->message->is_paid,
                    'is_expired' => true,
                    'is_back' => true,
                ]);
            },
            // Экспирация запроса видео
            VideoCancelled::class => function (VideoCancelled $event): void {
                $created_at = Date::createFromTimestampUTC($event->video->created_at);
                $expire_at = Date::createFromTimestampUTC($event->video->created_at)->addHours(env('AUTO_REFUND_TIME', 1));
                $this->sendAs($event->video->user_id, 'video:cancel', 'cron:payments_refund_video', [
                    'seller_id' => $event->video->target_id,
                    'cost' => $event->video->cost,
                    'created_at' => $created_at->timestamp,
                    'expire_at' => $expire_at->timestamp,
                    'is_expired' => true,
                    'is_back' => true,
                ]);
            },
            // creator:show
            SetCreatorUserEvent::class => function (SetCreatorUserEvent $event): void {
                $this->sendAs($event->userId, 'creator:show', '', $event->raw);
            },
            // creator:hide
            UnsetCreatorUserEvent::class => function (UnsetCreatorUserEvent $event): void {
                $this->sendAs($event->userId, 'creator:hide', '', $event->raw);
            },
        ];
        foreach ($listeners as $class => $listener) {
            $events->listen($class, $listener);
        }
    }

    /**
     * @param string $action
     * @param string $page
     * @param array $payload
     * @param int $status
     */
    protected function send(string $action, string $page, array $payload = [], int $status = 200): void
    {
        $request = request();
        $session = $request->attributes->get(Session::SESSION);
        $location = $request->attributes->get(Location::ATTRIBUTE);
        $country = null;
        if (isset($location['countryCode'], $location['countryName'], $location['cityName'])) {
            $country = implode('/', [
                $location['countryCode'],
                $location['countryName'],
                $location['cityName'],
            ]);
        }
        $ipAddress = $location['ipAddress'] ?? null;
        $deviceToken = $request->attributes->get(Hardware::DEVICE_TOKEN);
        $this->sendHttp([
            'action_id' => $action,
            'page' => $page,
            'raw' => $payload,
            'status' => $status,
            'uid' => $session->user_id ?? null,
            'session_id' => $session->session_id ?? null,
            'hwid' => [
                'id' => $deviceToken,
                'domain' => $request->getHost(),
            ],
            'location' => null,
            'country' => $country,
            'ip' => $ipAddress,
        ]);
    }

    /**
     * @param array $payload
     */
    protected function sendHttp(array $payload): void
    {
        $payload['hwid'] = json_encode($payload['hwid']) ?: '{}';
        $payload['raw'] = json_encode($payload['raw']) ?: '{}';
        $request = Http::asJson()->timeout((int) $this->config['timeout']);

        try {
            $request->post($this->config['url'], $payload)->throw();
        } catch (ConnectionException $exception) {
            Log::error($exception->getMessage(), [
                'request' => $request,
            ]);
        } catch (RequestException $exception) {
            Log::error($exception->getMessage(), [
                'request' => $request,
                'response' => $exception->response,
            ]);
        }
    }

    /**
     * @param int $user_id
     * @param string $action
     * @param string $page
     * @param array $payload
     * @param int $status
     */
    protected function sendAs(int $user_id, string $action, string $page, array $payload = [], int $status = 200): void
    {
        $this->sendHttp([
            'action_id' => $action,
            'page' => $page,
            'raw' => $payload,
            'status' => $status,
            'uid' => $user_id,
            'session' => null,
            'hwid' => [
                'domain' => env('APP_URL'),
            ],
            'location' => null,
            'country' => null,
            'ip' => null,
        ]);
    }

}
