<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Repository\User\UsersRepository;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserUpdateListener implements ShouldQueue
{

    public function handle(object $event): void
    {
		UsersRepository::setRoleCreator($event->userId);
    }

}
