<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Enums\SocketEventsEnum;
use App\Events\Payment\BalancesUpdated;
use App\Events\Payment\CardAdded;
use App\Events\Payment\CardDeleted;
use App\Events\Payment\PaymentMethodAdded;
use App\Events\Transaction\BalanceChanged;
use App\Events\Transaction\BalanceRefilled;
use App\Events\Transaction\ChargeCreated;
use App\Events\Transaction\ChargeUpdated;
use App\Events\Transaction\FallbackPayoutMade;
use App\Events\Transaction\PaymentMade;
use App\Events\Transaction\PayoutMade;
use Emitter;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

class EmitterEventSubscriber implements ShouldQueue
{

    public const OPERATIONS = [
        -1 => 'decrement',
        1 => 'increment',
    ];

    protected Emitter $emitter;

    // phpcs:disable SlevomatCodingStandard.Functions.DisallowEmptyFunction.EmptyFunction
    public function __construct()
    {
    }

    public function subscribe(Dispatcher $events): void
    {
        /** @var SerializerInterface $serializer */
        $serializer = app(SerializerInterface::class);

        $listeners = [

            // Добавление кредитной карты api v2

            PaymentMethodAdded::class => function (PaymentMethodAdded $event) use ($serializer): void {
                // TODO $userCards change to DTO
                $this->emit(
                    SocketEventsEnum::CREDIT_CARD_ADD,
                    json_decode($serializer->serialize($event->card, JsonEncoder::FORMAT), true),
                    $event->userId
                );
            },

            BalancesUpdated::class => function (BalancesUpdated $event) use ($serializer): void {
                $this->emit(
                    SocketEventsEnum::USER_BALANCES_UPDATED,
                    json_decode($serializer->serialize($event->userFinances, JsonEncoder::FORMAT), true),
                    $event->user->id
                );
            },

            // Добавление кредитной карты api v1

            CardAdded::class => function (CardAdded $event): void {
                $this->emit('finance:card:add', $event->card->toArray(), $event->userId);
            },
            // Удаление кредитной карты
            CardDeleted::class => function (CardDeleted $event): void {
                $this->emit('finance:card:remove', $event->card->toArray(), $event->userId);
            },
            // Создание транзакции
            ChargeCreated::class => function (ChargeCreated $event): void {
                $this->emit('charge:created', $event->charge->toArray(), $event->userId);
            },
            // Изменение транзакции
            ChargeUpdated::class => function (ChargeUpdated $event): void {
                $this->emit('charge:updated', $event->charge->toArray(), $event->userId);
            },
            // Пополнение баланса
            BalanceRefilled::class => function (BalanceRefilled $event): void {
                $this->emit('user:balance:increment', ['amount' => $event->amount], $event->userId);
            },
            BalanceChanged::class => function (BalanceChanged $event): void {
                $this->diff('balance', $event->balance, $event->userId);
                $this->diff('withdraw', $event->withdraw, $event->userId);
            },
            // Платеж
            PaymentMade::class => function (PaymentMade $event): void {
                $this->emit('user:withdraw:increment', ['amount' => $event->amount], $event->userId);
            },
            // Выплата
            PayoutMade::class => function (PayoutMade $event): void {
                $this->emit(
                    'user:withdraw:decrement',
                    ['amount' => $event->amount + $event->commission + $event->donation],
                    $event->userId
                );
            },
            FallbackPayoutMade::class => function (FallbackPayoutMade $event): void {
                $this->emit(
                    'user:withdraw:decrement',
                    ['amount' => $event->amount + $event->commission + $event->donation],
                    $event->userId
                );
            },
        ];
        foreach ($listeners as $class => $listener) {
            $events->listen($class, $listener);
        }
    }

    public function emit(string $type, array $payload, int $userId): void
    {
        try {
            if ($userId) {
                $this->emitter = new Emitter();
                $this->emitter->to(sha1((string) $userId))->emit($type, $payload);
            }
        } catch (Throwable $exception) {
            Log::warning($exception->getMessage());
        }
    }

    // phpcs:disable SlevomatCodingStandard.ControlStructures.EarlyExit.EarlyExitNotUsed

    protected function diff(string $category, float $amount, int $userId): void
    {
        $diff = $amount <=> 0;
        $operation = self::OPERATIONS[$diff] ?? null;
        if ($operation !== null) {
            $this->emit(
                sprintf('user:%s:%s', $category, $operation),
                ['amount' => $amount * $diff],
                $userId
            );
        }
    }

}
