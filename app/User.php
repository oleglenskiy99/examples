<?php

declare(strict_types=1);

namespace App;

use App\Models\Network;
use App\Models\User\Info;
use App\Models\User\Linked;
use App\Repository\User\UsersRepository;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 *
 * @OA\Schema(
 *     description="User Model",
 *     title="User Model",
 *     required={"name", "email"},
 * @OA\Xml(
 *         name="User"
 *     )
 * )
 * @property int $id
 * @property string $domain
 * @property string $firstName
 * @property string $lastName
 * @property string $password
 * @property string $email
 * @property double $balance
 * @property int $last_activity
 * @property int $is_verify
 * @property int $is_online
 * @property int $is_blocked
 * @property string $phone
 * @property string $onboarding
 * @property float $withdraw
 * @property string $stripe_customer_id
 * @property string $_hsid
 * @property bool $is_email_verify
 * @property string $stripe_account_id
 * @method static User|null find($id, $columns = ['*'])
 */
class User extends Authenticatable implements JWTSubject
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    public $hidden = [
        'stripe_customer_id',
        'stripe_account_id',
        'masked_phone',
        'masked_email',
    ];

    /**
     * @var string
     */
    protected $table = 'module_users';

    /**
     * @var array
     */
    protected $casts = [
        'is_verify' => 'boolean',
        'is_online' => 'boolean',
        'is_blocked' => 'boolean',
        'is_contact' => 'boolean',
        'isc' => 'boolean',
        'is_email_verify' => 'boolean',
        'balance' => 'double',
        'phone' => 'string',
        'email' => 'string',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'password',
        'domain',
        'email',
        'phone',
        'is_deleted',
        'balance',
        'is_online',
        'guuid',
        'last_activity',
        'onboarding',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'masked_phone',
        'masked_email',
        'price',
        'rating',
    ];

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany('\App\Roles', 'module_users_roles', 'user_id', 'role_id')->withTimeStamps();
    }

    public function info(): HasOne
    {
        return $this->hasOne('\App\Models\User\Info', 'id', 'id');
    }

    public function founds(): HasMany
	{
        return $this->hasMany('\App\Models\User\Founds', 'user_id', 'id');
    }

    public function categories(): HasManyThrough
    {
        return $this->hasManyThrough('\App\Categories', '\App\Models\User\Categories', 'user_id', 'id', 'id', 'category_id');
    }

    public function medias(): HasManyThrough
    {
        return $this->hasManyThrough('App\Media', 'App\Models\User\Medias', 'user_id', 'id', '', 'media_id');
    }

    public function country(): HasOne
    {
        return $this->hasOne('\App\Country', 'id', 'country');
    }

    // phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint

    public function cards(): HasMany
    {
        return $this->hasMany('\App\Models\User\Cards', 'user_id', 'id');
    }

    // phpcs:disable SlevomatCodingStandard.Classes.ClassStructure.IncorrectGroupOrder
    public function fans(): HasMany
    {
        return $this->hasMany('\App\Models\Contacts\Requests', 'taker', 'id');
    }

    public function followers(): HasMany
    {
        return $this->hasMany('\App\Models\Contacts\Requests', 'sender', 'id');
    }

    public function reviews(): HasMany
    {
        return $this->hasMany('\App\Models\User\Reviews', 'target_id', 'id');
    }

    public function linkeds(): HasMany
    {
        return $this->hasMany('\App\Models\User\Linked', 'user_id', 'id');
    }

    public function getPriceAttribute(): ?float
    {
        return UsersRepository::getMiddlePrice($this);
    }

    public function getRatingAttribute(): ?float
    {
        return UsersRepository::getRating($this);
    }

    public function prices(): HasMany
    {
        return $this->hasMany('\App\Models\User\Prices', 'user_id', 'id');
    }

    public function rating(): HasOne
    {
        return $this->hasOne('\App\Models\User\Rating', 'user_id', 'id');
    }

    public function getMaskedPhoneAttribute(): ?string
    {
        $user = self::where('id', $this->id)->first();
        if ($user && $user->phone !== null && strlen($user->phone) > 6 && strlen($user->phone) < 17) {
            return substr(preg_replace('/[^0-9]/', '', $user->phone), -4);
        }
        return null;
    }

    /**
     * @return array
     */
    public function getMaskedEmailAttribute(): ?array
    {
        $user = self::where('id', $this->id)->first();
        if ($user) {
            return [
                'email' => $user->email,
                'verified' => $user->is_email_verify,
            ];
        }
        return null;
    }

    public function getLinkedAccounts(): array
    {
        $connected = Linked::get($this->id);

        $awaiting = Network::whereNotIn('id', array_column($connected, 'id'))
            ->where('is_deleted', false)
            ->get()->toArray();

        return [
            'connected' => $connected,
            'awaiting' => $awaiting,
        ];
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    public static function builder()
    {
        return self::select(
            'module_users.id',
            'firstName',
            'lastName',
            'domain',
            'photo',
            'biography',
            'is_online',
            'is_verify',
            'last_activity',
            'country'
        )
            ->addSelect(
                DB::raw('(SELECT value from module_users_prices WHERE user_id = module_users.id ORDER by value ASC LIMIT 1) as price')
            )
            ->with('categories')
            ->with('prices')
            ->join('module_users_about', 'module_users_about.id', 'module_users.id');
    }

    /**
     * @param User $user
     * @return array|void
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function formatResponse(User $user)
    {
        if (empty($user->info)) {
            $r = $user->toArray();
            $r['is_need_verify'] = true;
        } else {
            $r = array_merge($user->toArray(), $user->info->toArray());
        }

        unset($r['info']);
        unset($r['uid']);

        if (array_key_exists('notifications', $r)) {
            $oldNotifications = [];
            $newNotifications = [];
            $unread_count = 0;

            foreach ($r['notifications'] as $notify) {
                if ($notify['read_at'] === null) {
                    $unread_count++;

                    $newNotifications[] = [
                        'id' => $notify['id'],
                        'type' => $notify['type'],
                        'timestamp' => strtotime($notify['created_at']),
                        'data' => json_decode($notify['data'], false),
                    ];
                } else {
                    $oldNotifications[] = [
                        'id' => $notify['id'],
                        'type' => $notify['type'],
                        'timestamp' => strtotime($notify['created_at']),
                        'data' => json_decode($notify['data'], false),
                    ];
                }
            }

            $r['notifications'] = [
                'active' => $newNotifications,
                'history' => $oldNotifications,
                'unview_count' => $unread_count,
            ];
        }

        if (!array_key_exists('question', $r) || $r['question'] === null) {
            return;
        }

        if ($r['question']['user_id'] === session('user_id')) {
            if ($r['question']['user_id'] === 1) {
                $r['question']['user'] = $r['question']['target'];
                $r['question']['section'] = 'outgoing';
            } else {
                $r['question']['user'] = $r['question']['owner'];
                $r['question']['section'] = 'incoming';
            }

            unset($r['question']['target']);
            unset($r['question']['owner']);
        }

        $priceFrom = 0;

        if (array_key_exists('prices', $r)) {

            if (array_key_exists('min_call_duration', $r['prices'])) {
                $r['min_call_duration'] = round($r['prices']['min_call_duration']['value']);
                unset($r['prices']['min_call_duration']);
            }

            foreach ($r['prices'] as $price) {

                if ($price === null) {
                    continue;
                }

                if ($priceFrom === 0) {
                    $priceFrom = $price['value'];
                }

                if ($priceFrom > $price['value']) {
                    $priceFrom = $price;
                }
            }

        }

        $r['price_from'] = $priceFrom;

        if (array_key_exists('privacy', $r) && $r['privacy'] === null) {
            $r['privacy'] = [
                'messages' => true,
                'audio_calls' => true,
                'video_calls' => true,
                'questions' => true,
                'video_requests' => true,
                'live_video_requests' => true,
            ];
        }

        return $r;
    }

    public static function getUserById(int $id): ?User
    {
        return static::where('id', $id)->first();
    }

    public static function getUserByPhone(string $phone): ?User
    {
        return static::where('phone', $phone)->first();
    }

    public static function getUserByEmail(string $email): ?User
    {
        return static::where('email', 'ILIKE', $email)->first();
    }

    public static function getAccessToken(): string
    {
        $char_str = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z ';
        $char_str .= strtolower($char_str);
        $char_str .= '1 2 3 4 5 6 7 8 9 0';
        $char_arr = explode(' ', $char_str);
        $response = '';

        for ($i = 1; $i <= 45; $i++) {
            $response .= $char_arr[rand(0, count($char_arr) - 1)];
        }

        return sha1(md5(time() . $response));
    }

    protected static function booted(): void
    {
        static::created(function ($user): void {
            $r = GeoIP::detect();
            $location = DB::table('module_countries')
                ->select('id', 'name', 'phonecode', 'sortname')
                ->where('sortname', $r['countryCode'])->first();

            $code = $location ? $location->id : null;
            Info::insert([
                'id' => $user->id,
                'country' => $code,
            ]);
        });
    }

    /**
     * @return false|int
     */
    public function increment($column, $amount = 1, array $extra = [])
    {
        return parent::increment($column, $amount, $extra);
    }

    /**
     * @return false|int
     */
    public function decrement($column, $amount = 1, array $extra = [])
    {
        return parent::decrement($column, $amount, $extra);
    }

}
