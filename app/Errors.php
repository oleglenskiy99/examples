<?php

declare(strict_types=1);

namespace App;

class Errors
{

    /**
     * @return array
     */
	public static function codes(): array
	{
		return [

			1 => __('errors.unknow'),
			2 => __('errors.authorization'),
			3 => __('errors.too_many_requests'),
			4 => __('errors.permission_denied'),
			5 => __('errors.wrong_request'),
			6 => __('errors.server_side_error'),
			7 => __('errors.access_denied'),
			8 => __('errors.verify_required'),
			9 => __('errors.object_deleted_or_blocked'),
			10 => __('errors.csrf_issue'),

			11 => __('errors.invalid_code'),
			12 => __('errors.password_required'),

			13 => __('errors.chat_access_permission'),
			14 => __('errors.chat_access_denied'),
			15 => __('errors.chat_invalid_id'),
			16 => __('errors.channel_subscribe_required'),
			17 => __('errors.resource_not_found'),
			18 => __('errors.withdraw_balance_error'),
            19 => __('errors.nickname_already_exists'),

			100 => __('errors.validation_error'),
			101 => __('errors.wrong_user_id'),
			103 => __('errors.payment_error'),
            104 => __('errors.cancel_payment_error'),

			1000 => __('errors.session_expired'),

		];
	}

    // phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint
	public static function get(int $code)
	{
		if (array_key_exists($code, self::codes())) {
			return self::codes()[$code];
		}

		return false;
	}

}
