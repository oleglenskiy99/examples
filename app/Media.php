<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class Media
 *
 * @mixin Builder
 * @OA\Schema(
 *     schema="Media",
 *     description="Media Model",
 *     title="Media Model"
 * )
 * @property int $id
 * @property string $thumb
 * @property int $width
 * @property int $height
 * @property double $duration
 * @property int $size
 * @property string $type
 * @property string $placeholder
 * @property string $url
 * @property string $waveform
 * @property string $fileName
 * @property string $extension
 * @property string $server
 */
class Media extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_medias';

    /**
     * @var array<string>
     */
    protected $casts = [
        'duration' => 'double',
    ];

    /**
     * @var array<string>
     */
    protected $hidden = ['laravel_through_key', 'waveform', 'placeholder', 'thumb', 'url', 'server'];

    public function getObjectAttribute(): string
	{
        return $this->url;
    }

    /**
     * @return array<string>
     */
    public function getUrlsAttribute(): array
	{
        $connection = 's3';

        if ( $this->server) {
            $connection = $this->server;
        }

        if ( $this->type === 'image' || $this->type === 'video') {

            return [
                'placeholder' => filter_var($this->placeholder, FILTER_VALIDATE_URL) ? $this->placeholder : Storage::disk($connection)->url(
                    $this->placeholder
                ),
                'thumb' => filter_var($this->thumb, FILTER_VALIDATE_URL) ? $this->thumb : Storage::disk($connection)->url($this->thumb),
                'original' => filter_var($this->url, FILTER_VALIDATE_URL) ? $this->url : Storage::disk($connection)->url($this->url),
            ];

        }

        return [
            'original' => filter_var($this->url, FILTER_VALIDATE_URL) ? $this->url : Storage::disk($connection)->url($this->url),
        ];
    }

    /**
     * @return array<object>
     */
    public function toArray(): array
	{
        $array = parent::toArray();

        foreach ($this->getMutatedAttributes() as $key) {
            if ( !array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};
            }
        }

        return $array;
    }

}
