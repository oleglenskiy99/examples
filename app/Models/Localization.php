<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Localization extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_localizations';

    /**
     * @var array<string>
     */
    protected $fillable = [
        'lang',
        'name',
    ];

}
