<?php

declare(strict_types=1);

namespace App\Models\Contacts;

use Illuminate\Database\Eloquent\Model;

class ContactArchives extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_user_pd_archives';

}
