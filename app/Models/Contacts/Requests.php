<?php

declare(strict_types=1);

namespace App\Models\Contacts;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Requests
 *
 * @OA\Schema(
 *     description="Requests Model",
 *     title="Requests Model"
 * )
 * @property int $id
 * @property int $taker
 * @property int $sender
 * @property bool $status
 * @property bool $hidden
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and');
 */
class Requests extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_contacts_requests';

    /**
     * @var array<string>
     */
    protected $fillable = ['sender', 'taker', 'accept'];

    public function owner(): HasOne
	{
        return $this->hasOne('\App\Models\User\Info', 'id', 'sender');
    }

    public function user(): HasOne
	{
        return $this->hasOne('\App\Models\User\Info', 'id', 'taker');
    }

}
