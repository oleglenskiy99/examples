<?php

declare(strict_types=1);

namespace App\Models\GeoIp;

use Illuminate\Database\Eloquent\Model;

class CityBlocks extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_geoip_city_blocks';

    /**
     * @var array<string>
     */
    protected $fillable = [
        'city_id',
        'min_ip_address',
        'max_ip_address',
    ];

}
