<?php

declare(strict_types=1);

namespace App\Models\GeoIp;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_geoip_city';

    /**
     * @var array<string>
     */
    protected $fillable = [
        'region_name_ru',
        'city_name_es',
        'city_name_fr',
        'region_name_en',
        'region_name_de',
        'region_name_es',
        'region_name_fr',
        'city_name_ru',
        'city_name_en',
        'city_name_de',
    ];

}
