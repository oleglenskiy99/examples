<?php

declare(strict_types=1);

namespace App\Models\GeoIp;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_geoip_country';

    /**
     * @var array<string>
     */
    protected $fillable = [
        'code',
        'name_ru',
        'name_en',
        'name_de',
        'name_fr',
        'name_es',
    ];

}
