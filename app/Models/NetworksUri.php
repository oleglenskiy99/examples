<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NetworksUri extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_networks_uri';

    /**
     * @var array<string>
     */
    protected $fillable = [
        'nid',
        'uri',
    ];

}
