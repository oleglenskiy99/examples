<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Network extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_networks';

    /**
     * @var array<string>
     */
    protected $casts = [
        'is_deleted' => 'boolean',
    ];

    /**
     * @var array<string>
     */
    protected $fillable = [
        'name',
        'icon',
        'color',
        'is_deleted',
        'provider',
    ];

}
