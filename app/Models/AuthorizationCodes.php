<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuthorizationCodes extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var string
     */
    protected $primaryKey = 'identification';

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var string
     */
    protected $table = 'module_authorization_codes';

    /**
     * @var array<string>
     */
    protected $casts = [
        'identification' => 'string',
        'additional' => 'string',
        'codes' => 'integer',
    ];

    /**
     * @var array<string>
     */
    protected $fillable = [
        'identification',
        'additional',
        'codes',
    ];

}
