<?php

declare(strict_types=1);

namespace App\Models\V2;

use App\Models\User\Reviews;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property int $id
 * @property string $domain
 * @property string $email
 * @property float $balance
 * @property float $withdraw
 * @property string $phone
 * @property string $password
 * @property string $stripe_customer_id
 * @property int $followId
 * @property int $followingId
 * @property string $phone
 * @property UsersAbout $info
 * @property Collection $prices
 * @property Collection $categories
 * @property Collection $linkeds
 * @property Collection $followers
 * @property Collection $followings
 * @property bool $is_email_verify
 * @property int $last_activity
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and')
 * @method static Builder leftJoin($table, $first, $operator = null, $second = null)
 * @method static User|null find($id, $columns = ['*']);
 */
class User extends Authenticatable implements JWTSubject
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users';

    public function getJWTIdentifier(): int
    {
        return $this->getKey();
    }

    /**
     * @return array<string>
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    public function info(): HasOne
    {
        return $this->hasOne(UsersAbout::class, 'id', 'id');
    }

    public function prices(): HasMany
    {
        return $this->hasMany(UsersPrices::class, 'user_id', 'id');
    }

    public function linkeds(): HasMany
    {
        return $this->hasMany(UsersLinked::class, 'user_id', 'id');
    }

    public function categories(): HasManyThrough
    {
        return $this->hasManyThrough(
            Categories::class,
            UsersCategories::class,
            'user_id',
            'id',
            'id',
            'category_id'
        );
    }

    public function followers(): BelongsToMany
    {
        return $this->belongsToMany(
            self::class,
            'module_users_contacts_requests',
            'sender',
            'taker'
        );
    }

    public function followings(): BelongsToMany
    {
        return $this->belongsToMany(
            self::class,
            'module_users_contacts_requests',
            'taker',
            'sender'
        );
    }

    public function getFollowersCount(): int
    {
        return $this->belongsToMany(
            self::class,
            'module_users_contacts_requests',
            'sender',
            'taker'
        )->count();
    }

    public function getFollowingsCount(): int
    {
        return $this->belongsToMany(
            self::class,
            'module_users_contacts_requests',
            'taker',
            'sender'
        )->count();
    }

    public function getRating(): ?string
    {
        //TODO Rewrite method later when we get normal requirements
        return $this->hasMany(
            Reviews::class,
            'owner_id',
            'id'
        )->average('value');
    }

    public function getVideoResponseCount(): int
    {
        return $this->hasMany(
            Videos::class,
            'target_id',
            'id'
        )->where('is_completed', true)->count();
    }

    public function getChantResponseCount(): int
    {
        return $this->hasMany(
            Chat::class,
            'expert_id',
            'id'
        )->count();
    }

    public function getResponsesCount(): int
    {
        return $this->getChantResponseCount() + $this->getVideoResponseCount();
    }

    public function events(): BelongsToMany
    {
        return $this->belongsToMany(
            Event::class,
            'module_users_events'
        );
    }

    public function viewedUsers(): BelongsToMany
    {
        return $this->belongsToMany(
            self::class,
            'module_users_views',
            'user_id',
            'viewed_user_id'
        );
    }

    public function medias(): BelongsToMany
    {
        return $this->belongsToMany(
            Media::class,
            'module_users_medias',
            'user_id',
            'media_id'
        );
    }

    public function isMe(): bool
    {
        return Auth::id() === $this->id;
    }

}
