<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property string $username
 * @property Networks $network
 */
class UsersLinked extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_linked';

    public function network(): HasOne
    {
        return $this->hasOne(Networks::class, 'id', 'network_id');
    }

}
