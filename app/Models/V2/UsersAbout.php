<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;

/**
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property string $photo
 * @property string $biography
 * @property bool $is_online
 * @property int $last_activity
 * @property Categories $category
 * @property User $user
 * @method static Builder leftJoin($table, $first, $operator = null, $second = null)
 */
class UsersAbout extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_about';

    public function category(): HasOne
    {
        return $this->hasOne(Categories::class, 'id', 'industry');
    }

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'id');
    }

    public function gcountry(): HasOne
    {
        return $this->hasOne(Country::class, 'id', 'country');
    }

}
