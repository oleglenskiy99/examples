<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $description
 * @property string $image
 */
class EventsUserType extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_events_user_types';

    /**
     * @var string[]
     */
    protected $fillable = ['name'];

}
