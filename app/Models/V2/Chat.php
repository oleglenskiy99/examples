<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Auth;

/**
 * @property int $id
 * @property int $creator_id
 * @property int $expert_id
 * @property string $title;
 * @property ChatMessages $message
 * @property Collection $history
 * @property ChatMembers $memberInfo
 * @property User $user;
 */

class Chat extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    public $fillable = [
        'title',
        'creator_id',
        'expert_id',
    ];

    /**
     * @var string[]
     */
    public $appends = ['user'];

    /**
     * @var string
     */
    protected $table = 'module_chats';

    public function getUserAttribute(): ?User
	{
        if ( Auth::id() === $this->expert_id) {
            return User::where('id', $this->creator_id)->with('info')->first();
        }

        return User::where('id', $this->expert_id)->with('info')->first();
    }

    public function message(): HasOne
    {
        return $this->hasOne(
            ChatMessages::class,
            'dialog_id',
            'id'
        )->orderBy(
            'id',
            'desc'
        );
    }

    /**
     * @psalm-suppress InvalidReturnStatement
     * @psalm-suppress InvalidReturnType
     */
    public function history(): HasMany
    {
        /**
         * @psalm-suppress InvalidReturnStatement
         */
        return $this->hasMany(
            ChatMessages::class,
            'dialog_id',
            'id'
        )->where('is_paid', true);
    }


    public function memberInfo(): HasOne
    {
        return $this->hasOne(
            ChatMembers::class,
            'dialog_id',
            'id'
        );
    }


    public function totalSpent(int $userId): int
    {
        return (int) $this->history()->where('user_id', $userId)->sum('cost');
    }

    public function totalReceived(int $userId): int
    {
        return (int) $this->history()->where('user_id', '<>', $userId)->sum('cost');
    }

}
