<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;

class ChatAttachments extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $primaryKey = 'attach_id';

    /**
     * @var string
     */
    protected $table = 'module_chats_messages_attach';

    /**
     * @var array<string>
     */
    protected $cast = [
        'data' => 'array',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'dialog_id',
        'message_id',
        'media_id',
        'type',
    ];

}
