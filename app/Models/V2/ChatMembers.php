<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $user_id
 * @property int $unread_messages_count
 * @property int $balance_actual
 */

class ChatMembers extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_chats_users';

}
