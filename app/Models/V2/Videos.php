<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_videos';

}
