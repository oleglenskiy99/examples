<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id
 * @property int $user_id
 * @property int $media_id
 * @property User $user
 */
class UsersMedia extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_users_medias';

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function media(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'media_id');
    }

}
