<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $category_id
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and');
 * @method static bool insert(array $values);
 */
class UsersCategories extends Model
{

    /**
     * @var array<string>
     */
    public $hidden = ['default'];

    /**
     * @var string
     */
    protected $table = 'module_users_categories';

}
