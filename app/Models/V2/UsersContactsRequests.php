<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id
 * @property int $taker
 * @property int $sender
 * @property int $respond
 * @property User $owner
 * @property User $subscriber
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and');
 */
class UsersContactsRequests extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_contacts_requests';

    public function owner(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'taker');
    }

    // phpcs:disable Generic.NamingConventions.ConstructorName.OldStyle
    public function subscriber(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'sender');
    }

}
