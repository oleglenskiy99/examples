<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $sortname
 * @property string $name
 * @property int $phonecode
 */
class Country extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_countries';

}
