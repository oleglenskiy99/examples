<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id
 * @property int $user_id
 * @property int $dialog_id
 * @property string $message
 * @property string $charge_id
 * @property bool $is_paid
 * @property bool $is_read
 * @property bool $is_edit
 * @property int $cost
 * @property int $timestamp
 * @property ChatMessages $reply
 * @property Collection $attachments
 */

class ChatMessages extends  Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    public $fillable = [
        'user_id',
        'dialog_id',
        'message',
        'timestamp',
        'charge_id',
        'is_paid',
        'type',
        'cost',
    ];

    /**
     * @var string
     */
    protected $table = 'module_chats_messages';

    public function replyMessage(): HasOne
    {
        return $this->hasOne(
            self::class,
            'id',
            'reply'
        );
    }

    public function attachments(): HasManyThrough
    {
        return $this->hasManyThrough(
            Media::class,
            ChatAttachments::class,
            'message_id',
            'id',
            '',
            'media_id'
        );
    }

}
