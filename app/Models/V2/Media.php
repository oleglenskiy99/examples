<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;

/**
 * @property int $id
 * @property string $thumb
 * @property int $width
 * @property int $height
 * @property int $duration
 * @property int $size
 * @property string $type
 * @property string $url
 * @property string $fileName
 * @property string $extension
 * @property string $placeholder
 */
class Media extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_medias';

    public function getFormattedUrlAttribute(): ?string
    {
        /** @var FilesystemAdapter $disk */
        $disk = Storage::disk('s3');

        return !empty($this->url) ? $disk->url($this->url) : null;
    }

    public function getFormattedThumbUrlAttribute(): ?string
    {
        /** @var FilesystemAdapter $disk */
        $disk = Storage::disk('s3');

        return !empty($this->thumb) ? $disk->url($this->thumb) : null;
    }

    public function getFormattedPlaceholderUrlAttribute(): ?string
    {
        /** @var FilesystemAdapter $disk */
        $disk = Storage::disk('s3');

        return !empty($this->placeholder) ? $disk->url($this->placeholder) : null;
    }

}
