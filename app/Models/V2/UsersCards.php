<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property string $last4
 * @property string $brand
 * @property string $funding
 * @property int $exp_month
 * @property int $exp_year
 * @property string $customer
 * @property string $payment_method
 * @property string $usage
 */

class UsersCards extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_cards';

    /**
     * @var array<string>
     */
    protected $fillable = [
        'user_id',
        'customer',
        'payment_method',
        'usage',
        'last4',
        'brand',
        'funding',
        'exp_month',
        'exp_year',
    ];

}
