<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int $id
 * @property string $image
 * @property string $name
 * @property string $description
 * @property string $full_image
 * @property string $full_description
 */
class Event extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_events';

    public function getAllUserTypesAttribute(): Collection
    {
        return EventsUserType::all();
    }

    public function userTypes(): BelongsToMany
    {
        return $this->belongsToMany(EventsUserType::class, 'module_users_events', 'event_id', 'user_type_id')->distinct();
    }

}
