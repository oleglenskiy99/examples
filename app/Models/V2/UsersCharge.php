<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $object
 * @property float $amount
 * @property float $amount_captured
 * @property float $amount_refunded
 * @property int $created
 * @property string $currency
 * @property string $customer
 * @property bool $paid
 * @property string $payment_method
 * @property string $receipt_url
 * @property bool $refunded
 * @property string $status
 * @property bool $captured
 * @property string $payment_intent
 * @property float $amount_receivable
 * @property string $category
 * @property bool $out
 * @property string $parent
 */
class UsersCharge extends Model
{

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_charges';

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var array
     */
    protected $casts = [
        'amount' => 'double',
        'amount_captured' => 'double',
        'amount_refunded' => 'double',
        'amount_receivable' => 'double',
    ];

}
