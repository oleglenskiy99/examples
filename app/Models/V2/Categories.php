<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @method static Collection findMany(array $ids, array $columns = ['*'])
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and');
 */
class Categories extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_categories';

}
