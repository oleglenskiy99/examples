<?php

declare(strict_types=1);

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id
 * @property int $user_id
 * @property int $viewed_user_id
 * @property User $user
 */
class UsersView extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_users_views';

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function viewedUser(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'viewed_user_id');
    }

}
