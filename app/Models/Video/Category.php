<?php

declare(strict_types=1);

namespace App\Models\Video;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_videos_categories';

    /**
     * @var array<string>
     */
    protected $fillable = [
        'name_en',
        'name_ru',
        'icon',
    ];

}
