<?php

declare(strict_types=1);

namespace App\Models\Chats;

use App\Errors;
use App\Models\Chats\Messages\Deleted as DeletedMessages;
use App\Models\User\Info as UserInfo;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class Messages
 *
 * @OA\Schema(
 *     description="Messages Model",
 *     title="Messages Model"
 * )
 * @property int $id
 * @property int $dialog_id
 * @property int $reply
 * @property bool $is_has_reply
 * @property bool $is_liked
 * @property bool $is_edit
 * @property string $guuid
 * @property string $charge_id
 * @property int $user_id
 * @property string $message
 * @property bool $is_read
 * @property bool $is_deleted
 * @property float $cost
 * @property bool $is_paid
 * @property string $type
 * @property int $service
 * @property int $views
 * @property int $forward
 * @property int $inherited_dialog_id
 * @property int $inherited_message_id
 * @property int $author
 * @property int $timestamp
 * @property string $chargeId
 */
class Messages extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    public bool $isNeedPayment = true;
    public bool $isNeedPayout = false;
    public ?self $reply_message = null;

    /**
     * @var string
     */
    protected $table = 'module_chats_messages';

    /**
     * @var array
     */
    protected $hidden = [
        'inherited_dialog_id',
        'inherited_message_id',
        'charge_id',
        'message',
        'service',
        'day',
        'year',
        'month',
        'forward_message_id',
        'forward_peer_id',
        'channel',
        'from',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'dialog_id',
        'timestamp',
        'user_id',
        'inherited_dialog_id',
        'inherited_message_id',
        'message',
        'type',
        'guuid',
        'is_paid',
        'is_edit',
        'is_read',
        'is_deleted',
        'is_has_reply',
        'views',
        'charge_id',
        'cost',
    ];
    protected ?User $user;
    protected ?object $replyMessage = null;

    /**
     * @var array
     */
    protected $casts = [
        'is_edit' => 'boolean',
        'is_paid' => 'boolean',
        'is_read' => 'boolean',
        'is_has_reply' => 'boolean',
        'cost' => 'double',
        'is_deleted' => 'boolean',
    ];

    /**
     * @var array
     */
    protected array $attachments = [];

    /**
     * @var array
     */
    protected array $forward = [];

    public function info(): HasOne
    {
        return $this->hasOne('App\Models\User\Info', 'id', 'user_id');
    }

    public function from(): HasOne
    {
        return $this->hasOne('App\Models\User\Info', 'id', 'author');
    }

    public function reply(): HasOne
    {
        return $this->hasOne('App\Models\Chats\Messages', 'id', 'reply');
    }

    public function attachments(): HasManyThrough
    {
        return $this->hasManyThrough(
            'App\Media',
            'App\Models\Chats\Attachments',
            'message_id',
            'id',
            '',
            'media_id'
        );
    }

    public function user(): HasOne
    {
        return $this->hasOne('App\Models\User\Info', 'id', 'user_id');
    }

    public function file(): HasOneThrough
    {
        return $this->hasOneThrough(
            'App\Media',
            'App\Models\Chats\Attachments',
            'message_id',
            'id',
            '',
            'media_id'
        );
    }

    public function lastAttachmentType(): HasOne
    {
        return $this->hasOne('App\Models\Chats\Attachments', 'message_id', 'id');
    }

    public function external(): HasOne
    {
        return $this->hasOne('App\Models\Chats\Attachments', 'message_id', 'id');
    }

    public function request(): HasOne
    {
        return $this->hasOne('App\Models\Chats\Attachments', 'message_id', 'id');
    }

    public function audio(): HasOneThrough
    {
        return $this->hasOneThrough('App\Media', 'App\Models\Chats\Attachments', 'message_id', 'id', '', 'media_id');
    }

    public function chat(): HasOne
    {
        return $this->hasOne('App\Models\Chats\Chat', 'id', 'dialog_id');
    }

    public function inherited(): HasOne
    {
        return $this->hasOne('App\Models\Chats\Chat', 'id', 'inherited_dialog_id');
    }

    public function forwared(): HasMany
    {
        return $this->hasMany('App\Models\Chats\Messages\Forward', 'message', 'id');
    }

    public function deletedmessages(): HasMany
    {
        return $this->hasMany(DeletedMessages::class, 'message_id', 'id');
    }

    /**
     * @return array
     */
    public function getAuthorAttribute(): ?array
    {
        try {
            if ($this->from) {
                if ($this->from > 0) {
                    $user = UserInfo::select('id', 'firstName', 'lastName')->where('id', $this->from)->first();

                    if ($user === null) {
                        return null;
                    }

                    return [
                        'id' => $user->id,
                        'type' => 'user',
                        'user' => $user,
                        'name' => sprintf('%s %s', $user->firstName, $user->lastName),
                    ];
                }
            }
        } catch (Throwable $ex) {
            return null;
        }

        return null;
    }

    public function getLinkAttribute(): ?array
    {
        if (isset($this->external->data)) {
            return json_decode($this->external->data, true);
        }

        return null;
    }

    /**
     * @return array|null
     */
    public function getErrorAttribute(): ?array
    {
        if ($this->is_paid === false) {
            return [
                'code' => 103,
                'message' => Errors::get(103),
            ];
        }

        return null;
    }

    public function getReactAttribute(): ?string
	{
        if ( session('user_id')) {
            if ( $this->attributes['user_id'] === session('user_id')) {
                return 'sent';
            }

            return 'received';
        }

        return null;
    }

    /**
     * @return array|false|string|string[]|null
     */
    public function getTextAttribute()
    {
        return mb_convert_encoding($this->message, 'UTF-8', 'UTF-8');
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $array = parent::toArray();

        foreach ($array as $key => $value) {
            if (is_null($value)) {
                unset($array[$key]);
            } elseif (is_array($value) && count($value) === 0) {
                unset($array[$key]);
            }
        }

        foreach ($this->getMutatedAttributes() as $key) {
            $value = $this->{$key};
            if (is_null($value) === false) {
                $array[$key] = $value;
            }
        }

        return $array;
    }

    public static function getById(int $id): ?object
    {
        return self::builder()->where('id', $id)->first();
    }

    /**
     * @return array
     */
    public static function get(int $id, int $offset = 0, ?int $lastMessageId = null): array
    {
        if ($lastMessageId === null) {
            $lastMessageId = 0;
        }

        $messages = self::builder()
            ->where(
                (function ($q) use ($lastMessageId): void {
                    $q->where('id', '>', $lastMessageId);
                })
            )->where('dialog_id', $id)->orderBy('id', 'desc')->offset($offset)->limit(100)->get();

        foreach ($messages as $index => $message) {
            $messages[$index] = self::format($message);
        }

        return array_reverse($messages->toArray());
    }

    public static function getUnreplyMessagesByChat(int $id): Builder
    {
        return self::select('id', 'user_id', 'cost', 'charge_id')
            ->where(DB::raw('COALESCE(is_has_reply::boolean, false)'), false)
            ->where('is_paid', 1)
            ->where('cost', '>', 0)
            ->where('dialog_id', $id)
            ->where('is_deleted', 0);
    }

    /**
     * @return array
     */
    public static function format(array $message, bool $isAssignChatObject = false, ?int $userId = null): array
    {
        if (is_null($message['file'])) {
            unset($message['file']);
        }
        if (is_null($message['audio'])) {
            unset($message['audio']);
        }
        if (count($message['attachments']) === 0) {
            unset($message['attachments']);
        }
        if (is_null($message['reply'])) {
            unset($message['reply']);
        }
        if (is_null($message['external'])) {
            unset($message['external']);
        }
        if (is_null($message['author'])) {
            unset($message['author']);
        }

        if ($isAssignChatObject) {
            $message['text'] = preg_replace("/\r|\n/", '', $message['text']);

            $message['chat']['peer'] = $message['chat']['is_group'] ? [
                'id' => $message['chat']['id'],
                'name' => $message['chat']['title'],
                'image' => $message['chat']['image'],
                'type' => 'group',
                'is_online' => 0,
                'is_owner' => $userId === $message['chat']['creator_id'],
            ] : [
                'id' => $message['chat']['user']['user_id'],
                'type' => 'user',
                'name' => $message['chat']['user']['firstName'] . ' ' . $message['chat']['user']['lastName'],
                'image' => $message['chat']['user']['photo'],
                'is_online' => $message['chat']['user']['is_online'],
            ];
        }

        return $message;
    }

    public static function builder(): Builder
    {
        return self::select(
            'id',
            'user_id',
            'author as from',
            'dialog_id',
            'timestamp',
            'type',
            'guuid',
            'charge_id',
            'cost',
            'is_paid',
            'is_edit',
            'is_read',
            'views',
            'reply',
            'message'
        )
            ->with([
                'attachments' => function ($q): void {
                    $q->select(
                        'id as media_id',
                        'module_medias.type',
                        'size',
                        'width',
                        'height',
                        'duration',
                        'fileName',
                        'extension',
                        'thumb',
                        'placeholder',
                        'url'
                    )->where(
                        function ($where): void {
                            $where->where('module_chats_messages_attach.type', 'image')->orWhere(
                                'module_chats_messages_attach.type',
                                'video'
                            );
                        }
                    )->where('inherited', 0);
                },
            ])
            ->with([
                'reply' => function ($q): void {
                    $q->select('id', 'dialog_id', 'message', 'type', 'user_id')->whereHas('user')->with([
                        'user' => function ($query): void {
                            $query->select('id', 'firstName', 'lastName', 'photo');
                        },
                    ])->with([
                        'attachments' => function ($q): void {
                            $q->select(
                                'id',
                                'id as media_id',
                                'module_medias.type',
                                'size',
                                'width',
                                'height',
                                'duration',
                                'fileName',
                                'extension',
                                'thumb',
                                'placeholder',
                                'url'
                            )->where(
                                function ($where): void {
                                    $where->where('module_chats_messages_attach.type', 'image')->orWhere(
                                        'module_chats_messages_attach.type',
                                        'video'
                                    );
                                }
                            )->where('inherited', 0);
                        },
                    ])->with([
                        'file' => function ($q): void {
                            $q->select(
                                'id',
                                'id as media_id',
                                'module_medias.type',
                                'size',
                                'width',
                                'height',
                                'duration',
                                'fileName',
                                'extension',
                                'thumb',
                                'placeholder',
                                'url'
                            )->where(
                                'module_chats_messages_attach.type',
                                'file'
                            )->where(
                                'inherited',
                                0
                            );
                        },
                    ])->with([
                        'audio' => function ($q): void {
                            $q->select(
                                'id',
                                'id as media_id',
                                'module_medias.type',
                                'size',
                                'duration',
                                'fileName',
                                'extension',
                                'url'
                            )->where(
                                'module_chats_messages_attach.type',
                                'voice'
                            )->where(
                                'inherited',
                                0
                            );
                        },
                    ]);
                },
            ])
            ->with([
                'file' => function ($q): void {
                    $q->select(
                        'id',
                        'id as media_id',
                        'module_medias.type',
                        'size',
                        'width',
                        'height',
                        'duration',
                        'fileName',
                        'extension',
                        'thumb',
                        'placeholder',
                        'url'
                    )->where(
                        'module_chats_messages_attach.type',
                        'file'
                    )->where(
                        'inherited',
                        0
                    );
                },
            ])
            ->with([
                'audio' => function ($q): void {
                    $q->select('id', 'id as media_id', 'module_medias.type', 'size', 'duration', 'fileName', 'extension', 'url')->where(
                        'module_chats_messages_attach.type',
                        'voice'
                    )->where(
                        'inherited',
                        0
                    );
                },
            ])
            ->with([
                'external' => function ($query): void {
                    $query->select('data', 'message_id')->where('type', 'link');
                },
            ])
            ->whereHas('user')->with([
                'user' => function ($query): void {
                    $query->select('id', 'firstName', 'lastName', 'photo');
                },
            ])
            ->where('is_deleted', 0);
    }

}
