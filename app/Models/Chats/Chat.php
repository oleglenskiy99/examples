<?php

declare(strict_types=1);

namespace App\Models\Chats;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Chat
 *
 * @OA\Schema(
 *     description="Chat Model",
 *     title="Chat Model",
 * @OA\Xml(
 *         name="Chat"
 *     )
 * )
 * @property int $id
 * @property int $creator_id
 * @property bool $is_anonymous
 * @property int $expert_id
 * @property bool $is_deleted
 * @property bool $is_free
 * @property bool $is_help
 * @property bool $is_channel
 * @property bool $is_fund
 * @property string $title
 * @property bool $is_group
 * @property string $image
 * @property string $description
 * @property bool $is_conference
 * @property bool $is_saved_messages
 * @property bool $is_call
 */
class Chat extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_chats';

    /**
     * @var array
     */
    protected $casts = [
        'is_group' => 'boolean',
        'is_channel' => 'boolean',
        'is_help' => 'boolean',
        'is_free' => 'boolean',
        'is_fund' => 'boolean',
        'is_pinned' => 'boolean',
        'is_muted' => 'boolean',
        'is_call' => 'boolean',
        'is_conference' => 'boolean',
        'is_saved_messages' => 'boolean',
        'is_deleted' => 'boolean',
    ];

    /**
     * @OA\Property(
     *     format="int64",
     *     description="ID",
     *     title="ID",
     * )
     */
    /**
     * @var array<string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'is_anonymous',
        'is_conference',
        'is_deleted',
        'is_fund',
        'is_help',
        'last_update_timestamp',
        'channel',
        'is_group',
        'is_channel',
        'is_saved_messages',
        'is_call',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'creator_id',
        'expert_id',
    ];

    /**
     * @return array
     */
    public function toArray(): array
    {
        $array = parent::toArray();

        foreach ($array as $key => $value) {
            if ($value === null) {
                unset($array[$key]);
            }
        }

        foreach ($this->getMutatedAttributes() as $key) {
            if (!array_key_exists($key, $array)) {
                if ($this->{$key} !== null) {
                    $array[$key] = $this->{$key};
                }
            }
        }

        return $array;
    }

    public function info(): HasOne
    {
        return $this->hasOne('App\Models\Chats\Members', 'dialog_id', 'id');
    }

    public function member(): HasOne
    {
        return $this->hasOne('App\Models\Chats\Members', 'dialog_id', 'id');
    }

    public function user(): HasOne
    {
        return $this->hasOne('App\Models\Chats\Members', 'dialog_id', 'id');
    }

    public function creator(): HasOne
    {
        return $this->hasOne('App\User', 'id', 'creator_id');
    }

    public function expert(): HasOne
    {
        return $this->hasOne('App\User', 'id', 'expert_id');
    }

    // phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
    public function last_message(): HasOne
    {
        return $this->hasOne('App\Models\Chats\Messages', 'dialog_id', 'id');
    }

}
