<?php

declare(strict_types=1);

namespace App\Models\Chats\Messages;

use Illuminate\Database\Eloquent\Model;

class Deleted extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_chats_messages_deleted';

    /**
     * @var array<string>
     */
    protected $fillable = [
        'message_id',
        'user_id',
        'dialog_id',
    ];

}
