<?php

declare(strict_types=1);

namespace App\Models\Chats\Messages;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Forward extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_chats_messages_forward';

    public function message(): HasOne
	{
        return $this->hasOne('App\Models\Chats\Messages', 'id', 'forward_message_id');
    }

}
