<?php

declare(strict_types=1);

namespace App\Models\Chats;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Members
 *
 * @OA\Schema(
 *     description="Members Model",
 *     title="Members Model"
 * )
 * @property int $id
 * @property int $user_id
 * @property int $dialog_id
 * @property int $unread_messages_count
 * @property bool $is_pinned
 * @property bool $is_muted
 * @property int $balance_actual
 * @property bool $is_blocked
 * @property bool $is_deleted
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and');
 */
class Members extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_chats_users';

    /**
     * @var array<string>
     */
    protected $casts = [
        'is_muted' => 'boolean',
        'is_pinned' => 'boolean',
        'is_deleted' => 'boolean',
        'is_blocked' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'unread_messages_count',
        'balance_actual',
        'dialog_id',
        'user_id',
        'is_pinned',
        'is_deleted',
        'is_blocked',
        'is_muted',
    ];

    public function user(): HasOne
    {
        return $this->hasOne('\App\User', 'id', 'user_id');
    }

    public function prices(): HasOne
    {
        return $this->hasOne('\App\Models\User\Prices', 'user_id', 'user_id');
    }

    /**
     * @return false|int
     */
    public function increment($column, $amount = 1, array $extra = [])
    {
        return parent::increment($column, $amount, $extra);
    }

    public function actualDecrement(?int $unread_messages_count = null, ?float $balance_actual = null): void
	{
        if ($unread_messages_count !== null) {
            if ($this->unread_messages_count > 1) {
                $this->decrement('unread_messages_count', $unread_messages_count);
            } else {
                $this->update(['unread_messages_count' => 0]);
            }
        }
        if ($balance_actual === null) {
            return;
        }

        if ($this->balance_actual > $balance_actual && $balance_actual > 0) {
            $this->decrement('balance_actual', $balance_actual);
        } else {
            $this->update(['balance_actual' => 0]);
        }
    }

}
