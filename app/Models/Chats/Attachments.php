<?php

declare(strict_types=1);

namespace App\Models\Chats;

use App\Helpers\Chats\AttachmentsHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Attachments extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_chats_messages_attach';

    /**
     * @var string
     */
    protected $primaryKey = 'attach_id';

    /**
     * @var array<string>
     */
    protected $cast = [
        'data' => 'array',
    ];

    /**
     * @var array<string>
     */
    protected $hidden = [
        'attach_id',
        'sticker_id',
        'inherited',
        'data',
    ];

    public function chat(): HasOne
	{
        return $this->hasOne('App\Models\Chats\Chat', 'id', 'dialog_id');
    }

    public function message(): HasOne
	{
        return $this->hasOne('App\Models\Chats\Messages', 'id', 'message_id');
    }

    public function media(): HasOne
	{
        return $this->hasOne('App\Media', 'id', 'media_id');
    }

    public function getFileNameAttribute(): string
	{
        return $this->media->fileName;
    }

    public function getExtensionAttribute(): string
	{
        return $this->media->extension;
    }

    public function getWidthAttribute(): int
	{
        return $this->media->width;
    }

    public function getHeightAttribute(): int
	{
        return $this->media->height;
    }

    public function getDurationAttribute(): int
	{
        return $this->media->duration;
    }

    public function getSizeAttribute(): int
	{
        return $this->media->size;
    }

    /**
     * @return array<string>
     */
    public function getUrlsAttribute(): array
	{
        return [
            'placeholder' => $this->media->placeholder,
            'thumb' => $this->media->thumb,
            'full' => $this->media->url,
        ];
    }

    /**
     * @param array<string> $items
     * @return array<string>
     */
    public static function formatVoices(array $items): array
	{
        return AttachmentsHelper::formatVoices($items);
    }

    /**
     * @param array<string> $items
     * @return array<string>
     */
    public static function formatLinks(array $items): array
	{
        return AttachmentsHelper::formatLinks($items);
    }

    /**
     * @param array<string> $items
     * @return array<string>
     */
    public static function formatFiles(array $items): array
	{
        return AttachmentsHelper::formatFiles($items);
    }

    /**
     * @param array<string> $items
     * @param bool $withChat
     * @return array<string>
     */
    public static function format(array $items, bool $withChat = false): array
	{
        return AttachmentsHelper::format($items, $withChat);
    }

}
