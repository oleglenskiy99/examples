<?php

declare(strict_types=1);

namespace App\Models\Chats;

use Illuminate\Database\Eloquent\Model;

class Read extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_chats_messages_read';

}
