<?php

declare(strict_types=1);

namespace App\Models\Timeline;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id
 * @property int $user_id
 * @property bool $type
 * @property string $caption
 * @property int $timestamp
 */
class Post extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_timeline';

    /**
     * @var array<string>
     */
    protected $casts = [
        'is_liked' => 'boolean',
        'is_deleted' => 'boolean',
    ];

    /**
     * @var array<string>
     */
    protected $hidden = ['laravel_through_key'];

    public function media(): HasOneThrough
	{
        return $this->hasOneThrough('App\Media', 'App\Models\Timeline\Media', 'post_id', 'id', '', 'media_id');
    }

    public function user(): HasOne
	{
        return $this->hasOne('App\Models\User\Info', 'id', 'user_id');
    }

    public function isLiked(): HasMany
	{
        return $this->hasMany('App\Models\Timeline\Likes', 'post_id', 'id');
    }

    public function likes(): HasMany
	{
        return $this->hasMany('App\Models\Timeline\Likes', 'post_id', 'id');
    }

    public function comments(): HasMany
	{
        return $this->hasMany('App\Models\Timeline\Comments', 'post_id', 'id');
    }

    /**
     * @return array<object>
     */
    public static function get(int $ownerId, int $offset, int $limit = 10): array
	{
        return self::builder()->where('user_id', $ownerId)
            ->offset($offset)->limit($limit)
            ->get()->toArray();
    }

    /**
     * @param array<int> $ids
     */
    public static function getByIds(array $ids): LengthAwarePaginator
	{
		return self::builder()->whereIn('module_timeline.id', $ids)
            ->paginate(15, ['*'], 'next_max_id');
    }

    public static function builder(): Builder
	{
        return self::select('id', 'user_id', 'type', 'caption', 'timestamp')
            ->with([
                'media' => function ($q): void {
					$q->select(
                        DB::raw('module_medias.id as media_id'),
                        'url',
                        'thumb',
                        'placeholder',
                        'type',
                        'duration',
                        'size',
                        'fileName'
					);
				}])
               ->with([
                   'user' => function ($q): void {
                    $q->select('id', 'firstName', 'lastName', 'photo');
				   }])->withCount([
                       'isLiked as is_liked' => function ($q): void {
									$q->where('user_id', session('user_id'));
					   }])->withCount('likes as likes_count')
               ->withCount('comments')
               ->with([
                   'comments' => function ($q): void {
                    $q->select('module_timeline_comments.id', 'post_id', 'domain', 'text')
                        ->limit(2)
                        ->orderBy('id', 'desc')
                        ->join('module_users', 'module_timeline_comments.user_id', 'module_users.id');
				   }])
               ->where('is_deleted', 0)
               ->orderBy('module_timeline.timestamp', 'DESC');
    }

}
