<?php

declare(strict_types=1);

namespace App\Models\Timeline;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_timeline_media';

}
