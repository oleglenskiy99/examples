<?php

declare(strict_types=1);

namespace App\Models\Timeline;

use Illuminate\Database\Eloquent\Model;

class Likes extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_timeline_likes';

    /**
     * @var array<string>
     */
    protected $fillable = ['post_id', 'user_id'];

}
