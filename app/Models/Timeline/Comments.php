<?php

declare(strict_types=1);

namespace App\Models\Timeline;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_timeline_comments';

}
