<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_prices';

    /**
     * @var array<string>
     */
    protected $fillable = [
        'productIdentifier',
        'priceAmount',
        'priceCurrencyCode',
    ];

}
