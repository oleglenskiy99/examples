<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conference  extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_conferences';

}
