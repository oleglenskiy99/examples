<?php

declare(strict_types=1);

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Cards extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_cards';

    /**
     * @var array<string>
     */
    protected $guarded = ['id'];

    /**
     * @var array<string>
     */
    protected $fillable = [
        'user_id',
        'customer',
        'payment_method',
        'payout_method',
        'usage',
        'last4',
        'brand',
        'funding',
        'exp_month',
        'exp_year',
    ];

}
