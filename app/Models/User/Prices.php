<?php

declare(strict_types=1);

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

class Prices extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_prices';

    /**
     * @var array<string>
     */
    protected $casts = [
        'value' => 'double',
        'prev' => 'double',
    ];

    /**
     * @var array<string>
     */
    protected $fillable = ['user_id', 'key', 'value', 'ticker', 'apple_good_id', 'prev'];

    /**
     * @var array<string>
     */
    protected $hidden = ['id', 'user_id', 'key', 'min_call_duration', 'ticker', 'apple_good_id'];

    public static function getPriceObjectForUserType(int $id, string $type): ?Prices
    {
        return self::where('user_id', $id)->where('key', $type)->first();
    }

    // phpcs:disable SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingAnyTypeHint
    public static function getUserPriceForType(int $id, string $type)
    {
        return Cache::remember(
            sha1(sprintf('user:%s:%s:price', $id, $type)),
            Carbon::now()->addMinutes(30),
            function () use ($id, $type) {
                $price = Prices::where('user_id', $id)->where('key', $type)->first();

                if (!$price) {
                    return 0;
                }

                return $price->value;
            }
        );
    }

}
