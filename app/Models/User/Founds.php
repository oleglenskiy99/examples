<?php

declare(strict_types=1);

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Founds extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_users_founds';

}
