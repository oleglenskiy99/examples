<?php

declare(strict_types=1);

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Linked
 *
 * @OA\Schema(
 *     description="User Linked",
 *     title="User Linked",
 * @OA\Xml(
 *         name="Linked"
 *     )
 * )
 * @property int $id
 * @property int $user_id
 * @property int $network_id
 * @property string $username
 * @property string $photo
 * @property int $followers_count
 * @property bool $is_deleted
 * @property string $external_id
 */
class Linked extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_linked';

    /**
     * @var array
     */
    protected $casts = [
        'is_deleted' => 'boolean',
        'external_id' => 'string',
        'user_id' => 'integer',
        'network_id' => 'integer',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'network_id',
        'user_id',
        'external_id',
        'username',
        'photo',
        'followers_count',
        'is_deleted',
    ];

    public static function getNetwork(int $id, int $netId): ?Linked
    {
        return static::select(
            'module_networks.id AS id',
            'external_id',
            'provider',
            'name',
            'icon',
            'color',
            'username',
            'photo',
            'followers_count'
        )
            ->join('module_networks', 'module_networks.id', 'module_users_linked.network_id')
            ->where('module_users_linked.user_id', $id)
            ->where('module_users_linked.network_id', $netId)
            ->where('module_users_linked.is_deleted', false)
            ->first();
    }

    public function network(): HasOne
    {
        return $this->hasOne('\App\Models\Network', 'id', 'network_id');
    }

    /**
     * @return array
     */
    public static function get(int $id): array
    {
        $builder = static::select(
            'module_networks.id AS id',
            'external_id',
            'provider',
            'name',
            'icon',
            'color',
            'username',
            'photo',
            'followers_count'
        )
            ->join('module_networks', 'module_networks.id', 'module_users_linked.network_id')
            ->where('module_users_linked.user_id', $id)
            ->where('module_users_linked.is_deleted', false)
            ->get();

        return $builder->toArray();
    }

}
