<?php

declare(strict_types=1);

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{

    /**
     * @var array<string>
     */
    public $hidden = ['default'];

    /**
     * @var string
     */
    protected $table = 'module_users_categories';

}
