<?php

declare(strict_types=1);

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Info
 *
 * @OA\Schema(
 *     description="Info Model",
 *     title="Info Model",
 * )
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property string $biography
 * @property string $photo
 * @property int $industry
 * @property int $country
 * @property bool $pseudonym
 */
class Info extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_about';

    /**
     * @var array
     */
    protected $fillable = ['firstName', 'lastName', 'biography', 'industry', 'pseudonym', 'photo'];

    /**
     * @var array
     */
    protected $casts = [
        'is_verify' => 'boolean',
        'is_online' => 'boolean',
        'is_blocked' => 'boolean',
        'is_contact' => 'boolean',
        'pseudonym' => 'boolean',
    ];

    public function target(): HasOne
    {
        return $this->hasOne('\App\User', 'id', 'id');
    }

    public function industry(): HasOne
    {
        return $this->hasOne('\App\Categories', 'id', 'industry');
    }

    public function country(): HasOne
    {
        return $this->hasOne('\App\Country', 'id', 'country');
    }

    public function prices(): HasOne
    {
        return $this->hasOne('\App\Models\User\Prices', 'user_id', 'id');
    }

    public function activeContactRequest(): HasMany
    {
        return $this->hasMany('\App\Models\Contacts\Requests', 'taker', 'id');
    }

}
