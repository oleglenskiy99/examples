<?php

declare(strict_types=1);

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Medias
 *
 * @OA\Schema(
 *     description="Medias Model",
 *     title="Medias Model"
 * )
 * @property int $id
 * @property int $user_id
 * @property int $media_id
 */
class Medias extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_medias';

    /**
     * @var array<string>
     */
    protected $fillable = ['user_id', 'media_id'];

    public function media(): HasOne
    {
        return $this->hasOne('\App\Media', 'id', 'media_id');
    }

}
