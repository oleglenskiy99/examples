<?php

declare(strict_types=1);

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Charges extends Model
{

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_charges';

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var array<string>
     */
    protected $fillable = [
        'id',
        'customer',
        'amount',
        'amount_captured',
        'amount_refunded',
        'amount_receivable',
        'category',
        'currency',
        'status',
        'object',
        'captured',
        'refunded',
        'paid',
        'out',
        'created',
        'payment_intent',
        'payment_method',
        'receipt_url',
    ];

    /**
     * @var array<string>
     */
    protected $casts = [
        'id' => 'string',
        'paid' => 'boolean',
        'captured' => 'boolean',
        'refunded' => 'boolean',
        'out' => 'boolean',
        'amount' => 'double',
        'amount_captured' => 'double',
        'amount_refunded' => 'double',
        'amount_receivable' => 'double',
    ];

}
