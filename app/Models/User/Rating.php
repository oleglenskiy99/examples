<?php

declare(strict_types=1);

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rating
 *
 * @OA\Schema(
 *     description="Rating Model",
 *     title="Rating Model",
 * )
 * @property int $id
 * @property int $user_id
 * @property float $rating
 */
class Rating extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_rating';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'rating'];

}
