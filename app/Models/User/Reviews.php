<?php

declare(strict_types=1);

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'module_users_reviews';

    /**
     * @var array<string>
     */
    protected $fillable = [
        'owner_id',
        'target_id',
        'target_type',
        'service_id',
        'service_type',
        'value',
        'caption',
    ];

}
