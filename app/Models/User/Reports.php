<?php

declare(strict_types=1);

namespace App\Models\User;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Reports extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_users_reports';

    // phpcs:disable SlevomatCodingStandard.Classes.ClassStructure.IncorrectGroupOrder
    public static function isReportExists(
        int $ownerId,
        int $targetId,
        int $targetServiceId,
        int $targetSerivceType
    ): ?Reports
	{
        return self::where('owner_id', $ownerId)->where('target_id', $targetId)->where('target_service_id', $targetServiceId)
            ->where('target_service_type', $targetSerivceType)->first();
    }

    public static function builder(): Builder
    {
        return self::with('owner')
            ->with('target')
            ->with('report_type')
            ->where('target_type', 'user')
            ->orderBy('updated_at', 'DESC');
    }

    public function owner(): HasOne
    {
        return $this->hasOne('\App\Models\User\Info', 'id', 'owner_id');
    }


    public function target(): HasOne
    {
        return $this->hasOne('\App\Models\User\Info', 'id', 'target_id');
    }

}
