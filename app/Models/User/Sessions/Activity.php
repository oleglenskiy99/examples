<?php

declare(strict_types=1);

namespace App\Models\User\Sessions;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_users_sessions_activity';

    public static function store(
        string $key,
        int $userId = 0,
        int $sessionId = 0,
        int $sourceId = 0,
        ?array $rawData = null
    ): void
	{
        $action_id = ActivityTypes::getId($key);

        self::insert([
            'user_id' => $userId,
            'session_id' => $sessionId,
            'source_id' => $sourceId ?: 0,
            'action_id' => $action_id ?: 0,
            'timestamp' => time(),
            'raw' => $rawData ?: '',
        ]);
    }

}
