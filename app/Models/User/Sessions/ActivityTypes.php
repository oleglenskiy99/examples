<?php

declare(strict_types=1);

namespace App\Models\User\Sessions;

use Illuminate\Database\Eloquent\Model;

class ActivityTypes extends Model
{

    /**
     * @var string
     */
    protected $table = 'module_users_sessions_activity_types';

    public static function getId(string $key): ?int
	{
        return self::where('key', $key)->value('id');
    }

}
