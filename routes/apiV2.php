<?php

declare(strict_types=1);

use App\Http\Controllers\API\V2\AuthController;
use App\Http\Controllers\API\V2\BalanceController;
use App\Http\Controllers\API\V2\CardsController;
use App\Http\Controllers\API\V2\CategoryController;
use App\Http\Controllers\API\V2\ChargeController;
use App\Http\Controllers\API\V2\CommunicationController;
use \App\Http\Controllers\API\V2\ChatsController;
use App\Http\Controllers\API\V2\ConnectionsController;
use App\Http\Controllers\API\V2\CountryController;
use App\Http\Controllers\API\V2\DiscoverController;
use App\Http\Controllers\API\V2\EventController;
use App\Http\Controllers\API\V2\FinanceController;
use App\Http\Controllers\API\V2\FollowerController;
use App\Http\Controllers\API\V2\FollowingController;
use App\Http\Controllers\API\V2\InviteController;
use App\Http\Controllers\API\V2\PhoneController;
use App\Http\Controllers\API\V2\UploadController;
use App\Http\Controllers\API\V2\UserController;
use App\Http\Controllers\API\V2\WebHooksController;
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'apiV2',
], function (): void {
    Route::post('auth/logout', [AuthController::class, 'logout']);

    Route::patch('user/change-country', [UserController::class, 'changeCountry']);
    Route::patch('user/change-password', [UserController::class, 'changePassword']);
    Route::get('user', [UserController::class, 'get']);
    Route::get('user/all', [UserController::class, 'getAll']);
    Route::put('user', [UserController::class, 'update']);

    Route::get('follower', [FollowerController::class, 'getAll']);
    Route::patch('follower/subscribe', [FollowerController::class, 'subscribe']);
    Route::get('following', [FollowingController::class, 'getAll']);

    Route::get('category', [CategoryController::class, 'getAll']);
    Route::post('category/attach', [CategoryController::class, 'attach']);

    Route::get('communication/user', [CommunicationController::class, 'getByCurrentUser']);
    Route::post('communication/attach', [CommunicationController::class, 'attach']);
    Route::get('communication/user-viewed', [CommunicationController::class, 'userViewed']);

    Route::get('/discover', [DiscoverController::class, 'getAll']);
    Route::get('/discover/specially-for-you', [DiscoverController::class, 'speciallyForYou']);

    Route::post('/upload', [UploadController::class, 'upload']);

    Route::get('/charges', [ChargeController::class, 'getAll']);
    
    Route::post("user/addPhone", [PhoneController::class, 'addPhone']);
    Route::post("user/confirmPhone", [PhoneController::class, 'confirmPhone']);

    Route::post("finances/addPayout", [FinanceController::class, 'addPayout']);

    // Finances
    Route::group([
        'prefix' => 'finances',
    ], function (): void {
		// Credit cards
		Route::group([
            'prefix' => 'card',
		], function (): void {
			Route::post('attach', [CardsController::class, 'creditCardAttach']);
		});

        // Balances
        Route::group([
            'prefix' => 'balances',
        ], function (): void {
            Route::get('', [BalanceController::class, 'getUserBalances']);
            Route::post('top-up', [BalanceController::class, 'topUpUserBalance']);
        });
    });
});

Route::group([
    'prefix' => 'webhooks',
], function (): void {
    Route::post('stripe', [WebHooksController::class, 'stripe']);
});

Route::group([
    'middleware' => 'apiV2',
    'prefix' => 'connections',
], function (): void {
    Route::get('', [ConnectionsController::class, 'getAll']);
    Route::get('with-user', [ConnectionsController::class, 'getByUser']);
});

Route::group([
    'middleware' => 'apiV2',
    'prefix' => 'chats'
], function(): void {
    Route::post('create', [ChatsController::class, 'create']);
    Route::post('send', [ChatsController::class, 'sendMessage']);
    Route::get('history', [ChatsController::class, 'getHistory']);
});

Route::group([], function (): void {
    Route::get('/invites/check', [InviteController::class, 'check']);
    Route::get('/invites/apply', [InviteController::class, 'apply'])->middleware('apiV2');

    Route::get('/events', [EventController::class, 'getAll']);

    Route::get('/countries', [CountryController::class, 'getAll']);
});
